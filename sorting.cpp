/************************************************************************/
/*                  靠靠                                            */
/************************************************************************/

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>

#define ASC 0

#define DESC 1

#define int_sort(a,b) {a=a+b;b=a-b;a=a-b;} //只用于int类型

void swap_sort(void* a,void* b)
{
	void* temp;
    temp = a;
	a = b;
	b = temp;
	
}

/*  靠靠  */
int bubble_sort(int a[],int startlen,int endlen,int sort)
{
    int i, j;
	if (a == NULL || startlen >= endlen)
	{
		return -1;
	}
	for (i = endlen;i > startlen; i--)
	{
		for (j = startlen; j < i;j++)
		{
            if(sort > 0)
            {
                if(a[j] > a[j+1])
                {
                    swap_sort(&a[j],&a[j+1]);
                }
            }
			else                
			{
                if(a[j] < a[j+1])
                {
                    swap_sort(&a[j],&a[j + 1]);
                }
			}
		}
	}
}

/*  选择排序  */
int select_sort(int a[],int startlen,int endlen,int sort)
{
	int temp;
	int i, j;
	if (a == NULL || startlen >= endlen)
	{
		return -1;
	}
	for (i = startlen; i < endlen; i++)
	{
		temp = i;
		for (j = endlen; j > i; j--)
		{
			if (a[j] > a[temp])
			{
				temp = j;
			}
		}
		swap_sort(&a[temp],&a[i]);
	}
}

/*  插入排序  */
int insert_sort(int a[],int startlen,int endlen,int sort)
{
	int temp;
	int i, j;
	if (a == NULL || startlen >= endlen)
	{
		return -1;
	}
	for (i = startlen + 1;i <= endlen; i++)
	{
		if (a[i] < a[i - 1])
		{
			temp = a[i];
			j = i;
			do 
			{
				a[j] = a[j - 1];
				j--;
			} while (j > startlen && a[j - 1] > temp);
			a[j] = temp;
		}
	}
}

int quick_sort(int a[],int startlen,int endlen,int sort)
{
	int temp;
	int i, j;
	if (startlen >= endlen)
	{
		return 0;
	}
	temp = a[startlen];
	i = startlen + 1;
	j = endlen;
	if (i == j)
	{
		if (a[i] < a[startlen])
		{
			swap_sort(&a[i],&a[startlen]);
		}
	}
	while (j > i)
	{
		if (a[j] < temp)
		{
			while (j > i && a[i] < temp)
			{
				i++;
			}
			if (i != j)
			{
				swap_sort(&a[i],&a[j]);
			}
		}
		j--;
	}
	if (a[i] < a[startlen])
	{
		swap_sort(&a[i],&a[startlen]);
	}
	quick_sort(a,startlen,i - 1,sort);
	quick_sort(a,i,endlen,sort);
}

int merge_sort(int a[],int startlen,int endlen,int index)
{
	int *b = new int[endlen - startlen + 1];
	int i, j, k;

	if (a ==  NULL || index > endlen)
	{
		return -1;
	}

	quick_sort(a,startlen,index,0);
	quick_sort(a,index + 1,endlen,0);
	i = startlen;
	j = index + 1;
	k = 0;

	while (i <= index && j <= endlen)
	{
		if (a[i] < a[j])
		{
			b[k++] = a[i++];
		}
		else
		{
			b[k++] = a[j++];
		}
	}

    memcpy(a + startlen,b,(endlen + 1)*sizeof(int));
	
    delete []b;
	return 0;
}

//void heap_build(int a[],int startlen,int endlen,int sort)       //堆排序 效率低 每一堆都去检查
//{
//	int i;
//    if (endlen <= startlen)
//    {
//		return ;
//    }
//	i = (endlen - startlen + 1) / 2;
//	
//	if (2 * i == endlen + 1)
//	{
//		i--;
//		if (a[i] < a[2*i + 1])
//		{
//			swap_sort(&a[i],&a[2*i + 1]);
//		}
//	}
//	else
//	{
//		i--;
//		if (a[i] < a[2*i + 1])
//		{
//			swap_sort(&a[i],&a[2*i + 1]);
//		}
//		if (a[i] < a[2*i + 2])
//		{
//			swap_sort(&a[i],&a[2*i + 2]);
//		}
//	}
//	i--;
//	
//	while (i >= 0)
//	{
//		if (a[i] < a[2*i + 1])
//		{
//			swap_sort(&a[i],&a[2*i + 1]);
//		}
//		if (a[i] < a[2*i + 2])
//		{
//			swap_sort(&a[i],&a[2*i + 2]);
//		}
//		i--;
//	}
//}
//
//void heap_sort(int a[],int startlen,int endlen,int sort)
//{
//	int i;
//	heap_build(a,startlen,endlen,0);
//	
//    i = endlen;
//    while (i > 0)
//    {
//		swap_sort(&a[startlen],&a[i]);
//		heap_build(a,startlen,--i,0);
//    }
//}

void heap_swap(int a[],int startlen,int endlen,int sort)
{
	int i,temp;
	temp = a[startlen];
	i = startlen * 2 + 1;
	while (i <= endlen)
	{
		if (i < endlen && a[i] < a[i + 1])
		{
			i++;
		}
		if (temp > a[i])
		{
			break;
		}
		else
		{
			a[startlen] = a[i];
			startlen = i;
			i = i * 2 + 1;
		}
	}
	a[startlen] = temp;
}

void heap_sort(int a[],int startlen,int endlen,int sort)
{
	int i;
	
	for (i = ((endlen - startlen) + 1)/2 -1;i >= 0;i--)
	{
		heap_swap(a,i,endlen,0);
	}

	for (i = endlen;i >= 0;i--)
	{
		swap_sort(&a[startlen],&a[i]);
		heap_swap(a,startlen,i - 1,0);
	}
}

int main(int argc, char **argv)
{
	int i;
	time_t loctime;
	clock_t locclock;              //clock  每过千分之一秒（1毫秒），调用clock（）函数返回的值就加1 它用来表示一秒钟会有多少个时钟计时单元 从“开启这个程序进程”到“程序中调用clock()函数”时之间的CPU时钟计时单元
	long nowtime,nexttime;
	//int a[20] = {5,2,0,7,1,9,11,3,8,4,10,15,19,14,18,18,6,12,16,13};
	int a[100000];
	for (i = 0;i < 100000;i++)
	{
		a[i] = rand() % 10000000;
	}

	time(&loctime);
	locclock = clock();
	nowtime = loctime;
	nowtime = locclock;

	heap_sort(a,0,99999,0);
   
    locclock = clock();
	nexttime = locclock;
	time(&loctime);
    nexttime = loctime;
	printf("time is %ld",nexttime - nowtime);
	for (i = 0;i < 19;i++)
	{
		printf("%d",a[i]);
	}
	return 0;
}
