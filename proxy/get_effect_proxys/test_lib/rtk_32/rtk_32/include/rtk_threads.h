// rtk_threads.h : header file
//

#ifndef _RTK_THREADS_H_
#define _RTK_THREADS_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_threads_t rtk_threads_t;

struct rtk_threads_t
{
    int                 count;
    rtk_thread_t        *array;

#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_threads_init(rtk_threads_t *threads);
RTK_DECLARE(void) rtk_threads_term(rtk_threads_t *threads);

RTK_DECLARE(void) rtk_threads_expand(rtk_threads_t *threads,int count);

RTK_DECLARE(rtk_thread_t*) rtk_threads_new(rtk_threads_t *threads);
RTK_DECLARE(int) rtk_threads_is_use(rtk_threads_t *threads,rtk_thread_t *thread);
RTK_DECLARE(void) rtk_threads_free(rtk_threads_t *threads,rtk_thread_t *thread);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_THREADS_H_
