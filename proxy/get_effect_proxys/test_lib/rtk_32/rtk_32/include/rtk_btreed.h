// rtk_btreed.h : header file
//

#ifndef _RTK_BTREED_H_
#define _RTK_BTREED_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_btreed_entry_t rtk_btreed_entry_t;
typedef struct rtk_btreed_node_t rtk_btreed_node_t;
typedef struct rtk_btreed_t rtk_btreed_t;

typedef void (*rtk_btreed_init_t)(void *buffer);
typedef void (*rtk_btreed_term_t)(void *buffer);

struct rtk_btreed_entry_t
{
    DWORD               name;
    short               level;
    rtk_btreed_entry_t  *parent;
    rtk_arrays_t        children;
};

struct rtk_btreed_node_t
{
    DWORD               name;
    void                *buffer;
};

struct rtk_btreed_t
{
    rtk_btreed_entry_t  root;
    int                 count;
    rtk_pool_t          *xpool;
    int                 xflag;
    rtk_pool_t          *npool;
    int                 nflag;
    rtk_pool_t          *dpool;
    int                 dflag;
};

RTK_DECLARE(void) rtk_btreed_init(rtk_btreed_t* btree,int xflag,int nflag,int dflag);
RTK_DECLARE(void) rtk_btreed_term(rtk_btreed_t* btree);

RTK_DECLARE(void*) rtk_btreed_set(rtk_btreed_t* btree,DWORD name,rtk_btreed_init_t init);
RTK_DECLARE(void*) rtk_btreed_get(rtk_btreed_t* btree,DWORD name);

RTK_DECLARE(int) rtk_btreed_insert(rtk_btreed_t* btree,DWORD name,void *buffer);
RTK_DECLARE(void*) rtk_btreed_update(rtk_btreed_t* btree,DWORD name,void *buffer);

RTK_DECLARE(int) rtk_btreed_find(rtk_btreed_t* btree,DWORD name);
RTK_DECLARE(void*) rtk_btreed_remove(rtk_btreed_t* btree,DWORD name);

RTK_DECLARE(void) rtk_btreed_remove_all(rtk_btreed_t* btree);
RTK_DECLARE(void) rtk_btreed_free_all(rtk_btreed_t* btree,rtk_btreed_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_BTREED_H_
