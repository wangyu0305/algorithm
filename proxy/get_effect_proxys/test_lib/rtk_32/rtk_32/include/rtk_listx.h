// rtk_listx.h : header file
//

#ifndef _RTK_LISTX_H_
#define _RTK_LISTX_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_listx_entry_t rtk_listx_entry_t;
typedef struct rtk_listx_t rtk_listx_t;

typedef void (*rtk_listx_init_t)(void *buffer);
typedef void (*rtk_listx_term_t)(void *buffer);
typedef int (*rtk_listx_compare_t)(void *buffer,void *name);

struct rtk_listx_entry_t
{
    rtk_listx_entry_t   *next;
    DWORD               hkey;
    void                *buffer;
};

struct rtk_listx_t
{
    rtk_arrays_t        entry;
    int                 index;
    int                 count;
    rtk_pool_t          *xpool;
    short               xflag;
    rtk_pool_t          *dpool;
    short               dflag;
};

RTK_DECLARE(void) rtk_listx_init(rtk_listx_t *list,int xflag,int dflag);
RTK_DECLARE(void) rtk_listx_term(rtk_listx_t *list);

RTK_DECLARE(void*) rtk_listx_add_tail_new(rtk_listx_t *list,DWORD hkey,rtk_listx_init_t init);
RTK_DECLARE(void) rtk_listx_add_tail(rtk_listx_t *list,DWORD hkey,void *buffer);

RTK_DECLARE(void*) rtk_listx_get_next(rtk_listx_t *list,rtk_listx_compare_t compare);

RTK_DECLARE(void) rtk_listx_remove_all(rtk_listx_t *list);
RTK_DECLARE(void) rtk_listx_free_all(rtk_listx_t *list,rtk_listx_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_LISTX_H_
