// rtk_bits.h : header file
//

#ifndef _RTK_BITS_H_
#define _RTK_BITS_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_bits_t rtk_bits_t;

struct rtk_bits_t
{
    BYTE                *array;
    DWORD               size;
};

RTK_DECLARE(int) rtk_bits_get(BYTE bt,int pt);
RTK_DECLARE(BYTE) rtk_bits_set(BYTE bt,int pt,int flag);

RTK_DECLARE(void) rtk_bits_init(rtk_bits_t *array);
RTK_DECLARE(void) rtk_bits_term(rtk_bits_t *array);

RTK_DECLARE(void) rtk_bits_expand(rtk_bits_t *array,DWORD size);
RTK_DECLARE(void) rtk_bits_zero(rtk_bits_t *array);

RTK_DECLARE(void) rtk_bits_swap(rtk_bits_t *array,rtk_stream_t *stream);

RTK_DECLARE(void) rtk_bits_insert(rtk_bits_t *array,DWORD pos,int flag);
RTK_DECLARE(int) rtk_bits_find(rtk_bits_t *array,DWORD pos);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_BITS_H_
