// rtk_mfile.h : header file
//

#ifndef _RTK_MFILE_H_
#define _RTK_MFILE_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_mblock_t rtk_mblock_t;
typedef struct rtk_mfile_t rtk_mfile_t;

struct rtk_mblock_t
{
    DWORD               blockid;
    rtk_stream_t        stream;
    void                *pos;
    int                 update;
};

struct rtk_mfile_t
{
    char                *fname;
    int                 fid;
    int                 mode;
    int                 bsize;
    rtk_hashd_t         mhash;
    rtk_lists_t         mlist;
    int64_t             fsize;
#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_mfile_init(rtk_mfile_t *file);
RTK_DECLARE(void) rtk_mfile_term(rtk_mfile_t *file);

RTK_DECLARE(int) rtk_mfile_open(rtk_mfile_t *file);
RTK_DECLARE(void) rtk_mfile_close(rtk_mfile_t *file);

RTK_DECLARE(int) rtk_mfile_flush(rtk_mfile_t *file);

RTK_DECLARE(int) rtk_mfile_read_string(rtk_mfile_t *file,int64_t pos,int len,rtk_string_t *string);
RTK_DECLARE(int) rtk_mfile_write_string(rtk_mfile_t *file,int64_t pos,rtk_string_t *string);

RTK_DECLARE(int) rtk_mfile_read_stream(rtk_mfile_t *file,int64_t pos,int len,rtk_stream_t *stream);
RTK_DECLARE(int) rtk_mfile_write_stream(rtk_mfile_t *file,int64_t pos,rtk_stream_t *stream);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_MFILE_H_
