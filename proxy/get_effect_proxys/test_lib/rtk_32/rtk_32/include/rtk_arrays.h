// rtk_arrays.h : header file
//

#ifndef _RTK_ARRAYS_H_
#define _RTK_ARRAYS_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_arrays_entry_t rtk_arrays_entry_t;
typedef struct rtk_arrays_t rtk_arrays_t;

typedef void (*rtk_arrays_init_t)(void *buffer);
typedef void (*rtk_arrays_term_t)(void *buffer);
typedef int (*rtk_arrays_compare_t)(void *buffer,void *name);

struct rtk_arrays_entry_t
{
    void                *buffer;
};

struct rtk_arrays_t
{
    rtk_arrays_entry_t  *array;
    int                 size;
    int                 count;
    rtk_pool_t          *dpool;
    int                 dflag;
};

RTK_DECLARE(void) rtk_arrays_init(rtk_arrays_t *array,int dflag);
RTK_DECLARE(void) rtk_arrays_term(rtk_arrays_t *array);

RTK_DECLARE(void) rtk_arrays_expand(rtk_arrays_t *array,int size);

RTK_DECLARE(void*) rtk_arrays_append_new(rtk_arrays_t *array,rtk_arrays_init_t init);
RTK_DECLARE(void) rtk_arrays_append(rtk_arrays_t *array,void *buffer);

RTK_DECLARE(void) rtk_arrays_append_array(rtk_arrays_t *array,rtk_arrays_t *n_array);
RTK_DECLARE(void) rtk_arrays_swap(rtk_arrays_t *array,rtk_arrays_t *n_array);

RTK_DECLARE(void*) rtk_arrays_get_at(rtk_arrays_t *array,int index);
RTK_DECLARE(void*) rtk_arrays_set_at(rtk_arrays_t *array,int index,void *buffer);

RTK_DECLARE(void*) rtk_arrays_get_first(rtk_arrays_t *array);
RTK_DECLARE(void*) rtk_arrays_get_last(rtk_arrays_t *array);

RTK_DECLARE(int) rtk_arrays_insert(rtk_arrays_t *array,void *buffer,rtk_arrays_compare_t compare);
RTK_DECLARE(void*) rtk_arrays_update(rtk_arrays_t *array,void *buffer,rtk_arrays_compare_t compare);

RTK_DECLARE(void*) rtk_arrays_insert_new(rtk_arrays_t *array,void *name,rtk_arrays_init_t init,rtk_arrays_compare_t compare);
RTK_DECLARE(void*) rtk_arrays_remove(rtk_arrays_t *array,void *name,rtk_arrays_compare_t compare);

RTK_DECLARE(void*) rtk_arrays_find(rtk_arrays_t *array,void *name,rtk_arrays_compare_t compare);
RTK_DECLARE(void*) rtk_arrays_bfind(rtk_arrays_t *array,void *name,rtk_arrays_compare_t compare);

RTK_DECLARE(void) rtk_arrays_insert_at(rtk_arrays_t *array,int index,void *buffer);
RTK_DECLARE(void*) rtk_arrays_remove_at(rtk_arrays_t *array,int index);

RTK_DECLARE(void) rtk_arrays_remove_all(rtk_arrays_t *array);
RTK_DECLARE(void) rtk_arrays_free_all(rtk_arrays_t *array,rtk_arrays_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_ARRAYS_H_
