// rtk_thread.h : header file
//

#ifndef _RTK_THREAD_H_
#define _RTK_THREAD_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_message_t rtk_message_t;
typedef struct rtk_message_entry_t rtk_message_entry_t;
typedef struct rtk_thread_t rtk_thread_t;

typedef void (*rtk_thread_process_t)(void *buffer);
typedef void (*rtk_thread_message_process_t)(rtk_thread_t *thread,int command,void *buffer);

struct rtk_message_t
{
    int                 command;
    void                *buffer;
};

struct rtk_message_entry_t
{
    rtk_message_entry_t     *prev;
    rtk_message_entry_t     *next;
    rtk_message_t           *message;
};

struct rtk_thread_t
{
#ifdef WINDOWS
    HANDLE              thread;
#else
    pthread_t           thread;
#endif
    int                 flag;
    int                 closed;
    void                *param;

    rtk_message_entry_t     *messages;
    rtk_thread_message_process_t    process;

#ifdef RTK_MULTI_THREAD
    rtk_mutex_t             mutex;
#endif
};

RTK_DECLARE(void) rtk_thread_init(rtk_thread_t *thread);
RTK_DECLARE(void) rtk_thread_term(rtk_thread_t *thread);

RTK_DECLARE(void) rtk_thread_create(rtk_thread_t* thread,rtk_thread_process_t process,void *param);
RTK_DECLARE(void) rtk_thread_create_message(rtk_thread_t* thread,rtk_thread_message_process_t process);

RTK_DECLARE(void) rtk_thread_join(rtk_thread_t *thread);
RTK_DECLARE(void) rtk_thread_close(rtk_thread_t *thread);

RTK_DECLARE(void) rtk_thread_post_message(rtk_thread_t* thread,int command,void *buffer);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_THREAD_H_
