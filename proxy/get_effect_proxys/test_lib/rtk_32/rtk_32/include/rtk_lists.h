// rtk_lists.h : header file
//

#ifndef _RTK_LISTS_H_
#define _RTK_LISTS_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_lists_entry_t rtk_lists_entry_t;
typedef struct rtk_lists_t rtk_lists_t;

typedef void (*rtk_lists_init_t)(void *buffer);
typedef void (*rtk_lists_term_t)(void *buffer);
typedef int (*rtk_lists_compare_t)(void *buffer,void *name);

struct rtk_lists_entry_t
{
    rtk_lists_entry_t   *prev;
    rtk_lists_entry_t   *next;
    void                *buffer;
};

struct rtk_lists_t
{
    rtk_lists_entry_t   *head;
    rtk_lists_entry_t   *tail;
    int                 count;
    rtk_pool_t          *xpool;
    int                 xflag;
    rtk_pool_t          *dpool;
    int                 dflag;
};

RTK_DECLARE(void) rtk_lists_init(rtk_lists_t *list,int xflag,int dflag);
RTK_DECLARE(void) rtk_lists_term(rtk_lists_t *list);

RTK_DECLARE(void) rtk_lists_add_head(rtk_lists_t *list,void *buffer);
RTK_DECLARE(void) rtk_lists_add_tail(rtk_lists_t *list,void *buffer);

RTK_DECLARE(void*) rtk_lists_add_head_new(rtk_lists_t *list,rtk_lists_init_t init);
RTK_DECLARE(void*) rtk_lists_add_tail_new(rtk_lists_t *list,rtk_lists_init_t init);

RTK_DECLARE(void) rtk_lists_add_head_list(rtk_lists_t *list,rtk_lists_t *n_list);
RTK_DECLARE(void) rtk_lists_add_tail_list(rtk_lists_t *list,rtk_lists_t *n_list);
RTK_DECLARE(void) rtk_lists_swap(rtk_lists_t *list,rtk_lists_t *n_list);

RTK_DECLARE(void) rtk_lists_insert_before(rtk_lists_t *list,void *pos,void *buffer);
RTK_DECLARE(void) rtk_lists_insert_after(rtk_lists_t *list,void *pos,void *buffer);

RTK_DECLARE(void*) rtk_lists_get_head_position(rtk_lists_t *list);
RTK_DECLARE(void*) rtk_lists_get_tail_position(rtk_lists_t *list);

RTK_DECLARE(void*) rtk_lists_get_head(rtk_lists_t *list);
RTK_DECLARE(void*) rtk_lists_get_tail(rtk_lists_t *list);

RTK_DECLARE(void*) rtk_lists_get_prev(rtk_lists_t *list,void **pos);
RTK_DECLARE(void*) rtk_lists_get_next(rtk_lists_t *list,void **pos);

RTK_DECLARE(void*) rtk_lists_get_at(rtk_lists_t *list,void *pos);
RTK_DECLARE(void*) rtk_lists_find(rtk_lists_t *list,void *buffer,rtk_lists_compare_t compare);
RTK_DECLARE(void*) rtk_lists_find_at(rtk_lists_t *list,void *buffer);

RTK_DECLARE(void*) rtk_lists_remove_head(rtk_lists_t *list);
RTK_DECLARE(void*) rtk_lists_remove_tail(rtk_lists_t *list);
RTK_DECLARE(void*) rtk_lists_remove_at(rtk_lists_t *list,void *pos);

RTK_DECLARE(void) rtk_lists_remove_all(rtk_lists_t *list);
RTK_DECLARE(void) rtk_lists_free_all(rtk_lists_t *list,rtk_lists_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_LISTS_H_
