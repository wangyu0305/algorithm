// rtk_file.h : header file
//

#ifndef _RTK_FILE_H_
#define _RTK_FILE_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_file_t rtk_file_t;

struct rtk_file_t
{
    char                *fname;
    int                 fid;
    int                 mode;
    int64_t             fsize;
#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_file_init(rtk_file_t *file);
RTK_DECLARE(void) rtk_file_term(rtk_file_t *file);

RTK_DECLARE(int) rtk_file_open(rtk_file_t *file);
RTK_DECLARE(void) rtk_file_close(rtk_file_t *file);

RTK_DECLARE(int64_t) rtk_file_get_size(char *fname);
RTK_DECLARE(void) rtk_file_flush(rtk_file_t *file);

RTK_DECLARE(int) rtk_file_read_string(rtk_file_t *file,int64_t pos,int len,rtk_string_t *string);
RTK_DECLARE(int) rtk_file_write_string(rtk_file_t *file,int64_t pos,rtk_string_t *string);

RTK_DECLARE(int) rtk_file_read_stream(rtk_file_t *file,int64_t pos,int len,rtk_stream_t *stream);
RTK_DECLARE(int) rtk_file_write_stream(rtk_file_t *file,int64_t pos,rtk_stream_t *stream);

RTK_DECLARE(int) rtk_file_get_string(char *fname,rtk_string_t *string);
RTK_DECLARE(int) rtk_file_set_string(char *fname,int b_append,rtk_string_t *string);

RTK_DECLARE(int) rtk_file_get_stream(char *fname,rtk_stream_t *stream);
RTK_DECLARE(int) rtk_file_set_stream(char *fname,int b_append,rtk_stream_t *stream);

RTK_DECLARE(int) rtk_file_trunc(rtk_file_t *file);
RTK_DECLARE(int) rtk_file_exist(char *fname);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_FILE_H_
