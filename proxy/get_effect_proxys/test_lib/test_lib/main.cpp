#include "rtk_32.h"
#include "stk_get_proxy.h"


void test_get_proxy(void*)
{
	int ret;
	rtk_list_t m_list;

	rtk_list_init(&m_list,true);

	ret = stk_get_proxy(&m_list,1,"192.9.100.178",9981);
	if (ret == -1)
		printf("not enough\n");
	if (ret == 0)
		printf("login failed!\n");
	if(ret == -2)
		printf("transfer failed\n");

	rtk_list_remove_all(&m_list);
	rtk_list_term(&m_list);
}

int main(int argc, char **argv)
{
	rtk_threads_t threads;
	rtk_threads_init(&threads);
	rtk_threads_expand(&threads,10);
	for (int i = 0;i < threads.count;i++)
	{
		rtk_thread_create(&threads.array[i],test_get_proxy,(void*)0);
	}
	for (int i = 0;i < threads.count;i++)
	{
		rtk_thread_join(&threads.array[i]);
		rtk_thread_close(&threads.array[i]);
	}
	rtk_threads_term(&threads);
}