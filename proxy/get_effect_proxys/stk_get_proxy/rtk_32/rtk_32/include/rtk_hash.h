// rtk_hash.h : header file
//

#ifndef _RTK_HASH_H_
#define _RTK_HASH_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_hash_entry_t rtk_hash_entry_t;
typedef struct rtk_hash_t rtk_hash_t;

typedef void (*rtk_hash_init_t)(void *buffer);
typedef void (*rtk_hash_term_t)(void *buffer);

struct rtk_hash_entry_t
{
    rtk_hash_entry_t    *next;
    DWORD               hkey;
    char                *name;
    void                *buffer;
};

struct rtk_hash_t
{
    rtk_arrays_t        entry;
    int                 count;
    rtk_pool_t          *xpool;
    short               xflag;
    rtk_pool_t          *dpool;
    short               dflag;
};

RTK_DECLARE(DWORD) rtk_hash_key(char *name,int len);

RTK_DECLARE(void) rtk_hash_init(rtk_hash_t *hash,int xflag,int dflag);
RTK_DECLARE(void) rtk_hash_term(rtk_hash_t *hash);

RTK_DECLARE(void*) rtk_hash_set(rtk_hash_t *hash,char *name,int len,rtk_hash_init_t init);
RTK_DECLARE(void*) rtk_hash_get(rtk_hash_t *hash,char *name,int len);

RTK_DECLARE(int) rtk_hash_insert(rtk_hash_t *hash,char *name,int len,void *buffer);
RTK_DECLARE(void*) rtk_hash_update(rtk_hash_t *hash,char *name,int len,void *buffer);

RTK_DECLARE(int) rtk_hash_find(rtk_hash_t *hash,char *name,int len);
RTK_DECLARE(void*) rtk_hash_remove(rtk_hash_t *hash,char *name,int len);

RTK_DECLARE(char*) rtk_hash_get_name(rtk_hash_t *hash,int index,void *pos);
RTK_DECLARE(DWORD) rtk_hash_get_key(rtk_hash_t *hash,int index,void *pos);
RTK_DECLARE(void*) rtk_hash_get_next(rtk_hash_t *hash,int *index,void **pos);

RTK_DECLARE(void) rtk_hash_remove_all(rtk_hash_t *hash);
RTK_DECLARE(void) rtk_hash_free_all(rtk_hash_t *hash,rtk_hash_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_HASH_H_
