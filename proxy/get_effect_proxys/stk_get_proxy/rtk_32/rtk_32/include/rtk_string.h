// rtk_string.h : header file
//

#ifndef _RTK_STRING_H_
#define _RTK_STRING_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_string_t rtk_string_t;

struct rtk_string_t
{
    char            *buffer;
    int             length;
    int             size;
};

RTK_DECLARE(void) rtk_string_init(rtk_string_t *string);
RTK_DECLARE(void) rtk_string_term(rtk_string_t *string);

RTK_DECLARE(int) rtk_string_is_empty(rtk_string_t *string);
RTK_DECLARE(void) rtk_string_zero(rtk_string_t *string,int pos,int count);
RTK_DECLARE(void) rtk_string_expand(rtk_string_t *string,int size);
RTK_DECLARE(void) rtk_string_trim(rtk_string_t *string);
RTK_DECLARE(void) rtk_string_shrink(rtk_string_t *string,int length);

RTK_DECLARE(void) rtk_string_make_lower(rtk_string_t *string);
RTK_DECLARE(void) rtk_string_make_upper(rtk_string_t *string);

RTK_DECLARE(void) rtk_string_copy(rtk_string_t *string,char *buffer,int len);
RTK_DECLARE(void) rtk_string_copy_char(rtk_string_t *string,char n_char);
RTK_DECLARE(void) rtk_string_copy_number(rtk_string_t *string,int number);

RTK_DECLARE(void) rtk_string_append(rtk_string_t *string,char *buffer,int len);
RTK_DECLARE(void) rtk_string_append_char(rtk_string_t *string,char n_char);
RTK_DECLARE(void) rtk_string_append_number(rtk_string_t *string,int number);

RTK_DECLARE(void) rtk_string_swap(rtk_string_t *string,rtk_string_t *n_string);
RTK_DECLARE(int) rtk_string_get_number(rtk_string_t *string,int number);

RTK_DECLARE(char) rtk_string_get_at(rtk_string_t *string,int pos);
RTK_DECLARE(void) rtk_string_set_at(rtk_string_t *string,int pos,char chr);

RTK_DECLARE(int) rtk_string_compare(rtk_string_t *string,char *buffer,int len);
RTK_DECLARE(int) rtk_string_icompare(rtk_string_t *string,char *buffer,int len);

RTK_DECLARE(int) rtk_string_ncompare(rtk_string_t *string,char *buffer,int len);
RTK_DECLARE(int) rtk_string_nicompare(rtk_string_t *string,char *buffer,int len);

RTK_DECLARE(int) rtk_string_find_char(rtk_string_t *string,char ch);
RTK_DECLARE(int) rtk_string_find_char_pos(rtk_string_t *string,char ch,int pos);
RTK_DECLARE(int) rtk_string_reverse_find(rtk_string_t *string,char ch);

RTK_DECLARE(int) rtk_string_find(rtk_string_t *string,char *buffer,int len);
RTK_DECLARE(int) rtk_string_find_pos(rtk_string_t *string,char *buffer,int len,int pos);

RTK_DECLARE(int) rtk_string_replace_char(rtk_string_t *string,char m_chr,char n_chr);
RTK_DECLARE(int) rtk_string_replace(rtk_string_t *string,char *m_buffer,char *n_buffer);
RTK_DECLARE(int) rtk_string_format(rtk_string_t *string,char *format,...);
RTK_DECLARE(int) rtk_string_delete(rtk_string_t *string,int pos,int count);

RTK_DECLARE(void) rtk_string_left(rtk_string_t *string,int count);
RTK_DECLARE(void) rtk_string_mid(rtk_string_t *string,int pos,int count);
RTK_DECLARE(void) rtk_string_mids(rtk_string_t *string,int pos);
RTK_DECLARE(void) rtk_string_right(rtk_string_t *string,int count);

RTK_DECLARE(void) rtk_string_left_buffer(rtk_string_t *string,int count,char **buffer);
RTK_DECLARE(void) rtk_string_mid_buffer(rtk_string_t *string,int pos,int count,char **buffer);
RTK_DECLARE(void) rtk_string_mids_buffer(rtk_string_t *string,int pos,char **buffer);
RTK_DECLARE(void) rtk_string_right_buffer(rtk_string_t *string,int count,char **buffer);

RTK_DECLARE(void) rtk_string_left_string(rtk_string_t *string,int count,rtk_string_t *n_string);
RTK_DECLARE(void) rtk_string_mid_string(rtk_string_t *string,int pos,int count,rtk_string_t *n_string);
RTK_DECLARE(void) rtk_string_mids_string(rtk_string_t *string,int pos,rtk_string_t *n_string);
RTK_DECLARE(void) rtk_string_right_string(rtk_string_t *string,int count,rtk_string_t *n_string);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_STRING_H_
