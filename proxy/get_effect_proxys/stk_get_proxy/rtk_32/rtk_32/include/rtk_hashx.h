// rtk_hashx.h : header file
//

#ifndef _RTK_HASHX_H_
#define _RTK_HASHX_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_hashx_entry_t rtk_hashx_entry_t;
typedef struct rtk_hashx_t rtk_hashx_t;

typedef void (*rtk_hashx_init_t)(void *buffer);
typedef void (*rtk_hashx_term_t)(void *buffer);

struct rtk_hashx_entry_t
{
    rtk_hashx_entry_t   *next;
    LWORD               hkey;
    void                *buffer;
};

struct rtk_hashx_t
{
    rtk_arrays_t        entry;
    int                 count;
    rtk_pool_t          *xpool;
    short               xflag;
    rtk_pool_t          *dpool;
    short               dflag;
};

RTK_DECLARE(void) rtk_hashx_init(rtk_hashx_t *hash,int xflag,int dflag);
RTK_DECLARE(void) rtk_hashx_term(rtk_hashx_t *hash);

RTK_DECLARE(void*) rtk_hashx_set(rtk_hashx_t *hash,LWORD hkey,rtk_hashx_init_t init);
RTK_DECLARE(void*) rtk_hashx_get(rtk_hashx_t *hash,LWORD hkey);

RTK_DECLARE(int) rtk_hashx_insert(rtk_hashx_t *hash,LWORD hkey,void *buffer);
RTK_DECLARE(void*) rtk_hashx_update(rtk_hashx_t *hash,LWORD hkey,void *buffer);

RTK_DECLARE(int) rtk_hashx_find(rtk_hashx_t *hash,LWORD hkey);
RTK_DECLARE(void*) rtk_hashx_remove(rtk_hashx_t *hash,LWORD hkey);

RTK_DECLARE(LWORD) rtk_hashx_get_key(rtk_hashx_t *hash,int index,void *pos);
RTK_DECLARE(void*) rtk_hashx_get_next(rtk_hashx_t *hash,int *index,void **pos);

RTK_DECLARE(void) rtk_hashx_remove_all(rtk_hashx_t *hash);
RTK_DECLARE(void) rtk_hashx_free_all(rtk_hashx_t *hash,rtk_hashx_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_HASHX_H_
