// rtk_arrayx.h : header file
//

#ifndef _RTK_ARRAYX_H_
#define _RTK_ARRAYX_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_arrayx_entry_t rtk_arrayx_entry_t;
typedef struct rtk_arrayx_list_t rtk_arrayx_list_t;
typedef struct rtk_arrayx_t rtk_arrayx_t;

struct rtk_arrayx_entry_t
{
    rtk_arrayx_entry_t  *next;
    DWORD               array[16];
};

struct rtk_arrayx_list_t
{
    DWORD               hkey;
    int                 index;
    rtk_arrayx_entry_t  *head;
    rtk_arrayx_entry_t  *tail;
};

struct rtk_arrayx_t
{
    rtk_arrayx_list_t   *array;
    int                 index;
    int                 size;
    int                 count;
    int                 number;
    rtk_pool_t          *xpool;
    int                 xflag;
};

RTK_DECLARE(void) rtk_arrayx_init(rtk_arrayx_t *array,int xflag);
RTK_DECLARE(void) rtk_arrayx_term(rtk_arrayx_t *array);

RTK_DECLARE(void) rtk_arrayx_expand(rtk_arrayx_t *array,int size);

RTK_DECLARE(void) rtk_arrayx_insert(rtk_arrayx_t *array,DWORD hkey,DWORD unid);

RTK_DECLARE(void) rtk_arrayx_remove_next(rtk_arrayx_t *array,int size,rtk_arrayd_t *unids);
RTK_DECLARE(void) rtk_arrayx_remove_tail(rtk_arrayx_t *array,int size,rtk_arrayd_t *unids);

RTK_DECLARE(void) rtk_arrayx_remove_all(rtk_arrayx_t *array);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_ARRAYX_H_
