// rtk_arrayd.h : header file
//

#ifndef _RTK_ARRAYD_H_
#define _RTK_ARRAYD_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_arrayd_t rtk_arrayd_t;

struct rtk_arrayd_t
{
    DWORD               narray[4];
    DWORD               *array;
    int                 size;
    int                 count;
};

RTK_DECLARE(void) rtk_arrayd_init(rtk_arrayd_t *array);
RTK_DECLARE(void) rtk_arrayd_term(rtk_arrayd_t *array);

RTK_DECLARE(void) rtk_arrayd_expand(rtk_arrayd_t *array,int size);

RTK_DECLARE(void) rtk_arrayd_copy(rtk_arrayd_t* array,DWORD number);
RTK_DECLARE(void) rtk_arrayd_copy_array(rtk_arrayd_t* array,rtk_arrayd_t* n_array);

RTK_DECLARE(void) rtk_arrayd_append(rtk_arrayd_t *array,DWORD number);

RTK_DECLARE(void) rtk_arrayd_append_array(rtk_arrayd_t *array,rtk_arrayd_t *n_array);
RTK_DECLARE(void) rtk_arrayd_swap(rtk_arrayd_t *array,rtk_arrayd_t *n_array);

RTK_DECLARE(DWORD) rtk_arrayd_get_at(rtk_arrayd_t *array,int index);
RTK_DECLARE(void) rtk_arrayd_set_at(rtk_arrayd_t *array,int index,DWORD number);

RTK_DECLARE(DWORD) rtk_arrayd_get_first(rtk_arrayd_t *array);
RTK_DECLARE(DWORD) rtk_arrayd_get_last(rtk_arrayd_t *array);

RTK_DECLARE(int) rtk_arrayd_insert(rtk_arrayd_t *array,DWORD number);
RTK_DECLARE(int) rtk_arrayd_find(rtk_arrayd_t *array,DWORD number);

RTK_DECLARE(void) rtk_arrayd_insert_at(rtk_arrayd_t *array,int index,DWORD number);
RTK_DECLARE(void) rtk_arrayd_remove_at(rtk_arrayd_t *array,int index);

RTK_DECLARE(void) rtk_arrayd_from_string(rtk_arrayd_t *array,char *tag,rtk_string_t *string);
RTK_DECLARE(void) rtk_arrayd_to_string(rtk_arrayd_t *array,char *tag,rtk_string_t *string);

RTK_DECLARE(void) rtk_arrayd_remove_all(rtk_arrayd_t *array);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_ARRAYD_H_
