#include "rtk_32.h"
#include "stk_get_proxy.h"


rtk_socket_t* proxy_login(char* host,int port)
{
	rtk_socket_t *socket;

	socket = (rtk_socket_t*)rtk_malloc(sizeof(rtk_socket_t));
	rtk_socket_init(socket);

	if(!rtk_socket_open(socket))
	{
		rtk_socket_term(socket);
		rtk_free(socket);
		return 0;
	}

	rtk_socket_option(socket);

	socket->ipaddr = rtk_socket_get_ipaddr(host);
	socket->port = port;

	if(!rtk_socket_connect(socket))
	{
		rtk_socket_close(socket);
		rtk_socket_term(socket);
		rtk_free(socket);
		return 0;
	}

	return socket;
}

void proxy_logout(rtk_socket_t* socket)
{
	if (socket != 0)
	{
		rtk_socket_close(socket);
        rtk_socket_term(socket);
		rtk_free(socket);
	}
}

int proxy_transfer_quit(rtk_socket_t* socket)
{
	rtk_stream_t stream;
	int rv,ret,cmd_code;

	rtk_stream_init(&stream);

	ret = -1;

	if(socket != 0)
	{
		rv = rtk_socket_send_command(socket,URL_CMD_QUIT,&stream);
		if(rv > 0)
		{
			rv = rtk_socket_recv_command(socket,&cmd_code,&stream);
			if(rv > 0)
			{
				rtk_stream_term(&stream);
				return 0;
			}
		}
	}

	rtk_stream_term(&stream);
	return -1;
}

int proxy_transfer_message(rtk_socket_t* socket,rtk_string_t* RecvIpinfo)
{
	rtk_stream_t stream;
	int rv,cmd_code;
	int error = -1;

	rtk_stream_init(&stream);

	if (socket != 0)
	{	
		rv = rtk_socket_recv_command(socket,&cmd_code,&stream);
		if (rv > 0)
		{
			if (cmd_code == URL_CMD_OK)
			{
				rtk_stream_swap_string(&stream,RecvIpinfo);
				error = 1;
			}
		}
	}

	rtk_stream_term(&stream);

	return error;
}

void proxy_set_list_to_string(rtk_list_t* m_list,rtk_string_t* send_string)
{
	void* pos;
	char* proxy_str;
	pos = rtk_list_get_head_position(m_list);
	while (pos)
	{
		proxy_str = rtk_list_get_next(m_list,&pos);
		rtk_string_append(send_string,proxy_str,-1);
		rtk_string_append(send_string,"\r\n",-1);
	}
}

int proxy_get_lists(rtk_list_t* m_list,rtk_socket_t* m_socket,int proxynum)
{
	int errorcode,rv;
	int count = 0;
	rtk_stream_t stream;
	rtk_string_t RecvIpinfo;
	rtk_string_init(&RecvIpinfo);
	rtk_stream_init(&stream);
	
	rtk_string_shrink(&RecvIpinfo,0);
	rtk_string_copy(&RecvIpinfo,"GET ",-1);
	rtk_string_append_number(&RecvIpinfo,proxynum);
    rtk_stream_swap_string(&stream,&RecvIpinfo);

	rv = rtk_socket_send_command(m_socket,URL_CMD_OK,&stream);
	if (rv > 0)
	{
		while (1)
		{
			errorcode = proxy_transfer_message(m_socket,&RecvIpinfo);
			if (!rtk_string_is_empty(&RecvIpinfo) && (errorcode == 1 || (strcmp("NOTENOUGH",RecvIpinfo.buffer) == 0)))
				break;
			Sleep(2000);
			count++;
			if(count > 60*3)
				break;
		}
	}

	rtk_list_from_string(m_list,"\r\n",&RecvIpinfo);
	proxy_transfer_quit(m_socket);

	rtk_string_term(&RecvIpinfo);
	if(count > 60*3)
		return -2001;
	return m_list->count;
}

int proxy_recycl_set_list(rtk_list_t* m_list,rtk_socket_t* m_socket)
{
	int rv,errorcode = 1;
	rtk_string_t send_string;
    rtk_stream_t send_param;
	rtk_string_init(&send_string);
	rtk_stream_init(&send_param);

    proxy_set_list_to_string(m_list,&send_string);
	rtk_stream_swap_string(&send_param,&send_string);

	rv = rtk_socket_send_command(m_socket,URL_CMD_RECYCL,&send_param);
	if (rv <= 0)
	    errorcode = -1;

	proxy_transfer_quit(m_socket);
	rtk_string_term(&send_string);
	rtk_stream_term(&send_param);
	return errorcode;
}

int proxy_get_start(rtk_list_t* m_list,char* ip,int port,int proxynum)
{
	int recvnum = 0,tag = 0;
	rtk_socket_t* m_socket;
	m_socket = proxy_login(ip,port);
	if (m_socket != 0)
	{
		recvnum = proxy_get_lists(m_list,m_socket,proxynum);
		tag = 1;
	}
	
	proxy_logout(m_socket);
	if (recvnum < proxynum && tag == 1)
		return -1;
	if (recvnum < 0)
	    return -2;
    return recvnum;
}

PROXY_DECLARE(int) stk_get_proxy(rtk_list_t* m_list,int proxynum,char* ipaddr,int port)
{
	int rec;
	rtk_socket_initialize();

    rec = proxy_get_start(m_list,ipaddr,port,proxynum);

	rtk_socket_finalize();
	return rec;
}

int proxy_recycl_start(rtk_list_t* m_list,char* ip,int port)
{
	int recvnum = 0,tag = 0;
	rtk_socket_t* m_socket;
	m_socket = proxy_login(ip,port);
	if (m_socket != 0)
	{
		recvnum = proxy_recycl_set_list(m_list,m_socket);
		tag = 1;
	}

	proxy_logout(m_socket);

	if (recvnum < 0 && tag == 1)
		return -1;

	return recvnum;
}

PROXY_DECLARE(int) stk_recycl_proxy(rtk_list_t* m_list,char* ipaddr,int port)
{
	int rec;
	rtk_socket_initialize();

	rec = proxy_recycl_start(m_list,ipaddr,port);           //发送成功返回 1 没有登录上返回 0 发送失败返回 -1

	rtk_socket_finalize();
	return rec;
}