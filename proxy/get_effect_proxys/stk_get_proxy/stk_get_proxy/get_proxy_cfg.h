#ifndef __GET_PROXY_CFG
#define __GET_PROXY_CFG


#include "rtk_32.h"

typedef struct ip_info ip_info_t;

struct ip_info 
{
	rtk_string_t ip;
	int port;
};


void ip_info_init(ip_info_t* ipfo);

void ip_info_term(ip_info_t* ipfo);

void stk_ipinfo_get_profile(ip_info_t* ipfo);

void stk_ipinfo_set_profile(ip_info_t* ipfo);

#endif