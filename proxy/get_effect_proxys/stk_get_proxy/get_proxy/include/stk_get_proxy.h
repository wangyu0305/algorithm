#ifndef __STK_GET_PROXY
#define __STK_GET_PROXY


#define E_TIMEOUT -1002

#ifdef WINDOWS
#define PROXY_DECLARE(type) __declspec(dllexport) type __stdcall
#else
#define PROXY_DECLARE(type) type
#endif

enum CMD_CODE
{
	URL_CMD_ERROR,
	URL_CMD_OK,
	URL_CMD_START,
	URL_CMD_QUIT,
};

#ifdef __cplusplus
extern "C" {
#endif

     PROXY_DECLARE(int) stk_get_proxy(rtk_list_t* m_list,int proxynum,char* ipaddr,int port);

#ifdef __cplusplus
}
#endif

#endif