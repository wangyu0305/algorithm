// stk_crawl.h : header file
//

#ifndef _STK_CRAWL_H_
#define _STK_CRAWL_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////
// stk_crawl_t CObject

typedef void (*stk_crawl_script_t)(void *jsengine,char *objname,rtk_string_t *funcname,rtk_string_t *funcparam,rtk_string_t *urlname);
typedef int (*stk_crawl_image_t)(void *inet,void *logon,rtk_string_t *urlname,rtk_string_t *property,rtk_stream_t *urlbody);

typedef struct stk_crawl_t stk_crawl_t;
typedef struct stk_link_t stk_link_t;
typedef struct stk_image_t stk_image_t;
typedef struct stk_table_t stk_table_t;
typedef struct stk_property_t stk_property_t;
typedef struct stk_node_t stk_node_t;
typedef struct stk_dom_t stk_dom_t;

typedef struct stk_string_t stk_string_t;

struct stk_string_t
{
    char            *buffer;
    long            position;
    long            length;
};

struct stk_table_t
{
    char            tagflag;
    char            btable;
    char            bimage;
    char            bnoise;
    char            btext;
    char            blink;
    DWORD           mtextsize;
    DWORD           ntextsize;
    WORD            mlinks;
    WORD            nlinks;
    DWORD           mlinksize;
    DWORD           nlinksize;
};

struct stk_property_t
{
    rtk_string_t    propname;
    rtk_string_t    propvalue;
};

struct stk_node_t
{
    char            prenode;
    char            endnode;
    char            nodetype;
    char            nodename[16];
    stk_string_t    nodeproperty;
    stk_string_t    nodevalue;
    stk_table_t     table;
};

struct stk_dom_t
{
    int             nimage;
    int             ntable;
    rtk_hash_t      nodetags;
    rtk_hash_t      tabletags;
    rtk_hash_t      breaktags;
    rtk_hash_t      trimtags;
    rtk_hash_t      strongtags;
    rtk_tree_t      domtree;
    rtk_lists_t     framelist;
    rtk_lists_t     tablelist;
    rtk_lists_t     domlist;     //满足模版的节点列表
};

struct stk_link_t
{
    rtk_string_t       urlname;
    rtk_string_t       urltitle;
    rtk_string_t       pubtime;
};

struct stk_image_t
{
    rtk_string_t       urlname;
    rtk_stream_t       urlbody;
};

struct stk_crawl_t
{
    void               *hdom;
    void               *hmod;
    int                image;
    int                table;
    char               *basename;
    char               *objname;
    rtk_string_t       content;
    stk_crawl_script_t scriptfunc;
    stk_crawl_image_t  imagefunc;
    void               *inet;
    void               *logon;
    void               *jsengine;
};

void stk_property_init(stk_property_t *property);
void stk_property_term(stk_property_t *property);

int stk_property_compare(stk_property_t *property,char *propname);

void stk_node_init(stk_node_t *node);
void stk_node_term(stk_node_t *node);

void stk_link_init(stk_link_t *link);
void stk_link_term(stk_link_t *link);

void stk_image_init(stk_image_t *image);
void stk_image_term(stk_image_t *image);

int stk_image_compare(stk_image_t *image,char *urlname);
void stk_image_copy(stk_image_t *image,stk_image_t *nimage);

void stk_crawl_init(stk_crawl_t *crawl);
void stk_crawl_term(stk_crawl_t *crawl);

void stk_crawl_parse_begin(stk_crawl_t *crawl,rtk_string_t *content);
void stk_crawl_parse_end(stk_crawl_t *crawl);

void stk_crawl_get_dom_tree(stk_crawl_t *crawl,rtk_string_t *content,rtk_tree_entry_t *domtree);
void stk_crawl_set_dom_tree(stk_crawl_t *crawl,rtk_string_t *content,rtk_tree_entry_t *domtree);

void stk_crawl_list_script(stk_crawl_t *crawl,rtk_string_t *content,rtk_string_t *script,rtk_list_t *srclist);
void stk_crawl_list_script_funccall(rtk_string_t *content,rtk_array_t *funcnames,rtk_list_t *funclist);

void stk_crawl_get_links(stk_crawl_t *crawl,rtk_string_t *context,rtk_list_t *frames,rtk_lists_t *links);
void stk_crawl_get_content(stk_crawl_t *crawl,rtk_string_t *context,rtk_string_t *content);

void stk_crawl_get_dom_links(stk_crawl_t *crawl,rtk_list_t *frames,rtk_lists_t *links);
void stk_crawl_get_dom_content(stk_crawl_t *crawl,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_crawl_trim_links(stk_crawl_t *crawl,rtk_list_t *frames,rtk_lists_t *links);
void stk_crawl_trim_content(stk_crawl_t *crawl,rtk_string_t *content,rtk_string_t *noise,rtk_list_t *tables,rtk_lists_t *images);

int stk_crawl_get_module_links(stk_crawl_t *crawl,rtk_string_t *murllink,rtk_list_t *frames,rtk_lists_t *links);
int stk_crawl_get_module_content(stk_crawl_t *crawl,rtk_string_t *mcontent,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_crawl_get_rss_links(stk_crawl_t *crawl,rtk_lists_t *links);
void stk_crawl_get_tag_content(stk_crawl_t *crawl,char *tag,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_crawl_get_tag_value(stk_crawl_t *crawl,char *tag,rtk_string_t *content);
void stk_crawl_get_meta_value(stk_crawl_t *crawl,char *meta,char *name,char *descrip,char *content,rtk_string_t *value);

void stk_crawl_find_module(stk_crawl_t *crawl,rtk_string_t *content);
void stk_crawl_get_dom_list(stk_crawl_t *crawl,rtk_string_t *content);

void* stk_crawl_get_head_proplist_position(stk_crawl_t *crawl);
int stk_crawl_get_next_proplist(stk_crawl_t *crawl,void **pos,rtk_lists_t *proplist);

void stk_crawl_get_charset(rtk_string_t *content,rtk_string_t* charset);
int stk_crawl_is_rss_link(rtk_string_t *content);

void stk_crawl_get_form_param(stk_crawl_t *crawl,rtk_string_t *content,rtk_string_t *formtag,rtk_string_t *urlname,rtk_lists_t *paramlist);
void stk_crawl_get_base_href(rtk_string_t *content,rtk_string_t *basename);

int stk_crawl_get_next_tag(int pos,int max,rtk_string_t *content,rtk_string_t *strtag,rtk_string_t *strtext,int *rtag,int *begin,int *end);
int stk_crawl_find_next_tag(int pos,int max,rtk_string_t *content,char *stag,int btag,int *begin,int *end);

int stk_crawl_get_next_key(int *pos,rtk_string_t *content,rtk_string_t *strkey,rtk_string_t *strvalue);
void stk_crawl_get_key_value(rtk_string_t *content,char *key,rtk_string_t *value);

void stk_crawl_make_abs_url(char *basename,rtk_string_t *urlname);
////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_CRAWL_H_
