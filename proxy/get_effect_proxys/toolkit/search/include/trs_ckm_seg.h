/*分词模块的头文件*/
#ifndef _trs_api_seg_h_
#define _trs_api_seg_h_
#include "lexparse.h"

struct DICTWORD
{
    char *word;
    char *cate;
};

struct DICTINFO
{
    int dict_count;
    int page_size;
    int page_count;
    int page_ind;
    int page_r_size;
    struct DICTWORD *keyLst;
};
/*回调函数的原型*/
#if defined(_WIN32)
    typedef  void  (__stdcall* pfSegCallBack)( int hWnd,char *sMessage);
#else
    typedef void (*pfSegCallBack)(int hWnd,char *sMessage);
#endif

/*设置回调函数*/
/*_STDAPI_(int)  SEG_SetCallBack();*/

/*初始化全局分词环境*/
_STDAPI_(int)  SEG_InitGlobalEnv();

/*释放全局分词环境*/
_STDAPI_(int)  SEG_FreeGlobaleEnv();

/*按词切分*/
_STDAPI_(TKS_LEXENV *) SEG_GetEnvByWords();
_STDAPI_(void)  SEG_InitEnvByWords(TKS_LEXENV *rpSegEnv);
/*按字切分*/
_STDAPI_(TKS_LEXENV *) SEG_GetEnvByBytes();

/*For摘要*/
_STDAPI_(TKS_LEXENV *)SEG_GetEnvForAbs();

/*释放分词环境*/
_STDAPI_(int)  SEG_FreeEnv(TKS_LEXENV *ptrEnv);

/*分词*/
_STDAPI_(int)  SEG_LexicalParse(TKS_LEXENV* rpSegEnv, TKS_LEXRESULT* rpSegRes);
_STDAPI_(void) SEG_LexicalResultFree(TKS_LEXRESULT* rpSegRes);
_STDAPI_(unsigned short) SEG_LexGetChineseCode(char *tChar);
_STDAPI_(void)   GetAppHomeDir(char *sPath);
_STDAPI_(void) PrintInfo(char *sMessage);
_STDAPI_(void) SEG_InitEnvByWords_utf8(TKS_LEXENV *rpSegEnv);
_STDAPI_(int)  SEG_InitGlobalEnv_UTF8();
_STDAPI_(TKS_LEXTOKEN) SEG_LexicalTokenGB(char *term, TKS_LEXENV *rpSegEnv);
_STDAPI_(int)  SEG_SetCallBack(pfSegCallBack pf, int pHwnd);
_STDAPI_(unsigned int) SEG_GB18030ToUTF8(unsigned char *sGB18030,unsigned int iGB18030, unsigned char *sUTF8, unsigned int iSize);
_STDAPI_(unsigned int) SEG_UTF8ToGB18030(unsigned char *sUTF8, unsigned int iUTF8, unsigned char *sGB18030,unsigned int iSize);
_STDAPI_(unsigned int) SEG_AddWords(char *word);
_STDAPI_(unsigned int) SEG_DelWords(char *word);
_STDAPI_(unsigned int) SEG_GetWords(struct DICTINFO *dicLst);
_STDAPI_(unsigned int) SEG_FreeWords(struct DICTINFO *dicLst);
/*#if defined(__cplusplus)
//    }
//#endif*/
#endif