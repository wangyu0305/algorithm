// rtk_html.h : header file
//

#ifndef _RTK_HTML_H_
#define _RTK_HTML_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stk_string.h"
/////////////////////////////////////////////////////////////////////////////
// rtk_html_t CObject

typedef int (*stk_html_image_t)(void *html,rtk_string_t *urlname,rtk_string_t *urlbody);
typedef void (*stk_html_script_t)(void *jsengine,char *objname,rtk_string_t *funcname,rtk_string_t *funcparam,rtk_string_t *urlname);

typedef int(*stk_html_copyright_t)(void *model,stk_string_t *value);
typedef int(*stk_html_subinfo_t)(void *model,stk_string_t *value);

typedef struct image_cache_t image_cache_t;
typedef struct stk_link_t stk_link_t;
typedef struct stk_image_t stk_image_t;
typedef struct image_options_t image_options_t;

struct image_cache_t
{
    rtk_mutex_t        mutex;
    rtk_hash_t         urltree;
};

struct stk_link_t
{
    rtk_string_t       urlname;
    rtk_string_t       urltitle;
    rtk_stream_t       hub_value;
};

struct stk_image_t
{
    rtk_string_t       urlname;
    rtk_string_t       urltitle;
    rtk_stream_t       urlbody;
};

struct image_options_t
{
    int               minwidth;
    int               maxwidth;
    int               minheight;
    int               maxheight;
    int               scale;
    int               minsize;
    int               maxsize;
};

typedef struct stk_property_t stk_property_t;

struct stk_property_t
{
    rtk_string_t      name;
    rtk_string_t      value;
	rtk_string_t      source;
};

typedef struct stk_html_t stk_html_t;

struct stk_html_t
{
    void                   *hdom;
    void                   *hmod;

    rtk_string_t           context;

    int                    bimage;
    int                    btable;
    int                    bindent;      //是否首行缩进4字符
    int                    ignore_CRLF;  //是否忽略回车换行
    int                    ignore_hidden;

    rtk_hash_t             ignore_hash;

    rtk_array_t            onclick_first;
    rtk_hash_t             subinfo_tags;
    char                   *basename;
    char                   *objname;

    stk_html_image_t       imagefunc;
    stk_html_script_t      scriptfunc;
    stk_html_copyright_t   copyrightfunc;
    stk_html_subinfo_t     subinfofunc;

    image_options_t        image_opt;
    image_cache_t          *image_caches;
    void                   *cmodel;
    void                   *nmodel;
    void                   *http;
    void                   *jsengine;

};

void image_cache_init(image_cache_t *cache);
void image_cache_term(image_cache_t *cache);

void stk_link_init(stk_link_t *link);
void stk_link_term(stk_link_t *link);

int stk_link_compare(stk_link_t *link,rtk_string_t *urlname);
void stk_link_copy(stk_link_t *link,stk_link_t *mlink);

void stk_image_init(stk_image_t *image);
void stk_image_term(stk_image_t *image);

void stk_image_copy(stk_image_t *image,stk_image_t *n_image);

void stk_html_init(stk_html_t *html);
void stk_html_term(stk_html_t *html);

void stk_html_get_key_value(rtk_string_t *content,char *key,rtk_string_t *value);

void stk_html_make_abs_url(char *base,rtk_string_t *urlname);
void stk_html_escape_url(rtk_string_t *string);

int stk_html_need_encoding(rtk_string_t *string);
void stk_html_encode_param(rtk_string_t *string);

void stk_html_parse_begin(stk_html_t *html,rtk_string_t *content);
void stk_html_parse_end(stk_html_t *html);

void stk_html_get_dom_tree(stk_html_t *html,rtk_string_t *content);
void stk_html_set_dom_tree(stk_html_t *html,rtk_string_t *content);

int stk_html_is_rss_link(rtk_string_t *content);
void stk_html_get_rss_links(stk_html_t *html,rtk_lists_t *links);

void stk_html_get_dom_links(stk_html_t *html,rtk_list_t *frames,rtk_lists_t *links);
void stk_html_get_dom_content(stk_html_t *html,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_html_trim_links(stk_html_t *html,rtk_list_t *frames,rtk_lists_t *links);
void stk_html_trim_content(stk_html_t *html,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_html_get_tag_links(stk_html_t *html,char *tag,rtk_list_t *frames,rtk_lists_t *links);
void stk_html_get_tag_content(stk_html_t *html,char *tag,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_html_get_module_links(stk_html_t *html,rtk_array_t *modules,rtk_list_t *frames,rtk_lists_t *links);
int stk_html_get_module_content(stk_html_t *html,rtk_arrayd_t *content_modules,rtk_string_t *content,rtk_list_t *tables,rtk_lists_t *images);

void stk_html_get_tag_value(stk_html_t *html,char *tag,char* prefix,char *suffix,rtk_string_t *content);

void stk_html_get_node_text(stk_html_t *html,int *trim,rtk_tree_entry_t *domtree,rtk_string_t *text);

void stk_html_list_script_funccall(stk_html_t *html,rtk_array_t *funcnames,rtk_list_t *funclist);

void stk_html_get_charset(rtk_string_t* content,rtk_string_t* charset);
void stk_html_get_base_href(rtk_string_t *content,rtk_string_t *basename);

void stk_property_init(stk_property_t *prop);
void stk_property_term(stk_property_t *prop);

int stk_property_compare(stk_property_t *prop,char *name);

int stk_html_get_dom_list(stk_html_t *html,rtk_string_t *content);
int stk_html_get_proplist(stk_html_t *html,int index,rtk_lists_t *proplist);

void stk_html_get_form_param(stk_html_t *html,rtk_string_t *content,rtk_string_t *formtag,rtk_string_t *urlname,rtk_lists_t *paramlist);

void stk_html_get_meta_value(stk_html_t *html,char *meta,char *name,char *descrip,char *content,rtk_string_t *value);


////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_HTML_H_
