// stk_ulog.h : header file
//

#ifndef _STK_ULOG_H_
#define _STK_ULOG_H_

#ifdef __cplusplus
extern "C" {
#endif

enum LINK_STATUS
{
	LINK_NORMAL,
	LINK_NOTFIND,
	LINK_DELETED
};

////////////////////////////////////////////////////////////////////////////
typedef int (*stk_ulog_progress_t)(DWORD lparam,int progress);

typedef struct stk_refresh_t stk_refresh_t;
typedef struct stk_url_t stk_url_t;
typedef struct stk_ulog_t stk_ulog_t;

struct stk_refresh_t
{
    WORD            pagedepth;
    rtk_array_t     recrawl_urls;
    rtk_array_t     disrecrawl_urls;
};

struct stk_url_t
{
    DWORD           urlid;
    DWORD           purlid;
    LWORD           pkey;
    DWORD           ipaddr;   //�����ֶ�
    WORD            port;     //�����ֶ�
    DWORD           startid;
    WORD            pagelevel;

    LWORD           hkey;
    WORD            chknum;
    WORD            updnum;
    DWORD           chktime;

    BYTE            md5code[16];

    DWORD           urlsize;
    DWORD           urltime;
    WORD            errtype;
    WORD            errcode;
    BYTE            status;
    BYTE            idxrank;
    BYTE            delflag; 
    BYTE            is_variable;

    LWORD           varpos;
    WORD            varlen;
    LWORD           datpos;
    WORD            datlen;

    rtk_string_t    urlname;
    rtk_string_t    urltitle;
    rtk_stream_t    hub_value;
    rtk_string_t    last_modified;
    rtk_string_t    etag;
};

struct stk_ulog_t
{
    void                 *udata;
    void                 *sdata;
    stk_ulog_progress_t  progressfunc;
    DWORD                progressparam;
};

void stk_refresh_init(stk_refresh_t *refresh);
void stk_refresh_term(stk_refresh_t *refresh);

void stk_url_init(stk_url_t *url);
void stk_url_term(stk_url_t *url);

void stk_url_copy(stk_url_t *url,stk_url_t *nurl);
int stk_url_compare(stk_url_t *url,stk_url_t *nurl);

void stk_url_reset(stk_url_t *url);
int stk_url_get_length(stk_url_t *url);

void stk_url_from_stream(stk_url_t *url,int *pos,rtk_stream_t *stream);
void stk_url_to_stream(stk_url_t *url,int *pos,rtk_stream_t *stream);

void stk_ulog_init(stk_ulog_t *ulog);
void stk_ulog_term(stk_ulog_t *ulog);

void stk_ulog_open(stk_ulog_t *ulog,char *dirname);
void stk_ulog_close(stk_ulog_t *ulog);

int stk_ulog_select(stk_ulog_t *ulog,rtk_lists_t *urllist);
void stk_ulog_insert(stk_ulog_t *ulog,rtk_lists_t *urllist);

void stk_ulog_update(stk_ulog_t *ulog,rtk_lists_t *urllist);
void stk_ulog_delete(stk_ulog_t *ulog,rtk_arrayd_t *urlids);

void stk_ulog_tryagain(stk_ulog_t *ulog,rtk_lists_t *urllist);
int stk_ulog_refresh(stk_ulog_t *ulog,rtk_lists_t *urllist,rtk_hashd_t *refreshs,int bchknew);

void stk_ulog_select_all_urllist(stk_ulog_t *ulog,rtk_lists_t *urllist);
void stk_ulog_insert_all_urllist(stk_ulog_t *ulog,char *dirname,rtk_lists_t *urllist);

void stk_ulog_select_urllist(stk_ulog_t *ulog,int begin_id,int end_id,rtk_lists_t *urllist);

void stk_ulog_init_tree(stk_ulog_t *ulog);
void stk_ulog_recover(stk_ulog_t *ulog,char *dirname);

int stk_ulog_optimize(stk_ulog_t *ulog,char *dirname,DWORD deltime);

int stk_ulog_tree_count(stk_ulog_t *ulog);
int stk_ulog_task_count(stk_ulog_t *ulog);

int stk_ulog_url_count(stk_ulog_t *ulog);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif //_STK_ULOG_H_
