#ifndef _trs_api_cat_h_
#define _trs_api_cat_h_
#include <stdio.h>
#ifdef _WIN32
typedef void (__stdcall *pfcallback)(int handle,char *s);
#else
typedef void (*pfcallback)(int handle,char *s);
#endif
struct ClsInfo
{
   char* ClsName;
   char* path;
   int docNum;
   int right;
   int recog;
   float precision;
   float recall;
   float f;
   void *childCAT; 
};

struct CATInfo
{
  int catCount;
  struct ClsInfo *ClsTable;
  char *dstFolder;
  char *srcFolder;
  int docNum;
  FILE *fpWL;
  int right;
  void* handle;
};
struct CATREV
{
    int index;
    double v;
};
struct REVOFCAT
{
    char *catname;
    double v;
};

_STDAPI_(int) CAT_stat(struct CATInfo *pCATInfo);
_STDAPI_(int) CAT_train(struct CATInfo *pCATInfo);
_STDAPI_(int) CAT_setdisp(pfcallback func,int handle);
_STDAPI_(void) CAT_cancel();
_STDAPI_(void) CAT_errinfo(int i,char *s);
_STDAPI_(void) CAT_free(struct CATInfo *pCATInfo);
_STDAPI_(int) CAT_ClassifyTrain(struct CATInfo *pCATInfo,int layer);
_STDAPI_(int) CAT_ClassifyTrainsupply(struct CATInfo *pCATInfo,int layer,char* fNameorigin);
_STDAPI_(int) CAT_trainsupply(struct CATInfo *pCATInfo,char *fNameorigin);
_STDAPI_(int) CAT_statsupply(struct CATInfo *pCATInfo,char* fNameorigin);
_STDAPI_(int) CAT_CheckModels(char * path);
_STDAPI_(int) CAT_MergeModel(char *path1,char *path2,char *path3);
_STDAPI_(int) CAT_LoadModel(struct CATInfo *pCATInfo);
_STDAPI_(int) CAT_LoadAllModel(struct CATInfo *pCATInfo);
_STDAPI_(int) CAT_text (struct CATInfo *pCATInfo,char *pDoc,int *rev);
_STDAPI_(int) CAT_file(struct CATInfo *pCATInfo,char * fName,int *rev);
_STDAPI_(int)  CAT_files (struct CATInfo*pCATInfo ,char *infile,char *revfile);
_STDAPI_(int)  CAT_evaluate (struct CATInfo*pCATInfo ,char *infile,char *revfile);
_STDAPI_(void) CAT_unLoadModel(struct CATInfo *pCATInfo);
_STDAPI_(int)  CAT_LoadModels(struct CATInfo *pCATInfo,int charset);
_STDAPI_(int)  CAT_Text(struct CATInfo *pCATInfo,char *pDoc,int *rev,int charset);
_STDAPI_(int)  CAT_File(struct CATInfo *pCATInfo,char *pDoc,int *rev,int charset);
_STDAPI_(int)  CAT_LoadAllModel_UTF8(struct CATInfo *pCATInfo);
_STDAPI_(char*) CAT_classifyText_UTF8(struct CATInfo *pCATInfo,char *pDoc,int *num);
_STDAPI_(int)  CAT_LoadModel_UTF8(struct CATInfo *pCATInfo);
_STDAPI_(int)  CAT_text_UTF8(struct CATInfo *pCATInfo,char *pDoc,int *rev);
_STDAPI_(void) CAT_unLoadAllModel(struct CATInfo *pCATInfo);
_STDAPI_(char*) CAT_classifyText(struct CATInfo *pCATInfo,char *pDoc,int *num);
_STDAPI_(void) CAT_freeResult(char*s);
_STDAPI_(char*) CAT_classifyFile(struct CATInfo *pCATInfo,char *fName,int *num);
_STDAPI_(int) CAT_cat_xhs(struct CATInfo *pCATInfo,char *pDoc,struct REVOFCAT**prev);
_STDAPI_(void) CAT_freeResult_xhs(struct REVOFCAT**prev,int count);

#endif