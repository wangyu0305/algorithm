// rtk_mem.h : header file
//

#ifndef _RTK_MEM_H_
#define _RTK_MEM_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

RTK_DECLARE(void*) rtk_malloc(int size);
RTK_DECLARE(void) rtk_free(void* buffer);

RTK_DECLARE(void) rtk_zero(void* buffer,int count);

RTK_DECLARE(int) rtk_mem_chr(void *buffer,long size,BYTE chr);
RTK_DECLARE(int) rtk_mem_rchr(void *buffer,long size,BYTE chr);

RTK_DECLARE(int) rtk_mem_str(void *buffer,long size,void *mem,long len);

RTK_DECLARE(int) rtk_mem_cmp(void *buffer,long size,void *mem,long len);
RTK_DECLARE(int) rtk_mem_icmp(void *buffer,long size,void *mem,long len);

RTK_DECLARE(int) rtk_mem_ncmp(void *buffer,long size,void *mem,long len);
RTK_DECLARE(int) rtk_mem_nicmp(void *buffer,long size,void *mem,long len);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_MEM_H_
