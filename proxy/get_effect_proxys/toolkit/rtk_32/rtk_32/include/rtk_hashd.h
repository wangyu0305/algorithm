// rtk_hashd.h : header file
//

#ifndef _RTK_HASHD_H_
#define _RTK_HASHD_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_hashd_entry_t rtk_hashd_entry_t;
typedef struct rtk_hashd_t rtk_hashd_t;

typedef void (*rtk_hashd_init_t)(void *buffer);
typedef void (*rtk_hashd_term_t)(void *buffer);

struct rtk_hashd_entry_t
{
    rtk_hashd_entry_t   *next;
    DWORD               hkey;
    void                *buffer;
};

struct rtk_hashd_t
{
    rtk_arrays_t        entry;
    int                 count;
    rtk_pool_t          *xpool;
    short               xflag;
    rtk_pool_t          *dpool;
    short               dflag;
};

RTK_DECLARE(void) rtk_hashd_init(rtk_hashd_t *hash,int xflag,int dflag);
RTK_DECLARE(void) rtk_hashd_term(rtk_hashd_t *hash);

RTK_DECLARE(void*) rtk_hashd_set(rtk_hashd_t *hash,DWORD hkey,rtk_hashd_init_t init);
RTK_DECLARE(void*) rtk_hashd_get(rtk_hashd_t *hash,DWORD hkey);

RTK_DECLARE(int) rtk_hashd_insert(rtk_hashd_t *hash,DWORD hkey,void *buffer);
RTK_DECLARE(void*) rtk_hashd_update(rtk_hashd_t *hash,DWORD hkey,void *buffer);

RTK_DECLARE(int) rtk_hashd_find(rtk_hashd_t *hash,DWORD hkey);
RTK_DECLARE(void*) rtk_hashd_remove(rtk_hashd_t *hash,DWORD hkey);

RTK_DECLARE(DWORD) rtk_hashd_get_key(rtk_hashd_t *hash,int index,void *pos);
RTK_DECLARE(void*) rtk_hashd_get_next(rtk_hashd_t *hash,int *index,void **pos);

RTK_DECLARE(void) rtk_hashd_remove_all(rtk_hashd_t *hash);
RTK_DECLARE(void) rtk_hashd_free_all(rtk_hashd_t *hash,rtk_hashd_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_HASHD_H_
