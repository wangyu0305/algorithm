// rtk_socket.h : header file
//

#ifndef _RTK_SOCKET_H_
#define _RTK_SOCKET_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_socket_t rtk_socket_t;

struct rtk_socket_t
{
    DWORD               sockfd;
    DWORD               ipaddr;
    WORD                port;
    WORD                max_connect;
    WORD                connect_retries;
    DWORD               connect_timeout;
    DWORD               send_timeout;
    DWORD               recv_timeout;
    int                 flag;
    void                *param;
};

RTK_DECLARE(DWORD) rtk_socket_get_ipaddr(char *hostname);

RTK_DECLARE(int) rtk_socket_initialize();
RTK_DECLARE(void) rtk_socket_finalize();

RTK_DECLARE(void) rtk_socket_init(rtk_socket_t *sock);
RTK_DECLARE(void) rtk_socket_term(rtk_socket_t *sock);

RTK_DECLARE(int) rtk_socket_open(rtk_socket_t *sock);
RTK_DECLARE(void) rtk_socket_close(rtk_socket_t *sock);

RTK_DECLARE(int) rtk_socket_is_open(rtk_socket_t *sock);

RTK_DECLARE(int) rtk_socket_listen(rtk_socket_t *sock);
RTK_DECLARE(rtk_socket_t*) rtk_socket_accept(rtk_socket_t *sock);

RTK_DECLARE(void) rtk_socket_option(rtk_socket_t *sock);
RTK_DECLARE(int) rtk_socket_connect(rtk_socket_t *sock);

RTK_DECLARE(int) rtk_socket_send_message(rtk_socket_t *sock,rtk_stream_t *request);
RTK_DECLARE(int) rtk_socket_recv_message(rtk_socket_t *sock,int len,rtk_stream_t *response);

RTK_DECLARE(int) rtk_socket_send_command(rtk_socket_t *sock,int cmd_code,rtk_stream_t *cmd_param);
RTK_DECLARE(int) rtk_socket_recv_command(rtk_socket_t *sock,int *cmd_code,rtk_stream_t *cmd_param);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_SOCKET_H_
