// rtk_32.h : header file
//

#ifndef _RTK_32_H_
#define _RTK_32_H_

#ifndef WIN32
#define _LARGEFILE64_SOURCE
#endif

#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x400
#endif

#if !defined(MSC) && defined(_MSC_VER)
#define MSC
#endif

#if !defined(WIN64)
#if defined(_WIN64)
#define WIN64
#elif !defined(WIN32) && (defined(_WIN32) || defined(WIN32S))
#define WIN32
#endif
#endif

#if !defined(WINNT) && (defined(WIN32) || defined(WIN64))
#define WINNT
#endif

#if !defined(WINDOWS) && (defined(_Windows) || defined(WINSOCK) || defined(_WINDOWS) || defined(WIN31) || defined(WINNT))
#define WINDOWS
#endif

#if defined(MSC)
#pragma warning(disable:4996)
#pragma warning(disable:4819)
#endif

#ifdef WINDOWS
#include <windows.h>
#include <io.h>
#include <direct.h>
#else
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <dirent.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <memory.h>
#include <malloc.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

#define RTK_MULTI_THREAD

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

#ifdef WINDOWS
typedef __int64            int64_t;
typedef unsigned __int64   uint64_t;
#else
typedef long long          int64_t;
typedef unsigned long long uint64_t;
#endif

#if defined(WIN64)
typedef __int64            intptr_t;
typedef unsigned __int64   uintptr_t;
#else
typedef int                intptr_t;
typedef unsigned int       uintptr_t;
#endif

#ifndef BYTE
typedef unsigned char  BYTE;
#endif

#ifndef WORD
typedef unsigned short  WORD;
#endif

#ifndef DWORD
#ifdef WINDOWS
typedef unsigned long  DWORD;
#else
typedef unsigned int DWORD;
#endif
#endif

#ifndef LWORD
typedef uint64_t  LWORD;
#endif

#ifndef MAKEWORD
#define MAKEWORD(a, b)      ((WORD)(((BYTE)(a & 0xFF)) | ((WORD)((BYTE)(b))) << 8))
#endif

#ifndef MAKEDWORD
#define MAKEDWORD(a, b)     ((DWORD)(((WORD)(a & 0xFFFF)) | ((DWORD)((WORD)(b))) << 16))
#endif

#ifndef MAKELWORD
#define MAKELWORD(a, b)     ((LWORD)(((DWORD)(a & 0x3FFFFFFF)) | ((LWORD)((DWORD)(b))) << 30))
#endif

#ifndef LOBYTE
#define LOBYTE(w)           ((BYTE)(w & 0xFF))
#endif

#ifndef HIBYTE
#define HIBYTE(w)           ((BYTE)(((WORD)(w) >> 8) & 0xFF))
#endif

#ifndef LOWORD
#define LOWORD(l)           ((WORD)(l & 0xFFFF))
#endif

#ifndef HIWORD
#define HIWORD(l)           ((WORD)(((DWORD)(l) >> 16) & 0xFFFF))
#endif

#ifndef LODWORD
#define LODWORD(l)          ((DWORD)(l & 0x3FFFFFFF))
#endif

#ifndef HIDWORD
#define HIDWORD(l)          ((DWORD)(((LWORD)(l) >> 30) & 0xFFFFFFFF))
#endif

#ifndef MAX
#define MAX(a,b) ((a) < (b) ? (b) : (a))
#endif

#ifndef MIN
#define MIN(a,b) ((b) < (a) ? (b) : (a))
#endif

#ifdef WINDOWS
#define RTK_DECLARE(type) __declspec(dllexport) type __stdcall
#else
#define RTK_DECLARE(type) type
#endif

#include "rtk_mutex.h"
#include "rtk_mem.h"
#include "rtk_pool.h"
#include "rtk_char.h"
#include "rtk_string.h"
#include "rtk_stream.h"
#include "rtk_array.h"
#include "rtk_arrayd.h"
#include "rtk_arrays.h"
#include "rtk_arrayx.h"
#include "rtk_list.h"
#include "rtk_lists.h"
#include "rtk_listx.h"
#include "rtk_hash.h"
#include "rtk_hashd.h"
#include "rtk_hashx.h"
#include "rtk_tree.h"
#include "rtk_btree.h"
#include "rtk_btreed.h"
#include "rtk_bits.h"
#include "rtk_dir.h"
#include "rtk_file.h"
#include "rtk_mfile.h"
#include "rtk_thread.h"
#include "rtk_threads.h"
#include "rtk_socket.h"
#include "rtk_sockets.h"
#include "rtk_timer.h"

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif  /* _RTK_32_H_ */
