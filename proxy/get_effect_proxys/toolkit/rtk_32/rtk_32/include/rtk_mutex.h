// rtk_mutex.h : header file
//

#ifndef _RTK_MUTEX_H_
#define _RTK_MUTEX_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_mutex_t rtk_mutex_t;

struct rtk_mutex_t
{
#ifdef WINDOWS
    CRITICAL_SECTION        mutex;
#else
    pthread_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_mutex_init(rtk_mutex_t *mutex);
RTK_DECLARE(void) rtk_mutex_term(rtk_mutex_t *mutex);

RTK_DECLARE(int) rtk_mutex_trylock(rtk_mutex_t *mutex);

RTK_DECLARE(void) rtk_mutex_lock(rtk_mutex_t *mutex);
RTK_DECLARE(void) rtk_mutex_unlock(rtk_mutex_t *mutex);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_MUTEX_H_
