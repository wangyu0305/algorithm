// rtk_dir.h : header file
//

#ifndef _RTK_DIR_H_
#define _RTK_DIR_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

RTK_DECLARE(void) rtk_dir_merge(rtk_string_t *dirname,char *supdir,char *subdir);
RTK_DECLARE(void) rtk_dir_cwd(rtk_string_t *dirname);

RTK_DECLARE(void) rtk_dir_make(char *supdir,char *subdir);
RTK_DECLARE(void) rtk_dir_clear(char *dirname,int flag);

RTK_DECLARE(void) rtk_dir_dlist(char *dirname,rtk_list_t *list);
RTK_DECLARE(void) rtk_dir_flist(char *dirname,rtk_list_t *list);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_DIR_H_
