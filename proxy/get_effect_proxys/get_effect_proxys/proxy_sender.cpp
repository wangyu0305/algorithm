#include "proxy_sender.h"
#include "proxy_db.h"
#include "proxy_ctrl.h"
#include "proxy_transfer.h"
#include "db_cfg.h"
#include "stk_effect_proxy.h"
#include "stk_cfg.h"
#include "sender_cfg.h"

extern proxy_sender_storage_t sender_storage;
extern rtk_mutex_t l_mutex;
extern rtk_mutex_t h_mutex;

void proxy_sender_storage_init(proxy_sender_storage_t* sender_storage)
{	
	rtk_hash_init(&sender_storage->sender_used_hash,true,true);
	sender_storage->sender_used_hash.dpool->memsize = sizeof(stk_test_proxy_t);
	rtk_arrays_init(&sender_storage->sender_save_arrays,true);
	sender_storage->sender_save_arrays.dpool->memsize = sizeof(void*);
	rtk_lists_init(&sender_storage->sender_db_lists,true,true);
	sender_storage->sender_db_lists.dpool->memsize = sizeof(stk_test_proxy_t);
	rtk_lists_init(&sender_storage->sender_save_lists,true,true);
	sender_storage->sender_save_lists.dpool->memsize = sizeof(stk_test_proxy_t);
}

void proxy_sender_storage_term(proxy_sender_storage_t* sender_storage)
{
	rtk_hash_term(&sender_storage->sender_used_hash);
	rtk_arrays_term(&sender_storage->sender_save_arrays);
	rtk_lists_term(&sender_storage->sender_db_lists);
	rtk_lists_term(&sender_storage->sender_save_lists);
}

void sender_get_proxy_handle_cfg(stk_proxy_handle_t* m_proxy_handle)
{
	rtk_string_t str_current_dir;
	rtk_string_init(&str_current_dir);
	rtk_dir_cwd(&str_current_dir);
	stk_proxy_handle_get_profile(m_proxy_handle, str_current_dir.buffer, "effect_proxy.cfg");
	rtk_string_term(&str_current_dir);
}

void proxy_sender_get_from_db(proxy_sender_storage_t* sender_storage)
{
	void* pos;
	stk_test_proxy_t* temp_proxy;
	proxy_db_export(&sender_storage->sender_db_lists);
	pos = rtk_lists_get_head_position(&sender_storage->sender_db_lists);
	while (pos)
	{
		temp_proxy = (stk_test_proxy_t*)rtk_lists_get_next(&sender_storage->sender_db_lists,&pos);
		if (stk_proxy_hash_find(&sender_storage->sender_used_hash,temp_proxy) != 1)
		{
			//stk_proxy_hash_insert(&sender_storage->sender_used_hash,temp_proxy);
			stk_test_proxy_t* recycle_proxy = (stk_test_proxy_t*)rtk_lists_add_tail_new(&sender_storage->sender_save_lists,\
				(rtk_lists_init_t)stk_test_proxy_init);
			stk_test_proxy_copy(recycle_proxy,temp_proxy);
		}
	}
	rtk_lists_free_all(&sender_storage->sender_db_lists,(rtk_lists_term_t)stk_test_proxy_term);
}

int sender_set_proxy_lists(stk_test_proxy_lists_t* m_test_proxy_lists,int limitnum)
{
	stk_test_proxy_t* temp_proxy;
	int i = 0;
	while (1)
	{
		rtk_mutex_lock(&l_mutex);
		temp_proxy = (stk_test_proxy_t*)rtk_lists_remove_head(&sender_storage.sender_save_lists);
		rtk_mutex_unlock(&l_mutex);
		if (temp_proxy == 0)
			break;
		rtk_mutex_lock(&h_mutex);
		if (stk_proxy_hash_find(&sender_storage.sender_used_hash,temp_proxy) != 1)
		{
			stk_test_proxy_t* new_proxy = (stk_test_proxy_t*)rtk_lists_add_tail_new(&m_test_proxy_lists->test_general_proxy_lists,\
				(rtk_lists_init_t)stk_test_proxy_init);
			stk_test_proxy_copy(new_proxy,temp_proxy);
		}
		rtk_mutex_unlock(&h_mutex);
		stk_test_proxy_term(temp_proxy);
		i++;
		if (i >= limitnum)
			break;
	}
	return i;
}

void sender_param_change(stk_test_proxy_t* m_test_proxy,rtk_stream_t* send_param)
{
	rtk_string_t temp_str;
	rtk_string_init(&temp_str);
	rtk_string_append(&temp_str,m_test_proxy->str_proxy_ip.buffer,m_test_proxy->str_proxy_ip.length);
	rtk_string_append_char(&temp_str,':');
	rtk_string_append_number(&temp_str,m_test_proxy->n_port);
	rtk_string_append(&temp_str,"\r\n",-1);
	rtk_stream_swap_string(send_param,&temp_str);
	rtk_string_term(&temp_str);
}

void sender_set_hash_param(rtk_arrays_t* sender_arrays,rtk_stream_t* send_param,int limitnum)
{
	rtk_stream_t tmp_stream;
	rtk_stream_init(&tmp_stream);
	stk_test_proxy_t* m_test_proxy;
	for (int i = 0;i < sender_arrays->size;i++)
	{
		if (i == limitnum)
			break;
		m_test_proxy = (stk_test_proxy_t*)rtk_arrays_get_at(sender_arrays,i);
		sender_param_change(m_test_proxy,&tmp_stream);
		rtk_stream_append(send_param,tmp_stream.buffer,tmp_stream.length);
		rtk_mutex_lock(&h_mutex);
		stk_proxy_hash_insert(&sender_storage.sender_used_hash,m_test_proxy);
		rtk_mutex_unlock(&h_mutex);
	}
	rtk_stream_term(&tmp_stream);
}

bool sender_set_param(rtk_stream_t* send_param,int limitnum)
{
	bool notenough = false;
	stk_proxy_handle_t m_proxy_handle;
	rtk_hash_t m_proxy_hash;
	stk_test_proxy_lists_t m_test_proxy_lists;
	rtk_arrays_t sender_arrays;
	rtk_arrays_init(&sender_arrays,true);
	sender_arrays.dpool->memsize = sizeof(void*);
	stk_proxy_hash_init(&m_proxy_hash);
	stk_proxy_handle_init(&m_proxy_handle);
	stk_test_proxy_lists_init(&m_test_proxy_lists);
	sender_get_proxy_handle_cfg(&m_proxy_handle);

	while (sender_arrays.size < limitnum)
	{
		if (sender_set_proxy_lists(&m_test_proxy_lists,limitnum) > 0)
			m_proxy_handle.m_test_proxy_lists = &m_test_proxy_lists;
		else
			break;
		stk_effect_get_proxy_lists(&m_proxy_handle,&m_proxy_hash,&sender_arrays);
	}
	if (sender_arrays.size > 0)
		sender_set_hash_param(&sender_arrays,send_param,limitnum);
	else
		notenough = true;

	stk_proxy_hash_term(&m_proxy_hash);
	rtk_arrays_free_all(&sender_arrays,(rtk_arrays_term_t)stk_test_proxy_term);
	rtk_arrays_term(&sender_arrays);
	stk_test_proxy_lists_term(&m_test_proxy_lists);
	stk_proxy_handle_term(&m_proxy_handle);
	return notenough;
}

int sender_check_recv_stream(rtk_stream_t* recv_param)
{
	BYTE* recv_str;
	int num = 0,ichoose = 0,length = 0;
	recv_str = recv_param->buffer;

	while (length < recv_param->length)
	{
		switch (*recv_str)
		{
		case 'G':
		case 'E':
		case 'T':
			ichoose++;
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			num = num*10 + (*recv_str - '0');
			break;
		default:
			break;
		}
		recv_str++;
		length++;
	}

	if (ichoose == 3)
	{
		return num;
	}
	return -1;
}

void sender_remove_hash(rtk_list_t* iplist)
{
	void* pos;
	char* ipstr;
	pos = rtk_list_get_head_position(iplist);
	while (pos)
	{
		ipstr = rtk_list_get_next(iplist,&pos);
		rtk_mutex_lock(&h_mutex);
		rtk_hash_remove(&sender_storage.sender_used_hash,ipstr,-1);
		rtk_mutex_unlock(&h_mutex);
	}
}