#include "db_cfg.h"
#include "rtk_32.h"
#include "convert.h"
#include <Windows.h>

void stk_db_init(stk_db_t *db)
{
	rtk_zero(db,sizeof(stk_db_t));
	rtk_mutex_init(&db->db_mutex);
}

void stk_db_term(stk_db_t *db)
{
	rtk_char_term(&db->db_name);
	rtk_char_term(&db->db_dll);
	rtk_char_term(&db->db_server);
	rtk_char_term(&db->db_port);
	rtk_char_term(&db->db_database);
	rtk_char_term(&db->db_username);
	rtk_char_term(&db->db_password);
	rtk_char_term(&db->db_dirname);
	rtk_mutex_term(&db->db_mutex);
}

void db_cfg_init(db_cfg_t *cfg)
{
	rtk_zero(cfg,sizeof(db_cfg_t));
}

void db_cfg_term(db_cfg_t *cfg)
{
	rtk_string_term(&cfg->msqserver);
	rtk_string_term(&cfg->msqport);
	rtk_string_term(&cfg->msqusername);
	rtk_string_term(&cfg->msqpassword);
	rtk_string_term(&cfg->msqdatabase);
	rtk_string_term(&cfg->msqcharset);
	rtk_string_term(&cfg->mysqlselect);
	rtk_string_term(&cfg->mstable);
	rtk_string_term(&cfg->row1);
	rtk_string_term(&cfg->row2);
	rtk_string_term(&cfg->row3);
	rtk_string_term(&cfg->row4);
	rtk_string_term(&cfg->row5);
	rtk_string_term(&cfg->row6);
}

int stk_db_load_func(stk_db_t *db)
{
	if(!(db->db_func.rdb_initialize = (mdb_initialize*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_initialize"))) return false;
	if(!(db->db_func.rdb_finalize = (mdb_finalize*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_finalize"))) return false;
	if(!(db->db_func.rdb_login = (mdb_login*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_login"))) return false;
	if(!(db->db_func.rdb_logout = (mdb_logout*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_logout"))) return false;
	if(!(db->db_func.rdb_get_last_error = (mdb_get_last_error*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_get_last_error"))) return false;
	if(!(db->db_func.rdb_set_charset = (mdb_set_charset*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_set_charset"))) return false;
	if(!(db->db_func.rdb_use_db_name = (mdb_use_db_name*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_use_db_name"))) return false;
	if(!(db->db_func.rdb_prepare_sql = (mdb_prepare_sql*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_prepare_sql"))) return false;
	if(!(db->db_func.rdb_sql_excute = (mdb_sql_excute*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_sql_excute"))) return false;
	if(!(db->db_func.rdb_set_parameters = (mdb_set_parameters*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_set_parameters"))) return false;
	if(!(db->db_func.rdb_fetch_next = (mdb_fetch_next*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_fetch_next"))) return false;
	if(!(db->db_func.rdb_open_records = (mdb_open_records*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_open_records"))) return false;
	if(!(db->db_func.rdb_close_records = (mdb_close_records*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_close_records"))) return false;
	if(!(db->db_func.rdb_sql_commit = (mdb_sql_commit*)GetProcAddress((struct HINSTANCE__ *)db->db_inst,"rdb_sql_commit"))) return false;
	return true;
}

void stk_db_copy(stk_db_t *db,stk_db_t *ndb)
{
    rtk_char_copy(&db->db_name,ndb->db_name,-1);
    rtk_char_copy(&db->db_dll,ndb->db_dll,-1);
    rtk_char_copy(&db->db_server,ndb->db_server,-1);
    rtk_char_copy(&db->db_port,ndb->db_port,-1);
    rtk_char_copy(&db->db_database,ndb->db_database,-1);
    db->db_usertype = ndb->db_usertype;
    rtk_char_copy(&db->db_username,ndb->db_username,-1);
    rtk_char_copy(&db->db_password,ndb->db_password,-1);
    rtk_char_copy(&db->db_dirname,ndb->db_dirname,-1);
    db->db_filesize = ndb->db_filesize;
    db->db_use = ndb->db_use;
    db->db_inst = ndb->db_inst;
    db->db_func = ndb->db_func;
}

int stk_db_compare(stk_db_t *db,char *buffer)
{
    return rtk_char_icompare(db->db_name,buffer,-1);
}

void stk_db_load_library(stk_db_t *db)
{
	if(!rtk_char_is_empty(db->db_dll) && db->db_inst == NULL)
	{
		db->db_inst = LoadLibrary(db->db_dll);
		if(!stk_db_load_func(db))
		{
			FreeLibrary((struct HINSTANCE__ *)db->db_inst);
			db->db_inst = 0;
		}
		else
		{
			db->db_func.rdb_initialize();
		}
	}
}

void stk_db_free_library(stk_db_t *db)
{
	if(db->db_inst != NULL)
	{
		db->db_func.rdb_finalize();
		
		FreeLibrary((struct HINSTANCE__ *)db->db_inst);
		db->db_inst = 0;
	}
}

//void stk_db_set_ora_info(stk_db_t *db,db_cfg_t *cfg)
//{
//	rtk_char_copy(&db->db_name,"ora",-1);
//	rtk_char_copy(&db->db_dll,"rdb_ora.dll",-1);
//	rtk_char_copy(&db->db_server,cfg->oraserver.buffer,cfg->oraserver.length);
//	rtk_char_copy(&db->db_username,cfg->orausername.buffer,cfg->orausername.length);
//	rtk_char_copy(&db->db_password,cfg->orapassword.buffer,cfg->orapassword.length);
//}

void stk_db_set_msq_info(stk_db_t *db,db_cfg_t *cfg)
{
	rtk_char_copy(&db->db_name,"mysql",-1);
	rtk_char_copy(&db->db_dll,"rdb_msq.dll",-1);
	rtk_char_copy(&db->db_server,cfg->msqserver.buffer,cfg->msqserver.length);
	rtk_char_copy(&db->db_port,cfg->msqport.buffer,cfg->msqport.length);
	rtk_char_copy(&db->db_username,cfg->msqusername.buffer,cfg->msqusername.length);
	rtk_char_copy(&db->db_password,cfg->msqpassword.buffer,cfg->msqpassword.length);
    rtk_char_copy(&db->db_database,cfg->msqdatabase.buffer,cfg->msqdatabase.length);
	rtk_char_copy(&db->db_charset,cfg->msqcharset.buffer,cfg->msqcharset.length);
}

int64_t parse_number(const unsigned char *num)         /*rtk_string_get_number 不能得出数字*/
{
	double n=0,sign=1,scale=0;
	int subscale=0,signsubscale=1;

	// Could use sscanf for this?
	if (*num=='-') 
	{
		sign=-1;
		num++;    // Has sign?
	}

	if (*num=='0') 
	{
		num++;            // is zero
	}

	if (*num>='1' && *num<='9')    
	{
		do    
		{
			n=(n*10.0)+(*num++ -'0');
		}
		while (*num>='0' && *num<='9');    // Number?
	}

	if (*num=='.') 
	{
		num++;        
		do    
		{
			n=(n*10.0)+(*num++ -'0');
			scale--;
		}
		while (*num>='0' && *num<='9');
	}    // Fractional part?

	return (int64_t)n;
}

int robot_get_next_field(int pos,rtk_string_t *context,rtk_string_t *tag,int *begin,int *end)
{
	char* p_char;
	int index;
	int b_tag;
	int b_find;

	rtk_string_shrink(tag,0);

	*begin = -1;
	*end = -1;
	b_tag = false;
	b_find = false;

	if(pos < 0 || pos >= context->length)
	{
		return false;
	}

	p_char = context->buffer;
	p_char += pos;
	index = pos;
	while(*p_char && index < context->length)
	{
		switch(*p_char)
		{
		case '<':
			*begin = index;
			b_tag = false;
			break;
		case '>':
			*end = index;
			if(*begin >= 0 && *end >= 0 && *end > *begin)
			{
				rtk_string_mid_string(context,*begin+1,*end-*begin-1,tag);
				b_tag = true;
			}
			break;
		case '=':
			if(b_tag)
			{
				*end = index;
				b_find = true;
			}
			break;
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			break;
		default:
			if(b_tag)
			{
				b_tag = false;
				*begin = -1;
				*end = -1;
			}
			break;
		}
		if(b_find)
		{
			return true;
		}
		p_char ++;
		index ++;
	}

	return false;
}

void db_cfg_get_name(db_cfg_t *cfg,rtk_string_t *name,rtk_string_t *val)
{
	if(rtk_string_icompare(name,"msqserver",-1) == 0)
	{
		rtk_string_copy(&cfg->msqserver,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msqport",-1) == 0)
	{
		rtk_string_copy(&cfg->msqport,val->buffer,val->length);
	}
	else if(rtk_string_icompare(name,"msqusername",-1) == 0)
	{
		rtk_string_copy(&cfg->msqusername,val->buffer,val->length);
	}
	else if(rtk_string_icompare(name,"msqpassword",-1) == 0)
	{
		rtk_string_copy(&cfg->msqpassword,val->buffer,val->length);
	}
	else if(rtk_string_icompare(name,"msqdatabase",-1) == 0)
	{
		rtk_string_copy(&cfg->msqdatabase,val->buffer,val->length);
	}
	else if(rtk_string_icompare(name,"msqcharset",-1) == 0)
	{
		rtk_string_copy(&cfg->msqcharset,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msselect",-1) == 0)
	{
		rtk_string_copy(&cfg->mysqlselect,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"mstable",-1) == 0)
	{
		rtk_string_copy(&cfg->mstable,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow1",-1) == 0)
	{
		rtk_string_copy(&cfg->row1,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow2",-1) == 0)
	{
		rtk_string_copy(&cfg->row2,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow3",-1) == 0)
	{
		rtk_string_copy(&cfg->row3,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow4",-1) == 0)
	{
		rtk_string_copy(&cfg->row4,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow5",-1) == 0)
	{
		rtk_string_copy(&cfg->row5,val->buffer,val->length);
	}
	else if (rtk_string_icompare(name,"msrow6",-1) == 0)
	{
		rtk_string_copy(&cfg->row6,val->buffer,val->length);
	}
}

void db_cfg_get_param(db_cfg_t *cfg,rtk_string_t *context)
{
	int pos,begin,end;
	rtk_string_t tag;
	rtk_string_t name;
	rtk_string_t val;

	rtk_string_init(&tag);
	rtk_string_init(&name);
	rtk_string_init(&val);

	pos = 0;
	while(robot_get_next_field(pos,context,&tag,&begin,&end))
	{
		rtk_string_mid_string(context,pos,begin-pos,&val);
		rtk_string_trim(&val);
		if(!rtk_string_is_empty(&name))
		{
			db_cfg_get_name(cfg,&name,&val);
		}
		rtk_string_copy(&name,tag.buffer,tag.length);
		pos = end + 1;
	}

	begin = rtk_string_find_pos(context,"\r\n",-1,pos);
	if(begin != -1)
	{
		rtk_string_mid_string(context,pos,begin-pos,&val);
	}
	else
	{
		rtk_string_mids_string(context,pos,&val);
	}

	rtk_string_trim(&val);
	if(!rtk_string_is_empty(&name))
	{
		db_cfg_get_name(cfg,&name,&val);
	}

	rtk_string_term(&tag);
	rtk_string_term(&name);
	rtk_string_term(&val);
}

void db_cfg_get_profile(db_cfg_t *cfg)
{
	rtk_string_t fname;
	rtk_string_t context;
    rtk_string_t dirname;
	rtk_string_init(&context);
	rtk_string_init(&fname);
    rtk_string_init(&dirname);
	rtk_dir_cwd(&dirname);
	rtk_dir_merge(&fname,dirname.buffer,"db.cfg");
	rtk_file_get_string(fname.buffer,&context);

	db_cfg_get_param(cfg,&context);

	rtk_string_term(&context);
	rtk_string_term(&fname);
	rtk_string_term(&dirname);
}

void stk_db_prepare_sql(char *charset,rtk_string_t *sql)
{
	if(rtk_char_icompare(charset,"utf-8",-1) == 0)
	{
		ConvertToUTF8("gb18030",sql);
	}
}