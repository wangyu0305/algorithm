#include "log.h"
#include "time_fun.h"
#include "proxy_ctrl.h"

extern clock_t m_clock;

void stk_effect_proxy_log_out_starttime(int index, const char* supdir, rtk_string_t* str_time)
{
    rtk_string_t fname;
    rtk_string_t str_out;
    rtk_string_t str_index;

    rtk_string_init(&fname);
    rtk_string_init(&str_out);
    rtk_string_init(&str_index);

    rtk_dir_make((char*)supdir, "log");
    rtk_dir_merge(&fname,(char*)supdir,"log\\effect_proxy_log.txt");

    rtk_string_copy_number(&str_index, index);
    rtk_string_append(&str_out, "%d 开始获取可用代理\r\n", -1);
    rtk_string_format(&str_out, "%d", str_time->buffer);
   

    rtk_file_set_string(fname.buffer, 1, &str_out);

    rtk_string_term(&fname);
    rtk_string_term(&str_out);
    rtk_string_term(&str_index);
}

void stk_effect_proxy_log_out_endtime(int index, const char* supdir, rtk_string_t* str_time)
{
    rtk_string_t fname;
    rtk_string_t str_out;
    rtk_string_t str_index;

    rtk_string_init(&fname);
    rtk_string_init(&str_out);
    rtk_string_init(&str_index);

    rtk_dir_make((char*)supdir, "log");
    rtk_dir_merge(&fname,(char*)supdir,"log\\effect_proxy_log.txt");

    rtk_string_copy_number(&str_index, index);
    rtk_string_append(&str_out, "%d 结束获取可用代理\r\n", -1);
    rtk_string_format(&str_out, "%d", str_time->buffer);
    rtk_string_append(&str_out,"\r\n\r\n",-1);

    rtk_file_set_string(fname.buffer, 1, &str_out);

    rtk_string_term(&fname);
    rtk_string_term(&str_out);
    rtk_string_term(&str_index);

}

void stk_effect_proxy_log_out_string(const char* supdir, rtk_string_t* str_log_info)
{
    rtk_string_t fname;
    rtk_string_t str_out;


    rtk_string_init(&fname);
    rtk_string_init(&str_out);


    rtk_dir_make((char*)supdir, "log");
    rtk_dir_merge(&fname,(char*)supdir,"log\\effect_proxy_log.txt");

    rtk_file_set_string(fname.buffer, 1, str_log_info);

    rtk_string_term(&fname);
    rtk_string_term(&str_out);
}

void stk_effect_proxy_log_single_res(int flag, int number_effect_proxy, char* supdir)
{
    rtk_string_t str_log;

    rtk_string_init(&str_log);
    if (flag == PSTART)
    {
		rtk_string_copy(&str_log, "测试完网站下载代理  共找到有效代理\t", -1);
		rtk_string_append_number(&str_log, number_effect_proxy);
		rtk_string_append(&str_log, "\r\n", -1);
    }
	if (flag == PCHECK)
	{
		rtk_string_copy(&str_log, "检查完缓存代理  共找到有效代理\t", -1);
		rtk_string_append_number(&str_log, number_effect_proxy);
		rtk_string_append(&str_log, "\r\n", -1);
	}
	if (flag == PLOAD)
	{
		rtk_string_copy(&str_log, "导入完成  共导入代理\t", -1);
		rtk_string_append_number(&str_log, number_effect_proxy);
		rtk_string_append(&str_log, "\r\n", -1);
	}
	   
	 stk_effect_proxy_log_out_string(supdir, &str_log);
     rtk_string_term(&str_log);
}

void stk_effect_proxy_log_single_url_start(const char* supdir, rtk_string_t* str_url)
{
    rtk_string_t str_time;
    rtk_string_t str_log;


    rtk_string_init(&str_time);
    rtk_string_init(&str_log);

    get_current_time(&str_time);
    rtk_string_append(&str_log, str_time.buffer, str_time.length);
    rtk_string_append(&str_log, " 开始获取可用代理 ", -1);
    rtk_string_append(&str_log, str_url->buffer, str_url->length);
    rtk_string_append(&str_log, "\r\n",-1);
	

    stk_effect_proxy_log_out_string(supdir, &str_log);

    rtk_string_term(&str_time);
    rtk_string_term(&str_log);

}
void stk_effect_proxy_log_single_url_end(const char* supdir, rtk_string_t* str_url)
{
    rtk_string_t str_time;
    rtk_string_t str_log;


    rtk_string_init(&str_time);
    rtk_string_init(&str_log);

    get_current_time(&str_time);
    rtk_string_append(&str_log, str_time.buffer, str_time.length);
    rtk_string_append(&str_log, " 结束获取可用代理 ", -1);
    rtk_string_append(&str_log, str_url->buffer, str_url->length);
    rtk_string_append(&str_log, "\r\n",-1);
	
    stk_effect_proxy_log_out_string(supdir, &str_log);

    rtk_string_term(&str_time);
    rtk_string_term(&str_log);

}


void stk_effect_proxy_log_get_down_cache(const char* supdir)
{
    rtk_string_t str_log;

    rtk_string_init(&str_log);

    rtk_string_copy(&str_log, "从缓存文件中读取待测试代理\n", -1);

    stk_effect_proxy_log_out_string(supdir, &str_log);
    rtk_string_term(&str_log);
}

void stk_effect_proxy_log_get_down_sites(const char* supdir)
{
    rtk_string_t str_log;

    rtk_string_init(&str_log);

    rtk_string_copy(&str_log, "从代理来源网页中下载待测试代理\n", -1);

    stk_effect_proxy_log_out_string(supdir, &str_log);
    rtk_string_term(&str_log);

}


void stk_proxy_detail_log_set(rtk_string_t* str_log_info)
{
    rtk_string_t fname;
    rtk_string_t str_out;
    rtk_string_t supdir;

    rtk_string_init(&fname);
    rtk_string_init(&str_out);
    rtk_string_init(&supdir);

    rtk_dir_cwd(&supdir);
    rtk_dir_make((char*)supdir.buffer, "log");
    rtk_dir_merge(&fname,(char*)supdir.buffer,"log\\detail_log.txt");

    rtk_file_set_string(fname.buffer, 1, str_log_info);

    rtk_string_term(&fname);
    rtk_string_term(&str_out);
    rtk_string_term(&supdir);
}


void stk_proxy_detail_log_set(char* sz_log_info)
{
    rtk_string_t fname;
    rtk_string_t str_out;
    rtk_string_t supdir;
    rtk_string_t str_log_info;


    rtk_string_init(&fname);
    rtk_string_init(&str_out);
    rtk_string_init(&supdir);
    rtk_string_init(&str_log_info);

    rtk_dir_cwd(&supdir);
    rtk_dir_make((char*)supdir.buffer, "log");
    rtk_dir_merge(&fname,(char*)supdir.buffer,"log\\detail_log.txt");

    rtk_string_copy(&str_log_info, sz_log_info, -1);
    rtk_file_set_string(fname.buffer, 1, &str_log_info);

    rtk_string_term(&fname);
    rtk_string_term(&str_out);
    rtk_string_term(&supdir);
    rtk_string_term(&str_log_info);
}

void stk_proxy_log_test_proxy_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, int count)
{
    rtk_string_append(str_log_info, m_test_proxy->str_proxy_ip.buffer, m_test_proxy->str_proxy_ip.length);
    rtk_string_append_char(str_log_info, ' ');
    rtk_string_append_number(str_log_info, m_test_proxy->n_port);
    rtk_string_append(str_log_info, " is OK\t", -1);
    rtk_string_append_number(str_log_info, count);
    rtk_string_append(str_log_info, "\r\n",-1);

}

void stk_proxy_log_down_begin_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url)
{
    rtk_string_copy(str_log_info, "down %a %b by %c begin\t", -1);
    rtk_string_format(str_log_info,"%c", m_test_proxy->str_proxy_ip.buffer);
    char* c_port;
    rtk_char_init(&c_port);
    rtk_char_append_number(&c_port, m_test_proxy->n_port);
    rtk_string_format(str_log_info,"%b", c_port);
    rtk_char_term(&c_port);
    rtk_string_format(str_log_info,"%a", m_test_url->str_url.buffer);
    m_clock = clock();
    rtk_string_append_number(str_log_info, m_clock);
    rtk_string_append(str_log_info, "\r\n",-1); 
}

void stk_proxy_log_down_end_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url)
{
    rtk_string_copy(str_log_info, "down %a %b by %c end\t", -1);
    rtk_string_format(str_log_info,"%c", m_test_proxy->str_proxy_ip.buffer);
    char* c_port;
    rtk_char_init(&c_port);
    rtk_char_append_number(&c_port, m_test_proxy->n_port);
    rtk_string_format(str_log_info,"%b", c_port);
    rtk_char_term(&c_port);
    rtk_string_format(str_log_info,"%a", m_test_url->str_url.buffer);
    m_clock = clock();
    rtk_string_append_number(str_log_info, m_clock);
    rtk_string_append(str_log_info, "\r\n",-1); 
}

void proxy_dblog_tofile(char* rec)
{
	rtk_string_t logfile;
	rtk_string_init(&logfile);
	rtk_string_t recode;
	rtk_string_init(&recode);
	rtk_dir_cwd(&logfile);
	rtk_dir_make(logfile.buffer,"log");
	rtk_dir_merge(&logfile,logfile.buffer,"log\\db.log");

	rtk_string_copy(&recode,rec,-1);
	rtk_string_append(&recode,"\r\n",-1);
	rtk_file_set_string(logfile.buffer,true,&recode);

	rtk_string_term(&logfile);
	rtk_string_term(&recode);
}