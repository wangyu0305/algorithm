#ifndef _STK_EFFECT_PROXY__H_
#define _STK_EFFECT_PROXY__H_

#include <rtk_32.h>
#include "stk_cfg.h"
#include <stk_inet.h>
#include <math.h> 
#include <time.h>

//#define COMMON_PROXY

#define RIGHT 0
#define ENOUGH 1
#define RECV_TIMEOUT 2
#define RECV_ERROR 3
#define SEND_TIMEOUT 4
#define CONNECT_TIMEOUT 5
#define IOCTLSOCKET 6
#define NO_TAG 7
#define STATUS_ERROR 8
#define SIZE_ERROR 9
#define NOTENOUGH 10

struct stk_filename_t
{
    rtk_string_t supdir;
	   rtk_string_t subdir;
};

struct stk_respone_info_t
{
	bool is_effect;
	int respone_time;
	stk_test_proxy_t* m_test_proxy;
	int size; //返回页面的大小
	int status; //响应报头状态码
	int error;
	int errorcode;
	bool is_tag_in;
	int connect_time;
	int send_time;
	int recv_time;
};

struct stk_proxy_param_t 
{
	stk_proxy_handle_t* m_proxy_handle;
	rtk_arrays_t* arrays_effect_proxy_common;
	rtk_hash_t* m_proxy_hash;
	rtk_mutex_t handle_mutex;
	rtk_mutex_t list_mutex;
	rtk_mutex_t hash_mutex;
	rtk_mutex_t array_mutex;
	rtk_mutex_t log_mutex;
};





__declspec(dllexport) void stk_proxy_get_test_lists_from_cache_file(rtk_lists_t* test_general_proxy_lists, char* supdir, char* subdir);


__declspec(dllexport) void stk_effect_proxy_to_string(stk_test_proxy_t* m_test_proxy, rtk_string_t* string);
__declspec(dllexport) void stk_proxy_list_append_from_string_no_dup(rtk_lists_t* lists_proxy, rtk_string_t* str_all_proxy);


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`

void stk_effect_proxy_print_test(rtk_arrays_t* arrays_intersection_proxy, const char* supdir, const char* subdir);

void stk_respone_info_init(stk_respone_info_t* m_respone_info);
void stk_respone_info_term(stk_respone_info_t* m_respone_info);
void stk_respone_info_reset(stk_respone_info_t* m_respone_info);

void stk_proxy_param_init(stk_proxy_param_t* proxy_param);
void stk_proxy_param_term(stk_proxy_param_t* proxy_param);

void stk_proxy_hash_init(rtk_hash_t* m_proxy_hash);
void stk_proxy_hash_term(rtk_hash_t* m_proxy_hash);
int stk_proxy_hash_find(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy);
int stk_proxy_hash_insert(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy);
stk_test_proxy_t* stk_proxy_hash_get(rtk_hash_t* m_test_proxy_hash, stk_test_proxy_t* m_test_proxy);

int stk_test_proxy_compare_respone_time(stk_test_proxy_t* p1, stk_test_proxy_t* p2);
void stk_effect_proxy_get_cfg_string(stk_test_proxy_t* m_test_proxy, rtk_string_t* string);
void stk_effect_proxy_single_list_proxy_get_common(rtk_lists_t* test_proxy_lists, rtk_lists_t* test_url_lists, \
									  rtk_arrays_t* arrays_effect_proxys_common, rtk_hash_t* m_proxy_hash, int n, bool is_prior);
bool stk_effect_proxy_test_single_proxy_is_common(stk_test_proxy_t* m_test_proxy, rtk_lists_t* test_url_lists, stk_timeout_t* timeout,stk_proxy_param_t* ctrl);
void stk_effect_proxy_update_prior_proxy(rtk_arrays_t* arrays_effect_proxy_common, rtk_lists_t* test_prior_proxy_lists);
bool stk_effect_proxy_test_respone_effect(stk_respone_info_t* m_respone_info, stk_test_url_t* m_test_url);

void stk_get_arrays_from_lists(rtk_lists_t* lists_proxy,rtk_arrays_t* arrays_effect_proxy_common);


void stk_effect_proxy_get(void* param);

void stk_proxy_list_to_hash_remove_dup(rtk_lists_t* test_proxy_lists, rtk_hash_t* m_proxy_hash);
void stk_proxy_list_remove_dup(rtk_lists_t* test_proxy_lists);
void stk_proxy_list_append_from_list_no_dup(rtk_lists_t* lists_proxy, rtk_lists_t* nlists_proxy);
void stk_effect_proxy_sort(stk_proxy_handle_t* m_cfg);
void stk_proxy_list_sort(rtk_lists_t* test_proxy_lists);
int compare_by_use_rate(stk_test_proxy_t* p1, stk_test_proxy_t* p2);
double stk_proxy_get_use_rate(stk_test_proxy_t* p1);
void rtk_lists_get_from_arrays(rtk_lists_t* test_proxy_lists, rtk_arrays_t* arrays);
bool double_safe_equal(double q1, double q2);
bool stk_effect_proxy_test_single_proxy(stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url);
void stk_effect_proxy_remove_spilth(rtk_arrays_t* arrays_effect_proxy, int n);


void stk_get_proxy_supdir(rtk_string_t* str_proxy_supdir);
void stk_proxy_set_lists(rtk_string_t* cache_proxys);

void stk_effect_proxy_get_single_url_single_proxy(stk_test_url_t* m_test_url, stk_test_proxy_t* m_test_proxy, \
												  stk_timeout_t* timeout, stk_respone_info_t* m_respone_info,stk_proxy_param_t* ctrl);
void stk_proxy_detail_log_set_by_respone(stk_respone_info_t* m_respone_info,stk_proxy_param_t* ctrl);



#endif