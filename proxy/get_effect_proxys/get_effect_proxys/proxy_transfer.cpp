#include "proxy_transfer.h"
#include "proxy_sender.h"
#include "stk_cfg.h"
#include "sender_cfg.h"
#include "stk_effect_proxy.h"

extern bool m_sender;
extern bool m_close;

proxy_sender_storage_t sender_storage;
rtk_threads_t m_threads;
sender_cfg_t sender_cfg;
rtk_mutex_t l_mutex;
rtk_mutex_t h_mutex;


void sender_recycl_ip(rtk_stream_t* recv_param)
{
	rtk_list_t iplist;
	rtk_string_t param;
	rtk_string_init(&param);
	rtk_list_init(&iplist,true);

	rtk_stream_swap_string(recv_param,&param);
	rtk_list_from_string(&iplist,"\r\n",&param);
	sender_remove_hash(&iplist);

	rtk_string_term(&param);
	rtk_list_remove_all(&iplist);
	rtk_list_term(&iplist);
}

void sender_transfer(rtk_socket_t* socket,int recv_code,rtk_stream_t* recv_param)
{
	int limitnum,codesend;
	rtk_stream_t send_param;
	rtk_stream_init(&send_param);
    codesend = URL_CMD_OK;

	limitnum = sender_check_recv_stream(recv_param);
	if (limitnum != -1 && recv_code == URL_CMD_OK)
	{
		while (m_sender)
			Sleep(5000);
        if (sender_set_param(&send_param,limitnum))
        {
			BYTE* send_data = (BYTE*)"NOTENOUHG";
			rtk_stream_copy(&send_param,send_data,9);
			codesend = URL_CMD_ERROR;
        }
		rtk_socket_send_command(socket,codesend,&send_param);
	}

	rtk_stream_term(&send_param);
}

void sender_recv_error(rtk_socket_t* socket,rtk_thread_t* thread,int rv,int& count)
{
	rtk_stream_t send_param;
	rtk_stream_init(&send_param);
	BYTE* send_data = (BYTE*)"RECV_ORROR";
	rtk_stream_copy(&send_param,send_data,7);

	if(rv != E_TIMEOUT)
	{
		count ++;
		if(count >= 10)
		{
			rtk_socket_term(socket);
			rtk_free(socket);
			socket = 0;

			thread->param = 0;
			rtk_threads_free(&m_threads,thread);
		}
		else
		{
			rtk_socket_send_command(socket,URL_CMD_ERROR,&send_param);
		}
	}

	rtk_timer_sleep(10);
    rtk_stream_term(&send_param);
}

void sender_recv_success(rtk_socket_t* socket,rtk_stream_t* recv_param,int& count,int recv_code)
{
	rtk_stream_t send_param;
	rtk_stream_init(&send_param);
	BYTE* send_data = (BYTE*)"RECV_OK";
	count = 0;
	
	rtk_stream_copy(&send_param,send_data,7);
	if(recv_code > 0)
	{
		if(recv_code == URL_CMD_RECYCL)
			sender_recycl_ip(recv_param);
		else
		    sender_transfer(socket,recv_code,recv_param);

		//rv = rtk_socket_send_command(socket,URL_CMD_OK,&send_param);
	}
	rtk_stream_term(&send_param);
}

void sender_socket_set(rtk_socket_t* socket)
{
	socket->connect_retries = sender_cfg.connect_retries;
	socket->connect_timeout = sender_cfg.connect_timeout;
	socket->send_timeout = sender_cfg.send_timeout;
	socket->recv_timeout = sender_cfg.recv_timeout;
}

void sender_lsock_set(rtk_socket_t* l_socket)
{
	l_socket->max_connect = sender_cfg.max_connect;
	l_socket->port = sender_cfg.port;
	l_socket->ipaddr = 0;
}



void sender_prosess_start(void* param)
{
	rtk_thread_t* thread;
	rtk_socket_t* socket;
	bool s_close = false;
	rtk_stream_t recv_param;
	int recv_code;
    int rv,count = 0;

	rtk_stream_init(&recv_param);
    

	thread = (rtk_thread_t*)param;
	socket = 0;

	recv_code = 0;

	while (!s_close)
	{
		if (!rtk_threads_is_use(&m_threads,thread))
		{
			rtk_timer_sleep(2000);
			continue;
		}
		if (thread->param != 0)
		{
			socket = (rtk_socket_t*)thread->param;
			sender_socket_set(socket);
			rv = rtk_socket_recv_command(socket,&recv_code,&recv_param);
			if (rv <= 0)
			{
				sender_recv_error(socket,thread,rv,count);
			}
			else
			{
				if (recv_code == URL_CMD_QUIT)
					s_close = true;
				else
				    sender_recv_success(socket,&recv_param,count,recv_code);
			}
		}
	}

	if(socket != 0)
	{
		rtk_socket_term(socket);
		rtk_free(socket);
	}

	rtk_stream_term(&recv_param);
	
}

void sender_listen(rtk_socket_t* l_socket)
{
	rtk_thread_t* thread;
	rtk_socket_t* socket;

	rtk_socket_open(l_socket);
	rtk_socket_listen(l_socket);
	while (!m_close)
	{
		socket = rtk_socket_accept(l_socket);
		if (socket != 0)
		{
			thread = rtk_threads_new(&m_threads);
			if (thread != 0)
			{
				thread->param = socket;
			}
			else
			{
				rtk_socket_term(socket);
				rtk_free(socket);
			}
		}
	}

	rtk_socket_close(l_socket);
}


void proxy_sender_start()
{
	rtk_hash_t m_proxy_hash;
    rtk_socket_t l_socket;

	proxy_sender_storage_init(&sender_storage);
    
	rtk_mutex_init(&l_mutex);
	rtk_mutex_init(&h_mutex);
	rtk_socket_init(&l_socket);
    stk_proxy_hash_init(&m_proxy_hash);
	sender_cfg_init(&sender_cfg);
	soc_cfg_get_profile(&sender_cfg);
	
    proxy_sender_get_from_db(&sender_storage);
	rtk_threads_init(&m_threads);
	rtk_threads_expand(&m_threads,sender_cfg.max_pthread);
	for (int i = 0; i < m_threads.count;i++)
	{
		rtk_thread_create(&m_threads.array[i],sender_prosess_start,0);
	}

	//m_client_count = sender_cfg.client_count;
	sender_lsock_set(&l_socket);
	
	sender_listen(&l_socket);

	printf("waiting for thread end ...\n");
	for(int i=0;i<m_threads.count;i++)
	{
		rtk_thread_join(&m_threads.array[i]);
		rtk_thread_close(&m_threads.array[i]);
	}

	rtk_mutex_term(&l_mutex);
	rtk_mutex_term(&h_mutex);
	stk_proxy_hash_term(&m_proxy_hash);
	rtk_socket_term(&l_socket);
	proxy_sender_storage_term(&sender_storage);
	rtk_threads_term(&m_threads);
}
