#include "stk_effect_proxy.h"
#include "proxy_ctrl.h"
#include "get_proxy_ip.h"
#include "time_fun.h"
#include "log.h"
#include "merge_proxy_file.h"
#include "tools.h"
#include "proxy_transfer.h"
//#include <dos.h>
//#include <windows.h>

void proxy_provide(void*);
void work();
void proxy_merge();
bool m_close;
bool m_sender;
int m_flag;
clock_t m_clock;
stk_proxy_js_t proxyjs;


int cmd_process(char *cmd)
{
	if(strcmp(cmd,"quit") == 0)
	{	
		m_close = true;
		return 1;
	}
	else if(strcmp(cmd,"start") == 0)
	{
		m_sender = true;
		m_flag = PSTART;
		work();
		return 1;
	}
	else if(strcmp(cmd,"check") == 0)
	{
		m_sender = true;
		m_flag = PCHECK;
		work();
		return 1;
	}
	else if (strcmp(cmd,"load") == 0)
	{
		m_sender = true;
		m_flag = PLOAD;
		work();
		return 1;
	}
	else if (strcmp(cmd,"down") == 0)
	{
		m_flag = PDOWN;
		work();
		return 1;
	}
	else if(strcmp(cmd,"merge") == 0)
	{
		proxy_merge();
		return 1;
	}
	else
	{
		return -1;
	}
}

int main()
{
	char command[256];
	int ret = 0;
    m_close = false;
	/*rtk_thread_t m_thread;                         //5.15 改 
	rtk_thread_init(&m_thread);
	rtk_socket_initialize();	
	
	rtk_thread_create(&m_thread,proxy_provide,0);*/
	
	while(!m_close)
	{
		m_sender = false;
		scanf("%s",command);
		ret = cmd_process(command);

		if(ret == 0)
		{
			break;
		}
		else if(ret < 0)
		{
			printf("please input correct command!\r\n");
			printf("start    ............  start\n");
			printf("check     ............  check\r\n");
			printf("load    .............   file into sql\r\n");
			printf("down    .............   down to profile\r\n");
			printf("merge    ............  merge\r\n");
			printf("quit     ............  quit\r\n");
			printf("\n");
		}
	}

	/*rtk_thread_close(&m_thread);
	rtk_thread_term(&m_thread);*/
	rtk_socket_finalize();
    return 0;
}

void proxy_provide(void*)
{
	proxy_sender_start();
}

void work()
{
    stk_proxy_get_persist();
}

void proxy_merge()
{
	char filea[50];
	char fileb[50];
	printf("请将需合并文件放在当前目录\n");
	printf("输入第一个文件名\n");
	scanf("%s",filea);
	printf("输入第二个文件名\n");
	scanf("%s",fileb);
	get_proxy_list_from_profile(filea,fileb);
	printf("合并完成\n");
}
