#ifndef __PROXY_CTRL_H
#define __PROXY_CTRL_H

#include "rtk_32.h"
#include "stk_effect_proxy.h"

#define debug

#define PSTART 0
#define PCHECK 1
#define PLOAD  2
#define PDOWN  3

typedef void (*stk_effect_proxy_print_t)(rtk_arrays_t* arrays_effect_proxy_common, char* supdir, const char* subdir);
__declspec(dllexport) void stk_proxy_get_persist();

__declspec(dllexport) void stk_available_proxy_output(stk_proxy_handle_t* m_cfg, stk_effect_proxy_print_t stk_effect_proxy_print, char* supdir, char* subdir);

void stk_available_proxy_arrays_get(stk_proxy_handle_t* m_proxy_handle, rtk_arrays_t* arrays_proxys);

void stk_effect_get_proxy_lists(stk_proxy_handle_t* m_proxy_handle, rtk_hash_t* m_proxy_hash, 
								rtk_arrays_t* arrays_effect_proxy_common);

#endif


