#ifndef __SENDER_CFG_H
#define __SENDER_CFG_H

typedef struct sender_cfg sender_cfg_t;

struct sender_cfg
{
	int port;
	int max_pthread;
	int max_connect;
	int connect_retries;
	int connect_timeout;
	int send_timeout;
	int recv_timeout;
	int client_count;
};

void sender_cfg_init(sender_cfg_t* cfg);

void soc_cfg_get_profile(sender_cfg_t *cfg);

#endif