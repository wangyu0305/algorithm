#include "proxy_db.h"
#include "log.h"
#include "stk_effect_proxy.h"
#include "proxy_ctrl.h"

#define MAX_BUFLEN 100
extern int m_flag;

void proxy_db_init(stk_db_t* m_db,db_cfg_t* m_dbcfg)
{
	stk_db_init(m_db);
	db_cfg_init(m_dbcfg);

	db_cfg_get_profile(m_dbcfg);
	stk_db_set_msq_info(m_db,m_dbcfg);
	stk_db_load_library(m_db);
}

void proxy_db_term(stk_db_t* m_db,db_cfg_t* m_dbcfg)
{
	db_cfg_term(m_dbcfg);
	stk_db_free_library(m_db);
	stk_db_term(m_db);
}

void proxy_db_close_records(stk_db_t* p_stk_db,rdb_process dbproc)
{
	p_stk_db->db_func.rdb_close_records(dbproc);
}

void proxy_db_logout(stk_db_t* p_stk_db,rdb_process dbproc)
{
	p_stk_db->db_func.rdb_logout(dbproc);
	printf("disconnect database!\n");
    proxy_dblog_tofile("disconnect database!");
}

rdb_process proxy_db_login(stk_db_t* p_stk_db,db_cfg_t* p_db_cfg)
{
	rdb_process dbproc;
	rtk_string_t errmsg;
	rtk_string_init(&errmsg);

	proxy_db_init(p_stk_db,p_db_cfg);
	printf("connecting database...\n");
	proxy_dblog_tofile("connecting database...");
	dbproc = p_stk_db->db_func.rdb_login(p_stk_db->db_server,p_stk_db->db_port,p_stk_db->db_username,p_stk_db->db_password);
	if(dbproc != 0 && p_stk_db->db_func.rdb_use_db_name(dbproc,p_stk_db->db_database))
	{
		printf("connected successfully!\n");
		proxy_dblog_tofile("connected successfully!");
		rtk_string_term(&errmsg);
		return dbproc;
	}
	else
	{
		rtk_string_copy(&errmsg,p_stk_db->db_func.rdb_get_last_error(dbproc),-1);
		if(rtk_string_is_empty(&errmsg))
		{
			rtk_string_copy(&errmsg,"can not connect to database\n",-1);
		}
		printf("%s\n",errmsg.buffer);
		proxy_dblog_tofile(errmsg.buffer);
		rtk_string_term(&errmsg);
		return NULL;
	}
}

void proxy_db_prepare(stk_db_t* p_stk_db,rtk_string_t* sqlstr,rdb_process dbproc)
{
	stk_db_prepare_sql(p_stk_db->db_charset,sqlstr);
	p_stk_db->db_func.rdb_prepare_sql(dbproc,sqlstr->buffer);
}

void proxy_db_commit(stk_db_t* p_stk_db,rdb_process dbproc)
{
	rtk_string_t errmsg;
	rtk_string_init(&errmsg);
	if(!p_stk_db->db_func.rdb_sql_excute(dbproc))
	{
		rtk_string_copy(&errmsg,p_stk_db->db_func.rdb_get_last_error(dbproc),-1);
		proxy_dblog_tofile(errmsg.buffer);
		printf("%s\n",errmsg.buffer);
	}

	p_stk_db->db_func.rdb_sql_commit(dbproc);
	rtk_string_term(&errmsg);
}

void proxy_set_sql(stk_test_proxy_t* m_test_proxy,db_cfg_t* p_db_cfg,rtk_string_t* sqlstr,int i)
{
	rtk_string_shrink(sqlstr,0);
	rtk_string_copy(sqlstr,"insert into ",-1);
	rtk_string_append(sqlstr,p_db_cfg->mstable.buffer,-1);
	rtk_string_append(sqlstr,"(",-1);
	if (!rtk_string_is_empty(&p_db_cfg->row1))
	{
		rtk_string_append(sqlstr,p_db_cfg->row1.buffer,-1);
		rtk_string_append_char(sqlstr,',');
	}
	if (!rtk_string_is_empty(&p_db_cfg->row2))
	{
		rtk_string_append(sqlstr,p_db_cfg->row2.buffer,-1);
		rtk_string_append_char(sqlstr,',');
	}
	if (!rtk_string_is_empty(&p_db_cfg->row3))
	{
		rtk_string_append(sqlstr,p_db_cfg->row3.buffer,-1);
		rtk_string_append_char(sqlstr,',');
	}
	if (!rtk_string_is_empty(&p_db_cfg->row4))
	{
		rtk_string_append(sqlstr,p_db_cfg->row4.buffer,-1);
		rtk_string_append_char(sqlstr,',');
	}
	if (!rtk_string_is_empty(&p_db_cfg->row5))
	{
		rtk_string_append(sqlstr,p_db_cfg->row5.buffer,-1);
		rtk_string_append_char(sqlstr,',');
	}
	if (!rtk_string_is_empty(&p_db_cfg->row6))
	{
		rtk_string_append(sqlstr,p_db_cfg->row6.buffer,-1);
	}
	rtk_string_append(sqlstr,") values ('",-1);
	if (!rtk_string_is_empty(&p_db_cfg->row1))
	{
		rtk_string_append_number(sqlstr,i);
        rtk_string_append(sqlstr,"','",-1);
	}
	if (!rtk_string_is_empty(&p_db_cfg->row2))
	{
		rtk_string_append(sqlstr,m_test_proxy->str_proxy_ip.buffer,-1);
		rtk_string_append(sqlstr,"','",-1);
	}
	if (!rtk_string_is_empty(&p_db_cfg->row3))
	{
		rtk_string_append_number(sqlstr,m_test_proxy->n_port);
		rtk_string_append(sqlstr,"','",-1);
	}
	if (!rtk_string_is_empty(&p_db_cfg->row4))
	{
		rtk_string_append_number(sqlstr,m_test_proxy->respone_time);
		rtk_string_append(sqlstr,"','",-1);
	}
	if (!rtk_string_is_empty(&p_db_cfg->row5))
	{
		rtk_string_append_number(sqlstr,(int)m_test_proxy->right);
		rtk_string_append(sqlstr,"','",-1);
	}
	if (!rtk_string_is_empty(&p_db_cfg->row1))
	{
		rtk_string_append_number(sqlstr,(int)m_test_proxy->wrong);
	}
	rtk_string_append(sqlstr,"');",-1);
}

void proxy_set_empty(rtk_string_t* sqlstr,db_cfg_t* p_db_cfg)
{
	rtk_string_shrink(sqlstr,0);
	rtk_string_copy(sqlstr,"TRUNCATE TABLE ",-1);
	rtk_string_append(sqlstr,p_db_cfg->mstable.buffer,-1);
}

void proxy_get_single_info(rtk_arrays_t* arrays_effect_proxy_common,stk_db_t* p_stk_db,db_cfg_t* p_db_cfg,rdb_process dbproc)
{
	rtk_string_t sqlstr;
	rtk_string_init(&sqlstr);
	stk_test_proxy_t* m_test_proxy;
   
	if (m_flag == PCHECK)
	{
		proxy_set_empty(&sqlstr,p_db_cfg);
		proxy_db_prepare(p_stk_db,&sqlstr,dbproc);
		proxy_db_commit(p_stk_db,dbproc);
	}

	for (int i = 0;i < arrays_effect_proxy_common->size;i++)
	{
		m_test_proxy = (stk_test_proxy_t*)rtk_arrays_get_at(arrays_effect_proxy_common,i);
		proxy_set_sql(m_test_proxy,p_db_cfg,&sqlstr,i);
		proxy_db_prepare(p_stk_db,&sqlstr,dbproc);
		proxy_db_commit(p_stk_db,dbproc);
	}

	rtk_string_term(&sqlstr);
}

void proxy_db_import(rtk_arrays_t* arrays_effect_proxy_common)
{
	stk_db_t p_stk_db;
	db_cfg_t p_db_cfg;
	rdb_process dbproc;

	dbproc = proxy_db_login(&p_stk_db,&p_db_cfg);
    if (dbproc != NULL)
    {
#ifdef debug
		printf("prepare import the data...\n");
		proxy_dblog_tofile("prepare import the data...");
#endif
		proxy_get_single_info(arrays_effect_proxy_common,&p_stk_db,&p_db_cfg,dbproc);
		proxy_db_logout(&p_stk_db,dbproc);
    }
	
	proxy_db_term(&p_stk_db,&p_db_cfg);
}

void proxy_get_string_to_lists(rtk_lists_t* lists_proxy,rtk_string_t* p_ip,rtk_string_t* p_port,rtk_string_t* p_right,rtk_string_t* p_wrong)
{
	stk_test_proxy_t* m_ntest_proxy = (stk_test_proxy_t*)rtk_lists_add_tail_new(lists_proxy, \
		(rtk_lists_init_t)stk_test_proxy_init);
	rtk_string_copy(&m_ntest_proxy->str_proxy_ip,p_ip->buffer,-1);
	m_ntest_proxy->n_port = (int)parse_number((unsigned char*)p_port->buffer);
	m_ntest_proxy->right = (double)parse_number((unsigned char*)p_right->buffer);
	m_ntest_proxy->wrong = (double)parse_number((unsigned char*)p_wrong->buffer);
}

void proxy_get_string_to_string(rtk_string_t* proxys_cache,rtk_string_t* p_ip,rtk_string_t* p_port,rtk_string_t* p_right,rtk_string_t* p_wrong)
{
	rtk_string_append(proxys_cache,p_ip->buffer,-1);
    rtk_string_append_char(proxys_cache,';');
    rtk_string_append(proxys_cache,p_port->buffer,-1);
	rtk_string_append_char(proxys_cache,';');
	rtk_string_append(proxys_cache,p_right->buffer,-1);
	rtk_string_append_char(proxys_cache,';');
	rtk_string_append(proxys_cache,p_wrong->buffer,-1);
	rtk_string_append(proxys_cache,"\r\n",-1);
}

void proxy_get_lists(rtk_lists_t* proxy_lists,stk_db_t* p_stk_db,rdb_process dbproc,db_cfg_t* p_db_cfg)
{
	rtk_string_t errmsg;
	rtk_string_t p_ip;
	rtk_string_t p_port;
	rtk_string_t p_right;
	rtk_string_t p_wrong;
	rtk_string_t proxys;
	rtk_string_init(&proxys);
	rtk_string_init(&errmsg);
	rtk_string_init(&p_ip);
	rtk_string_init(&p_port);
	rtk_string_init(&p_right);
	rtk_string_init(&p_wrong);
	rtk_string_expand(&p_ip,MAX_BUFLEN);
	rtk_string_expand(&p_port,MAX_BUFLEN);
	rtk_string_expand(&p_right,MAX_BUFLEN);
	rtk_string_expand(&p_wrong,MAX_BUFLEN);
	
	char** bufs;
	int size = 4;
	bufs = (char**)rtk_malloc(sizeof(char*)*size);
	bufs[0] = p_ip.buffer;
	bufs[1] = p_port.buffer;
	bufs[2] = p_right.buffer;
	bufs[3] = p_wrong.buffer;

	proxy_db_prepare(p_stk_db,&p_db_cfg->mysqlselect,dbproc);

	if(p_stk_db->db_func.rdb_open_records(dbproc))
	{
		p_stk_db->db_func.rdb_set_parameters(dbproc,bufs,MAX_BUFLEN,size);

		while(p_stk_db->db_func.rdb_fetch_next(dbproc) > 0)//能够获得一条数据，并复制给start
		{
			proxy_get_string_to_lists(proxy_lists,&p_ip,&p_port,&p_right,&p_wrong);
			proxy_get_string_to_string(&proxys,&p_ip,&p_port,&p_right,&p_wrong);
		}
	}
	else
	{
		rtk_string_copy(&errmsg,p_stk_db->db_func.rdb_get_last_error(dbproc),-1);
		proxy_dblog_tofile(errmsg.buffer);
		printf("%s\n",errmsg.buffer);
	}

	stk_proxy_set_lists(&proxys);
#ifdef debug
	printf("processing end!\n");
#endif
	proxy_db_close_records(p_stk_db,dbproc);
	rtk_free(bufs);
	rtk_string_term(&proxys);
	rtk_string_term(&errmsg);
	rtk_string_term(&p_ip);
	rtk_string_term(&p_port);
	rtk_string_term(&p_right);
	rtk_string_term(&p_wrong);
}

void proxy_db_export(rtk_lists_t* proxy_lists)
{
	stk_db_t p_stk_db;
	db_cfg_t p_db_cfg;
	rdb_process dbproc;

	dbproc = proxy_db_login(&p_stk_db,&p_db_cfg);
	if (dbproc != NULL)
	{
#ifdef debug
		printf("prepare process the data...\n");
		proxy_dblog_tofile("prepare process the data...");
#endif
		proxy_get_lists(proxy_lists,&p_stk_db,dbproc,&p_db_cfg);
		proxy_db_logout(&p_stk_db,dbproc);
	}
	
	proxy_db_term(&p_stk_db,&p_db_cfg);
	
}