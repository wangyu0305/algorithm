#include "get_proxy_ip.h"
#include <boost/regex.hpp>
#include <string>
#include "log.h"
#include "tools.h"

extern clock_t m_clock;
extern stk_proxy_js_t proxyjs;
boost::regex expression;
boost::smatch what;
void stk_test_proxy_get_from_sites(rtk_lists_t* proxy_site_lists, char* supdir, char* subdir)
{
    rtk_string_t str_all_proxy;
    rtk_string_t str_proxy;
    rtk_string_t fname;
    rtk_string_t str_log_info;

    rtk_string_init(&str_all_proxy);
    rtk_string_init(&str_proxy);
    rtk_string_init(&fname);
    rtk_string_init(&str_log_info);

    rtk_string_copy(&str_log_info, "从代理来源网页中下载待测试代理\t", -1);
    m_clock = clock();
    rtk_string_append_number(&str_log_info, m_clock);
    rtk_string_append(&str_log_info,"\r\n",-1);

    stk_proxy_detail_log_set(&str_log_info);

    stk_proxy_site_t* m_proxy_site;
    void* pos = rtk_lists_get_head_position(proxy_site_lists);
    while (pos)
    {
        m_proxy_site = (stk_proxy_site_t*)rtk_lists_get_next(proxy_site_lists, &pos);
        stk_proxy_get_from_single_site(m_proxy_site, &str_proxy);
        rtk_string_append(&str_all_proxy, str_proxy.buffer, str_proxy.length);
    }

    rtk_dir_merge(&fname, supdir, subdir);
    rtk_file_set_string(fname.buffer, 0, &str_all_proxy);

    rtk_string_term(&str_all_proxy);
    rtk_string_term(&str_proxy);
    rtk_string_term(&fname);
    rtk_string_term(&str_log_info);
}

void stk_proxy_get_from_single_site(stk_proxy_site_t* m_proxy_site, rtk_string_t* str_proxy)
{
    rtk_string_t content;
	rtk_string_t jscontent;
    rtk_string_t str_log_info;

    rtk_string_init(&jscontent);
    rtk_string_init(&content);
    rtk_string_init(&str_log_info);

    down_page_by_url(&m_proxy_site->str_url, &content);

	if (proxyjs.use_js != 0)
		stk_proxy_js_content(&content,&jscontent,&m_proxy_site->str_url);
	else
		rtk_string_copy(&jscontent,content.buffer,-1);
    rtk_string_copy(&str_log_info, "开始用正则解析出代理\t", -1);
    m_clock = clock();
    rtk_string_append_number(&str_log_info, m_clock);
    rtk_string_append_char(&str_log_info, '\t');
    rtk_string_append_number(&str_log_info, jscontent.length);
    rtk_string_append(&str_log_info, "\r\n",-1);
    stk_proxy_detail_log_set(&str_log_info);
	
    if (rtk_string_is_empty(&jscontent) != 1)
	       stk_get_test_proxy_boost_regex_from_str(&jscontent, &m_proxy_site->str_regex, str_proxy);

	
    rtk_string_copy(&str_log_info, "结束用正则解析出代理\t", -1);
    m_clock = clock();
    rtk_string_append_number(&str_log_info, m_clock);
    rtk_string_append(&str_log_info,"\r\n",-1);
    stk_proxy_detail_log_set(&str_log_info);

	rtk_string_term(&jscontent);
    rtk_string_term(&content);
    rtk_string_term(&str_log_info);
}


int down_page_by_url(rtk_string_t* in_url, rtk_string_t* out_content)
{
	stk_http_t		m_inet;
	stk_session_t	session;
    rtk_string_t source;
    rtk_string_t str_log_info;

	stk_session_init(&session);
	stk_http_init(&m_inet);
	m_inet.session = &session;
	rtk_string_init(&source);
    rtk_string_init(&str_log_info);

    //rtk_socket_initialize();
    rtk_hash_insert(&m_inet.allow_mime_types, "*/*", -1, 0); 

    rtk_string_copy(&str_log_info, "开始下载:\t", -1);
    rtk_string_append(&str_log_info, in_url->buffer, in_url->length);
    rtk_string_append_char(&str_log_info, '\t');
    m_clock = clock();
    rtk_string_append_number(&str_log_info, m_clock);
    rtk_string_append(&str_log_info, "\r\n",-1);
    
    stk_proxy_detail_log_set(&str_log_info);
	
	
    stk_http_get(&m_inet,in_url->buffer,&source);


    rtk_string_shrink(&str_log_info, 0);
    rtk_string_copy(&str_log_info, "结束下载:\t", -1);
    rtk_string_append(&str_log_info, in_url->buffer, in_url->length);
    rtk_string_append_char(&str_log_info, '\t');
    m_clock = clock();
    rtk_string_append_number(&str_log_info, m_clock);
    rtk_string_append(&str_log_info, "\r\n",-1);
    stk_proxy_detail_log_set(&str_log_info);

    //rtk_socket_finalize();
    rtk_string_copy(out_content, (char*)source.buffer, source.length);
    other_code_to_GB(out_content);

    rtk_string_term(&source);
	stk_session_term(&session);
	stk_http_term(&m_inet);
    rtk_string_term(&str_log_info);
    return 0;
}


void other_code_to_GB(rtk_string_t* str_content)
{
    rtk_string_t str_charset;

    rtk_string_init(&str_charset);
    stk_html_get_charset(str_content,&str_charset);

    if ( rtk_string_icompare(&str_charset, "utf-8", -1) == 0 )
    {
        ConvertToGB18030(str_charset.buffer,str_content);
	  	}

    rtk_string_term(&str_charset);
}


void stk_get_single_proxy_from_list(rtk_list_t* list, rtk_string_t* str_single_proxy)
{
    rtk_string_t string;
    int index = 0;
    rtk_string_init(&string);
    void* pos = rtk_list_get_head_position(list);

    rtk_string_shrink(str_single_proxy, 0);
    while (pos)
    {
	       rtk_list_get_next_string(list, &pos, &string);
		      switch ( index )
		      {
		      case 0:
		      case 1:
		      case 2:
			         rtk_string_append(str_single_proxy, string.buffer, string.length);
			         rtk_string_append_char(str_single_proxy, '.');
			         break;
		      case 3:
			         rtk_string_append(str_single_proxy, string.buffer, string.length);
					 rtk_string_append_char(str_single_proxy, ';');
			         break;
		      case 4:
            rtk_string_append(str_single_proxy, string.buffer, string.length);
			         break;
		      default:
			         break;
        }
		      index++;
	   }


    rtk_string_term(&string);
}

void stk_get_test_proxy_boost_regex_from_str(rtk_string_t* content, rtk_string_t* regex, rtk_string_t* str_proxy)
{
    if (rtk_string_is_empty(content) == 1)
        return;
	std::string reg(regex->buffer);
    expression = reg;

    //char* temp;

    std::string str = content->buffer;

    std::string strk;
    rtk_list_t list;
    rtk_string_t str_single_proxy;

    rtk_list_init(&list, true);
    rtk_string_init(&str_single_proxy);

    std::string::const_iterator it = str.begin();
    std::string::const_iterator end = str.end();
    rtk_string_shrink(str_proxy, 0);

    while ( boost::regex_search(it, end, what, expression) )
    {
        rtk_list_remove_all(&list);
		      for (int i = 1; i < what.size(); i++)
		      {
		          strk = what[i].str();
			         rtk_list_add_tail(&list, (char*)strk.c_str(), -1);
		     	}

		      stk_get_single_proxy_from_list(&list, &str_single_proxy);

		      rtk_string_append(&str_single_proxy, ";0;0", -1);
		      rtk_string_append(str_proxy, str_single_proxy.buffer, str_single_proxy.length);
		      rtk_string_append(str_proxy, "\r\n",-1);
		
		      it = what[0].second;

	   }
	
    rtk_list_term(&list);
    rtk_string_term(&str_single_proxy);
}