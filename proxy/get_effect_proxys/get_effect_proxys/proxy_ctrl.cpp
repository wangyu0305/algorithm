
#include "stk_effect_proxy.h"
#include "get_proxy_ip.h"
#include "log.h"
#include <time.h>
#include <string>
#include "tools.h"
#include "proxy_ctrl.h"
#include "proxy_db.h"
#include "time_fun.h"


extern stk_proxy_js_t proxyjs;
extern clock_t m_clock;
extern int m_flag;

void stk_proxy_get_persist()
{
	stk_proxy_handle_t m_proxy_handle;
	rtk_string_t str_current_dir;
	rtk_string_t str_time;
	rtk_string_t str_log_info;
	rtk_string_t fname;

	stk_proxy_js_init(&proxyjs);
	stk_proxy_compile_script(&proxyjs,"effect_proxy.cfg");
	stk_proxy_handle_init(&m_proxy_handle);
	rtk_string_init(&str_current_dir);
	rtk_string_init(&str_time);
	rtk_string_init(&str_log_info);
	rtk_string_init(&fname);

	rtk_dir_cwd(&str_current_dir);
	rtk_string_copy(&str_log_info, "\r\n\r\nSTART  ", -1);
	rtk_string_append_number(&str_log_info, m_clock/1000);
	rtk_string_append(&str_log_info, "\r\n",-1);
	stk_effect_proxy_log_out_string(str_current_dir.buffer,&str_log_info);

	//stk_proxy_handle_reset(&m_proxy_handle);
	stk_proxy_handle_get_profile(&m_proxy_handle, str_current_dir.buffer, "effect_proxy.cfg");
	stk_available_proxy_output(&m_proxy_handle, (stk_effect_proxy_print_t)stk_effect_proxy_print_test, \
		m_proxy_handle.common_dir.buffer, m_proxy_handle.common_file.buffer);
#ifdef debug
	printf("Done...\n");
#endif
	m_clock = clock();
	rtk_string_shrink(&str_log_info,0);
	rtk_string_copy(&str_log_info,"END\r\n ",-1);
	rtk_string_append_number(&str_log_info, m_clock/1000);
	stk_effect_proxy_log_out_string(str_current_dir.buffer,&str_log_info);
	if (proxyjs.use_js != 0)
		stk_proxy_js_engine_destory(&proxyjs);
	stk_proxy_js_term(&proxyjs);
	rtk_string_term(&str_time);
	stk_proxy_handle_term(&m_proxy_handle);
	rtk_string_term(&str_current_dir);
	rtk_string_term(&str_log_info);
	rtk_string_term(&fname);
}


//获取共用代理接口函数
void stk_available_proxy_output(stk_proxy_handle_t* m_cfg, stk_effect_proxy_print_t stk_effect_proxy_print, char* supdir, char* subdir)
{
	rtk_arrays_t arrays_effect_proxy_common;
	rtk_string_t str_current_dir;
	rtk_string_t str_proxy_supdir;
	stk_test_proxy_lists_t m_test_proxy_lists;
	rtk_string_t str_log;

	rtk_arrays_init(&arrays_effect_proxy_common, true);
	arrays_effect_proxy_common.dpool->memsize = sizeof(void*);
	rtk_string_init(&str_current_dir);
	rtk_string_init(&str_proxy_supdir);
	stk_test_proxy_lists_init(&m_test_proxy_lists);
	rtk_string_init(&str_log);

	rtk_dir_cwd(&str_current_dir);
	rtk_dir_merge(&str_proxy_supdir, (char*)str_current_dir.buffer, "proxy_temps");
	rtk_dir_make((char*)str_current_dir.buffer, "proxy_temps");

	//stk_test_proxy_lists_get_profile(&m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");
	m_cfg->m_test_proxy_lists = &m_test_proxy_lists;

	stk_available_proxy_arrays_get(m_cfg, &arrays_effect_proxy_common);

	int number_effect_proxy = arrays_effect_proxy_common.size;
	//stk_effect_proxy_update_prior_proxy(&arrays_effect_proxy_common, &m_cfg->m_test_proxy_lists->test_prior_proxy_lists);
	if (m_flag == PSTART || m_flag == PCHECK)
	{
		stk_effect_proxy_remove_spilth(&arrays_effect_proxy_common, m_cfg->n_common);
		stk_effect_proxy_print(&arrays_effect_proxy_common, supdir, subdir);
	}
	//stk_test_proxy_lists_set_profile(m_cfg->m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");

	stk_effect_proxy_log_single_res(m_flag, number_effect_proxy, str_current_dir.buffer);

	rtk_arrays_free_all(&arrays_effect_proxy_common,(rtk_arrays_term_t)stk_test_proxy_term);
	rtk_arrays_term(&arrays_effect_proxy_common);
	rtk_string_term(&str_current_dir);
	rtk_string_term(&str_proxy_supdir);
	stk_test_proxy_lists_term(&m_test_proxy_lists);
	rtk_string_init(&str_log);
}

void stk_available_proxy_arrays_get(stk_proxy_handle_t* m_proxy_handle, rtk_arrays_t* arrays_proxys)
{
    rtk_hash_t m_proxy_hash;
    rtk_string_t str_proxy_supdir;
    rtk_string_t str_current_dir;

    stk_proxy_hash_init(&m_proxy_hash);
    rtk_string_init(&str_proxy_supdir);
    rtk_string_init(&str_current_dir);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&str_proxy_supdir, (char*)str_current_dir.buffer, "proxy_temps");
	//check
    if (m_flag == PCHECK)
    {
		proxy_db_export(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists);
#ifdef debug
		printf("checking...\n");
#endif
        stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, arrays_proxys);
		proxy_db_import(arrays_proxys);
   	}
    //从文件导入到数据库
	if (m_flag == PLOAD)
	{
		stk_proxy_get_test_lists_from_cache_file(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists, \
			     str_proxy_supdir.buffer, "common.lst");
		if (m_proxy_handle->is_check == 1)
		{
#ifdef debug
			printf("checking...\n");
#endif
			stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, arrays_proxys);
		}
		else
			stk_get_arrays_from_lists(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists,arrays_proxys);
		proxy_db_import(arrays_proxys);
	}

//	重代理网站上下载待测试代理
    if (m_flag == PSTART)
    {
#ifdef debug
		printf("download... Webpage...\n");
#endif
		
        stk_test_proxy_get_from_sites(&m_proxy_handle->proxy_site_lists, str_proxy_supdir.buffer, "down_cache.lst");
        stk_proxy_get_test_lists_from_cache_file(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists, \
        str_proxy_supdir.buffer, "down_cache.lst");
#ifdef debug
		printf("checking...\n");
#endif
        stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, arrays_proxys);
		proxy_db_import(arrays_proxys);
    }

	//从数据库导出到文件
	if (m_flag == PDOWN)
	{
		proxy_db_export(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists);
	}
    
    stk_proxy_hash_term(&m_proxy_hash);
    rtk_string_term(&str_proxy_supdir);
    rtk_string_term(&str_current_dir);
}

void stk_effect_get_proxy_lists(stk_proxy_handle_t* m_proxy_handle, rtk_hash_t* m_proxy_hash, 
							   rtk_arrays_t* arrays_effect_proxy_common)
{
	int num;
	stk_proxy_param_t ctrl;
	stk_proxy_param_init(&ctrl);
    rtk_threads_t threads_pool;
	rtk_threads_init(&threads_pool);

	ctrl.m_proxy_handle = m_proxy_handle;
	ctrl.m_proxy_hash = m_proxy_hash;
	ctrl.arrays_effect_proxy_common = arrays_effect_proxy_common;
	num = m_proxy_handle->threadnum;
	rtk_threads_expand(&threads_pool,num);
	for (int i = 0;i < threads_pool.count;i++)
	{
		rtk_thread_create(&threads_pool.array[i],stk_effect_proxy_get,&ctrl);
	}
	for (int i = 0;i < threads_pool.count;i++)
	{
		rtk_thread_join(&threads_pool.array[i]);
		rtk_thread_close(&threads_pool.array[i]);
	}
	rtk_threads_term(&threads_pool);
	stk_proxy_param_term(&ctrl);
}