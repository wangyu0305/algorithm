#ifndef __PROXY_DB_H
#define __PROXY_DB_H

#include "db_cfg.h"
#include "rtk_32.h"

void proxy_db_export(rtk_lists_t* proxy_lists);
void proxy_db_import(rtk_arrays_t* arrays_effect_proxy_common);

#endif