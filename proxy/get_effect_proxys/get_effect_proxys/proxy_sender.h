#ifndef __PROXY_SENDER_H
#define __PROXY_SENDER_H

#include "rtk_32.h"

typedef struct proxy_sender_storage proxy_sender_storage_t;


struct proxy_sender_storage 
{
	rtk_hash_t sender_used_hash;
	rtk_arrays_t sender_save_arrays;
	rtk_lists_t sender_db_lists;
	rtk_lists_t sender_save_lists;
};


void proxy_sender_storage_init(proxy_sender_storage_t* sender_storage);
void proxy_sender_storage_term(proxy_sender_storage_t* sender_storage);

void proxy_sender_get_from_db(proxy_sender_storage_t* sender_storage);
bool sender_set_param(rtk_stream_t* send_param,int limitnum);
int sender_check_recv_stream(rtk_stream_t* recv_param);
void sender_remove_hash(rtk_list_t* iplist);


#endif