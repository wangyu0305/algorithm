#ifndef MEGER_PROXY_FILE
#define MEGER_PROXY_FILE

#include "rtk_32.h"

typedef struct proxy_string proxy_string_t;

struct proxy_string 
{
	rtk_string_t ipinfo;
};

__declspec(dllexport) void get_proxy_list_from_profile(char* profilea,char* profileb);

#endif
