#ifndef __PROXY_TRANSFER_H
#define __PROXY_TRANSFER_H


#include "rtk_32.h"

#define E_TIMEOUT -1002

enum CMD_CODE
{
	URL_CMD_ERROR,
	URL_CMD_OK,
	URL_CMD_START,
	URL_CMD_RECYCL,
	URL_CMD_QUIT,
};

void proxy_sender_start();

#endif