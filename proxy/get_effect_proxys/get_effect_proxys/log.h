#ifndef _LOG__H_
#define _LOG__H_

#include <rtk_32.h>
#include "stk_cfg.h"

//�ⲿ�ӿ�
__declspec(dllexport) void stk_effect_proxy_log_out_starttime(int index, const char* supdir, rtk_string_t* str_time);
__declspec(dllexport) void stk_effect_proxy_log_out_endtime(int index, const char* supdir, rtk_string_t* str_time);
__declspec(dllexport) void stk_effect_proxy_log_out_string(const char* supdir, rtk_string_t* str_log_info);
__declspec(dllexport) void stk_effect_proxy_log_single_res(int flag, int number_effect_proxy, char* supdir);
__declspec(dllexport) void stk_effect_proxy_log_single_url_start(const char* supdir, rtk_string_t* str_url);
__declspec(dllexport) void stk_effect_proxy_log_single_url_end(const char* supdir, rtk_string_t* str_url);
__declspec(dllexport) void stk_effect_proxy_log_get_down_cache(const char* supdir);
__declspec(dllexport) void stk_effect_proxy_log_get_down_sites(const char* supdir);


void stk_proxy_detail_log_set(rtk_string_t* str_log_info);
void stk_proxy_detail_log_set(char* sz_log_info);

void stk_proxy_log_test_proxy_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, int count);

void stk_proxy_log_down_begin_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url);
void stk_proxy_log_down_end_get(rtk_string_t* str_log_info, stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url);
void proxy_dblog_tofile(char* rec);
#endif