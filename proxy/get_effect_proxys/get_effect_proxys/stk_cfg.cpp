#include "stk_cfg.h"
#include "stk_effect_proxy.h"


void stk_test_url_init(stk_test_url_t* m_test_url)
{
    rtk_string_init(&m_test_url->str_url);
    m_test_url->n_max_out_number = -1;
    m_test_url->ui_min_size = -1;
    rtk_string_init(&m_test_url->str_tag);
}

void stk_test_url_term(stk_test_url_t* m_test_url)
{
    rtk_string_term(&m_test_url->str_url);
    rtk_string_term(&m_test_url->str_tag);
}

void stk_subsection_init(stk_subsection_t* m_subsection)
{
    rtk_string_init(&m_subsection->str_proxy_ip);
    m_subsection->bit_count = -1;
}
void stk_subsection_term(stk_subsection_t* m_subsection)
{
    rtk_string_term(&m_subsection->str_proxy_ip);
}

void stk_proxy_handle_init(stk_proxy_handle_t* m_proxy_handl)
{
    m_proxy_handl->n_common = -1;
    m_proxy_handl->update_time = -1;
    m_proxy_handl->flag = -1;

    rtk_string_init(&m_proxy_handl->common_dir);
    rtk_string_init(&m_proxy_handl->common_file);

    rtk_lists_init(&m_proxy_handl->test_url_lists, true, true);
    m_proxy_handl->test_url_lists.dpool->memsize = sizeof(stk_test_url_t);
    rtk_lists_init(&m_proxy_handl->proxy_site_lists, true, true);
    m_proxy_handl->proxy_site_lists.dpool->memsize = sizeof(stk_proxy_site_t);
    m_proxy_handl->timeout.recv_timeout = 0;
    m_proxy_handl->timeout.connect_time = 0;
    m_proxy_handl->timeout.send_timeout = 0;
}


void stk_proxy_handle_term(stk_proxy_handle_t* m_effect_proxy_cfg)
{
    rtk_string_term(&m_effect_proxy_cfg->common_dir);
    rtk_string_term(&m_effect_proxy_cfg->common_file);

    rtk_lists_free_all(&m_effect_proxy_cfg->test_url_lists, (rtk_lists_term_t)stk_test_url_term);
    rtk_lists_term(&m_effect_proxy_cfg->test_url_lists);

    rtk_lists_free_all(&m_effect_proxy_cfg->proxy_site_lists, (rtk_lists_term_t)stk_proxy_site_term);
    rtk_lists_term(&m_effect_proxy_cfg->proxy_site_lists);
}



void stk_test_proxy_lists_init(stk_test_proxy_lists_t* m_test_proxy_lists)
{
    rtk_lists_init(&m_test_proxy_lists->test_prior_proxy_lists, true, false);
    rtk_lists_init(&m_test_proxy_lists->test_general_proxy_lists, true, true);
    m_test_proxy_lists->test_general_proxy_lists.dpool->memsize = sizeof(stk_test_proxy_t);
}

void stk_test_proxy_lists_term(stk_test_proxy_lists_t* m_test_proxy_lists)
{
    rtk_lists_term(&m_test_proxy_lists->test_prior_proxy_lists);
    rtk_lists_free_all(&m_test_proxy_lists->test_general_proxy_lists, (rtk_lists_term_t)stk_test_proxy_term);
    rtk_lists_term(&m_test_proxy_lists->test_general_proxy_lists);
}

//void stk_test_proxy_lists_get_profile(stk_test_proxy_lists_t* m_test_proxy_lists, const char* supdir, const char* subdir)
//{
//    rtk_string_t context;
//    rtk_string_t content;
//    rtk_string_t fname;
//    rtk_lists_t prior_lists;
//    rtk_hash_t m_test_proxy_hash;
//
//    rtk_string_init(&context);
//    rtk_string_init(&content);
//    rtk_string_init(&fname);
//    rtk_lists_init(&prior_lists, true, true);
//    prior_lists.dpool->memsize = sizeof(stk_test_proxy_t);
//    rtk_hash_init(&m_test_proxy_hash, true, false);
//
//    rtk_dir_merge(&fname, (char*)supdir, (char*)subdir);
//
//    rtk_file_get_string(fname.buffer, &context);
//
//    stk_cfg_get_list("general",&context,&content);
//    if(!rtk_string_is_empty(&content))
//    {
//        stk_proxy_list_from_string(&m_test_proxy_lists->test_general_proxy_lists,&content);
//        stk_proxy_list_to_hash_remove_dup(&m_test_proxy_lists->test_general_proxy_lists, &m_test_proxy_hash);
//    }
//
//
//    stk_cfg_get_list("prior", &context, &content);
//    if (!rtk_string_is_empty(&content))
//    {
//        stk_proxy_list_from_string(&prior_lists, &content);
//        stk_proxy_list_append_from_list_no_dup(&m_test_proxy_lists->test_general_proxy_lists, \
//			                &m_test_proxy_lists->test_prior_proxy_lists);
//    }
//
//    stk_proxy_prior_from_general(&prior_lists, &m_test_proxy_hash, &m_test_proxy_lists->test_prior_proxy_lists);
//
//    rtk_string_term(&context);
//    rtk_string_term(&content);
//    rtk_string_term(&fname);
//    rtk_lists_free_all(&prior_lists, (rtk_lists_term_t)stk_test_proxy_term);
//    rtk_lists_term(&prior_lists);
//    rtk_hash_term(&m_test_proxy_hash);
//}

void stk_proxy_prior_from_general(rtk_lists_t* source_lists, rtk_hash_t* m_test_proxy_hash, rtk_lists_t* des_lists)
{
    void* pos = rtk_lists_get_head_position(source_lists);
    while (pos)
    {
	       stk_test_proxy_t* m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(source_lists, &pos);
        stk_test_proxy_t* m_test_proxy_in_hash = (stk_test_proxy_t*)stk_proxy_hash_get(m_test_proxy_hash, m_test_proxy);
        if (m_test_proxy_in_hash)
            rtk_lists_add_tail(des_lists, m_test_proxy_in_hash);
        else
            printf("优先待测试的代理在普通待测试代理中没找到\n");
    }
}


//void stk_test_proxy_lists_set_profile(stk_test_proxy_lists_t* m_test_proxy_lists, const char* supdir, const char* subdir)
//{
//    rtk_string_t context;
//    rtk_string_t content;
//    rtk_string_t fname;
//    rtk_string_init(&context);
//    rtk_string_init(&content);
//    rtk_string_init(&fname);
//	
//    rtk_dir_merge(&fname, (char*)supdir, (char*)subdir);
//    stk_test_proxy_list_to_string(&m_test_proxy_lists->test_prior_proxy_lists,&content);
//    rtk_string_append(&context, "\r\n", -1);
//    stk_cfg_set_list("prior",&context,&content);
//    stk_test_proxy_list_to_string(&m_test_proxy_lists->test_general_proxy_lists,&content);
//    rtk_string_append(&context, "\r\n", -1);
//    stk_cfg_set_list("general",&context,&content);
//    rtk_file_set_string(fname.buffer, 0, &context);
//
//    rtk_string_term(&context);
//    rtk_string_term(&content);
//    rtk_string_term(&fname);
//}



int stk_cfg_get_next_val(int *pos,rtk_string_t *content,rtk_string_t *val)
{
    char* p_char;
    int index;
    int begin;
    int end;
    int b_quat1;
    int b_quat2;
    int b_char;

    begin = -1;
    end = -1;
    b_quat1 = false;
    b_quat2 = false;
    b_char = false;

    rtk_string_shrink(val,0);

    if(*pos < 0 || *pos >= content->length)
    {
        return false;
    }

    p_char = content->buffer;
    p_char += *pos;
    index = *pos;
    while(*p_char && index < content->length)
    {
        switch(*p_char)
        {
        case ';':
            if(b_char || (b_quat1 && b_quat2) || (!b_char && !b_quat1))
            {
                if(!b_quat2)
                {
                    end = index;
                }
                if(end >= 0 && begin >= 0 && end > begin)
                {
                    rtk_string_mid_string(content,begin,end-begin,val);
                }

                index ++;
                *pos = index;

                return true;
            }
            break;
        case '\"':
            if(b_quat1)
            {
                b_quat2 = true;
                end = index;
            }
            if(!b_char)
            {
                b_quat1 = true;
            }
            break;
        case ' ':
        case '\t':
        case '\r':
        case '\n':
            break;
        default:
            if(!b_quat1)
            {
                b_char = true;
            }
            b_quat2 = false;
            if(begin == -1)
            {
                begin = index;
            }
            break;
        }
        p_char ++;
        index ++;
    }

    if(end == -1)
    {
        end = index;
    }
    if(end >= 0 && begin >= 0 && end > begin)
    {
        rtk_string_mid_string(content,begin,end-begin,val);
    }

    *pos = content->length;

    return true;
}

int stk_cfg_get_next_tag(int npos,rtk_string_t* context,rtk_string_t* strtag,int* rtag,int* nbegin,int* nend)
{
    char* pchar;
    int ncount;
    int index;
    int nchar;
    int nbar;
    int bfind;
    int bquat;
    int squat;
    int dquat;

    *rtag = false;
    rtk_string_shrink(strtag,0);
    ncount = context->length;
    nchar = -1;
    nbar = -1;
    *nbegin = -1;
    *nend = -1;
    bfind = false;
    bquat = false;
    squat = false;
    dquat = false;

    if(npos < 0 || npos >= ncount)
    {
        return false;
    }

    pchar = context->buffer;
    pchar += npos;
    index = npos;
    while(*pchar && index < ncount)
    {
        switch(*pchar)
        {
        case '<':
            if(!bquat)
            {
                *nbegin = index;
                *rtag = false;
                nchar = -1;
                nbar = -1;
            }
            else if(*nbegin == -1)
            {
                *nbegin = index;
                *rtag = false;
                nchar = -1;
                nbar = -1;
                bquat = false;
                squat = false;
                dquat = false;
            }
            break;
        case '>':
            *nend = index;
            if(*nbegin >= 0 && *nend >= 0 && *nend > *nbegin)
            {
                if(nchar >= 0)
                {
                    if(nbar >= 0)
                    {
                        rtk_string_mid_string(context,nchar,nbar-nchar,strtag);
                    }
                    else
                    {
                        rtk_string_mid_string(context,nchar,(*nend)-nchar,strtag);
                    }
                }
                bfind = true;
            }
            break;
        case '\'':
            if(!dquat)
            {
                squat = !squat;
                bquat = squat;
            }
            break;
        case '\"':
            if(!squat)
            {
                dquat = !dquat;
                bquat = dquat;
            }
            break;
        case ' ':
        case '\t':
        case '\r':
        case '\n':
            if(*nbegin >= 0 && nchar >= 0 && nbar == -1)
            {
                nbar = index;
            }
            break;
        case '/':
            if(!bquat)
            {
                if(*nbegin >= 0)
                {
                    if(nchar == -1)
                    {
                        *rtag = true;
                    }
                    else if(nbar == -1)
                    {
                        nbar = index;
                    }
                }
            }
            break;
        default:
            if(!bquat)
            {
                if(*nbegin >= 0 && nchar == -1)
                {
                    nchar = index;
                }
            }
            break;
        }
        if(bfind)
        {
            return true;
        }
        pchar ++;
        index ++;
    }

    return false;
}

int stk_cfg_get_next_key(int* npos,rtk_string_t* content,rtk_string_t* strkey,rtk_string_t* strvalue)
{
    char* pchar;
    int ncount;
    int index;
    int nequal;
    int bquat;
    int squat;
    int dquat;

    rtk_string_shrink(strkey,0);
    rtk_string_shrink(strvalue,0);

    ncount = content->length;
    nequal = -1;
    bquat = false;
    squat = false;
    dquat = false;

    if(*npos < 0 || *npos >= ncount)
    {
        return false;
    }

    pchar = content->buffer;
    pchar += *npos;
    index = *npos;
    while(*pchar && index < ncount)
    {
        switch(*pchar)
        {
        case '=':
            if(!bquat && nequal < 0)
            {
                nequal = index;
            }
            break;
        case ' ':
        case ';':
        case '\t':
        case '\r':
        case '\n':
            if(!bquat && nequal >= 0)
            {
                rtk_string_mid_string(content,*npos,nequal-(*npos),strkey);
                rtk_string_mid_string(content,nequal+1,index-nequal-1,strvalue);

                index ++;
                *npos = index;

                return true;
            }
            break;
        case '\'':
            if(!dquat)
            {
                squat = !squat;
                bquat = squat;
            }
            break;
        case '\"':
            dquat = !dquat;
            bquat = dquat;
            squat = false;
            break;
        default:
            break;
        }

        pchar ++;
        index ++;
    }

    if(nequal >= 0)
    {
        rtk_string_mid_string(content,*npos,nequal-(*npos),strkey);
        rtk_string_mids_string(content,nequal+1,strvalue);
    }

    *npos = ncount;

    return true;
}

int stk_cfg_find_next_tag(int npos,rtk_string_t* context,char* strtag,int bTag,int* nbegin,int* nend)
{
    rtk_string_t stag;
    int rtag;
    int nbase;

    rtk_string_init(&stag);

    *nbegin = -1;
    *nend = -1;

    nbase = npos;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,nbegin,nend))
    {
        npos = (*nend) + 1;
        if(rtag == bTag)
        {
            rtk_string_make_lower(&stag);
            if(rtk_string_compare(&stag,(char*)strtag,-1) == 0)
            {
                rtk_string_term(&stag);

                return true;
            }
        }
    }

    rtk_string_term(&stag);

    return false;
}

int stk_cfg_get_end_pos(rtk_string_t* strtext)
{
    char* pchar;
    int ncount;
    int index;

    ncount = strtext->length;
    index = 0;
    pchar = strtext->buffer;
    while(*pchar && index < ncount)
    {
        switch(*pchar)
        {
        case ';':
        case '\t':
        case '\r':
        case '\n':
        case ' ':
            return index;
        default:
            break;
        }
        index ++;
        pchar ++;
    }

    return ncount;
}

int stk_cfg_get_key_value_name(char* name,rtk_string_t* context,rtk_string_t* value)
{
    rtk_string_t strkey;
    rtk_string_t strvalue;
    int bfind;
    int npos,npos1;

    rtk_string_init(&strkey);
    rtk_string_init(&strvalue);

    bfind = false;
    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_key(&npos,context,&strkey,&strvalue))        
    {
        rtk_string_trim(&strkey);
        if(!rtk_string_is_empty(&strkey))
        {
            rtk_string_trim(&strvalue);
            if(rtk_string_get_at(&strvalue,0) == '\"')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\"');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }
            else if(rtk_string_get_at(&strvalue,0) == '\'')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\'');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }

            if(rtk_string_icompare(&strkey,"name",-1) == 0)
            {
                rtk_string_trim(&strvalue);

                if(rtk_string_icompare(&strvalue,(char*)name,-1) == 0)
                {
                    bfind = true;
                }
            }
            else if(bfind)
            {
                if(rtk_string_icompare(&strkey,"value",-1) == 0)
                {
                    rtk_string_trim(&strvalue);
                    rtk_string_copy(value,strvalue.buffer,strvalue.length);

                    rtk_string_term(&strkey);
                    rtk_string_term(&strvalue);

                    return true;
                }
            }
        }
    }

    rtk_string_term(&strkey);
    rtk_string_term(&strvalue);

    return false;
}

int stk_cfg_get_key_value(rtk_string_t* context,rtk_string_t* name,rtk_string_t* value)
{
    rtk_string_t strkey;
    rtk_string_t strvalue;
    int bfind;
    int npos,npos1;

    rtk_string_init(&strkey);
    rtk_string_init(&strvalue);

    bfind = false;
    rtk_string_shrink(name,0);
    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_key(&npos,context,&strkey,&strvalue))
    {
        rtk_string_trim(&strkey);
        if(!rtk_string_is_empty(&strkey))
        {
            rtk_string_trim(&strvalue);
            if(rtk_string_get_at(&strvalue,0) == '\"')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\"');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }
            else if(rtk_string_get_at(&strvalue,0) == '\'')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\'');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }

            if(rtk_string_icompare(&strkey,"name",-1) == 0)
            {
                rtk_string_trim(&strvalue);
                rtk_string_copy(name,strvalue.buffer,strvalue.length);
                bfind = true;
            }
            else if(bfind)
            {
                if(rtk_string_icompare(&strkey,"value",-1) == 0)
                {
                    rtk_string_trim(&strvalue);
                    rtk_string_copy(value,strvalue.buffer,strvalue.length);

                    rtk_string_term(&strkey);
                    rtk_string_term(&strvalue);

                    return true;
                }
            }
        }
    }

    rtk_string_term(&strkey);
    rtk_string_term(&strvalue);

    return false;
}

int stk_cfg_get_value(char* strtag,rtk_string_t* context,rtk_string_t* value)
{
    int npos,nbegin,nend;

    rtk_string_shrink(value,0);

    npos = 0;
    if(stk_cfg_find_next_tag(npos,context,strtag,false,&nbegin,&nend))
    {
        npos = nend + 1;
        if(stk_cfg_find_next_tag(npos,context,strtag,true,&nbegin,&nend))
        {
            rtk_string_mid_string(context,npos,nbegin-npos,value);
            rtk_string_trim(value);
        }

        return true;
    }

    return false;
}

int stk_cfg_get_value_name(char* strtag,char* name,rtk_string_t* context,rtk_string_t* value)
{
    int npos,nbegin,nend;
    rtk_string_t stag;
    rtk_string_t strtext;
    int rtag;

    rtk_string_init(&stag);
    rtk_string_init(&strtext);

    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,&nbegin,&nend))
    {
        if(!rtag)
        {
            if(rtk_string_icompare(&stag,(char*)strtag,-1) == 0)
            {
                rtk_string_mid_string(context,nbegin+1,nend-nbegin-1,&strtext);
                rtk_string_mids(&strtext,stag.length);
                rtk_string_trim(&strtext);
                if(stk_cfg_get_key_value_name(name,&strtext,value))
                {
                    rtk_string_term(&stag);
                    rtk_string_term(&strtext);

                    return true;
                }
            }
        }
        npos = nend + 1;
    }

    rtk_string_term(&stag);
    rtk_string_term(&strtext);

    return false;
}

int stk_cfg_get_number(char* name,rtk_string_t* context,int ndefault)
{
    rtk_string_t strvalue;
    int nvalue;

    rtk_string_init(&strvalue);

    if(stk_cfg_get_value_name("number",name,context,&strvalue))
    {
        nvalue = rtk_string_get_number(&strvalue,ndefault);
    }
    else
    {
        nvalue = ndefault;
    }

    rtk_string_term(&strvalue);

    return nvalue;
}

void stk_cfg_get_text(char* name,rtk_string_t* context,rtk_string_t* strvalue, char* strdefault)
{
    if(!stk_cfg_get_value_name("text",name,context,strvalue))
    {
        rtk_string_copy(strvalue,(char*)strdefault,-1);
    }
}

int stk_cfg_get_list(char *name,rtk_string_t *context,rtk_string_t *content)
{
    int npos,nbegin,nend;
    rtk_string_t stag;
    rtk_string_t strtext;
    rtk_string_t strvalue;
    int rtag;

    rtk_string_init(&stag);
    rtk_string_init(&strtext);
    rtk_string_init(&strvalue);

    rtk_string_shrink(content,0);

    npos = 0;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,&nbegin,&nend))
    {
        npos = nend + 1;
        if(!rtag)
        {
            if(rtk_string_icompare(&stag,"list",-1) == 0)
            {
                rtk_string_mid_string(context,nbegin+1,nend-nbegin-1,&strtext);
                rtk_string_mids(&strtext,stag.length);
                rtk_string_trim(&strtext);

                if (stk_cfg_get_key_value(&strtext,&stag,&strvalue))
                {
                    if(rtk_string_icompare(&stag,name,-1) == 0)
                    {
                        if(stk_cfg_find_next_tag(npos,context,"list",true,&nbegin,&nend))
                        {
                            rtk_string_mid_string(context,npos,nbegin-npos,content);
                            rtk_string_trim(content);

                            rtk_string_term(&stag);
                            rtk_string_term(&strtext);
                            rtk_string_term(&strvalue);
                            return true;
                        }
                    }
                }
            }
        }
    }

    rtk_string_term(&stag);
    rtk_string_term(&strtext);
    rtk_string_term(&strvalue);

    return false;
}

void stk_cfg_set_number(char* name,rtk_string_t* context,int nvalue)
{
    rtk_string_append(context,"<number name=\"",-1);
    rtk_string_append(context,(char*)name,-1);
    rtk_string_append(context,"\" value=\"",-1);
    rtk_string_append_number(context,nvalue);
    rtk_string_append(context,"\"></number>\r\n",-1);
}

void stk_cfg_set_text(char* name,rtk_string_t* context,rtk_string_t* strvalue)
{
    rtk_string_append(context,"<text name=\"",-1);
    rtk_string_append(context,name,-1);
    rtk_string_append(context,"\" value=\"",-1);
    rtk_string_append(context,strvalue->buffer,strvalue->length);
    rtk_string_append(context,"\"></text>\r\n",-1);
}

void stk_cfg_set_list(char *name,rtk_string_t *context,rtk_string_t *content)
{
    rtk_string_append(context,"<list name=\"",-1);
    rtk_string_append(context,name,-1);
    rtk_string_append(context,"\" value=\"",-1);
    rtk_string_append(context,"\">\r\n",-1);
    rtk_string_append(context,content->buffer,content->length);
    rtk_string_append(context,"</list>\r\n",-1);
}

void stk_test_proxy_init(stk_test_proxy_t* m_test_proxy)
{
	rtk_string_init(&m_test_proxy->str_proxy_ip);
	m_test_proxy->n_port = -1;
	m_test_proxy->respone_time = 0;
}
void stk_test_proxy_term(stk_test_proxy_t* m_test_proxy)
{
	if (!rtk_string_is_empty(&m_test_proxy->str_proxy_ip))
	{
		rtk_string_term(&m_test_proxy->str_proxy_ip);
	}
}

void stk_test_proxy_reset(stk_test_proxy_t* m_test_proxy)
{
   
}

void stk_test_proxy_get_key(stk_test_proxy_t* m_test_proxy, rtk_string_t* str_key)
{
    rtk_string_shrink(str_key, 0);
	rtk_string_append(str_key, m_test_proxy->str_proxy_ip.buffer, m_test_proxy->str_proxy_ip.length);
	rtk_string_append_char(str_key, ';');
	rtk_string_append_number(str_key, m_test_proxy->n_port);
}


void stk_proxy_handle_get_profile(stk_proxy_handle_t *cfg, const char *supdir, const char *subdir)
{
    rtk_string_t context;
    rtk_string_t content;
    rtk_string_t fname;
    rtk_string_t str_current_dir;

    rtk_string_init(&context);
    rtk_string_init(&content);
    rtk_string_init(&fname);
    rtk_string_init(&str_current_dir);

    rtk_dir_merge(&fname, (char*)supdir, (char*)subdir);
    rtk_file_get_string(fname.buffer,&context);

    cfg->n_common = stk_cfg_get_number("n_common",&context,9988);
    cfg->update_time = stk_cfg_get_number("update_time", &context, 9988);
    cfg->max_time = stk_cfg_get_number("max_time", &context, 9988);
    cfg->n_filter = stk_cfg_get_number("n_filter", &context, 9988);
   	cfg->is_sort = stk_cfg_get_number("is_sort", &context, 9988);
    cfg->is_check = stk_cfg_get_number("is_check", &context, 9988);
	cfg->threadnum = stk_cfg_get_number("thread_num", &context, 10);
    cfg->timeout.connect_time = stk_cfg_get_number("connect_timeout", &context, 1);
    cfg->timeout.send_timeout = stk_cfg_get_number("send_timeout", &context, 2);
    cfg->timeout.recv_timeout = stk_cfg_get_number("recv_timeout", &context, 3);

    rtk_dir_cwd(&str_current_dir);
    stk_cfg_get_text("common_dir",&context,&cfg->common_dir,str_current_dir.buffer);
    if (rtk_string_is_empty(&cfg->common_dir) == 1)
        rtk_string_copy(&cfg->common_dir, str_current_dir.buffer, str_current_dir.length);
    stk_cfg_get_text("common_file", &context, &cfg->common_file, "Proxy.lst");

    stk_cfg_get_list("proxy_sites", &context, &content);
    if (!rtk_string_is_empty(&content))
    {
        stk_proxy_sites_list_from_string(&cfg->proxy_site_lists, &content);
    }

    stk_cfg_get_list("url", &context, &content);
    if (!rtk_string_is_empty(&content))
    {
        stk_url_list_from_string(&cfg->test_url_lists, &content);
    }

    rtk_string_term(&context);
    rtk_string_term(&content);
    rtk_string_term(&fname);
    rtk_string_term(&str_current_dir);
}

void stk_proxy_list_from_string(rtk_lists_t* test_proxy_lists, rtk_string_t* content)
{
   /* rtk_hash_t hash;*/
	rtk_list_t list;
    rtk_string_t string;
    stk_test_proxy_t *m_test_proxy;
    void *pos;

	/*rtk_hash_init(&hash, true, false);*/
    rtk_list_init(&list,true);
    rtk_string_init(&string);

    rtk_lists_free_all(test_proxy_lists,(rtk_lists_term_t)stk_test_proxy_term);

    rtk_list_from_string(&list,"\r\n",content);

    pos = rtk_list_get_head_position(&list);
    while(pos != 0)
    {
        rtk_list_get_next_string(&list,&pos,&string);

        m_test_proxy = (stk_test_proxy_t*)rtk_lists_add_tail_new(test_proxy_lists,(rtk_lists_init_t)stk_test_proxy_init);

        stk_test_proxy_from_string(m_test_proxy,&string);

		
		//if ( rtk_hash_find(&hash, m_test_proxy->key.buffer, m_test_proxy->key.length) == 1 )
		//	rtk_lists_remove_tail(test_proxy_lists);
		//else
		//	rtk_hash_insert(&hash, m_test_proxy->key.buffer, m_test_proxy->key.length, 0);

    }

	/*rtk_hash_term(&hash);*/
    rtk_list_term(&list);
    rtk_string_term(&string);

}


void stk_test_proxy_from_string(stk_test_proxy_t* m_test_proxy,rtk_string_t* string)
{
    rtk_string_t val;
    int pos = 0;
    int index = 0;

    rtk_string_init(&val);

	while(stk_cfg_get_next_val(&pos,string,&val))
    {
        rtk_string_trim(&val);

        switch(index)
        {
        case 0:
			rtk_string_copy(&m_test_proxy->str_proxy_ip,val.buffer,val.length);
            break;
        case 1:
			m_test_proxy->n_port = rtk_string_get_number(&val, -1);
            break;
		case 2:
			m_test_proxy->right = rtk_string_get_number(&val, 0);
		case 3:
			m_test_proxy->wrong = rtk_string_get_number(&val, 0);
        default:
            break;
		}
        rtk_string_shrink(&val,0);

        index++;
	}
	
	rtk_string_term(&val);

}


void stk_url_list_from_string(rtk_lists_t* test_url_lists, rtk_string_t* content)
{
    rtk_list_t list;
    rtk_string_t string;
    stk_test_url_t *m_test_url;
    void *pos;

    rtk_list_init(&list,true);
    rtk_string_init(&string);

    rtk_lists_free_all(test_url_lists,(rtk_lists_term_t)stk_test_url_term);

    rtk_list_from_string(&list,"\r\n",content);

    pos = rtk_list_get_head_position(&list);
    while(pos != 0)
    {
        rtk_list_get_next_string(&list,&pos,&string);

        m_test_url = (stk_test_url_t*)rtk_lists_add_tail_new(test_url_lists,(rtk_lists_init_t)stk_test_url_init);

        stk_test_url_from_string(m_test_url,&string);

    }

    rtk_list_term(&list);
    rtk_string_term(&string);

}

void stk_test_url_from_string(stk_test_url_t* m_test_url,rtk_string_t* string)
{
    rtk_string_t val;
    int pos = 0;
    int index = 0;

    rtk_string_init(&val);

	while(stk_cfg_get_next_val(&pos,string,&val))
    {
        rtk_string_trim(&val);

        switch(index)
        {
        case 0:
			rtk_string_copy(&m_test_url->str_url,val.buffer,val.length);
            break;
        case 1:
			m_test_url->n_max_out_number = rtk_string_get_number(&val, -1);
            break;
		case 2:
			m_test_url->ui_min_size = rtk_string_get_number(&val, -1);
		case 3:
			rtk_string_copy(&m_test_url->str_tag, val.buffer, val.length);
        default:
            break;
		}
        rtk_string_shrink(&val,0);

        index++;
	}
	rtk_string_term(&val);
}



void stk_proxy_handle_set_profile(stk_proxy_handle_t *cfg, const char *supdir, const char *subdir)
{
    rtk_string_t context;
    rtk_string_t content;
    rtk_string_t fname;
	rtk_string_t str_proxy_supdir;

    rtk_string_init(&context);
    rtk_string_init(&content);
    rtk_string_init(&fname);
	rtk_string_init(&str_proxy_supdir);

    rtk_dir_merge(&fname,(char*)supdir,(char*)subdir);

    rtk_string_append(&context,"<?xml version=\"1.0\" encoding=\"GBK\"?>\r\n\r\n",-1);
 
	stk_cfg_set_number("update_time",&context,cfg->update_time);
	stk_cfg_set_number("n_common",&context,cfg->n_common);
	stk_cfg_set_number("max_time", &context, cfg->max_time);
	stk_cfg_set_number("n_filter", &context, cfg->n_filter);
	stk_cfg_set_number("is_sort", &context, cfg->is_sort);
    stk_cfg_set_number("is_check", &context, cfg->is_check);
	stk_cfg_set_number("connect_timeout", &context, cfg->timeout.connect_time);
	stk_cfg_set_number("send_timeout", &context, cfg->timeout.send_timeout);
	stk_cfg_set_number("recv_timeout", &context, cfg->timeout.recv_timeout);



	stk_cfg_set_text("common_dir",&context,&cfg->common_dir);
	stk_cfg_set_text("common_file",&context,&cfg->common_file);

	stk_proxy_sites_list_to_string(&cfg->proxy_site_lists, &content);
	rtk_string_append(&context, "\r\n", -1);
	stk_cfg_set_list("proxy_sites", &context, &content);

	stk_test_url_list_to_string(&cfg->test_url_lists,&content);
	rtk_string_append(&context, "\r\n", -1);
	stk_cfg_set_list("url",&context,&content);

	rtk_dir_make((char*)supdir, "proxy_temps");
    rtk_dir_merge(&str_proxy_supdir, (char*)supdir, "proxy_temps");

	//if (cfg->flag == 0)
	//	stk_test_proxy_lists_set_profile(&cfg->m_test_proxy_lists, (char*)str_proxy_supdir.buffer, "common.lst");
	//else
	//{
	//	rtk_string_t filename;

	//	rtk_string_init(&filename);
	//
	//	stk_convert_url_to_filename(&cfg->test_url, &filename);
	//	stk_test_proxy_lists_set_profile(&cfg->m_test_proxy_lists, str_proxy_supdir.buffer, filename.buffer);
	//	rtk_string_term(&filename);
	//
	//}

	rtk_file_set_string(fname.buffer, 0, &context);
	rtk_string_term(&context);
	rtk_string_term(&content);
	rtk_string_term(&fname);
    rtk_string_term(&str_proxy_supdir);

}

void stk_test_url_list_to_string(rtk_lists_t* test_url_lists,rtk_string_t* content)
{
    rtk_string_t string;
    stk_test_url_t *m_test_url;
    void *pos;

    rtk_string_init(&string);

    rtk_string_shrink(content,0);

    pos = rtk_lists_get_head_position(test_url_lists);
    while(pos != 0)
    {
        m_test_url = (stk_test_url_t*)rtk_lists_get_next(test_url_lists,&pos);
        stk_test_url_to_string(m_test_url,&string);

        rtk_string_append(content,string.buffer,string.length);
        rtk_string_shrink(&string,0);
    }

    rtk_string_term(&string);
}

void stk_test_proxy_list_to_string(rtk_lists_t* test_proxy_lists,rtk_string_t* content)
{
    rtk_string_t string;
    stk_test_proxy_t *m_test_proxy;
    void *pos;

    rtk_string_init(&string);

    rtk_string_shrink(content,0);

    pos = rtk_lists_get_head_position(test_proxy_lists);
    while(pos != 0)
    {
        m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(test_proxy_lists,&pos);
        stk_test_proxy_to_string(m_test_proxy,&string);

        rtk_string_append(content,string.buffer,string.length);
        rtk_string_shrink(&string,0);
    }

    rtk_string_term(&string);

}

void stk_proxy_sites_list_to_string(rtk_lists_t* proxy_site_lists, rtk_string_t* content)
{
    rtk_string_t string;
    stk_proxy_site_t *m_proxy_site;
    void *pos;

    rtk_string_init(&string);

    rtk_string_shrink(content,0);

    pos = rtk_lists_get_head_position(proxy_site_lists);
    while(pos != 0)
    {
        m_proxy_site = (stk_proxy_site_t*)rtk_lists_get_next(proxy_site_lists,&pos);
        stk_proxy_site_to_string(m_proxy_site,&string);

        rtk_string_append(content,string.buffer,string.length);
        rtk_string_shrink(&string,0);
    }

    rtk_string_term(&string);

}
void stk_test_url_to_string(stk_test_url_t* m_test_url, rtk_string_t* string)
{
    rtk_string_shrink(string, 0);
	rtk_string_append(string, m_test_url->str_url.buffer, m_test_url->str_url.length);
	rtk_string_append_char(string, ';');
	rtk_string_append_number(string,m_test_url->n_max_out_number);
	rtk_string_append_char(string, ';');
	rtk_string_append_number(string, m_test_url->ui_min_size);
	rtk_string_append_char(string, ';');
	rtk_string_append(string, (char*)m_test_url->str_tag.buffer, m_test_url->str_tag.length);
	rtk_string_append(string, "\r\n",-1);

}

void stk_test_proxy_to_string(stk_test_proxy_t* m_test_proxy,rtk_string_t* string)
{
    rtk_string_shrink(string, 0);
	rtk_string_append(string, m_test_proxy->str_proxy_ip.buffer, m_test_proxy->str_proxy_ip.length);
	rtk_string_append_char(string,';');
	rtk_string_append_number(string, m_test_proxy->n_port);
	rtk_string_append_char(string, ';');
	rtk_string_append_number(string, (int)m_test_proxy->right);
	rtk_string_append_char(string, ';');
	rtk_string_append_number(string, (int)m_test_proxy->wrong);
	rtk_string_append_char(string, ';');
	rtk_string_append_number(string, m_test_proxy->respone_time);
	rtk_string_append(string, "\r\n",-1);
}

void stk_proxy_site_to_string(stk_proxy_site_t* m_proxy_site, rtk_string_t* string)
{
    rtk_string_shrink(string, 0);
	rtk_string_append(string, m_proxy_site->str_url.buffer, m_proxy_site->str_url.length);
	rtk_string_append_char(string, ';');
	rtk_string_append(string, m_proxy_site->str_regex.buffer, m_proxy_site->str_regex.length);
	rtk_string_append(string, "\r\n",-1);
}

void stk_proxy_handle_reset(stk_proxy_handle_t* m_effect_proxy_cfg)
{
	m_effect_proxy_cfg->n_common = -1;
	m_effect_proxy_cfg->update_time = -1;

	//rtk_lists_free_all(&m_effect_proxy_cfg->test_prior_proxy_lists, (rtk_lists_term_t)stk_test_proxy_term);
	//rtk_lists_free_all(&m_effect_proxy_cfg->test_proxy_lists, (rtk_lists_term_t)stk_test_proxy_term);
	rtk_lists_free_all(&m_effect_proxy_cfg->test_url_lists, (rtk_lists_term_t)stk_test_url_term);

}

void stk_test_proxy_copy(stk_test_proxy_t* m_test_proxy, stk_test_proxy_t* m_ntest_proxy)
{

	rtk_string_copy(&m_test_proxy->str_proxy_ip, m_ntest_proxy->str_proxy_ip.buffer, m_ntest_proxy->str_proxy_ip.length);
	m_test_proxy->n_port = m_ntest_proxy->n_port;
	m_test_proxy->respone_time = m_ntest_proxy->respone_time;
	m_test_proxy->right = m_ntest_proxy->right;
	m_test_proxy->wrong = m_ntest_proxy->wrong;
}

void stk_proxy_site_init(stk_proxy_site_t* m_proxy_site)
{
	rtk_string_init(&m_proxy_site->str_url);
	rtk_string_init(&m_proxy_site->str_regex);
}

void stk_proxy_site_term(stk_proxy_site_t* m_proxy_site)
{
	rtk_string_term(&m_proxy_site->str_url);
	rtk_string_term(&m_proxy_site->str_regex);
}

void stk_proxy_sites_list_from_string(rtk_lists_t* proxy_site_lists, rtk_string_t* content)
{
    rtk_list_t list;
    rtk_string_t string;
    stk_proxy_site_t *m_proxy_site;
    void *pos;

    rtk_list_init(&list,true);
    rtk_string_init(&string);

    rtk_lists_free_all(proxy_site_lists,(rtk_lists_term_t)stk_proxy_site_term);

    rtk_list_from_string(&list,"\r\n",content);

    pos = rtk_list_get_head_position(&list);
    while(pos != 0)
    {
        rtk_list_get_next_string(&list,&pos,&string);

        m_proxy_site = (stk_proxy_site_t*)rtk_lists_add_tail_new(proxy_site_lists,(rtk_lists_init_t)stk_proxy_site_init);

        stk_proxy_site_from_string(m_proxy_site,&string);

    }

    rtk_list_term(&list);
    rtk_string_term(&string);


}

void stk_proxy_site_from_string(stk_proxy_site_t* m_proxy_site, rtk_string_t* string)
{
    rtk_string_t val;
    int pos = 0;
    int index = 0;

    rtk_string_init(&val);

	while(stk_cfg_get_next_val(&pos,string,&val))
    {
        rtk_string_trim(&val);

        switch(index)
        {
        case 0:
			rtk_string_copy(&m_proxy_site->str_url,val.buffer,val.length);
            break;
        case 1:
			rtk_string_copy(&m_proxy_site->str_regex, val.buffer, val.length);
            break;
	
        default:
            break;
		}
        rtk_string_shrink(&val,0);

        index++;
	}
	rtk_string_term(&val);

}

void stk_subsection_get_profile(rtk_lists_t* lists_subsection, char* supdir, char*subdir)
{
    rtk_string_t fname;
	rtk_string_t content;
    rtk_string_t string;	
	rtk_list_t list;
	stk_subsection_t* m_subsection;

	rtk_string_init(&fname);
	rtk_string_init(&content);
    rtk_string_init(&string);
    rtk_list_init(&list, true);

    rtk_dir_merge(&fname, supdir, subdir);
	rtk_file_get_string(fname.buffer, &content);

	rtk_list_from_string(&list,"\r\n",&content);
    void* pos = rtk_list_get_head_position(&list);
    while(pos != 0)
    {
        rtk_list_get_next_string(&list,&pos,&string);
        m_subsection = (stk_subsection_t*)rtk_lists_add_tail_new(lists_subsection,(rtk_lists_init_t)stk_subsection_init);
        stk_subsection_from_string(&string, m_subsection);
    
	}


    rtk_string_term(&fname);
	rtk_string_term(&content);
    rtk_string_term(&string); 
	rtk_list_term(&list);
}

void stk_subsection_from_string(rtk_string_t* string, stk_subsection_t* m_subsection)
{
    rtk_string_t val;
    int pos = 0;
    int index = 0;

    rtk_string_init(&val);

	while(stk_cfg_get_next_val(&pos,string,&val))
    {
        rtk_string_trim(&val);

        switch(index)
        {
        case 0:
			rtk_string_copy(&m_subsection->str_proxy_ip,val.buffer,val.length);
            break;
        case 1:
			m_subsection->bit_count = rtk_string_get_number(&val, -1);
			
            break;
	
        default:
            break;
		}
        rtk_string_shrink(&val,0);

        index++;
	}
	rtk_string_term(&val);

}