#ifndef _STK_DB_H
#define _STK_DB_H
#include "rtk_32.h"
#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////
typedef void *rdb_process;
typedef struct dtk_func_t dtk_func_t;
typedef struct stk_db_t stk_db_t;
typedef struct db_cfg_t db_cfg_t;
typedef struct stk_tasknum_t stk_tasknum_t;

typedef int (__stdcall mdb_initialize) (void);
typedef int (__stdcall mdb_finalize) (void);
typedef rdb_process (__stdcall mdb_login) (char*,char*,char*,char*);
typedef int (__stdcall mdb_logout) (rdb_process);
typedef char* (__stdcall mdb_get_last_error) (rdb_process);
typedef int (__stdcall mdb_set_charset) (rdb_process,char *charset);
typedef int (__stdcall mdb_use_db_name) (rdb_process,char*);
typedef int (__stdcall mdb_prepare_sql) (rdb_process,char*);
typedef int (__stdcall mdb_sql_excute) (rdb_process);
typedef int (__stdcall mdb_set_parameters) (rdb_process,char**,int,int);
typedef int (__stdcall mdb_fetch_next) (rdb_process);
typedef int (__stdcall mdb_open_records) (rdb_process);
typedef int (__stdcall mdb_close_records) (rdb_process);
typedef int (__stdcall mdb_sql_commit) (rdb_process);

typedef struct rdb_func_t
{
	mdb_initialize				* rdb_initialize;
	mdb_finalize				* rdb_finalize;
	mdb_login					* rdb_login;
	mdb_logout					* rdb_logout;
	mdb_get_last_error			* rdb_get_last_error;	
	mdb_set_charset				* rdb_set_charset;
	mdb_use_db_name				* rdb_use_db_name;
	mdb_prepare_sql				* rdb_prepare_sql;
	mdb_sql_excute              * rdb_sql_excute;
    mdb_set_parameters          * rdb_set_parameters;
	mdb_fetch_next              * rdb_fetch_next;
	mdb_open_records            * rdb_open_records;
	mdb_close_records           * rdb_close_records;
	mdb_sql_commit              * rdb_sql_commit;

} rdb_func_t;

struct stk_db_t
{
    char            *db_name;            /* 数据库类型 */
    char            *db_dll;             /* 动态库名称 */
    char            *db_server;          /* 数据库服务器 */
    char            *db_port;            /* 数据库端口号 */
    char            *db_database;        /* 数据库名称 */
    int             db_usertype;         /* 数据库登陆类型*/
    char            *db_username;        /* 数据库用户名 */
    char            *db_password;        /* 数据库密码 */
	char            *db_charset;         /* 数据库字符集*/
    char            *db_dirname;         /* 数据库路径名称 */
    long            db_filesize;         /* 数据库文件大小 */
    int             db_use;              /* 数据库是否使用 */
    void            *db_inst;            /* 数据库句柄 */
    rdb_func_t      db_func;             /* 数据库接口 */
	rtk_mutex_t     db_mutex;
};

struct db_cfg_t
{
	rtk_string_t  msqserver;              //服务器地址
	rtk_string_t  msqport;                //服务器端口
	rtk_string_t  msqusername;            //用户名
	rtk_string_t  msqpassword;            //密码
	rtk_string_t  msqdatabase;            //库名
	rtk_string_t  msqcharset;             //数据库编码方式
	rtk_string_t  mstable;                //表名
	rtk_string_t  mysqlselect;            //sql语句
	rtk_string_t  row1;                   //字段1
	rtk_string_t  row2;                   //字段2
	rtk_string_t  row3;                   //字段3
	rtk_string_t  row4;                   //字段4
	rtk_string_t  row5;                   //字段5
	rtk_string_t  row6;                   //字段6 （预留字段）
};


void stk_db_init(stk_db_t *db);
void stk_db_term(stk_db_t *db);
void db_cfg_init(db_cfg_t *cfg);
void db_cfg_term(db_cfg_t *cfg);
void stk_db_copy(stk_db_t *db,stk_db_t *ndb);
int stk_db_compare(stk_db_t *db,char *buffer);

int stk_db_load_func(stk_db_t *db);
void stk_db_load_library(stk_db_t *db);
void stk_db_free_library(stk_db_t *db);
void stk_db_set_msq_info(stk_db_t *db,db_cfg_t *cfg);

void db_cfg_get_profile(db_cfg_t *cfg);
int robot_get_next_field(int pos,rtk_string_t *context,rtk_string_t *tag,int *begin,int *end);
int64_t parse_number(const unsigned char *num);
void stk_db_prepare_sql(char *charset,rtk_string_t *sql);
/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_DB_H