#include "db_cfg.h"
#include "sender_cfg.h"

void sender_cfg_init(sender_cfg_t* cfg)
{
	cfg->port = 0;
	cfg->max_pthread = 0;
	cfg->max_connect = 0;
	cfg->connect_timeout = 0;
	cfg->connect_retries = 0;
	cfg->recv_timeout = 0;
	cfg->send_timeout = 0;
	cfg->client_count = 0;
}

void con_cfg_get_name(sender_cfg_t *cfg,rtk_string_t *name,rtk_string_t *val)
{
	if(rtk_string_icompare(name,"send_timeout",-1) == 0)
	{
		cfg->send_timeout = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"recv_timeout",-1) == 0)
	{
		cfg->recv_timeout = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"connect_retries",-1) == 0)
	{
		cfg->connect_retries = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"connect_timeout",-1) == 0)
	{
		cfg->connect_timeout = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"max_connect",-1) == 0)
	{
		cfg->max_connect = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"max_pthread",-1) == 0)
	{
		cfg->max_pthread = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if(rtk_string_icompare(name,"port",-1) == 0)
	{
		cfg->port = (int)parse_number((const unsigned char*)val->buffer);
	}
	else if (rtk_string_icompare(name,"client_count",-1) == 0)
	{
		cfg->client_count = (int)parse_number((const unsigned char*)val->buffer);
	}
}

void con_cfg_get_param(sender_cfg_t *cfg,rtk_string_t *context)
{
	int pos,begin,end;
	rtk_string_t tag;
	rtk_string_t name;
	rtk_string_t val;

	rtk_string_init(&tag);
	rtk_string_init(&name);
	rtk_string_init(&val);

	pos = 0;
	while(robot_get_next_field(pos,context,&tag,&begin,&end))
	{
		rtk_string_mid_string(context,pos,begin-pos,&val);
		rtk_string_trim(&val);
		if(!rtk_string_is_empty(&name))
		{
			con_cfg_get_name(cfg,&name,&val);
		}
		rtk_string_copy(&name,tag.buffer,tag.length);
		pos = end + 1;
	}

	begin = rtk_string_find_pos(context,"\r\n",-1,pos);
	if(begin != -1)
	{
		rtk_string_mid_string(context,pos,begin-pos,&val);
	}
	else
	{
		rtk_string_mids_string(context,pos,&val);
	}

	rtk_string_trim(&val);
	if(!rtk_string_is_empty(&name))
	{
		con_cfg_get_name(cfg,&name,&val);
	}

	rtk_string_term(&tag);
	rtk_string_term(&name);
	rtk_string_term(&val);
}

void soc_cfg_get_profile(sender_cfg_t *cfg)
{
	rtk_string_t fname;
	rtk_string_t context;
	rtk_string_t dirname;
	rtk_string_init(&context);
	rtk_string_init(&fname);
	rtk_string_init(&dirname);
	rtk_dir_cwd(&dirname);
	rtk_dir_merge(&fname,dirname.buffer,"proxy_server.cfg");
	rtk_file_get_string(fname.buffer,&context);

	con_cfg_get_param(cfg,&context);

	rtk_string_term(&context);
	rtk_string_term(&fname);
	rtk_string_term(&dirname);
}