#ifndef _SERVER__H_
#define _SERVER__H_

#include "head.h"

struct server_cfg_t
{
    int thread_num;
    int get_and_check_proxy;
    int port;
    rtk_string_t supdir;
    rtk_string_t subdir;
};

void server_cfg_init(server_cfg_t* server_cfg);
void server_cfg_term(server_cfg_t* server_cfg);
void server_cfg_get_from_profile(server_cfg_t* server_cfg, char* supdir, char* subdir);


#endif