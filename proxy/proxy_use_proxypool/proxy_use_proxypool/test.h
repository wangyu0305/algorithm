#ifndef _TEST__H_
#define _TEST__H_

#include "head.h"

void test_socket_server(void);
void test_data_stream(void);
void test_http_request(void);
void test_proxy_pool(void);
void work(void* param);

#endif