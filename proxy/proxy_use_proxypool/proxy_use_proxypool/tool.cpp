#include "tool.h"
#include <time.h>

int get_current_time(rtk_string_t* str_time, rtk_string_t* format)
{
    char szTimeString[128];
    memset(szTimeString, 0, 128);
    struct tm *today;
    time_t ltime;
    time( &ltime );
    today = localtime( &ltime );
    strftime(szTimeString, 128, format->buffer, today);
    //str_time = szTimeString;
    rtk_string_copy(str_time, szTimeString, -1);
    return 0;
}