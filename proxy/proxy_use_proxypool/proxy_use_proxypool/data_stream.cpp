#include "data_stream.h"
#include "http_request.h"
#include "log.h"
#include "tool.h"
#include <string>
#include <stdlib.h>
//#define debug

using namespace std;

data_stream::data_stream()
{
    rtk_string_init(&str_http_request);
    rtk_string_init(&urlname);
    rtk_stream_init(&source);
    stk_http_init(&inet);
    stk_session_init(&session);
    inet.session = &session;
    rtk_mutex_init(&mutex_log);
    port = 1234;
}

data_stream::data_stream(int nport)
{
    rtk_string_init(&str_http_request);
    rtk_string_init(&urlname);
    rtk_stream_init(&source);
    stk_http_init(&inet);
    stk_session_init(&session);
    inet.session = &session;
    rtk_mutex_init(&mutex_log);
    port = nport;
}

data_stream::~data_stream()
{
    rtk_string_term(&str_http_request);
    rtk_string_term(&urlname);
    rtk_stream_term(&source);
    stk_http_term(&inet);
    stk_session_term(&session);
}

int data_stream::set_socket_server(rtk_socket_t* socket)
{
    socket_server = socket;
    return 0;
}

int data_stream::set_proxy_pool(proxy_pool* proxy_pool)
{
    handle_proxy_pool = proxy_pool;
    return 0;
}

int data_stream::set_threadID(int id)
{
    threadID = id;
    return 0;
}

int socket_server_listen(rtk_socket_t* socket_server, int port)
{
    int result = 0;
    result = rtk_socket_open(socket_server);
    if (result == 0)
        return OPEN_ERROR;
    socket_server->ipaddr = 0;
    socket_server->port = port;
    result = rtk_socket_listen(socket_server);
    if (result == 0)
        return LISTEN_ERROR;
    printf("socket listening... on 127.0.0.1 %d\n", port);
    return SUCCESS;
}

rtk_socket_t*  data_stream::socket_server_accept()
{
    rtk_socket_t* psocket;
    psocket = rtk_socket_accept(socket_server);
    //printf("accept socket success!\n");
    return psocket;
}

int data_stream::socket_server_recv_http_request(rtk_socket_t* psocket)
{
    rtk_stream_t recv;
    rtk_string_t str_length;
    int current_length;
    int length = 0;
    int rv = 1;
    rtk_stream_init(&recv);
    rtk_string_init(&str_length);
    rtk_string_shrink(&str_http_request, 0);
    int pos = 0;
    while (rv > 0)
    {
        rv = rtk_socket_recv_message(psocket,BUFFSIZE,&recv);
        rtk_string_append(&str_http_request, (char*)recv.buffer, recv.length);

       
        int pos = rtk_string_find(&str_http_request, "\r\n\r\n", -1);
  
        if ( pos > 0 )
        {
            int flag_post = rtk_string_find(&str_http_request, "POST", -1);
            if (flag_post == 0)
            {
                int pos1 = rtk_string_find(&str_http_request, "Content-Length: ", -1);
                pos1 += strlen("Content-Length: ");
                int pos2 = rtk_string_find_pos(&str_http_request, "\r\n", -1, pos);
                rtk_string_mid_string(&str_http_request, pos1, pos2-pos1, &str_length);
                length = rtk_string_get_number(&str_length, -1);
                current_length = str_http_request.length - 4 - pos;
            }
            else
            {
                length = 0;
            }
            break;
   
        }
    }

    if (length > 0)
    {
        while (rv > 0)
        {
            rv = rtk_socket_recv_message(psocket,BUFFSIZE,&recv);
            rtk_string_append(&str_http_request, (char*)recv.buffer, recv.length);
            current_length += rv;
            if (current_length == length)
                break;
        }
    }
    rtk_string_replace(&str_http_request, "Keep-Alive", "close");
    rtk_stream_term(&recv);
    rtk_string_term(&str_length);
    return 0;
}

static string get_next_val(char* url,char* cover)
{
	string urlname(url);
	string substring;
	string::size_type pos;
	pos = urlname.find(cover);
	if (pos == string::npos)
		return "";
	if (pos == 0)
	    return cover;
	substring = urlname.substr(0,pos);
	return substring;
}

void data_stream::get_objname(rtk_string_t* urlname,rtk_string_t* hostname,unsigned short *port,rtk_string_t* objname)
{
	string substring;
	char* url;
	int pos;
	url = urlname->buffer;
	if (url == NULL)
	{
		return ;
	}
	substring = get_next_val(url,"http://");
	if (substring.size() != 0)
	{
		url += 7;
	}
	substring = get_next_val(url,":");
	if (substring.size() != 0)
	{
        rtk_string_copy(hostname,(char*)substring.c_str(),-1);
		pos = substring.size()+1;
		url += pos;
		substring = get_next_val(url,"/");
		if (substring.size() != 0)
		{
			*port = atoi((char*)substring.c_str());
			pos = substring.size();
			url += pos;
			rtk_string_copy(objname,url,-1);
		}
		else
		{
			*port = atoi(url);
			rtk_string_copy(objname,"/",-1);
		}
	}
	else
	{
		substring = get_next_val(url,"/");
		if (substring.size() != 0)
		{
			rtk_string_copy(hostname,(char*)substring.c_str(),-1);
			pos = substring.size();
			url += pos;
			rtk_string_copy(objname,url,-1);
		}
		else
		{
			rtk_string_copy(hostname,url,-1);
			rtk_string_copy(objname,"/",-1);
		}
	}
}

int data_stream::get_source_from_web(rtk_socket_t* psocket)
{
    rtk_socket_t socket;
    rtk_string_t urlname;
    rtk_string_t hostname;
    rtk_string_t objectname;
    rtk_stream_t stream;
    rtk_stream_t response; 
    rtk_string_t response_head;
    rtk_string_t str_time;
    rtk_string_t format;
    unsigned short  port = 80;
    int status = 0;
	DWORD error;
    char sz_temp[1024*10];


    rtk_socket_init(&socket);
    rtk_string_init(&urlname);
    rtk_string_init(&hostname);
    rtk_string_init(&objectname);
    rtk_stream_init(&stream);
    rtk_stream_init(&response);
    rtk_string_init(&response_head);
    rtk_string_init(&str_time);
    rtk_string_init(&format);
    rtk_string_copy(&format, "%Y-%m-%d %H:%M:%S", -1);
    rtk_stream_shrink(&source, 0);
    socket.connect_timeout = 100;
    socket.send_timeout = 100;
    socket.recv_timeout = 100;

    do
    {
        if (rtk_string_is_empty(&str_http_request) == 1)
            break;
        http_request handle_http_request(&str_http_request);
        handle_http_request.get_urlname_from_request(&urlname);
        if (rtk_string_is_empty(&urlname) == 1)
            break;
        get_objname(&urlname, &hostname, &port, &objectname);

        if (rtk_socket_open(&socket)  == 0)
        {
            status = OPEN_ERROR;
            break;
        }
        single_proxy_t* single_proxy = handle_proxy_pool->get_single_proxy();
        
       

        if (single_proxy)
        {
            socket.ipaddr = rtk_socket_get_ipaddr(single_proxy->host);
            socket.port = single_proxy->port;
            sprintf(sz_temp, "thread %d: download %s use proxy %s %d", threadID, urlname.buffer, single_proxy->host, single_proxy->port);
            rtk_mutex_lock(&mutex_log);
            write_log_by_sz_char(sz_temp);
            rtk_mutex_unlock(&mutex_log);
            handle_proxy_pool->add_single_proxy(single_proxy);
        }
        else
        {
            socket.ipaddr = rtk_socket_get_ipaddr(hostname.buffer);
            socket.port = port;
            sprintf(sz_temp, "thread %d: download %s no proxy", threadID, urlname.buffer);
            rtk_mutex_lock(&mutex_log);
            write_log_by_sz_char(sz_temp);
            rtk_mutex_unlock(&mutex_log);
        }
        if ( rtk_socket_connect(&socket) == 0 )
        {
            status = CONNECT_ERROR;
            break;
        }
        rtk_stream_swap_string(&stream,&str_http_request);    


        rtk_mutex_lock(&mutex_log);
        get_current_time(&str_time, &format);
        sprintf(sz_temp, "start request %s\r\n%s\r\n", urlname.buffer, str_time.buffer);
        write_log_by_sz_char(sz_temp);
        rtk_mutex_unlock(&mutex_log);


        rtk_socket_send_message(&socket, &stream);

        rtk_mutex_lock(&mutex_log); 
        write_log_by_sz_char((char*)stream.buffer);
        get_current_time(&str_time, &format);
        sprintf(sz_temp, "end request %s\r\n%s", urlname.buffer, str_time.buffer);
        write_log_by_sz_char(sz_temp);
        rtk_mutex_unlock(&mutex_log); 

        int rv = 0;
        int head_done = 0; 
        if ( status == SUCCESS )
        {
            rtk_string_shrink(&response_head, 0);
            while ((rv = rtk_socket_recv_message(&socket, 8192,&response)) > 0)
            {
                if ( head_done == 0 )
                {
                    rtk_string_append(&response_head, (char*)response.buffer, response.length);
                    int pos = rtk_string_find(&response_head, "\r\n\r\n", -1);
                    if ( pos != -1 )
                    {
                        rtk_string_left(&response_head, pos+4);
                        head_done = 1;
                    }
                }
                if ( head_done == 1 )
                {
                    rtk_mutex_lock(&mutex_log); 
                    get_current_time(&str_time, &format);
                    sprintf(sz_temp, "response head end %s\n%s", urlname.buffer, str_time.buffer);
                    write_log_by_sz_char(sz_temp);   
                    write_log_by_sz_char((char*)response_head.buffer);                   
                    rtk_mutex_lock(&mutex_log); 
                    head_done = 2;
                }

                rtk_socket_send_message(psocket, &response);
                #ifdef debugk
                printf("%d\t", rv);
                #endif
            }
			if (rv == -1)
			    error = GetLastError();
            #ifdef debug
            printf("%d\t", rv);
            if (rv == -1002)
            {
                printf("time_out\n");
            }
            #endif
        }
    } while (0);  

    rtk_mutex_lock(&mutex_log);
    get_current_time(&str_time, &format);
    sprintf(sz_temp, "end response %s\r\n%s\r\n\r\n", urlname.buffer, str_time.buffer);
    write_log_by_sz_char(sz_temp);
    rtk_mutex_unlock(&mutex_log);
    
    #ifdef debug
    printf("�������������:%d\n", source.length);
    #endif    

    rtk_socket_term(&socket);
    rtk_string_term(&urlname);
    rtk_string_term(&hostname);
    rtk_string_term(&objectname);
    rtk_stream_term(&stream);
    rtk_stream_term(&response);
    rtk_string_term(&response_head);
    rtk_string_term(&str_time);
    rtk_string_term(&format);
    return 0;
}

int data_stream::send_source_to_client(rtk_socket_t* psocket)
{
   // int r = rtk_socket_send_message(psocket,&source);

    return 0;
}

int data_stream::process_single_request()
{
    rtk_socket_t* psocket;
    psocket = socket_server_accept();
    socket_server_recv_http_request(psocket);
    get_source_from_web(psocket);
    rtk_socket_close(psocket);
    rtk_socket_term(psocket);
    rtk_free(psocket);
    return 0;
}