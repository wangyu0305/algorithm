#ifndef _PROXY_POOL__H_
#define _PROXY_POOL__H_

#include "head.h"

struct single_proxy_t
{
    char* host;
    int port;
    int respone_time;
};

void single_proxy_init(single_proxy_t* handle_single_proxy);
void single_proxy_term(single_proxy_t* handle_single_proxy);
void single_proxy_from_string(single_proxy_t* single_proxy, rtk_string_t* string);

class proxy_pool
{
public:
    proxy_pool();
    proxy_pool(char* supdir, char* subdir);
    ~proxy_pool();    
    int proxy_pool_get_from_profile(char* supdir, char* subdir);
    int proxy_pool_get_from_profile();
    single_proxy_t* get_and_remove_head_proxy();
    single_proxy_t* get_single_proxy();
	void add_single_proxy(single_proxy_t* single_proxy);
private:
    rtk_lists_t lists_proxy_pool;
    rtk_mutex_t mutex_lists_proxy_pool;
    rtk_string_t fname;
};

#endif