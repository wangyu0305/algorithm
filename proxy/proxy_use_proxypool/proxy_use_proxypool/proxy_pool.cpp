#include "proxy_pool.h"
#include "stk_cfg.h"


void single_proxy_init(single_proxy_t* handle_single_proxy)
{
    rtk_char_init(&handle_single_proxy->host);
    handle_single_proxy->port = 80;
    handle_single_proxy->respone_time = 0;
    
}

void single_proxy_term(single_proxy_t* handle_single_proxy)
{
    rtk_char_term(&handle_single_proxy->host);
}

void single_proxy_from_string(single_proxy_t* single_proxy, rtk_string_t* string)
{
    rtk_string_t val;
    int pos = 0;
    int index = 0;

    rtk_string_init(&val);

    while(stk_cfg_get_next_val(&pos,string,&val))
    {
        rtk_string_trim(&val);
        switch (index)
        {
        case 0:
            rtk_char_copy(&single_proxy->host, val.buffer, val.length);
            break;
        case 1:
            single_proxy->port = rtk_string_get_number(&val, -1);
            break;
        case 2:
            single_proxy->respone_time = rtk_string_get_number(&val, -1);
            break;
        default:
            break;
        }
        rtk_string_shrink(&val,0);
        index ++;
    }

    rtk_string_term(&val);
}

proxy_pool::proxy_pool()
{
    rtk_lists_init(&lists_proxy_pool, true, true);
    lists_proxy_pool.dpool->memsize = sizeof(single_proxy_t);
    rtk_mutex_init(&mutex_lists_proxy_pool);
    rtk_string_init(&fname);
}

proxy_pool::proxy_pool(char* supdir, char* subdir)
{
    rtk_lists_init(&lists_proxy_pool, true, true);
    lists_proxy_pool.dpool->memsize = sizeof(single_proxy_t);
    rtk_mutex_init(&mutex_lists_proxy_pool);
    rtk_string_init(&fname);
    rtk_dir_merge(&fname, supdir, subdir);
}

proxy_pool::~proxy_pool()
{
    rtk_lists_free_all(&lists_proxy_pool, (rtk_lists_term_t)single_proxy_term);
    rtk_lists_term(&lists_proxy_pool);
    rtk_mutex_term(&mutex_lists_proxy_pool);
    rtk_string_term(&fname);
}

int proxy_pool::proxy_pool_get_from_profile()
{
    proxy_pool_get_from_profile(fname.buffer, "");
    return 0;
}

void proxy_pool::add_single_proxy(single_proxy_t* single_proxy)
{
	rtk_mutex_lock(&mutex_lists_proxy_pool);
	single_proxy_t* tmp_single = (single_proxy_t*)rtk_lists_add_tail_new(&lists_proxy_pool,(rtk_lists_init_t)single_proxy_init);
	tmp_single = single_proxy;
	rtk_mutex_unlock(&mutex_lists_proxy_pool);
}

int proxy_pool::proxy_pool_get_from_profile(char *supdir, char *subdir)
{
    rtk_mutex_lock(&mutex_lists_proxy_pool);
    do
    {
        if (lists_proxy_pool.count != 0)
            break;

        rtk_string_t fname;
        rtk_string_t context;
        rtk_list_t list;
        rtk_string_t string;
        rtk_string_t content;

        rtk_string_init(&fname);
        rtk_string_init(&context);
        rtk_list_init(&list, true);
        rtk_string_init(&string);
        rtk_string_init(&content);

        rtk_dir_merge(&fname, supdir, subdir);
        rtk_file_get_string(fname.buffer, &content);
        stk_cfg_get_list("", &content, &context);
        rtk_lists_free_all(&lists_proxy_pool, (rtk_lists_term_t)single_proxy_term);
        rtk_list_from_string(&list, "\r\n", &context);
        void* pos = rtk_list_get_head_position(&list);
        while (pos)
        {
            rtk_list_get_next_string(&list, &pos, &string);
            single_proxy_t* single_proxy = (single_proxy_t*)rtk_lists_add_tail_new(&lists_proxy_pool, (rtk_lists_init_t)single_proxy_init);
            single_proxy_from_string(single_proxy, &string);
        }   

        rtk_string_term(&fname);
        rtk_string_term(&context);
        rtk_list_term(&list);
        rtk_string_term(&string); 
        rtk_string_term(&content);
    } while (0);


    rtk_mutex_unlock(&mutex_lists_proxy_pool);
    return 0;
}

single_proxy_t* proxy_pool::get_and_remove_head_proxy()
{
    rtk_mutex_lock(&mutex_lists_proxy_pool);
    single_proxy_t* single_proxy = (single_proxy_t *)rtk_lists_get_head(&lists_proxy_pool);
    if (single_proxy)
        rtk_lists_remove_head(&lists_proxy_pool);
    rtk_mutex_unlock(&mutex_lists_proxy_pool);
    return single_proxy;
}

single_proxy_t* proxy_pool::get_single_proxy()
{

    single_proxy_t* single_proxy = (single_proxy_t *)get_and_remove_head_proxy();

    if (!single_proxy)
    {
        rtk_string_t dir;
        rtk_string_init(&dir);
        rtk_dir_cwd(&dir);
        proxy_pool_get_from_profile();
        single_proxy = (single_proxy_t *)get_and_remove_head_proxy();        
        rtk_string_term(&dir);
    }
    return single_proxy;
}