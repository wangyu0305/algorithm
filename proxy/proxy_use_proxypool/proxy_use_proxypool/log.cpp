#include "log.h"

void write_log_by_sz_char(char* log)
{
    rtk_string_t string;
    rtk_string_t str_current_dir;

    rtk_string_init(&string);
    rtk_string_init(&str_current_dir);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_make(str_current_dir.buffer, "log");
    rtk_string_append(&str_current_dir, "\\log", -1);
    rtk_string_append(&str_current_dir, "\\url_down.txt", -1);
    rtk_string_copy(&string, log, -1);
    rtk_string_append(&string, "\r\n", -1);
    rtk_file_set_string(str_current_dir.buffer, 1, &string);

    rtk_string_term(&string);
    rtk_string_term(&str_current_dir);
}