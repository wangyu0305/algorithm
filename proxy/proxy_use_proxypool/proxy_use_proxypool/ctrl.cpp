#include "ctrl.h"
#include "get_proxy.h"

bool p_close;
rtk_threads_t threads;
rtk_thread_t thread_get_proxy;

void ctrl_start_server(server_cfg_t* server_cfg)
{
    int res;
    rtk_socket_t socket_server;
    proxy_pool handle_proxy_pool(server_cfg->supdir.buffer, server_cfg->subdir.buffer);

	p_close = true;
    rtk_socket_initialize();
    rtk_socket_init(&socket_server);
    rtk_threads_init(&threads);
    rtk_thread_init(&thread_get_proxy);
    socket_server.connect_timeout = 100;
    socket_server.recv_timeout = 1000*3;
    socket_server.send_timeout = 1000;

    if (server_cfg->get_and_check_proxy == 1)
    {
        rtk_thread_create(&thread_get_proxy, get_proxy, 0);
		rtk_thread_join(&thread_get_proxy);
    }

    handle_proxy_pool.proxy_pool_get_from_profile();

    const int NUM = server_cfg->thread_num;
    ctrl_t* ctrl = (ctrl_t*)rtk_malloc(sizeof(ctrl_t)*NUM);
    res = socket_server_listen(&socket_server, server_cfg->port); 
    if ( res == SUCCESS )   
    { 
        rtk_threads_expand(&threads,NUM);
        for(int i=0;i<threads.count;i++)
        {
            ctrl[i].socket_server = &socket_server;
            ctrl[i].threadid = i;
            ctrl[i].handle_proxy_pool = &handle_proxy_pool;
            rtk_thread_create(&threads.array[i], work, &ctrl[i]);
        }
    }
	
	int flag = 1;
	while (1)
	{
		printf("停止请输入0\n");
		scanf("%d", &flag);
		if (flag == 0)
		{
			p_close = false;
			break;
		}
	}
	printf("请等待此轮结束后推出");
    for(int i=0;i<threads.count;i++)
    {
        rtk_thread_join(&threads.array[i]);
        rtk_thread_close(&threads.array[i]);
    }

    rtk_socket_finalize();
    rtk_socket_close(&socket_server);
    rtk_socket_term(&socket_server);
    rtk_threads_term(&threads);
    rtk_thread_term(&thread_get_proxy);
    rtk_free(ctrl);
}

void work(void* param)
{

    rtk_thread_t* pthread;
    ctrl_t* ctrl;
    data_stream handle_data_stream; 

    
    pthread = (rtk_thread_t*)param;
    ctrl = (ctrl_t*)pthread->param;

    handle_data_stream.set_socket_server(ctrl->socket_server);
    handle_data_stream.set_proxy_pool(ctrl->handle_proxy_pool);
    handle_data_stream.set_threadID(ctrl->threadid);
    while (p_close)
    {
        handle_data_stream.process_single_request();
    }

    //for (int i=0; i < 10; i++)
    //{
    //    handle_data_stream.process_single_request();
    //}

    return;
}