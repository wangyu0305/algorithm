#include "http_request.h"

http_request::http_request(rtk_string_t* nrequest)
{
    rtk_string_init(&request);
    rtk_string_copy(&request, nrequest->buffer, nrequest->length);
}

http_request::~http_request()
{
    rtk_string_term(&request);
}

void http_request::get_urlname_from_request(rtk_string_t* urlname)
{
    rtk_string_shrink(urlname, 0);
    int pos = rtk_string_find_char(&request, ' ');
    int npos = rtk_string_find_char_pos(&request, ' ', pos+1);
    rtk_string_mid_string(&request, pos+1, npos - pos -1, urlname);
}