#ifndef _GET_PROXY__H_
#define _GET_PROXY__H_

#include <stk_effect_proxy.h>

void get_proxy(void* param);

#endif