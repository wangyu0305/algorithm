#include "stk_cfg.h"

int stk_cfg_get_next_val(int *pos,rtk_string_t *content,rtk_string_t *val)
{
    char* p_char;
    int index;
    int begin;
    int end;
    int b_quat1;
    int b_quat2;
    int b_char;

    begin = -1;
    end = -1;
    b_quat1 = false;
    b_quat2 = false;
    b_char = false;

    rtk_string_shrink(val,0);

    if(*pos < 0 || *pos >= content->length)
    {
        return false;
    }

    p_char = content->buffer;
    p_char += *pos;
    index = *pos;
    while(*p_char && index < content->length)
    {
        switch(*p_char)
        {
        case ';':
            if(b_char || (b_quat1 && b_quat2) || (!b_char && !b_quat1))
            {
                if(!b_quat2)
                {
                    end = index;
                }
                if(end >= 0 && begin >= 0 && end > begin)
                {
                    rtk_string_mid_string(content,begin,end-begin,val);
                }

                index ++;
                *pos = index;

                return true;
            }
            break;
        case '\"':
            if(b_quat1)
            {
                b_quat2 = true;
                end = index;
            }
            if(!b_char)
            {
                b_quat1 = true;
            }
            break;
        case ' ':
        case '\t':
        case '\r':
        case '\n':
            break;
        default:
            if(!b_quat1)
            {
                b_char = true;
            }
            b_quat2 = false;
            if(begin == -1)
            {
                begin = index;
            }
            break;
        }
        p_char ++;
        index ++;
    }

    if(end == -1)
    {
        end = index;
    }
    if(end >= 0 && begin >= 0 && end > begin)
    {
        rtk_string_mid_string(content,begin,end-begin,val);
    }

    *pos = content->length;

    return true;
}

int stk_cfg_get_number(char* name,rtk_string_t* context,int ndefault)
{
    rtk_string_t strvalue;
    int nvalue;

    rtk_string_init(&strvalue);

    if(stk_cfg_get_value_name("number",name,context,&strvalue))
    {
        nvalue = rtk_string_get_number(&strvalue,ndefault);
    }
    else
    {
        nvalue = ndefault;
    }

    rtk_string_term(&strvalue);

    return nvalue;
}

int stk_cfg_get_value_name(char* strtag,char* name,rtk_string_t* context,rtk_string_t* value)
{
    int npos,nbegin,nend;
    rtk_string_t stag;
    rtk_string_t strtext;
    int rtag;

    rtk_string_init(&stag);
    rtk_string_init(&strtext);

    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,&nbegin,&nend))
    {
        if(!rtag)
        {
            if(rtk_string_icompare(&stag,(char*)strtag,-1) == 0)
            {
                rtk_string_mid_string(context,nbegin+1,nend-nbegin-1,&strtext);
                rtk_string_mids(&strtext,stag.length);
                rtk_string_trim(&strtext);
                if(stk_cfg_get_key_value_name(name,&strtext,value))
                {
                    rtk_string_term(&stag);
                    rtk_string_term(&strtext);

                    return true;
                }
            }
        }
        npos = nend + 1;
    }

    rtk_string_term(&stag);
    rtk_string_term(&strtext);

    return false;
}

int stk_cfg_get_next_tag(int npos,rtk_string_t* context,rtk_string_t* strtag,int* rtag,int* nbegin,int* nend)
{
    char* pchar;
    int ncount;
    int index;
    int nchar;
    int nbar;
    int bfind;
    int bquat;
    int squat;
    int dquat;

    *rtag = false;
    rtk_string_shrink(strtag,0);
    ncount = context->length;
    nchar = -1;
    nbar = -1;
    *nbegin = -1;
    *nend = -1;
    bfind = false;
    bquat = false;
    squat = false;
    dquat = false;

    if(npos < 0 || npos >= ncount)
    {
        return false;
    }

    pchar = context->buffer;
    pchar += npos;
    index = npos;
    while(*pchar && index < ncount)
    {
        switch(*pchar)
        {
        case '<':
            if(!bquat)
            {
                *nbegin = index;
                *rtag = false;
                nchar = -1;
                nbar = -1;
            }
            else if(*nbegin == -1)
            {
                *nbegin = index;
                *rtag = false;
                nchar = -1;
                nbar = -1;
                bquat = false;
                squat = false;
                dquat = false;
            }
            break;
        case '>':
            *nend = index;
            if(*nbegin >= 0 && *nend >= 0 && *nend > *nbegin)
            {
                if(nchar >= 0)
                {
                    if(nbar >= 0)
                    {
                        rtk_string_mid_string(context,nchar,nbar-nchar,strtag);
                    }
                    else
                    {
                        rtk_string_mid_string(context,nchar,(*nend)-nchar,strtag);
                    }
                }
                bfind = true;
            }
            break;
        case '\'':
            if(!dquat)
            {
                squat = !squat;
                bquat = squat;
            }
            break;
        case '\"':
            if(!squat)
            {
                dquat = !dquat;
                bquat = dquat;
            }
            break;
        case ' ':
        case '\t':
        case '\r':
        case '\n':
            if(*nbegin >= 0 && nchar >= 0 && nbar == -1)
            {
                nbar = index;
            }
            break;
        case '/':
            if(!bquat)
            {
                if(*nbegin >= 0)
                {
                    if(nchar == -1)
                    {
                        *rtag = true;
                    }
                    else if(nbar == -1)
                    {
                        nbar = index;
                    }
                }
            }
            break;
        default:
            if(!bquat)
            {
                if(*nbegin >= 0 && nchar == -1)
                {
                    nchar = index;
                }
            }
            break;
        }
        if(bfind)
        {
            return true;
        }
        pchar ++;
        index ++;
    }

    return false;
}

int stk_cfg_get_key_value_name(char* name,rtk_string_t* context,rtk_string_t* value)
{
    rtk_string_t strkey;
    rtk_string_t strvalue;
    int bfind;
    int npos,npos1;

    rtk_string_init(&strkey);
    rtk_string_init(&strvalue);

    bfind = false;
    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_key(&npos,context,&strkey,&strvalue))        
    {
        rtk_string_trim(&strkey);
        if(!rtk_string_is_empty(&strkey))
        {
            rtk_string_trim(&strvalue);
            if(rtk_string_get_at(&strvalue,0) == '\"')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\"');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }
            else if(rtk_string_get_at(&strvalue,0) == '\'')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\'');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }

            if(rtk_string_icompare(&strkey,"name",-1) == 0)
            {
                rtk_string_trim(&strvalue);

                if(rtk_string_icompare(&strvalue,(char*)name,-1) == 0)
                {
                    bfind = true;
                }
            }
            else if(bfind)
            {
                if(rtk_string_icompare(&strkey,"value",-1) == 0)
                {
                    rtk_string_trim(&strvalue);
                    rtk_string_copy(value,strvalue.buffer,strvalue.length);

                    rtk_string_term(&strkey);
                    rtk_string_term(&strvalue);

                    return true;
                }
            }
        }
    }

    rtk_string_term(&strkey);
    rtk_string_term(&strvalue);

    return false;
}

int stk_cfg_get_next_key(int* npos,rtk_string_t* content,rtk_string_t* strkey,rtk_string_t* strvalue)
{
    char* pchar;
    int ncount;
    int index;
    int nequal;
    int bquat;
    int squat;
    int dquat;

    rtk_string_shrink(strkey,0);
    rtk_string_shrink(strvalue,0);

    ncount = content->length;
    nequal = -1;
    bquat = false;
    squat = false;
    dquat = false;

    if(*npos < 0 || *npos >= ncount)
    {
        return false;
    }

    pchar = content->buffer;
    pchar += *npos;
    index = *npos;
    while(*pchar && index < ncount)
    {
        switch(*pchar)
        {
        case '=':
            if(!bquat && nequal < 0)
            {
                nequal = index;
            }
            break;
        case ' ':
        case ';':
        case '\t':
        case '\r':
        case '\n':
            if(!bquat && nequal >= 0)
            {
                rtk_string_mid_string(content,*npos,nequal-(*npos),strkey);
                rtk_string_mid_string(content,nequal+1,index-nequal-1,strvalue);

                index ++;
                *npos = index;

                return true;
            }
            break;
        case '\'':
            if(!dquat)
            {
                squat = !squat;
                bquat = squat;
            }
            break;
        case '\"':
            dquat = !dquat;
            bquat = dquat;
            squat = false;
            break;
        default:
            break;
        }

        pchar ++;
        index ++;
    }

    if(nequal >= 0)
    {
        rtk_string_mid_string(content,*npos,nequal-(*npos),strkey);
        rtk_string_mids_string(content,nequal+1,strvalue);
    }

    *npos = ncount;

    return true;
}

void stk_cfg_get_text(char* name,rtk_string_t* context,rtk_string_t* strvalue, char* strdefault)
{
    if(!stk_cfg_get_value_name("text",name,context,strvalue))
    {
        rtk_string_copy(strvalue,(char*)strdefault,-1);
    }
}

int stk_cfg_get_list(char *name,rtk_string_t *context,rtk_string_t *content)
{
    int npos,nbegin,nend;
    rtk_string_t stag;
    rtk_string_t strtext;
    rtk_string_t strvalue;
    int rtag;

    rtk_string_init(&stag);
    rtk_string_init(&strtext);
    rtk_string_init(&strvalue);

    rtk_string_shrink(content,0);

    npos = 0;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,&nbegin,&nend))
    {
        npos = nend + 1;
        if(!rtag)
        {
            if(rtk_string_icompare(&stag,"list",-1) == 0)
            {
                rtk_string_mid_string(context,nbegin+1,nend-nbegin-1,&strtext);
                rtk_string_mids(&strtext,stag.length);
                rtk_string_trim(&strtext);

                if (stk_cfg_get_key_value(&strtext,&stag,&strvalue))
                {
                    if(rtk_string_icompare(&stag,name,-1) == 0)
                    {
                        if(stk_cfg_find_next_tag(npos,context,"list",true,&nbegin,&nend))
                        {
                            rtk_string_mid_string(context,npos,nbegin-npos,content);
                            rtk_string_trim(content);

                            rtk_string_term(&stag);
                            rtk_string_term(&strtext);
                            rtk_string_term(&strvalue);
                            return true;
                        }
                    }
                }
            }
        }
    }

    rtk_string_term(&stag);
    rtk_string_term(&strtext);
    rtk_string_term(&strvalue);

    return false;
}

int stk_cfg_get_key_value(rtk_string_t* context,rtk_string_t* name,rtk_string_t* value)
{
    rtk_string_t strkey;
    rtk_string_t strvalue;
    int bfind;
    int npos,npos1;

    rtk_string_init(&strkey);
    rtk_string_init(&strvalue);

    bfind = false;
    rtk_string_shrink(name,0);
    rtk_string_shrink(value,0);

    npos = 0;
    while(stk_cfg_get_next_key(&npos,context,&strkey,&strvalue))
    {
        rtk_string_trim(&strkey);
        if(!rtk_string_is_empty(&strkey))
        {
            rtk_string_trim(&strvalue);
            if(rtk_string_get_at(&strvalue,0) == '\"')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\"');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }
            else if(rtk_string_get_at(&strvalue,0) == '\'')
            {
                npos1 = rtk_string_reverse_find(&strvalue,'\'');
                if(npos1 > 0)
                {
                    rtk_string_mid(&strvalue,1,npos1-1);
                    rtk_string_trim(&strvalue);
                }
                else
                {
                    rtk_string_mids(&strvalue,1);
                    rtk_string_trim(&strvalue);
                }
            }

            if(rtk_string_icompare(&strkey,"name",-1) == 0)
            {
                rtk_string_trim(&strvalue);
                rtk_string_copy(name,strvalue.buffer,strvalue.length);
                bfind = true;
            }
            else if(bfind)
            {
                if(rtk_string_icompare(&strkey,"value",-1) == 0)
                {
                    rtk_string_trim(&strvalue);
                    rtk_string_copy(value,strvalue.buffer,strvalue.length);

                    rtk_string_term(&strkey);
                    rtk_string_term(&strvalue);

                    return true;
                }
            }
        }
    }

    rtk_string_term(&strkey);
    rtk_string_term(&strvalue);

    return false;
}

int stk_cfg_find_next_tag(int npos,rtk_string_t* context,char* strtag,int bTag,int* nbegin,int* nend)
{
    rtk_string_t stag;
    int rtag;
    int nbase;

    rtk_string_init(&stag);

    *nbegin = -1;
    *nend = -1;

    nbase = npos;
    while(stk_cfg_get_next_tag(npos,context,&stag,&rtag,nbegin,nend))
    {
        npos = (*nend) + 1;
        if(rtag == bTag)
        {
            rtk_string_make_lower(&stag);
            if(rtk_string_compare(&stag,(char*)strtag,-1) == 0)
            {
                rtk_string_term(&stag);

                return true;
            }
        }
    }

    rtk_string_term(&stag);

    return false;
}