#include "test.h"
#include "ctrl.h"
#include "server_cfg.h"
#include "tool.h"
//#include "data_stream.h"

void test();

int main(void)
{
    //test();
    server_cfg_t server_cfg;
    rtk_string_t str_current_dir;

    server_cfg_init(&server_cfg);
    rtk_string_init(&str_current_dir);

    rtk_dir_cwd(&str_current_dir);
    server_cfg_get_from_profile(&server_cfg, str_current_dir.buffer, "server.cfg");
    ctrl_start_server(&server_cfg);

    server_cfg_term(&server_cfg);
    rtk_string_term(&str_current_dir);
}

void test()
{
    rtk_string_t str_time;
    rtk_string_t format;

    rtk_string_init(&str_time);
    rtk_string_init(&format);

    rtk_string_copy(&format, "%Y-%m-%d %H:%M:%S", -1);
    get_current_time(&str_time, &format);

    rtk_string_term(&str_time);
    rtk_string_term(&format);
}