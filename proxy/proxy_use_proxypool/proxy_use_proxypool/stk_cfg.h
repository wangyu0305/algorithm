#ifndef _STK_CFG__H_
#define _STK_CFG__H_

#include "head.h"
int stk_cfg_get_next_val(int *pos,rtk_string_t *content,rtk_string_t *val);
int stk_cfg_get_number(char* name,rtk_string_t* context,int ndefault);
int stk_cfg_get_value_name(char* strtag,char* name,rtk_string_t* context,rtk_string_t* value);
int stk_cfg_get_next_tag(int npos,rtk_string_t* context,rtk_string_t* strtag,int* rtag,int* nbegin,int* nend);
int stk_cfg_get_key_value_name(char* name,rtk_string_t* context,rtk_string_t* value);
int stk_cfg_get_next_key(int* npos,rtk_string_t* content,rtk_string_t* strkey,rtk_string_t* strvalue);
void stk_cfg_get_text(char* name,rtk_string_t* context,rtk_string_t* strvalue, char* strdefault);
int stk_cfg_get_list(char *name,rtk_string_t *context,rtk_string_t *content);
int stk_cfg_get_key_value(rtk_string_t* context,rtk_string_t* name,rtk_string_t* value);
int stk_cfg_find_next_tag(int npos,rtk_string_t* context,char* strtag,int bTag,int* nbegin,int* nend);
#endif