#ifndef _DATA_STREAM__H_
#define _DATA_STREAM__H_

#include "head.h"
#include "proxy_pool.h"

#define SUCCESS 0
#define OPEN_ERROR 1
#define LISTEN_ERROR 2
#define CONNECT_ERROR 3

#define BUFFSIZE 4096

class data_stream
{
public:
    data_stream();
    data_stream(int nport);
    ~data_stream();
    //int socket_server_listen();
    rtk_socket_t* socket_server_accept();
    int socket_server_recv_http_request(rtk_socket_t* psocket);
    int get_source_from_web(rtk_socket_t* psocket);
    int send_source_to_client(rtk_socket_t* psocket);
    int process_single_request();
    int set_socket_server(rtk_socket_t* socket);
    int set_proxy_pool(proxy_pool* proxy_pool);
    int set_threadID(int id);
	void get_objname(rtk_string_t* urlname,rtk_string_t* hostname,unsigned short *port,rtk_string_t* objname);
    //int get_urlname_from_request();
private:
    rtk_socket_t* socket_server;
    int port;
    rtk_string_t str_http_request;
    rtk_string_t urlname;
    rtk_stream_t source;
    stk_http_t inet;
    stk_session_t session;
    proxy_pool* handle_proxy_pool;
    int threadID;
    rtk_mutex_t mutex_log;
};
int socket_server_listen(rtk_socket_t* socket_server, int port);
#endif