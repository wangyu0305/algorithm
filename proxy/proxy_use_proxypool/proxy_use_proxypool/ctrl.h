#ifndef _CTRL__H_
#define _CTRL__H_

#include "head.h"
#include "data_stream.h"
#include "proxy_pool.h"
#include "server_cfg.h"


struct ctrl_t
{
    int threadid;
    rtk_socket_t* socket_server;
    proxy_pool* handle_proxy_pool;
};

void ctrl_start_server(server_cfg_t* server_cfg);
void work(void* param);


#endif