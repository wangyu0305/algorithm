#ifndef _HTTP_REQUEST__H_
#define _HTTP_REQUEST__H_

#include "head.h"

class http_request
{
public:
    http_request(rtk_string_t* nrequest);
    ~http_request();
    void get_urlname_from_request(rtk_string_t* urlname);
private:
    rtk_string_t request;


};

#endif