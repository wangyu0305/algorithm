#include "test.h"
#include "data_stream.h"
#include "http_request.h"
#include "proxy_pool.h"
#include "ctrl.h"

extern rtk_threads_t threads;

//void test_data_stream(void)
//{
//    int res;
//    rtk_socket_t socket_server;
//    proxy_pool handle_proxy_pool;
//
//    rtk_socket_initialize();
//    rtk_socket_init(&socket_server);
//    ctrl_t* ctrl = (ctrl_t*)rtk_malloc(sizeof(ctrl_t)*NUM);
//    res = socket_server_listen(&socket_server, 3456); 
//    if ( res == SUCCESS )   
//    { 
//        rtk_threads_expand(&threads,NUM);
//        for(int i=0;i<threads.count;i++)
//        {
//            ctrl[i].socket_server = &socket_server;
//            ctrl[i].threadid = i;
//            ctrl[i].handle_proxy_pool = &handle_proxy_pool;
//            rtk_thread_create(&threads.array[i], work, &ctrl[i]);
//        }
//    }
//    for(int i=0;i<threads.count;i++)
//    {
//        rtk_thread_join(&threads.array[i]);
//        rtk_thread_close(&threads.array[i]);
//    }
//
//    rtk_socket_finalize();
//    rtk_socket_close(&socket_server);
//    rtk_socket_term(&socket_server);
//    rtk_free(ctrl);
//}



void test_http_request(void)
{
    rtk_string_t str_current_dir;
    rtk_string_t request;
    rtk_string_t fname;
    rtk_string_t urlname;

    rtk_string_init(&str_current_dir);
    rtk_string_init(&request);
    rtk_string_init(&fname);
    rtk_string_init(&urlname);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&fname, str_current_dir.buffer, "http_request.txt");    
    rtk_file_get_string(fname.buffer, &request);
    http_request handle_http_request(&request);
    handle_http_request.get_urlname_from_request(&urlname);
    
    rtk_string_term(&str_current_dir);
    rtk_string_term(&request);
    rtk_string_term(&fname);
    rtk_string_term(&urlname);
}

void test_socket_server()
{
    rtk_socket_t socket;
    rtk_socket_t *psocket;
    rtk_stream_t recv_param;
    rtk_stream_t send_param;
    rtk_stream_t buf;
    rtk_stream_t source;
    int recv_code,send_code;
    int rv;

    rtk_socket_initialize();
    rtk_socket_init(&socket);

    recv_code = send_code = -1;
    rtk_stream_init(&recv_param);
    rtk_stream_init(&send_param);
    rtk_stream_init(&buf);
    rtk_stream_init(&source);

    rtk_socket_open(&socket);

    socket.ipaddr = 0;
    socket.port = 1234;

    rtk_socket_listen(&socket);
    printf("socket listening...\n");

    
    
    while (true)
    {
        psocket = rtk_socket_accept(&socket);
        printf("accept socket success!\n");
        recv_code = 1;
        rv = rtk_socket_recv_message(psocket,65535,&recv_param);
        rtk_file_get_stream("sohu.htm", &source);

        int r = rtk_socket_send_message(psocket,&source);
   
        rtk_socket_term(psocket);
        //rtk_free(psocket);
    } 
    printf("socket disconnect!\n");

    rtk_socket_close(&socket);

    rtk_stream_term(&recv_param);
    rtk_stream_term(&send_param);
    rtk_socket_term(&socket);
    rtk_stream_term(&buf);
    rtk_socket_finalize();
    rtk_stream_term(&source);
}

void test_proxy_pool(void)
{
    proxy_pool handle_proxy_pool;
    rtk_string_t str_current_dir;
    single_proxy_t* single_proxy; 

    rtk_string_init(&str_current_dir);

    rtk_dir_cwd(&str_current_dir);
    handle_proxy_pool.proxy_pool_get_from_profile(str_current_dir.buffer, "proxy.lst");
    int index = 0;
    do
    {
        single_proxy = handle_proxy_pool.get_and_remove_head_proxy();
        index++;
    } while (single_proxy);
    rtk_string_term(&str_current_dir);
}