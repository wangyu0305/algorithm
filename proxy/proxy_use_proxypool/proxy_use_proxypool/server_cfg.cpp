#include "server_cfg.h"
#include "stk_cfg.h"

void server_cfg_init(server_cfg_t* server_cfg)
{
    server_cfg->get_and_check_proxy = 1;
    server_cfg->port = 7777;
    server_cfg->thread_num = 10;
    rtk_string_init(&server_cfg->subdir);
    rtk_string_init(&server_cfg->supdir);
}

void server_cfg_term(server_cfg_t* server_cfg)
{
    server_cfg->get_and_check_proxy = -1;
    server_cfg->port = -1;
    server_cfg->thread_num = -1;
    rtk_string_term(&server_cfg->subdir);
    rtk_string_term(&server_cfg->supdir);
}

void server_cfg_get_from_profile(server_cfg_t* server_cfg, char* supdir, char* subdir)
{
    rtk_string_t context;
    rtk_string_t fname;
    rtk_string_t str_current_dir;

    rtk_string_init(&context);
    rtk_string_init(&fname);
    rtk_string_init(&str_current_dir);

    rtk_dir_merge(&fname, supdir, subdir);
    rtk_file_get_string(fname.buffer, &context);
    server_cfg->get_and_check_proxy = stk_cfg_get_number("get_and_check_proxy", &context, 0);
    server_cfg->port = stk_cfg_get_number("port", &context, 8888);
    server_cfg->thread_num = stk_cfg_get_number("thread_num", &context, 15);

    rtk_dir_cwd(&str_current_dir);
    stk_cfg_get_text("supdir",&context,&server_cfg->supdir,str_current_dir.buffer);
    if (rtk_string_is_empty(&server_cfg->supdir) == 1)
        rtk_string_copy(&server_cfg->supdir, str_current_dir.buffer, str_current_dir.length);
    stk_cfg_get_text("subdir", &context, &server_cfg->subdir, "proxy.lst");

    rtk_string_term(&context);
    rtk_string_term(&fname);
    rtk_string_term(&str_current_dir);
}
