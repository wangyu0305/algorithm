#ifndef _STK_CFG_H_
#define _STK_CFG_H_

#ifdef __cplusplus
extern "C" {
#endif


#include <rtk_32.h>



struct stk_test_proxy_t
{
    rtk_string_t	str_proxy_ip;	//代理主机
    int				n_port;			//代理端口
    int				respone_time;	//响应时间
    double right;						//测试正确的次数
    double wrong;						//测试错误的次数
};

struct stk_test_url_t
{
    rtk_string_t str_url;			//待测试网页
    int n_max_out_number;			//该网页需要可用代理个数
    unsigned int ui_min_size;		// 网页最小合法值，返回网页大于该值，测试代理才有效
    rtk_string_t str_tag;           //待验证网页包含特征串
};

struct stk_proxy_site_t
{
    rtk_string_t str_url;			//待测试代理来源网页
    rtk_string_t str_regex;			//解析出代理需要的正则表达式
};

struct stk_test_proxy_lists_t
{
    rtk_lists_t	test_general_proxy_lists;	//待测试代理列表
    rtk_lists_t test_prior_proxy_lists; //优先测试的代理列表
};


struct stk_subsection_t
{
    rtk_string_t str_proxy_ip;
    int bit_count;
};

struct stk_timeout_t
{
    int send_timeout;
    int recv_timeout;
    int connect_time;
};

struct stk_proxy_handle_t
{
    long		update_time;		//更新时间间隔
    long		n_common;			//共用代理获取个数
    long		max_time;			//单次获取可用代理最大时间数
    long		is_sort;			//是否按代理可用率进行排序
    long		n_filter;			//过滤阈值
    long		is_common;			//是否找共用代理
    rtk_string_t common_dir;        //存放共用代理的目录名
    rtk_string_t common_file;		//存放共用代理的文件名
    rtk_lists_t test_url_lists;		//待测试网址列表
    rtk_lists_t proxy_site_lists; //带测试代理来源网页信息
    stk_test_proxy_lists_t* m_test_proxy_lists; //待验证的代理列表
    int flag;						//是否获取共用代理
    stk_timeout_t timeout;          //超时时间	
};


__declspec(dllexport) void stk_test_url_init(stk_test_url_t* m_test_url);
__declspec(dllexport) void stk_test_url_term(stk_test_url_t* m_test_url);
__declspec(dllexport) void stk_proxy_handle_init(stk_proxy_handle_t* m_effect_proxy_cfg);
__declspec(dllexport) void stk_proxy_handle_term(stk_proxy_handle_t* m_effect_proxy_cfg);
__declspec(dllexport) void stk_proxy_handle_reset(stk_proxy_handle_t* m_effect_proxy_cfg);
__declspec(dllexport) void stk_proxy_handle_get_profile(stk_proxy_handle_t *cfg, const char *supdir, const char *subdir);
__declspec(dllexport) void stk_proxy_handle_set_profile(stk_proxy_handle_t *cfg, const char *supdir, const char *subdir);
__declspec(dllexport) void stk_test_proxy_lists_init(stk_test_proxy_lists_t* m_test_proxy_lists);
__declspec(dllexport) void stk_test_proxy_lists_term(stk_test_proxy_lists_t* m_test_proxy_lists);
__declspec(dllexport) void stk_test_proxy_lists_get_profile(stk_test_proxy_lists_t* m_test_proxy_lists, const char* supdir, const char* subdir);
__declspec(dllexport) void stk_test_proxy_lists_set_profile(stk_test_proxy_lists_t* m_test_proxy_lists, const char* supdir, const char* subdir);
__declspec(dllexport) void stk_test_proxy_list_to_string(rtk_lists_t* test_proxy_lists,rtk_string_t* content);
__declspec(dllexport) void stk_test_proxy_init(stk_test_proxy_t* m_test_proxy);
__declspec(dllexport) void stk_test_proxy_term(stk_test_proxy_t* m_test_proxy);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


void stk_test_proxy_copy(stk_test_proxy_t* m_test_proxy, stk_test_proxy_t* m_ntest_proxy);
void stk_test_proxy_reset(stk_test_proxy_t* m_test_proxy);
void stk_test_proxy_get_key(stk_test_proxy_t* m_test_proxy, rtk_string_t* str_key);
void stk_proxy_site_init(stk_proxy_site_t* m_proxy_site);
void stk_proxy_site_term(stk_proxy_site_t* m_proxy_site);
void stk_subsection_init(stk_subsection_t* m_subsection);
void stk_subsection_term(stk_subsection_t* m_subsection);
void stk_subsection_get_profile(rtk_lists_t* lists_subsection, char* supdir, char*subdir);
void stk_subsection_from_string(rtk_string_t* string, stk_subsection_t* m_subsection);


int stk_cfg_get_next_val(int *pos,rtk_string_t *content,rtk_string_t *val);
int stk_cfg_get_next_tag(int npos,rtk_string_t* context,rtk_string_t* strtag,int* rtag,int* nbegin,int* nend);
int stk_cfg_get_key_value(rtk_string_t* context,rtk_string_t* name,rtk_string_t* value);
int stk_cfg_find_next_tag(int npos,rtk_string_t* context,char* strtag,int bTag,int* nbegin,int* nend);
int stk_cfg_get_value(char* strtag,rtk_string_t* context,rtk_string_t* value);
int stk_cfg_get_list(char *name,rtk_string_t *context,rtk_string_t *content);
void stk_cfg_set_list(char *name,rtk_string_t *context,rtk_string_t *content);
void stk_cfg_get_text(char* name,rtk_string_t* context,rtk_string_t* strvalue, char* strdefault);
int stk_cfg_get_number(char* name,rtk_string_t* context,int ndefault);


void stk_proxy_list_from_string(rtk_lists_t* test_proxy_lists, rtk_string_t* content);
void stk_test_proxy_from_string(stk_test_proxy_t* m_test_proxy,rtk_string_t* string);
void stk_url_list_from_string(rtk_lists_t* test_url_lists, rtk_string_t* content);
void stk_test_url_from_string(stk_test_url_t* m_test_url,rtk_string_t* string);
void stk_test_url_list_to_string(rtk_lists_t* test_url_lists,rtk_string_t* content);
void stk_test_url_to_string(stk_test_url_t* m_test_url, rtk_string_t* string);
void stk_test_proxy_to_string(stk_test_proxy_t* m_test_proxy,rtk_string_t* string);
void stk_proxy_sites_list_from_string(rtk_lists_t* proxy_site_lists, rtk_string_t* content);
void stk_proxy_site_from_string(stk_proxy_site_t* m_proxy_site, rtk_string_t* string);
void stk_proxy_sites_list_to_string(rtk_lists_t* proxy_site_lists, rtk_string_t* content);
void stk_proxy_site_to_string(stk_proxy_site_t* m_proxy_site,rtk_string_t* string);

void stk_proxy_prior_from_general(rtk_lists_t* source_lists, rtk_hash_t* m_test_proxy_hash, rtk_lists_t* des_lists);

#ifdef __cplusplus
}
#endif

#endif // _STK_CFG_H_