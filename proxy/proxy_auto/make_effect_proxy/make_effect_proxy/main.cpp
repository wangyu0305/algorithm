#include "stk_effect_proxy.h"
#include "get_proxy_ip.h"
#include "time_fun.h"
#include "log.h"
#include "test.h"
#include "merge_proxy_file.h"
#include "tools.h"
#include <dos.h>
#include <windows.h>

void work(void *param);
bool m_close;

clock_t m_clock;
stk_proxy_js_t proxyjs;

void test_file_time();
int main()
{
    //test_file_time();
    rtk_thread_t thread;

    rtk_thread_init(&thread);

    m_close = true;
    rtk_thread_create(&thread, work, 0);
    
	
    int flag = 1;
    while (1)
    {
        printf("停止请输入0\n");
        scanf("%d", &flag);
        if (flag == 0)
        {
            m_close = false;
		          break;
		      }
	   }
    printf("请等待此轮测试结束后推出");
	
    rtk_thread_join(&thread);
    rtk_thread_close(&thread);

    rtk_thread_term(&thread);
    
    return 0;
}

void work(void *param)
{
    stk_proxy_get_persist();
}

void proxy_merge()
{
	char filea[50];
	char fileb[50];
	printf("请将需合并文件放在当前目录\n");
	printf("输入第一个文件名\n");
	scanf("%s",filea);
	printf("输入第二个文件名\n");
	scanf("%s",fileb);
	get_proxy_list_from_profile(filea,fileb);
	printf("合并完成\n");
}

void test_file_time()
{
	proxy_merge();
    time_t tm;
    get_file_last_modify_time("inet_whl.cpp", &tm);
    struct tm * timeinfo;
   // time(&t);

    timeinfo = localtime ( &tm );

    return;
}