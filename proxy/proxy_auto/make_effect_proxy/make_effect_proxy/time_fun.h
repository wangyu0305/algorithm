#ifndef _TIME_FUN__H_
#define _TIME_FUN__H_

#include <rtk_32.h>
#include <time.h>  


__declspec(dllexport) void get_current_time(rtk_string_t* str_time);


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void get_current_time_for_filename(rtk_string_t* str_filename);
void rtk_string_append_time(rtk_string_t* str_log_info);
void get_file_last_modify_time(char* fname, time_t* tm);

#endif