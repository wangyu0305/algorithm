#ifndef _TEST__H_
#define _TEST__H_

#include "stk_effect_proxy.h"
#include "get_proxy_ip.h"
#include "time_fun.h"
#include "log.h"
#include <dos.h>


void test_stk_proxy_list_append_from_string_no_dup();
void test_clock();
void test_stk_get_test_proxy_boost_regex_from_str();

#endif