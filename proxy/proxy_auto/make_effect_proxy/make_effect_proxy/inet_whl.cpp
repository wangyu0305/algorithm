#include "inet_whl.h"
#include "stk_inet.h"
#include <stdlib.h>
#include <string>

using namespace std;

void get_objname(rtk_string_t* urlname,rtk_string_t* hostname,rtk_string_t* objname)
{
	int pos;
	if ((pos = rtk_string_find(urlname,"http://",-1)) == -1)
	{
		pos = rtk_string_find_char(urlname,'/');
	}
	else
	{
		pos += 7;
		if ((pos = rtk_string_find_pos(urlname,"/",-1,pos+1)) == -1)
		{
			rtk_string_copy(hostname,urlname->buffer,-1);
            rtk_string_copy(objname,"/",-1);
			return ;
		}
		rtk_string_mid_string(urlname,7,pos-7,hostname);
		rtk_string_mids_string(urlname,pos,objname);
		return ;
	}
	rtk_string_left_string(urlname,pos,hostname);
	rtk_string_mids_string(urlname,pos,objname);

}

bool effect_ip(char* ip)
{
	string ipaddr(ip);
	string subip;
	int numip;
	string::size_type pos;
	string::size_type posi;
	posi = 0;
	for (int i = 0;i < 3;i++)
	{
		pos = ipaddr.find('.',posi);
		if (pos == string::npos)
			break;
		subip = ipaddr.substr(posi,pos-posi);
		numip = atoi(subip.c_str());
		if (numip > 255)
			return false;
		posi = pos+1;
	}
	pos = ipaddr.rfind('.');
	subip = ipaddr.substr(pos+1,ipaddr.size()-(pos+1));
	numip = atoi(subip.c_str());
	if (numip > 255)
		return false;
	
	return true;
}

int stk_get_url_by_proxy(char* sz_urlname, int max, stk_test_proxy_t* m_test_proxy, stk_timeout_t* timeout, rtk_stream_t* source, stk_respone_info_t* m_respone_info)
{
    WSADATA WSAData={0}; 
    SOCKET sockfd; 
    struct sockaddr_in addr; 
    struct hostent *pURL; 
    char sz_format_url[BUFSIZ]; 
    char *pHost = 0, *pGET = 0; 
    char host[BUFSIZ], GET[BUFSIZ]; 
    char header[BUFSIZ] = ""; 
    char text[BUFSIZ]; 
    int ret; 
	int connect_count = 0;
    rtk_string_t urlname;
    rtk_string_t hostname;
    rtk_string_t objectname;
    unsigned short port;
		
    clock_t total_start = clock();

    rtk_string_init(&urlname);
    rtk_string_init(&hostname);
    rtk_string_init(&objectname);
	
//  windows下使用socket必须用WSAStartup初始化，否则不能调用 
 
    if(WSAStartup(MAKEWORD(2,2), &WSAData)) 
    { 
         return 0; 
    }  
 //	分离url中的主机地址和相对路径 
    rtk_string_copy(&urlname, sz_urlname, -1);
    //stk_inet_parse_url(&urlname, &hostname, &port, &objectname);
	get_objname(&urlname,&hostname,&objectname);
    if (!effect_ip(m_test_proxy->str_proxy_ip.buffer))
    {
		 WSACleanup(); 
		 return 0;
    }
    strcpy(host, hostname.buffer); 
//  设定socket参数,并未真正初始化 
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); 
    pURL = gethostbyname(m_test_proxy->str_proxy_ip.buffer);

    addr.sin_family = AF_INET; 
    addr.sin_addr.S_un.S_addr = *((unsigned long*)pURL->h_addr); 
    addr.sin_port = htons(m_test_proxy->n_port); 
//  组织发送到web服务器的信息 
//  为何要发送下面的信息请参考HTTP协议的约定 
    format_url(sz_format_url, sz_urlname);
    strcat(header, "GET ");
    strcat(header, sz_format_url);
   /* if ( rtk_string_compare(&objectname, "/",  -1 ) == 0)
	       strcat(header, "/");
	else
	       strcat(header,objectname.buffer);*/
    strcat(header, " HTTP/1.1\r\n"); 
	strcat(header,"Accept: */*\r\n");
	strcat(header,"Referer: http://");
	strcat(header,host);
	strcat(header,"\/\r\nAccept-Language: zh-CN\r\n");
	strcat(header,"User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.2)\r\n");
    strcat(header, "Host: "); 
    strcat(header, host); 
    strcat(header, "\r\nCache-Control: no-cache");
    strcat(header, "\r\nConnection: Close\r\n\r\n"); 
//  连接到服务器，发送请求header，并接受反馈（即网页源代码） 
 
    int nSendTimeout = timeout->send_timeout * 1000;
    int nRecvTimeout = timeout->recv_timeout * 1000;
    setsockopt(sockfd, SOL_SOCKET,SO_SNDTIMEO, (char *)&nSendTimeout,sizeof(int));
    setsockopt(sockfd, SOL_SOCKET,SO_RCVTIMEO, (char *)&nRecvTimeout,sizeof(int));

    unsigned long ul = 1;
    ret = ioctlsocket(sockfd, FIONBIO, (unsigned long*)&ul);
    if(ret==SOCKET_ERROR)
    {
	//	printf("ioctlsocket failed with error %d\n", WSAGetLastError());
	//	return 0;
    }
    clock_t connect_start = clock();
    ret = connect(sockfd,(SOCKADDR *)&addr,sizeof(addr)); 
    if(ret==SOCKET_ERROR)
    {
	    //printf("connect failed with error %d\n", WSAGetLastError());
    }

    struct timeval timeout1 ;
    fd_set r;
    FD_ZERO(&r);
    FD_SET(sockfd, &r);
    timeout1.tv_sec = timeout->connect_time; 
    timeout1.tv_usec =0;
    ret = select(0, 0, &r, 0, &timeout1);
    clock_t connect_end = clock();
    m_respone_info->connect_time = connect_end - connect_start;
    if ( ret <= 0 )
    {
        m_respone_info->error = CONNECT_TIMEOUT;
    }
    else
    {
        unsigned long ul1= 0 ;
        ret = ioctlsocket(sockfd, FIONBIO, (unsigned long*)&ul1);
        if(ret==SOCKET_ERROR)
        {
            m_respone_info->error = IOCTLSOCKET;
        }
        else
        {
            clock_t send_start = clock();
            ret = send(sockfd, header, strlen(header), 0); 
            clock_t send_end = clock();
            m_respone_info->send_time = send_end - send_start;
            if(ret==SOCKET_ERROR)
            {
                m_respone_info->error = SEND_TIMEOUT;
            }
		
            clock_t recv_start = clock();
            clock_t recv_end = clock();
            while ( true ) 
            {  
				            memset(text, 0, BUFSIZ);
							recv_start = clock();
   		           ret = recv(sockfd, text, BUFSIZ, 0);
			             recv_end = clock();
				            m_respone_info->recv_time = recv_end - recv_start;
			             if (ret == 0)
			             {
				                m_respone_info->error = RIGHT;
				                break;
			             }
			
		    	         if ( (recv_end - recv_start) > timeout->recv_timeout*1000 )
			             {
				                m_respone_info->error = RECV_TIMEOUT;
				                rtk_stream_shrink(source, 0);
			                 break;
			             }
			             if (ret == SOCKET_ERROR)
			             {
			                 int wsa = WSAGetLastError();
				                if (wsa == 10060)
								{
									if (connect_count > 5)
									{
										break;
									}
									   connect_count++;
					                   continue;
								}
				                else 
				                {
						                  m_respone_info->error = RECV_ERROR;
						                  m_respone_info->errorcode = wsa;
					                   rtk_stream_shrink(source, 0);
					                   break;
				                }
				            }
				            rtk_stream_append(source, (BYTE*)text, sizeof(text));
			             if (source->length > max)
			             {
				                m_respone_info->error = ENOUGH;
			
			               		break;
			             }
			        }
        }  
        if (rtk_stream_is_empty(source) != 1)    
			         get_status(source, &m_respone_info->status);
		      else
			         m_respone_info->status = -1;

	    }

    clock_t total_end = clock();
    m_respone_info->respone_time = total_end - total_start;
    m_respone_info->size = source->length;
    
    closesocket(sockfd); 
    WSACleanup(); 
    rtk_string_term(&objectname);
    rtk_string_term(&hostname);
    rtk_string_term(&urlname);
    return m_respone_info->status;
}


int format_url(char* sz_format_url, char* sz_urlname)
{
    char* p = strstr(sz_urlname, "http://");
    if (p)
        strcpy(sz_format_url, sz_urlname);
    else
    {
        sprintf(sz_format_url, "%s%s", "http://", sz_urlname);
    }
    return 0;
}

void get_status(rtk_stream_t* source, int* status)
{
    int pos1, pos2;
    rtk_string_t str_status;

    rtk_string_init(&str_status);
    pos1 = rtk_stream_find(source, ' ');
    pos2 = rtk_stream_find_pos(source, ' ', pos1+1);
    rtk_stream_mid_string(source, pos1+1, pos2-pos1-1, &str_status);

    *status = rtk_string_get_number(&str_status, -1);
    rtk_string_term(&str_status);
}