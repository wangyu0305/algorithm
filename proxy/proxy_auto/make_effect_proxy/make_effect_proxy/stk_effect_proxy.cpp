#include "stk_effect_proxy.h"

#include "stk_inet.h"
#include "get_proxy_ip.h"
#include "log.h"
#include "inet_whl.h"
#include <time.h>
#include "tools.h"
#include "time_fun.h"

#define MSG_WAITALL     0x8

#define RETRIES 3
#define TIMEOUT 1
#define M 0

extern bool m_close;
extern clock_t m_clock;
extern stk_proxy_js_t proxyjs;

void stk_proxy_get_persist()
{
    stk_proxy_handle_t m_proxy_handle;
    rtk_string_t str_current_dir;
    rtk_string_t str_time;
    rtk_string_t str_log_info;
    rtk_string_t fname;

	stk_proxy_js_init(&proxyjs);
	stk_proxy_compile_script(&proxyjs,"effect_proxy.cfg");
    stk_proxy_handle_init(&m_proxy_handle);
    rtk_string_init(&str_current_dir);
    rtk_string_init(&str_time);
    rtk_string_init(&str_log_info);
    rtk_string_init(&fname);

    rtk_dir_cwd(&str_current_dir);
    rtk_string_copy(&str_log_info, "\r\n\r\nSTART\t", -1);
    rtk_string_append_number(&str_log_info, m_clock);
    m_clock = clock();
    rtk_string_append(&str_log_info, "\r\n",-1);
    stk_proxy_detail_log_set(&str_log_info);

#ifdef COMMON_PROXY
	while (m_close)
	{
		stk_proxy_handle_reset(&m_proxy_handle);
		stk_proxy_handle_get_profile(&m_proxy_handle, str_current_dir.buffer, "effect_proxy.cfg");
		stk_available_proxy_output(&m_proxy_handle, (stk_effect_proxy_print_t)stk_effect_proxy_print_test, \
			m_proxy_handle.common_dir.buffer, m_proxy_handle.common_file.buffer);
		stk_proxy_detail_log_set("Sleeping...\r\n");
		Sleep(60*1000*m_proxy_handle.update_time);
		stk_proxy_detail_log_set("Wakeing...\r\n");
	}
#else
	do 
	{
		stk_proxy_handle_reset(&m_proxy_handle);
		stk_proxy_handle_get_profile(&m_proxy_handle, str_current_dir.buffer, "effect_proxy.cfg");
		stk_available_proxy_output(&m_proxy_handle, (stk_effect_proxy_print_t)stk_effect_proxy_print_test, \
			m_proxy_handle.common_dir.buffer, m_proxy_handle.common_file.buffer);
		stk_proxy_detail_log_set("Sleeping...\r\n");
		Sleep(60*1000*m_proxy_handle.update_time);
		stk_proxy_detail_log_set("Wakeing...\r\n");
	} while (0);
#endif
	   
    stk_proxy_detail_log_set("END\r\n");
    if (proxyjs.use_js != 0)
		stk_proxy_js_engine_destory(&proxyjs);
	stk_proxy_js_term(&proxyjs);
    rtk_string_term(&str_time);
    stk_proxy_handle_term(&m_proxy_handle);
    rtk_string_term(&str_current_dir);
    rtk_string_term(&str_log_info);
    rtk_string_term(&fname);
    
}

void stk_respone_info_init(stk_respone_info_t* m_respone_info)
{
    m_respone_info->is_effect = false;
    m_respone_info->m_test_proxy = NULL;
    m_respone_info->respone_time = 0;
    m_respone_info->size = 0;
    m_respone_info->status = 0;
    m_respone_info->is_tag_in = false;
    m_respone_info->recv_time = 0;
    m_respone_info->send_time = 0;
    m_respone_info->connect_time = 0;
}
void stk_respone_info_term(stk_respone_info_t* m_respone_info)
{

}

void stk_respone_info_reset(stk_respone_info_t* m_respone_info)
{
    m_respone_info->is_effect = false;
    m_respone_info->m_test_proxy = NULL;
    m_respone_info->respone_time = 0;
    m_respone_info->size = 0;
    m_respone_info->status = 0;
    m_respone_info->is_tag_in = false;
}


void stk_proxy_hash_init(rtk_hash_t* m_proxy_hash)
{
    rtk_hash_init(m_proxy_hash, true, false);
}

void stk_proxy_hash_term(rtk_hash_t* m_proxy_hash)
{
    rtk_hash_term(m_proxy_hash);
}

int stk_proxy_hash_find(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy)
{
    rtk_string_t str_key;
    rtk_string_init(&str_key);

    stk_test_proxy_get_key(m_test_proxy, &str_key);
    int flag = rtk_hash_find(m_proxy_hash, str_key.buffer, str_key.length);

    rtk_string_term(&str_key);
    return flag;
}

int stk_proxy_hash_insert(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy)
{
	
    rtk_string_t str_key;
    rtk_string_init(&str_key);

    stk_test_proxy_get_key(m_test_proxy, &str_key);
    rtk_hash_insert(m_proxy_hash, str_key.buffer, str_key.length, m_test_proxy);
    rtk_string_term(&str_key);
    return 0;
}

stk_test_proxy_t* stk_proxy_hash_get(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy)
{
    stk_test_proxy_t* presult;
    rtk_string_t str_key;
    rtk_string_init(&str_key);

    stk_test_proxy_get_key(m_test_proxy, &str_key);
    presult = (stk_test_proxy_t*)rtk_hash_get(m_proxy_hash, str_key.buffer, str_key.length);

    rtk_string_term(&str_key);
    return presult;
}


void set_respone(stk_http_t* http,stk_respone_info_t* m_respone_info)
{
	switch (http->resp.errcode)
	{
	case 0:
		m_respone_info->error = ENOUGH;
		break;
	case 2001:
		m_respone_info->error = STATUS_ERROR;
		break;
	case 2002:
		m_respone_info->error = IOCTLSOCKET;
		break;
	case 5006:
		m_respone_info->error = RECV_TIMEOUT;
		break;
	case 5001:
		m_respone_info->error = RECV_ERROR;
		break;
	case 5002:
		m_respone_info->error = NO_TAG;
		break;
	case 5003:
		m_respone_info->error = SIZE_ERROR;
		break;
	default:
		m_respone_info->error = 100;
		break;
	}
	m_respone_info->status = http->resp.status;
}


//用一个带验证代理去测试一个待验证网页，存储得到的相应信息
void stk_effect_proxy_get_single_url_single_proxy(stk_test_url_t* m_test_url, stk_test_proxy_t* m_test_proxy, \
												  stk_timeout_t* timeout, stk_respone_info_t* m_respone_info)
{

	stk_http_t		m_inet;
	stk_session_t	session;

    rtk_string_t content;
    rtk_string_t str_log_info;

    int status;

    rtk_socket_initialize();
	stk_session_init(&session);
	stk_http_init(&m_inet);
	m_inet.session = &session;
    rtk_string_init(&content);
    rtk_string_init(&str_log_info);

	
    stk_respone_info_reset(m_respone_info);
    stk_proxy_log_down_begin_get(&str_log_info, m_test_proxy, m_test_url);    
    stk_proxy_detail_log_set(&str_log_info);
    int max = 1024*10;
	
    status = 0;


    //stk_get_url_by_proxy(m_test_url->str_url.buffer, max, m_test_proxy, timeout, &source, m_respone_info);

	rtk_string_copy(&m_inet.opt.proxy_server,m_test_proxy->str_proxy_ip.buffer,-1);
	m_inet.opt.proxy_port = m_test_proxy->n_port;

	clock_t total_start = clock();
	if (effect_ip(m_test_proxy->str_proxy_ip.buffer))
	{
		m_inet.socket->recv_timeout = timeout->recv_timeout;
		m_inet.socket->connect_timeout = timeout->connect_time;
		m_inet.socket->send_timeout = timeout->send_timeout;
		stk_http_get(&m_inet,m_test_url->str_url.buffer,&content);
	}
    
	clock_t total_end = clock();
	set_respone(&m_inet,m_respone_info);
	m_respone_info->recv_time = m_respone_info->respone_time = total_end - total_start;
	m_respone_info->size = content.length;
	if (content.length < 2048)
	    m_respone_info->error = NOTENOUGH;
	
 
    stk_proxy_log_down_end_get(&str_log_info, m_test_proxy, m_test_url);
    stk_proxy_detail_log_set(&str_log_info);
    other_code_to_GB(&content);
	
	
	
    if ( rtk_string_icompare(&m_test_url->str_tag, "NULL", -1) == 0 )
        m_respone_info->is_tag_in = true;
    else
    {
	       int pos = rtk_string_find(&content, m_test_url->str_tag.buffer, m_test_url->str_tag.length);
	       if ( pos != -1 )
		   {
		      m_respone_info->is_tag_in = true;
			   m_respone_info->is_effect = true;
		   }
	       else
		   {
		          m_respone_info->is_tag_in = false;
		          m_respone_info->is_effect = false;
		   }
	   }
    
    m_respone_info->m_test_proxy = m_test_proxy;

    rtk_socket_finalize();
	stk_session_term(&session);
	stk_http_term(&m_inet);
    rtk_string_term(&content);
    rtk_string_term(&str_log_info);
}

//按相应时间对代理进行比较
int stk_test_proxy_compare_respone_time(stk_test_proxy_t* p1, stk_test_proxy_t* p2)
{
    int flag = p1->respone_time - p2->respone_time;
    if (flag == 0)
        flag = 1;
    return flag;
}



//将stk_test_proxy_t*中的代理结构，转换为host;ip形式
void stk_effect_proxy_to_string(stk_test_proxy_t* m_test_proxy, rtk_string_t* string)
{
    rtk_string_shrink(string, 0);
    rtk_string_append_char(string, '\"');
    rtk_string_append(string, m_test_proxy->str_proxy_ip.buffer, m_test_proxy->str_proxy_ip.length);
    rtk_string_append_char(string, '\"');
    rtk_string_append_char(string, ';');
    rtk_string_append_number(string, m_test_proxy->n_port);
    rtk_string_append_char(string, ';');
}

int stk_available_proxy_arrays_get(stk_proxy_handle_t* m_proxy_handle, rtk_arrays_t* arrays_proxys)
{
    rtk_hash_t m_proxy_hash;
    clock_t start = clock();
    int flag = 0;
    rtk_string_t str_proxy_supdir;
    rtk_string_t str_current_dir;

    stk_proxy_hash_init(&m_proxy_hash);
    rtk_string_init(&str_proxy_supdir);
    rtk_string_init(&str_current_dir);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&str_proxy_supdir, (char*)str_current_dir.buffer, "proxy_temps");

    flag = stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, start, arrays_proxys);
	
    if (flag == 0)
    {
        stk_proxy_get_test_lists_from_cache_file(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists, \
			     str_proxy_supdir.buffer, "down_cache.lst");
        flag = stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, start, arrays_proxys);
   	}

//	重代理网站上下载待测试代理
    if (flag == 0 /*|| flag == 1*/)
    {
        stk_test_proxy_get_from_sites(&m_proxy_handle->proxy_site_lists, str_proxy_supdir.buffer, "down_cache.lst");
        stk_proxy_get_test_lists_from_cache_file(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists, \
        str_proxy_supdir.buffer, "down_cache.lst");

        flag = stk_effect_get_proxy_lists(m_proxy_handle, &m_proxy_hash, start, arrays_proxys);
    }
    
    stk_proxy_hash_term(&m_proxy_hash);
    rtk_string_term(&str_proxy_supdir);
    rtk_string_term(&str_current_dir);
    return flag;
}

//获取共用代理接口函数
void stk_available_proxy_output(stk_proxy_handle_t* m_cfg, stk_effect_proxy_print_t stk_effect_proxy_print, char* supdir, char* subdir)
{
    rtk_arrays_t arrays_effect_proxy_common;
    rtk_string_t str_current_dir;
    rtk_string_t str_proxy_supdir;
    stk_test_proxy_lists_t m_test_proxy_lists;
    rtk_string_t str_log;
    clock_t start;
    int flag;

    
    rtk_arrays_init(&arrays_effect_proxy_common, false);
    rtk_string_init(&str_current_dir);
    rtk_string_init(&str_proxy_supdir);
    stk_test_proxy_lists_init(&m_test_proxy_lists);
    rtk_string_init(&str_log);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&str_proxy_supdir, (char*)str_current_dir.buffer, "proxy_temps");
    rtk_dir_make((char*)str_current_dir.buffer, "proxy_temps");

    stk_test_proxy_lists_get_profile(&m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");
    m_cfg->m_test_proxy_lists = &m_test_proxy_lists;


    flag = stk_available_proxy_arrays_get(m_cfg, &arrays_effect_proxy_common);

    int number_effect_proxy = arrays_effect_proxy_common.size;
    stk_effect_proxy_update_prior_proxy(&arrays_effect_proxy_common, &m_cfg->m_test_proxy_lists->test_prior_proxy_lists);
    stk_effect_proxy_remove_spilth(&arrays_effect_proxy_common, m_cfg->n_common);
    stk_effect_proxy_print(&arrays_effect_proxy_common, supdir, subdir);
    stk_proxy_lists_filter(&m_cfg->m_test_proxy_lists->test_general_proxy_lists, m_cfg->n_filter);
    stk_test_proxy_lists_set_profile(m_cfg->m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");

    rtk_string_t str_time_filename;
    rtk_string_init(&str_time_filename);
    get_current_time_for_filename(&str_time_filename);
	//stk_test_proxy_lists_set_profile(m_cfg->m_test_proxy_lists, str_current_dir.buffer, str_time_filename.buffer);
    rtk_string_init(&str_time_filename);
    stk_effect_proxy_log_single_res(flag, number_effect_proxy, str_current_dir.buffer);
 
    rtk_arrays_term(&arrays_effect_proxy_common);
    rtk_string_term(&str_current_dir);
    rtk_string_term(&str_proxy_supdir);
    stk_test_proxy_lists_term(&m_test_proxy_lists);
    rtk_string_init(&str_log);
}

int stk_effect_get_proxy_lists(stk_proxy_handle_t* m_proxy_handle, rtk_hash_t* m_proxy_hash, clock_t start, 
							   rtk_arrays_t* arrays_effect_proxy_common)
{
    rtk_string_t str_proxy_supdir;
    int flag;

    rtk_string_init(&str_proxy_supdir);

    stk_get_proxy_supdir(&str_proxy_supdir);
   	#ifndef debug
	   stk_test_proxy_lists_set_profile(m_proxy_handle->m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");
    #endif	
    flag = stk_effect_proxy_get(&m_proxy_handle->m_test_proxy_lists->test_prior_proxy_lists, m_proxy_handle, arrays_effect_proxy_common, m_proxy_hash, start);
    if ( flag == 0 )
    {
        stk_proxy_subsection_get(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists);
        #ifndef debug
	       stk_test_proxy_lists_set_profile(m_proxy_handle->m_test_proxy_lists, str_proxy_supdir.buffer, "common.lst");
        #endif	
			     flag = stk_effect_proxy_get(&m_proxy_handle->m_test_proxy_lists->test_general_proxy_lists, m_proxy_handle, arrays_effect_proxy_common, m_proxy_hash, start);
	    }
    
    rtk_string_term(&str_proxy_supdir);
    return flag;
}

void stk_get_proxy_supdir(rtk_string_t* str_proxy_supdir)
{
    rtk_string_t str_current_dir;
    rtk_string_init(&str_current_dir);
    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(str_proxy_supdir, (char*)str_current_dir.buffer, "proxy_temps");
    rtk_dir_make((char*)str_current_dir.buffer, "proxy_temps");
    rtk_string_term(&str_current_dir);
}

int stk_effect_proxy_get(rtk_lists_t* lists_proxy, stk_proxy_handle_t* m_proxy_handle, \
						  rtk_arrays_t* arrays_effect_proxy_common, rtk_hash_t* m_proxy_hash, clock_t start)
{
    bool is_effect = false;
    stk_test_proxy_t* m_test_proxy;
    void* pos = rtk_lists_get_head_position(lists_proxy);
    clock_t end;

    while (pos)
    {
	       end = clock();
		      /*if ( (end - start) > m_proxy_handle->max_time*60*1000*10 )
			         return 1;*/
		      m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(lists_proxy, &pos);
        if (stk_proxy_hash_find(m_proxy_hash, m_test_proxy) == 1)
		      {
		
		      }
		      else
		      {
		          stk_proxy_hash_insert(m_proxy_hash, m_test_proxy);
			         is_effect = stk_effect_proxy_test_single_proxy_is_common(m_test_proxy, &m_proxy_handle->test_url_lists, &m_proxy_handle->timeout);
			         if (is_effect)
			         {
				            m_test_proxy->right++;
				            rtk_arrays_insert(arrays_effect_proxy_common, m_test_proxy, (rtk_arrays_compare_t)stk_test_proxy_compare_respone_time);
			             rtk_string_t str_log_info;
				            rtk_string_init(&str_log_info);
				            stk_proxy_log_test_proxy_get(&str_log_info, m_test_proxy, arrays_effect_proxy_common->size);
				            stk_proxy_detail_log_set(&str_log_info);
				            rtk_string_term(&str_log_info);
				            if (arrays_effect_proxy_common->size >= m_proxy_handle->n_common + M)
				                return 2;
			         }
			         else
			         {
				            m_test_proxy->wrong++;
			         }
		      }
    }
	if (arrays_effect_proxy_common->size > 0 && arrays_effect_proxy_common->size < m_proxy_handle->n_common + M)
		return 1;
    return 0;
}



bool stk_effect_proxy_test_single_proxy_is_common(stk_test_proxy_t* m_test_proxy, rtk_lists_t* test_url_lists, stk_timeout_t* timeout)
{
    bool flag = true;
    void* pos;
    stk_test_url_t* m_test_url;
    stk_respone_info_t m_respone_info;

    stk_respone_info_init(&m_respone_info);
    
    pos = rtk_lists_get_head_position(test_url_lists);
    while (pos)
    {
		
		      m_test_url = (stk_test_url_t*)rtk_lists_get_next(test_url_lists, &pos);
	       stk_effect_proxy_get_single_url_single_proxy(m_test_url, m_test_proxy, timeout, &m_respone_info);
	      	bool ok = stk_effect_proxy_test_respone_effect(&m_respone_info, m_test_url);
		      stk_proxy_detail_log_set_by_respone(&m_respone_info);
		      if (!ok)
		      {
		          flag = false;
			         break;
		      }
		      else
		      {
			         m_test_proxy->respone_time += m_respone_info.respone_time;
		      }
	   }

	   stk_respone_info_term(&m_respone_info);

    return flag;
}

void stk_proxy_detail_log_set_by_respone(stk_respone_info_t* m_respone_info)
{
    rtk_string_t str_log;

    rtk_string_init(&str_log);

    switch (m_respone_info->error)
    {
    case RIGHT:
		      rtk_string_copy(&str_log, "已完整下载\n", -1);
		      stk_proxy_detail_log_set(&str_log);
		      break;
	   case ENOUGH:
		      rtk_string_copy(&str_log, "已下载指定大小的内容\n", -1);
		      stk_proxy_detail_log_set(&str_log);
		      break;
	   case RECV_TIMEOUT:
        rtk_string_copy(&str_log, "接收超时\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    case RECV_ERROR:
        rtk_string_copy(&str_log, "接收错误\t", -1);
        rtk_string_append_number(&str_log, m_respone_info->errorcode);
        rtk_string_append_char(&str_log, '\n');
        stk_proxy_detail_log_set(&str_log);
        break;
    case SEND_TIMEOUT:
        rtk_string_copy(&str_log, "发送超时\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    case CONNECT_TIMEOUT:
        rtk_string_copy(&str_log, "连接超时\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    case IOCTLSOCKET:
        rtk_string_copy(&str_log, "socoket设置错误\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    case STATUS_ERROR:
        rtk_string_copy(&str_log, "响应报文状态吗错误\t", -1);
        rtk_string_append_number(&str_log, m_respone_info->errorcode);
        rtk_string_append_char(&str_log, '\n');
        stk_proxy_detail_log_set(&str_log);
        break;
    case NO_TAG:
        rtk_string_copy(&str_log, "发送http请求错误\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    case SIZE_ERROR:
        rtk_string_copy(&str_log, "网页错误\n", -1);
        stk_proxy_detail_log_set(&str_log);
        break;
    default:
        rtk_string_copy(&str_log, "无效代理\n", -1);
        stk_proxy_detail_log_set(&str_log);
    }
    rtk_string_shrink(&str_log, 0);
    rtk_string_append_number(&str_log, m_respone_info->connect_time);
    rtk_string_append_char(&str_log, '\t');
    rtk_string_append_number(&str_log, m_respone_info->send_time);
    rtk_string_append_char(&str_log, '\t');
    rtk_string_append_number(&str_log, m_respone_info->recv_time);
    rtk_string_append_char(&str_log, '\t');
    rtk_string_append_number(&str_log, m_respone_info->respone_time);
    rtk_string_append_char(&str_log, '\n');
    stk_proxy_detail_log_set(&str_log);

    rtk_string_term(&str_log);
}

void stk_effect_proxy_update_prior_proxy(rtk_arrays_t* arrays_effect_proxy_common, rtk_lists_t* test_prior_proxy_lists)
{
    stk_test_proxy_t* m_ntest_proxy;
    rtk_lists_remove_all(test_prior_proxy_lists);
    for (int i = 0; i < arrays_effect_proxy_common->size; i++)
    {
	       m_ntest_proxy = (stk_test_proxy_t*)rtk_arrays_get_at(arrays_effect_proxy_common, i);
	       rtk_lists_add_tail(test_prior_proxy_lists, m_ntest_proxy);
    }
}

bool stk_effect_proxy_test_respone_effect(stk_respone_info_t* m_respone_info, stk_test_url_t* m_test_url)
{
    rtk_string_t str_log;

    rtk_string_init(&str_log);
    m_respone_info->is_effect = true;
	
    if (m_respone_info->error == RIGHT || m_respone_info->error == ENOUGH)
    {
        do
        {
		          if (m_respone_info->status < 200 || m_respone_info->status >= 400)
		          {
		              m_respone_info->is_effect = false;
			             m_respone_info->error = STATUS_ERROR;
	               m_respone_info->errorcode = m_respone_info->status;
		              break;
		          }
		          if ( m_respone_info->size < m_test_url->ui_min_size)
		          {
				            m_respone_info->is_effect = false;
				            m_respone_info->error = SIZE_ERROR;
				            break;
		          }
		          if ( !m_respone_info->is_tag_in )
		          {
				            m_respone_info->is_effect = false;
		              m_respone_info->error = NO_TAG;
				            break;
		         }
		      }while (0);
	   }
	   else
	   {
	       m_respone_info->is_effect = false;
	   }

    return m_respone_info->is_effect;
}

void stk_proxy_list_to_hash_remove_dup(rtk_lists_t* test_proxy_lists, rtk_hash_t* m_proxy_hash)
{
    void* pos = rtk_lists_get_head_position(test_proxy_lists);
    void* cur_pos = NULL;
    stk_test_proxy_t* m_test_proxy;
    while (pos)
    {
        cur_pos = pos;
        m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(test_proxy_lists, &pos);
        if ( stk_proxy_hash_find(m_proxy_hash, m_test_proxy) == 1 )
        {
            rtk_lists_remove_at(test_proxy_lists, cur_pos);
        }
        else 
        {
            stk_proxy_hash_insert(m_proxy_hash, m_test_proxy);
        }
    }
}

void stk_proxy_list_remove_dup(rtk_lists_t* test_proxy_lists)
{
    rtk_hash_t m_proxy_hash;
    stk_proxy_hash_init(&m_proxy_hash);
    stk_proxy_list_to_hash_remove_dup(test_proxy_lists, &m_proxy_hash);
    rtk_hash_term(&m_proxy_hash);
}

void stk_proxy_list_append_from_string_no_dup(rtk_lists_t* lists_proxy, rtk_string_t* str_all_proxy)
{
    rtk_lists_t nlists_proxy;
    rtk_lists_init(&nlists_proxy, true, true);
    nlists_proxy.dpool->memsize = sizeof(stk_test_proxy_t);
    stk_proxy_list_from_string(&nlists_proxy, str_all_proxy);
    stk_proxy_list_append_from_list_no_dup(lists_proxy, &nlists_proxy);
    rtk_lists_free_all(&nlists_proxy, (rtk_lists_term_t)stk_test_proxy_term);
    rtk_lists_term(&nlists_proxy);
}

void stk_proxy_list_append_from_list_no_dup(rtk_lists_t* lists_proxy, rtk_lists_t* nlists_proxy)
{
    rtk_hash_t m_proxy_hash;

    stk_proxy_hash_init(&m_proxy_hash);
	

    stk_proxy_list_to_hash_remove_dup(lists_proxy, &m_proxy_hash);

    void* pos = rtk_lists_get_head_position(nlists_proxy);
    stk_test_proxy_t* m_test_proxy;
    while (pos)
    {
	       m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(nlists_proxy, &pos);
		      if ( stk_proxy_hash_find(&m_proxy_hash, m_test_proxy) == 1 )
		      {

		      }
		      else
		      {
		          stk_test_proxy_t* m_ntest_proxy = (stk_test_proxy_t*)rtk_lists_add_tail_new(lists_proxy, \
				        (rtk_lists_init_t)stk_test_proxy_init);
            stk_test_proxy_copy(m_ntest_proxy, m_test_proxy);
			         stk_proxy_hash_insert(&m_proxy_hash, m_ntest_proxy);
       	}
	   }
    rtk_hash_term(&m_proxy_hash);
}

void stk_effect_proxy_sort(stk_proxy_handle_t* m_cfg)
{
    stk_proxy_list_sort(&m_cfg->m_test_proxy_lists->test_general_proxy_lists);
}

void stk_proxy_list_sort(rtk_lists_t* test_proxy_lists)
{
    rtk_arrays_t arrays;
    rtk_arrays_init(&arrays, false);

    void* pos = rtk_lists_get_head_position(test_proxy_lists);
    stk_test_proxy_t* m_test_proxy;
    while (pos)
    {
	       m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(test_proxy_lists, &pos);
		      rtk_arrays_insert(&arrays, m_test_proxy, (rtk_arrays_compare_t)compare_by_use_rate);
	   }

    rtk_lists_remove_all(test_proxy_lists);
    rtk_lists_get_from_arrays(test_proxy_lists, &arrays);
    rtk_arrays_term(&arrays);
}

int compare_by_use_rate(stk_test_proxy_t* p1, stk_test_proxy_t* p2)
{
    int flag = -1;
    double c1, c2;

    c1 = stk_proxy_get_use_rate(p1);
    c2 = stk_proxy_get_use_rate(p2);

    if ( (c1-c2) > 0 )
        flag = -1;
    else 
        flag = 1;
    return flag;
}

double stk_proxy_get_use_rate(stk_test_proxy_t* p)
{
    double c;
    if ( double_safe_equal(p->right, 0) && double_safe_equal(p->wrong, 0) )
        c = 0.001;
    else
    {
	       if ( double_safe_equal(p->right, 0) )
		          p->right = 0.000001;
	       else if ( double_safe_equal(p->wrong, 0) )
		          p->wrong = 0.000001;
	       c = p->right / p->wrong;
	   }
	   return c;
}

bool double_safe_equal(double q1, double q2)
{
    double c = q1-q2;
    if (c < 0)
        c = -c;
    if (c < 0.0000000001)
        return true;
    else
        return false;
}

void rtk_lists_get_from_arrays(rtk_lists_t* test_proxy_lists, rtk_arrays_t* arrays)
{
    stk_test_proxy_t* m_test_proxy;
    for (int i = 0; i < arrays->size; i++)
    {
	       m_test_proxy = (stk_test_proxy_t*)rtk_arrays_get_at(arrays, i);
		      rtk_lists_add_tail(test_proxy_lists, m_test_proxy);
	   }
}

void stk_convert_url_to_filename(rtk_string_t* url, rtk_string_t* filename)
{
    rtk_string_shrink(filename, 0);
    char c;
    for (int i = 0; i < url->length; i++)
    {
		      c = url->buffer[i];
	 	     if ( !isdigit(c) && !islower(c) && !isupper(c) )
		      {
		          c = '_';
		      }
		      rtk_string_append_char(filename, c);
	   }

    rtk_string_append(filename, ".lst", -1);
}



//将获取的可用代理进行输出
void stk_effect_proxy_print_test(rtk_arrays_t* arrays_intersection_proxy, const char* supdir, const char* subdir)
{
    rtk_string_t string;
    rtk_string_t str_out;
    rtk_string_t fname;

#ifdef debugmessage
	rtk_string_t debugfile;
	rtk_string_t debug_str;
	rtk_string_init(&debugfile);
	rtk_string_init(&debug_str);
#endif
	
    rtk_string_init(&string);
    rtk_string_init(&str_out);
    rtk_string_init(&fname);

    stk_test_proxy_t* m_test_proxy;
	


	//rtk_string_copy(&str_out, "以上url可以共用的代理\n", -1);
	
    rtk_string_copy(&str_out, "/*********************************/\r\n", -1);
    rtk_string_append(&str_out, "/* 代理名称;代理端口;用户名;密码 */\r\n", -1);
    rtk_string_append(&str_out, "/**********************************/\r\n\r\n", -1);

    rtk_string_append(&str_out, "<list name=\"\" value=\"1\">\r\n", -1);
    for (int i = 0; i < arrays_intersection_proxy->size; i++)	
    {
	       m_test_proxy = (stk_test_proxy_t*)rtk_arrays_get_at(arrays_intersection_proxy, i);
	
        stk_effect_proxy_to_string(m_test_proxy, &string);
        rtk_string_append(&str_out, string.buffer, string.length);
        rtk_string_append(&str_out, "\"\";\"\"", -1);
        rtk_string_append(&str_out, "\r\n",-1);
#ifdef debugmessage
		rtk_string_append(&debug_str,m_test_proxy->str_proxy_ip.buffer,-1);
		rtk_string_append_char(&debug_str,';');
		rtk_string_append_number(&debug_str,m_test_proxy->n_port);
		rtk_string_append_char(&debug_str,';');
		rtk_string_append_number(&debug_str,m_test_proxy->respone_time);
		rtk_string_append(&debug_str, "\"\";\"\"", -1);
		rtk_string_append(&debug_str, "\r\n",-1);
#endif
		
    }
    rtk_string_append(&str_out, "</list>", -1);
    rtk_string_append(&str_out, "\r\n",-1);

    rtk_dir_make((char*)supdir, 0);
    rtk_dir_merge(&fname, (char*)supdir, (char*)subdir);

	rtk_file_set_string((char*)fname.buffer, 0, &str_out);

#ifdef debugmessage
	rtk_dir_merge(&debugfile,(char*)supdir,"proxy_resp.lst");
	rtk_file_set_string(debugfile.buffer,0,&debug_str);
	rtk_string_term(&debug_str);
    rtk_string_term(&debugfile);
#endif
    rtk_string_term(&str_out);
    rtk_string_term(&string);
    rtk_string_term(&fname);

}

void stk_effect_proxy_remove_spilth(rtk_arrays_t* arrays_effect_proxy, int n)
{
    for (int i = arrays_effect_proxy->size - 1; i >= n; i--)
    {
        rtk_arrays_remove_at(arrays_effect_proxy, i);
    }

}

void stk_proxy_get_test_lists_from_cache_file(rtk_lists_t* test_general_proxy_lists, char* supdir, char* subdir)
{
    rtk_string_t str_cache_proxys;
    rtk_string_t fname;
    rtk_string_t str_log_info;
    time_t tm_now;
    time_t tm;

    rtk_string_init(&str_cache_proxys);
    rtk_string_init(&fname);
    rtk_string_init(&str_log_info);

    rtk_string_copy(&str_log_info, "开始从缓存文件中读取待测试代理\t", -1);
    rtk_string_append_time(&str_log_info);
    rtk_string_append(&str_log_info, "\r\n",-1);
    stk_proxy_detail_log_set(&str_log_info);
	

    rtk_dir_merge(&fname, supdir, subdir);
    get_file_last_modify_time(fname.buffer, &tm);
    time(&tm_now);
    int time_file_exit = tm_now - tm;
    if ( time_file_exit > 24*60*60 )
        remove(fname.buffer);
    rtk_file_get_string(fname.buffer, &str_cache_proxys);
    stk_proxy_list_append_from_string_no_dup(test_general_proxy_lists, &str_cache_proxys);
    rtk_string_copy(&str_log_info, "结束从缓存文件中读取待测试代理\t", -1);
    rtk_string_append_time(&str_log_info);
    rtk_string_append_char(&str_log_info, '\n');
    stk_proxy_detail_log_set(&str_log_info);
    
    rtk_string_term(&str_cache_proxys);
    rtk_string_term(&fname);
}

void stk_proxy_lists_filter(rtk_lists_t* test_general_proxy_lists, int n)
{
    stk_test_proxy_t* m_test_proxy;
    void* pos = rtk_lists_get_head_position(test_general_proxy_lists);
    void* cur_pos;
    while (pos)
    {
	       cur_pos = pos;
        m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(test_general_proxy_lists, &pos);
        if (stk_porxy_single_is_filter(m_test_proxy, n))
            rtk_lists_remove_at(test_general_proxy_lists, cur_pos);
	   }
}

bool stk_porxy_single_is_filter(stk_test_proxy_t* m_test_proxy, int n)
{
    double temp = m_test_proxy->wrong - 0.000001;
    if (temp > n)
        return true;
    else
        return false;
}

void stk_proxy_subsection_get(rtk_lists_t* test_general_proxy_lists)
{
    rtk_lists_t lists_subsection;
    rtk_string_t str_current_dir;
    rtk_string_t str_proxy_dir;
    rtk_lists_t lists_proxy_after_subsection;
    

    rtk_lists_init(&lists_subsection, true, true);
    lists_subsection.dpool->memsize = sizeof(stk_subsection_t);
    rtk_string_init(&str_current_dir);
    rtk_string_init(&str_proxy_dir);
    rtk_lists_init(&lists_proxy_after_subsection, true, false);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&str_proxy_dir, str_current_dir.buffer, "proxy_temps");
    stk_subsection_get_profile(&lists_subsection, str_proxy_dir.buffer, "subsection.lst");
    void* pos = rtk_lists_get_head_position(&lists_subsection);
    while (pos)
    {
	       stk_subsection_t* m_subsection = (stk_subsection_t*)rtk_lists_get_next(&lists_subsection, &pos);
        stk_proxy_single_subsection_get(m_subsection, test_general_proxy_lists, &lists_proxy_after_subsection);
    }
    
    rtk_lists_add_head_list(test_general_proxy_lists, &lists_proxy_after_subsection);


    rtk_lists_free_all(&lists_subsection, (rtk_lists_term_t)stk_subsection_init);
    rtk_lists_term(&lists_subsection);
    rtk_string_term(&str_current_dir);
    rtk_string_term(&str_proxy_dir);
    rtk_lists_term(&lists_proxy_after_subsection);
}

void stk_proxy_single_subsection_get(stk_subsection_t* m_subsection, rtk_lists_t* source_lists, \
									 rtk_lists_t* destination_lists)
{
    void* pos = rtk_lists_get_head_position(source_lists);
    void* pos_pre;
    while (pos)
    {
        pos_pre = pos;
        stk_test_proxy_t* m_test_proxy = (stk_test_proxy_t*)rtk_lists_get_next(source_lists, &pos);
        if (is_proxy_in_subsection(m_test_proxy, m_subsection))
        {
            rtk_lists_add_tail(destination_lists, m_test_proxy);
		          rtk_lists_remove_at(source_lists, pos_pre);
		      }
	   }

}

bool is_proxy_in_subsection(stk_test_proxy_t* m_test_proxy, stk_subsection_t* m_subsection)
{
    unsigned int num1, num2;

    num1 = stk_proxyip_to_int(&m_test_proxy->str_proxy_ip);
    stk_mask(&num1, m_subsection->bit_count);
    num2 = stk_proxyip_to_int(&m_subsection->str_proxy_ip);
    stk_mask(&num2, m_subsection->bit_count);

    if (num1 == num2)
        return true;
    else
        return false;
}

unsigned int stk_proxyip_to_int(rtk_string_t* proxyip)
{
    int a;
    int pos1, pos2;
    rtk_string_t temp;
    unsigned int result;

    rtk_string_init(&temp);

    pos1 = 0;
    pos2 = 0;
    result = 0;
    for (int i = 0; i < 4; i++)
    {
	       pos2 = rtk_string_find_char_pos(proxyip, '.', pos1);
		      if (pos2 != -1)
		      {
			         rtk_string_mid_string(proxyip, pos1, pos2-pos1, &temp);
		          pos1 = pos2+1;
		      }
		      else
		      {
			         rtk_string_right_string(proxyip, proxyip->length-pos1, &temp);
		      }
		      a = rtk_string_get_number(&temp, -1);
		      if (a != -1)
		      {
		          result |= (a << (24-8*i));
		      }
	   }

    rtk_string_term(&temp);
    return result;
}

void stk_mask(unsigned int* num, int bit_count)
{
    unsigned int mask = 0xFFFFFFFF;
    mask = mask << (32-bit_count);
    *num &= mask;
}





