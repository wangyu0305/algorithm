#ifndef _GET_PROXY_IP__H_
#define _GET_PROXY_IP__H_

#include <rtk_32.h>
#include <stk_inet.h>
#include <convert.h>
#include <stk_html.h>

#include "stk_cfg.h"
#include "stk_effect_proxy.h"



//�ⲿ�ӿ�
__declspec(dllexport) void stk_test_proxy_get_from_sites(rtk_lists_t* proxy_site_lists, char* supdir, char* subdir);
__declspec(dllexport) void stk_proxy_get_from_single_site(stk_proxy_site_t* m_proxy_site, rtk_string_t* str_proxy);

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



void other_code_to_GB(rtk_string_t* str_content);
int down_page_by_url(rtk_string_t* in_url, rtk_string_t* out_content);
void stk_get_single_proxy_from_list(rtk_list_t* list, rtk_string_t* str_single_proxy);
void stk_get_test_proxy_boost_regex_from_str(rtk_string_t* content, rtk_string_t* regex, rtk_string_t* str_proxy);


#endif