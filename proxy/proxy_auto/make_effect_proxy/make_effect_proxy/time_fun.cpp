#include "time_fun.h"
#include <windows.h>
#include <convert.h>
extern clock_t m_clock;

void get_current_time(rtk_string_t* str_time)
{
    rtk_string_shrink(str_time, 0);
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    rtk_string_append_number(str_time, timeinfo->tm_year + 1900);
    rtk_string_append_char(str_time, '-');
    rtk_string_append_number(str_time, timeinfo->tm_mon+1);
    rtk_string_append_char(str_time, '-');
    rtk_string_append_number(str_time, timeinfo->tm_mday);
    rtk_string_append_char(str_time, ' ');
    rtk_string_append_number(str_time, timeinfo->tm_hour);
    rtk_string_append_char(str_time, ':');
    rtk_string_append_number(str_time, timeinfo->tm_min);
    rtk_string_append_char(str_time, ':');
    rtk_string_append_number(str_time, timeinfo->tm_sec);

}

void get_current_time_for_filename(rtk_string_t* str_filename)
{
    rtk_string_shrink(str_filename, 0);
    time_t rawtime;
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );

    rtk_string_append_number(str_filename, timeinfo->tm_year + 1900);
    rtk_string_append_char(str_filename, '_');
    rtk_string_append_number(str_filename, timeinfo->tm_mon+1);
    rtk_string_append_char(str_filename, '_');
    rtk_string_append_number(str_filename, timeinfo->tm_mday);
    rtk_string_append_char(str_filename, '_');
    rtk_string_append_number(str_filename, timeinfo->tm_hour);
    rtk_string_append_char(str_filename, '_');
    rtk_string_append_number(str_filename, timeinfo->tm_min);
    rtk_string_append_char(str_filename, '_');
    rtk_string_append_number(str_filename, timeinfo->tm_sec);
    rtk_string_append(str_filename, ".txt", -1);
}

void rtk_string_append_time(rtk_string_t* str_log_info)
{
    m_clock = clock();
    rtk_string_append_number(str_log_info, m_clock);
}

void get_file_last_modify_time(char* fname, time_t* tm)
{
    rtk_string_t str_fname;

    rtk_string_init(&str_fname);
    rtk_string_copy(&str_fname, fname, -1);
    ConvertToUnicode("GBK", &str_fname);
    HANDLE hDir = CreateFile ((LPCWSTR)str_fname.buffer, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_DELETE,  
                 NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL); 
    FILETIME creat_time;
    FILETIME acess_time;
    FILETIME modify_time;
    SYSTEMTIME stime;
    GetFileTime(hDir, &creat_time, &acess_time, &modify_time);
    FileTimeToSystemTime(&modify_time, &stime);
    LONGLONG llTime;
    ULARGE_INTEGER ui;
    ui.LowPart = modify_time.dwLowDateTime;
    ui.HighPart = modify_time.dwHighDateTime;
    llTime = (modify_time.dwHighDateTime << 32) + modify_time.dwLowDateTime;
    *tm = (DWORD)((LONGLONG)(ui.QuadPart - 116444736000000000) / 10000000);
    CloseHandle(hDir); 
    rtk_string_term(&str_fname);
}