
#include "tools.h"
#include "stk_cfg.h"

extern stk_proxy_js_t proxyjs;

void stk_proxy_js_init(stk_proxy_js_t* proxyjs)
{
	proxyjs->use_js = 0;
	stk_jsengine_init(&proxyjs->jsengine);
    rtk_string_init(&proxyjs->jsobjname);
	rtk_lists_init(&proxyjs->fieldlist,true,true);
	proxyjs->fieldlist.dpool->memsize = sizeof(stk_jsfield_t);
	rtk_array_init(&proxyjs->functions);
	rtk_list_init(&proxyjs->funclist,true);
	rtk_array_init(&proxyjs->parray);
}

void stk_proxy_js_term(stk_proxy_js_t* proxyjs)
{
	proxyjs->use_js = 0;
	stk_jsengine_term(&proxyjs->jsengine);
	rtk_string_term(&proxyjs->jsobjname);
	rtk_array_term(&proxyjs->functions);
	rtk_list_term(&proxyjs->funclist);
	rtk_array_term(&proxyjs->parray);
	rtk_lists_free_all(&proxyjs->fieldlist,(rtk_lists_term_t)stk_jsfield_term);
	rtk_lists_term(&proxyjs->fieldlist);
}

void stk_proxy_compile_script(stk_proxy_js_t* proxyjs,char* subdir)
{
	rtk_string_t dirname;
	rtk_string_init(&dirname);
	rtk_string_t jstrcpt;
	rtk_string_init(&jstrcpt);
	rtk_string_t context;
	rtk_string_init(&context);

	rtk_dir_cwd(&dirname);
	rtk_dir_merge(&dirname,dirname.buffer,subdir);
	rtk_file_get_string(dirname.buffer,&context);
	proxyjs->use_js = stk_cfg_get_number("use_js",&context,0);
	if (proxyjs->use_js != 0)
	{
		stk_cfg_get_text("jscript",&context,&proxyjs->jsobjname,"proxyip.js");

		rtk_string_shrink(&dirname,0);
		rtk_dir_cwd(&dirname);
		rtk_dir_merge(&dirname,dirname.buffer,proxyjs->jsobjname.buffer);
		rtk_file_get_string(dirname.buffer,&jstrcpt);

		stk_jsengine_create_instance(&proxyjs->jsengine);
		stk_jsengine_compile_script(&proxyjs->jsengine,proxyjs->jsobjname.buffer,&jstrcpt);
	}

	rtk_string_term(&context);
	rtk_string_term(&jstrcpt);
	rtk_string_term(&dirname);
}

void stk_proxy_js_engine_destory(stk_proxy_js_t* proxyjs)
{
	stk_jsengine_destroy_instance(&proxyjs->jsengine);
}

void stk_proxy_get_script_content(stk_proxy_js_t* proxyjs,rtk_string_t *urlname,rtk_string_t* source,rtk_string_t* content)
{
	rtk_string_t funcname;
	stk_jsfield_t* field;
	void *pos;
	int i;

	rtk_string_init(&funcname);

	stk_jsengine_get_object_functions(&proxyjs->jsengine,proxyjs->jsobjname.buffer,&proxyjs->functions);
	for (i = 0 ; i < proxyjs->functions.size ; i++)
	{
		rtk_array_get_at_string(&proxyjs->functions,i,&funcname);
		if (rtk_string_compare(&funcname,"TRS_Document_Source",-1) ==  0)
		{
			rtk_array_copy(&proxyjs->parray,urlname->buffer,urlname->length);
			rtk_array_append(&proxyjs->parray,"title",-1);
			rtk_array_append(&proxyjs->parray,source->buffer,source->length);

			stk_jsengine_evaluate_function(&proxyjs->jsengine,proxyjs->jsobjname.buffer,funcname.buffer,&proxyjs->parray,&proxyjs->fieldlist);

			pos = rtk_lists_get_head_position(&proxyjs->fieldlist);
			while(pos != 0)
			{
				field = (stk_jsfield_t*)rtk_lists_get_next(&proxyjs->fieldlist,&pos);

				if (rtk_string_compare(&field->name,"source",-1) == 0 || rtk_string_compare(&field->name,"JS_DEFAULT_RVAL",-1) == 0)
				{
					if (!rtk_string_is_empty(&field->value))
					{
						rtk_string_copy(content,field->value.buffer,-1);
					}
				}
			}

			rtk_array_remove_at(&proxyjs->functions,i);
		}
	}

	/*stk_html_list_script_funccall(&task->m_crawl,&functions,&funclist);

	pos = rtk_list_get_head_position(&funclist);
	while (pos != 0)
	{
		rtk_list_get_next_string(&funclist,&pos,&script);

		stk_jsengine_evaluate_script(&task->m_jsengine,content_script->buffer,&script,&fieldlist);

		pos = rtk_lists_get_head_position(&fieldlist);
		while(pos != 0)
		{
			field = (stk_jsfield_t*)rtk_lists_get_next(&fieldlist,&pos);

			if (rtk_string_compare(&field->name,"source",-1) == 0 || rtk_string_compare(&field->name,"JS_DEFAULT_RVAL",-1) == 0)
			{
				if (!rtk_string_is_empty(&field->value))
				{
					stk_html_parse_end(&task->m_crawl);
					stk_html_parse_begin(&task->m_crawl,&field->value);
					stk_html_get_dom_content(&task->m_crawl,&info->content,&info->table_list,&info->image_list);
				}
			}

			robot_task_get_script_value(field,info);
		}
	}*/
	rtk_string_term(&funcname);
}

void stk_proxy_js_content(rtk_string_t* content,rtk_string_t* jscontent,rtk_string_t* urlname)
{
	stk_proxy_get_script_content(&proxyjs,urlname,content,jscontent);
}