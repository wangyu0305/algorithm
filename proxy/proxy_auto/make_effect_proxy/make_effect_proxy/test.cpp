#include "test.h"


void test_stk_proxy_list_append_from_string_no_dup()
{
    stk_proxy_handle_t m_cfg;
    rtk_string_t str_current_dir;
    rtk_string_t str_all_proxy;
    rtk_string_t fname;

    stk_proxy_handle_init(&m_cfg);
    rtk_string_init(&str_current_dir);
    rtk_string_init(&str_all_proxy);
    rtk_string_init(&fname);

    rtk_dir_cwd(&str_current_dir);
    rtk_dir_merge(&fname, str_current_dir.buffer, "str_all_proxy.lst");

    stk_proxy_handle_get_profile(&m_cfg, str_current_dir.buffer, "effect_proxy.cfg");
    rtk_file_get_string(fname.buffer, &str_all_proxy);

	//stk_proxy_list_append_from_string_no_dup(&m_cfg.test_proxy_lists, &str_all_proxy);

    stk_proxy_handle_set_profile(&m_cfg, str_current_dir.buffer, "effect_proxy_test_out.cfg");

    stk_proxy_handle_term(&m_cfg);
    rtk_string_term(&str_current_dir);
    rtk_string_term(&str_all_proxy);
    rtk_string_term(&fname);
}

void test_clock()
{
    clock_t start, end;
    start = clock();     
    end = clock();
    double a =  (end-start)/CLK_TCK;
    int b = end-start;
}

void test_stk_get_test_proxy_boost_regex_from_str()
{
    rtk_string_t content;
    rtk_string_t regex;
    rtk_string_t str_proxy;

    rtk_string_init(&content);
    rtk_string_init(&regex);
    rtk_string_init(&str_proxy);

    rtk_file_get_string("test.ini", &content);
    //rtk_string_copy(&content, "onDbl Click =what878onDbl Clicka =what", -1);
    rtk_string_copy(&regex, "onDblClick=\"clip\\('(\\d+).(\\d+).(\\d+).(\\d+):(\\d+)'\\)", -1);
	//rtk_string_copy(&regex, "onDbl ([a-zA-Z]*) =", -1);
    //rtk_string_copy(&content, "select name from table", -1);
	//rtk_string_copy(&regex, "^select ([a-zA-Z]*) from ([a-zA-Z]*)", -1);

    stk_get_test_proxy_boost_regex_from_str(&content, &regex, &str_proxy); 
	
    rtk_string_term(&content);
    rtk_string_term(&regex);
    rtk_string_term(&str_proxy);
}