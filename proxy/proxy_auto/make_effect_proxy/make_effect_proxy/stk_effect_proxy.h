#ifndef _STK_EFFECT_PROXY__H_
#define _STK_EFFECT_PROXY__H_

#include <rtk_32.h>
#include "stk_cfg.h"
#include <stk_inet.h>
#include <math.h> 
#include <time.h>
#include "inet_whl.h"

#define debug

#define debugmessage

//#define COMMON_PROXY

struct stk_filename_t
{
    rtk_string_t supdir;
	   rtk_string_t subdir;
};



typedef void (*stk_effect_proxy_print_t)(rtk_arrays_t* arrays_effect_proxy_common, char* supdir, const char* subdir);
__declspec(dllexport) void stk_proxy_get_persist();


__declspec(dllexport) void stk_available_proxy_output(stk_proxy_handle_t* m_cfg, stk_effect_proxy_print_t stk_effect_proxy_print, char* supdir, char* subdir);
__declspec(dllexport) void stk_proxy_get_test_lists_from_cache_file(rtk_lists_t* test_general_proxy_lists, char* supdir, char* subdir);


__declspec(dllexport) void stk_effect_proxy_to_string(stk_test_proxy_t* m_test_proxy, rtk_string_t* string);
__declspec(dllexport) void stk_proxy_list_append_from_string_no_dup(rtk_lists_t* lists_proxy, rtk_string_t* str_all_proxy);


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`

void stk_effect_proxy_print_test(rtk_arrays_t* arrays_intersection_proxy, const char* supdir, const char* subdir);

void stk_respone_info_init(stk_respone_info_t* m_respone_info);
void stk_respone_info_term(stk_respone_info_t* m_respone_info);
void stk_respone_info_reset(stk_respone_info_t* m_respone_info);

void stk_proxy_hash_init(rtk_hash_t* m_proxy_hash);
void stk_proxy_hash_term(rtk_hash_t* m_proxy_hash);
int stk_proxy_hash_find(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy);
int stk_proxy_hash_insert(rtk_hash_t* m_proxy_hash, stk_test_proxy_t* m_test_proxy);
stk_test_proxy_t* stk_proxy_hash_get(rtk_hash_t* m_test_proxy_hash, stk_test_proxy_t* m_test_proxy);

int stk_test_proxy_compare_respone_time(stk_test_proxy_t* p1, stk_test_proxy_t* p2);
void stk_effect_proxy_get_cfg_string(stk_test_proxy_t* m_test_proxy, rtk_string_t* string);
void stk_effect_proxy_single_list_proxy_get_common(rtk_lists_t* test_proxy_lists, rtk_lists_t* test_url_lists, \
									  rtk_arrays_t* arrays_effect_proxys_common, rtk_hash_t* m_proxy_hash, int n, bool is_prior);
bool stk_effect_proxy_test_single_proxy_is_common(stk_test_proxy_t* m_test_proxy, rtk_lists_t* test_url_lists, stk_timeout_t* timeout);
void stk_effect_proxy_update_prior_proxy(rtk_arrays_t* arrays_effect_proxy_common, rtk_lists_t* test_prior_proxy_lists);
bool stk_effect_proxy_test_respone_effect(stk_respone_info_t* m_respone_info, stk_test_url_t* m_test_url);




int stk_effect_proxy_get(rtk_lists_t* lists_proxy, stk_proxy_handle_t* m_proxy_handle, rtk_arrays_t* arrays_effect_proxy_common, \
						  rtk_hash_t* m_proxy_hash, clock_t start);

void stk_proxy_list_to_hash_remove_dup(rtk_lists_t* test_proxy_lists, rtk_hash_t* m_proxy_hash);
void stk_proxy_list_remove_dup(rtk_lists_t* test_proxy_lists);
void stk_proxy_list_append_from_list_no_dup(rtk_lists_t* lists_proxy, rtk_lists_t* nlists_proxy);
void stk_effect_proxy_sort(stk_proxy_handle_t* m_cfg);
void stk_proxy_list_sort(rtk_lists_t* test_proxy_lists);
int compare_by_use_rate(stk_test_proxy_t* p1, stk_test_proxy_t* p2);
double stk_proxy_get_use_rate(stk_test_proxy_t* p1);
void rtk_lists_get_from_arrays(rtk_lists_t* test_proxy_lists, rtk_arrays_t* arrays);
bool double_safe_equal(double q1, double q2);
void stk_convert_url_to_filename(rtk_string_t* url, rtk_string_t* filename);
bool stk_effect_proxy_test_single_proxy(stk_test_proxy_t* m_test_proxy, stk_test_url_t* m_test_url);
void stk_effect_proxy_remove_spilth(rtk_arrays_t* arrays_effect_proxy, int n);



int stk_effect_get_proxy_lists(stk_proxy_handle_t* m_proxy_handle, rtk_hash_t* m_proxy_hash, clock_t start, 
							   rtk_arrays_t* arrays_effect_proxy_common);

void stk_proxy_lists_filter(rtk_lists_t* test_general_proxy_lists, int n);
bool stk_porxy_single_is_filter(stk_test_proxy_t* m_test_proxy, int n);

void stk_proxy_subsection_get(rtk_lists_t* test_general_proxy_lists);
void stk_proxy_single_subsection_get(stk_subsection_t* m_subsection, rtk_lists_t* source_lists, \
									 rtk_lists_t* destination_lists);
bool is_proxy_in_subsection(stk_test_proxy_t* m_test_proxy, stk_subsection_t* m_subsection);
unsigned int stk_proxyip_to_int(rtk_string_t* proxyip);
void stk_mask(unsigned int* num, int bit_count);
void stk_get_proxy_supdir(rtk_string_t* str_proxy_supdir);

void stk_effect_proxy_get_single_url_single_proxy(stk_test_url_t* m_test_url, stk_test_proxy_t* m_test_proxy, \
												  stk_timeout_t* timeout, stk_respone_info_t* m_respone_info);
void stk_proxy_detail_log_set_by_respone(stk_respone_info_t* m_respone_info);

int stk_available_proxy_arrays_get(stk_proxy_handle_t* m_proxy_handle, rtk_arrays_t* arrays_proxys);

#endif