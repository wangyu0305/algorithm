#ifndef STK_PROXY_TOOLS
#define STK_PROXY_TOOLS

#include "rtk_32.h"
#include "stk_js.h"


typedef struct proxy_js stk_proxy_js_t;

struct proxy_js 
{
	int use_js;
	stk_jsengine_t jsengine;
	rtk_string_t jsobjname;
	rtk_lists_t  fieldlist;
	rtk_array_t functions;
	rtk_list_t funclist;
	rtk_array_t parray;
};

#ifdef __cplusplus
extern "C" {
#endif

	void stk_proxy_js_init(stk_proxy_js_t* proxyjs);
	void stk_proxy_js_term(stk_proxy_js_t* proxyjs);
	void stk_proxy_compile_script(stk_proxy_js_t* proxyjs,char* subdir);
	void stk_proxy_js_engine_destory(stk_proxy_js_t* proxyjs);
    void stk_proxy_get_script_content(stk_jsengine_t *jsengine,rtk_string_t *urlname,rtk_string_t *content_script,rtk_string_t* source,rtk_string_t* content);
    void stk_proxy_js_content(rtk_string_t* content,rtk_string_t* jscontent,rtk_string_t* urlname);

#ifdef __cplusplus
}
#endif

#endif