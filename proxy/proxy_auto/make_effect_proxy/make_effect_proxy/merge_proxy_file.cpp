#include "merge_proxy_file.h"

void proxy_string_init(proxy_string_t* ip)
{
	rtk_string_init(&ip->ipinfo);
}

void proxy_string_term(proxy_string_t* ip)
{
	rtk_string_term(&ip->ipinfo);
}

void proxy_string_copy(proxy_string_t* ip,char* fromip)
{
	rtk_string_copy(&ip->ipinfo,fromip,-1);
}

void proxy_get_profile(char* profilea,char* profileb,rtk_string_t* contenta,rtk_string_t* contentb)
{
	rtk_string_t dirname;
	rtk_string_t filenamea;
	rtk_string_t filenameb;
	rtk_string_t texta;
	rtk_string_t textb;
	int indexa,indexb;
	rtk_string_init(&dirname);
	rtk_string_init(&filenamea);
	rtk_string_init(&filenameb);
	rtk_string_init(&texta);
	rtk_string_init(&textb);
	rtk_dir_cwd(&dirname);
	rtk_dir_merge(&filenamea,dirname.buffer,profilea);
	rtk_dir_merge(&filenameb,dirname.buffer,profileb);

	rtk_file_get_string(filenamea.buffer,&texta);
	rtk_file_get_string(filenameb.buffer,&textb);
	if ((indexa = rtk_string_find_char(&texta,'>')) != -1)
	{
		indexb = rtk_string_find_char_pos(&texta,'<',indexa+1);
		rtk_string_mid_string(&texta,indexa+1,indexb-indexa-1,contenta);
	}
	else
	{
		rtk_string_copy(contenta,texta.buffer,texta.length);
	}
	if ((indexa = rtk_string_find_char(&textb,'>')) != -1)
	{
		indexb = rtk_string_find_char_pos(&textb,'<',indexa+1);
		rtk_string_mid_string(&textb,indexa+1,indexb-indexa-1,contentb);
	}
	else
	{
		rtk_string_copy(contentb,textb.buffer,textb.length);
	}

	rtk_string_term(&dirname);
	rtk_string_term(&filenamea);
	rtk_string_term(&filenameb);
	rtk_string_term(&texta);
	rtk_string_term(&textb);
}

void proxy_content_to_hash(rtk_hash_t* hhash,rtk_string_t* contenta,rtk_string_t* contentb)
{
	rtk_list_t lista;
	rtk_list_t listb;
	char* ipinfo;
	void* posa,*posb;
	rtk_list_init(&lista,true);
	rtk_list_init(&listb,true);

	rtk_list_from_string(&lista,"\r\n",contenta);
	rtk_list_from_string(&listb,"\r\n",contentb);

	posa = rtk_list_get_head_position(&lista);
	posb = rtk_list_get_head_position(&listb);
	while (posa || posb)
	{
		if (posa)
		{
			ipinfo = rtk_list_get_next(&lista,&posa);
			proxy_string_t* proxyip = (proxy_string_t*)rtk_hash_set(hhash,ipinfo,-1,(rtk_hash_init_t)proxy_string_init);
            proxy_string_copy(proxyip,ipinfo);
		}
		if (posb)
		{
			ipinfo = rtk_list_get_next(&listb,&posb);
			proxy_string_t* proxyip = (proxy_string_t*)rtk_hash_set(hhash,ipinfo,-1,(rtk_hash_init_t)proxy_string_init);
			proxy_string_copy(proxyip,ipinfo);
		}
	}

	rtk_list_term(&lista);
	rtk_list_term(&listb);
}

void proxy_commom_to_profile(rtk_string_t* commonip)
{
	rtk_string_t dirname;
	rtk_string_init(&dirname);

	rtk_dir_cwd(&dirname);
	rtk_string_append(&dirname,"\\proxycom.lst",-1);
	rtk_file_set_string(dirname.buffer,0,commonip);

	rtk_string_term(&dirname);
}

void get_proxy_list_from_profile(char* profilea,char* profileb)
{
	rtk_string_t contenta;
	rtk_string_t contentb;
    rtk_string_t commonip;
	rtk_hash_t hhash;
	void* pos;
	int index;
	proxy_string_t* proxyip;
	
	rtk_string_init(&commonip);
	rtk_string_init(&contenta);
	rtk_string_init(&contentb);
   
	rtk_hash_init(&hhash,true,true);
	hhash.dpool->memsize = sizeof(proxy_string_t);

	proxy_get_profile(profilea,profileb,&contenta,&contentb);

    proxy_content_to_hash(&hhash,&contenta,&contentb);
	for (index = 0,pos = 0;index < hhash.entry.size;)
	{
		proxyip = (proxy_string_t*)rtk_hash_get_next(&hhash,&index,&pos);
		if (proxyip != 0)
		{
			rtk_string_append(&commonip,proxyip->ipinfo.buffer,-1);
			rtk_string_append(&commonip,"\r\n",-1);
		}
	}
    proxy_commom_to_profile(&commonip);
	rtk_hash_free_all(&hhash,(rtk_hash_term_t)proxy_string_term);
	rtk_hash_term(&hhash);
    rtk_string_term(&commonip);
	rtk_string_term(&contenta);
	rtk_string_term(&contentb);
}