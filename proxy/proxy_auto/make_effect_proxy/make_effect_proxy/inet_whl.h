#ifndef _INET_WHL__H_
#define _INET_WHL__H_

#include <rtk_32.h>
#include "stk_cfg.h"
#include <time.h>


#define RIGHT 0
#define ENOUGH 1
#define RECV_TIMEOUT 2
#define RECV_ERROR 3
#define SEND_TIMEOUT 4
#define CONNECT_TIMEOUT 5
#define IOCTLSOCKET 6
#define NO_TAG 7
#define STATUS_ERROR 8
#define SIZE_ERROR 9
#define NOTENOUGH 10

struct stk_respone_info_t
{
    bool is_effect;
	int respone_time;
	stk_test_proxy_t* m_test_proxy;
	int size; //返回页面的大小
	int status; //响应报头状态码
	int error;
	int errorcode;
	bool is_tag_in;
    int connect_time;
	int send_time;
	int recv_time;
};

int stk_get_url_by_proxy(char* sz_urlname, int max, stk_test_proxy_t* m_test_proxy, stk_timeout_t* timeout,  \
						 rtk_stream_t* source, stk_respone_info_t* m_respone_info);
int format_url(char* sz_format_url, char* sz_urlname);
void get_status(rtk_stream_t* source, int* status);
bool effect_ip(char* ip);
#endif