
#ifndef _STK_CODER_H_
#define _STK_CODER_H_

#ifdef __cplusplus
extern "C" {
#endif

//////////////////////////////////////////////////

typedef struct MD5_CTX_128 MD5_CTX_128;
typedef struct stk_base64_t stk_base64_t;

struct MD5_CTX_128
{
  unsigned long int state[4];        /* state (ABCD) */
  unsigned long int count[2];        /* number of bits, modulo 2^64 (lsb first) */
  unsigned char buffer[64];          /* input buffer */
} ;

struct stk_base64_t  
{
    unsigned char *m_pDBuffer;
    unsigned char *m_pEBuffer;
    DWORD         m_nDBufLen;
    DWORD         m_nEBufLen;
    DWORD         m_nDDataLen;
    DWORD         m_nEDataLen;
    char m_DecodeTable[256];
    int m_Init;
};

LWORD stk_md5_64(char *content);
void stk_md5_128(char* content, unsigned char digest[16]);

void stk_base64_init(stk_base64_t *base64);
void stk_base64_term(stk_base64_t *base64);

void stk_base64_encode(stk_base64_t *base64,const unsigned char* pBuffer, DWORD nBufLen);
void stk_base64_decode(stk_base64_t *base64,const unsigned char* pBuffer, DWORD dwBufLen);

void stk_base64_encode_msg(stk_base64_t *base64,const char* sMessage);
void stk_base64_decode_msg(stk_base64_t *base64,const char* sMessage);

char* stk_base64_encoded_message(stk_base64_t *base64) ;
char* stk_base64_decoded_message(stk_base64_t *base64) ;


#ifdef __cplusplus
}
#endif

#endif // _STK_CODER_H_
