// urlset.h : header file
//

#ifndef _STK_JS_H_
#define _STK_JS_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#ifndef XP_WIN
#define  XP_WIN
#endif
#endif
/////////////////////////////////////////////////////////////////////////////

typedef struct stk_jsfield_t stk_jsfield_t;
typedef struct stk_jsengine_t stk_jsengine_t;

struct stk_jsfield_t
{
    rtk_string_t  name;
    rtk_string_t  value;
};

struct stk_jsengine_t
{
    void          *runtime;
    void          *context;
    void          *globalobj;
    int           rtsize;
    int           ctsize;
    rtk_hash_t    objects;
    rtk_string_t  filename;
    rtk_mutex_t   mutex;
};

void stk_jsfield_init(stk_jsfield_t *field);
void stk_jsfield_term(stk_jsfield_t *field);

int stk_jsfield_compare(stk_jsfield_t *field,char *name);

void stk_jsengine_init(stk_jsengine_t *jsengine);
void stk_jsengine_term(stk_jsengine_t *jsengine);

int stk_jsengine_create_instance(stk_jsengine_t *jsengine);
void stk_jsengine_destroy_instance(stk_jsengine_t *jsengine);

int stk_jsengine_compile_script(stk_jsengine_t *jsengine,char *objname,rtk_string_t *script);

int stk_jsengine_evaluate_script(stk_jsengine_t *jsengine,char *objname,rtk_string_t *script,rtk_lists_t *field_list);
int stk_jsengine_evaluate_function(stk_jsengine_t *jsengine,char *objname,char *funcname,rtk_array_t *funcparam,rtk_lists_t *field_list);

int stk_jsengine_evaluate_custom_function(stk_jsengine_t *jsengine,char *objname,char *funcname,rtk_string_t *urlname,rtk_string_t *urltitle,rtk_string_t *source,rtk_lists_t *field_list);

void stk_jsengine_get_object_functions(stk_jsengine_t *jsengine,char *objname,rtk_array_t *funcnames);
const char* stk_jsengine_get_last_error(stk_jsengine_t *jsengine);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_JS_H_

