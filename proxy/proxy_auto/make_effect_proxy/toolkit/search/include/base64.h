
// base64.h : header file
//

#ifndef _BASE64_H_
#define _BASE64_H_

#ifdef __cplusplus
extern "C" {
#endif

/* How many bytes it will take to store LEN bytes in base64.  */
#define BASE64_LENGTH(len) (4 * (((len) + 2) / 3))

int base64_encode (const char *str, int length, char *b64store);
int base64_decode (const char *base64, char *to);

#ifdef __cplusplus
}
#endif

#endif // _BASE64_H_