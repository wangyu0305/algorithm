// stk_dom.h : header file
//

#ifndef _STK_DOM_H_
#define _STK_DOM_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////
// stk_dom_t CObject

typedef struct stk_kv_t stk_kv_t;
typedef struct stk_node_t stk_node_t;
typedef struct stk_block_t stk_block_t;
typedef struct stk_dom_t  stk_dom_t;

struct stk_kv_t
{
    stk_string_t    key;
    stk_string_t    value;
    char            quote;  //属性值为单引号？双引号？默认双引号
};

struct stk_node_t
{
    stk_string_t    tagname;         /* 节点名称, 空表示是非标签*/
    stk_string_t    value;           /* 节点内容，标签+属性如<td align="right"> 或 <!--/xxx/--> 或 scipt块 或内容 */
    rtk_arrays_t    attributes;      /* 节点的属性数组*/
    char            is_closed;       /* 是否有闭合标签 */
    char            is_ignore;       /* 是否属于页面不显示的标签或内容，如<script>、<style>、"display: none"、<!--/xxx/-->等*/
    char            tagflag;         /* 标签类型，TAG_META,TAG_FRAME,TAG_TABLE,TAG_P等 */

    char            type;            /* 节点类型，TABLE_NODE,IMAGE_NODE,SUBINFO_NODE,TEXT_NODE,LINK_NODE等*/
};

struct stk_block_t
{
    rtk_tree_entry_t *entry;

    int              mtextsize;
    int              ntextsize;
    int              mlinks;
    int              nlinks;
    int              mlinksize;
    int              nlinksize;
    int              mcells;   
    int              ncells;   
    int              mrows;
    int              mheads;
    int              mimages;  
    int              nimages;  
    int              minput;   

    int              text_begin;

    int              index;    
    int              p_index;  
    rtk_arrayd_t     c_indexs; 
};

struct stk_dom_t
{
    rtk_hash_t      *ignore_hash;
    int             ignore_hidden;

    int             nimage;
    int             ntable;

    int             mtextsize;  //网页的非链接纯文本字数

    int             totalsize;  //正文文字数
    int             currentsize;   

    int             sub_length; 

    rtk_hash_t      trimtags;
    rtk_hash_t      nodetags;
    rtk_hash_t      breaktags;
    rtk_hash_t      spacetags;
    rtk_hash_t      doubletags;

    rtk_tree_t      domtree;
    rtk_lists_t     metalist;
    rtk_lists_t     framelist;
    rtk_lists_t     blocklist;
    rtk_arrays_t    blocktree;
    rtk_arrays_t    domlist;     //满足模版的节点列表

    stk_block_t     *content_block;

    rtk_arrays_t    header_nodes;
    rtk_arrays_t    footer_nodes;
};

void stk_block_init(stk_block_t *block);
void stk_block_term(stk_block_t *block);

void stk_node_init(stk_node_t *node);
void stk_node_term(stk_node_t *node);

void stk_node_from_string(stk_node_t *node,const unsigned char *b,const unsigned char *e);

int stk_node_is_available(stk_node_t *node);

void stk_dom_init(stk_dom_t *stkdom);
void stk_dom_term(stk_dom_t *stkdom);

int stk_node_get_key_value(stk_node_t *node,char *key,rtk_string_t *value);

const unsigned char *find_next_quote(const unsigned char *c, const unsigned char *end, char quote);

void stk_dom_get_tree(stk_dom_t *stkdom,rtk_string_t *content,rtk_tree_entry_t *domtree);
void stk_dom_set_tree(stk_dom_t *stkdom, rtk_tree_entry_t *domtree,int index,rtk_string_t *content);

void stk_dom_get_nodes(stk_dom_t *stkdom,rtk_tree_entry_t *domtree,rtk_arrayd_t *blockstack,rtk_lists_t *metas,rtk_lists_t *frames,rtk_arrays_t *blocktree);
void stk_dom_get_cells(rtk_tree_entry_t *domtree,rtk_arrays_t *cells);

int stk_dom_is_breaktag(stk_dom_t *stkdom,stk_string_t *tagname);
int stk_dom_is_spacetag(stk_dom_t *stkdom,stk_string_t *tagname);
int stk_dom_is_doubletag(stk_dom_t *stkdom,stk_string_t *name);

void stk_dom_set_table(stk_dom_t *stkdom,rtk_string_t *content,int index,rtk_tree_entry_t *domtree);

void stk_dom_get_childs(rtk_tree_entry_t *domtree,rtk_lists_t *childlist);

rtk_tree_entry_t* stk_dom_find_module(rtk_tree_entry_t *domtree,int alltag,rtk_arrayd_t* modules);

void stk_dom_set_module(stk_dom_t *stkdom,rtk_string_t *content,int index,rtk_tree_entry_t *domtree);
/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_DOM_H_
