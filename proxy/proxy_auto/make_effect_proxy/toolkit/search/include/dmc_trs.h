// dmc_trs.h : header file
//

#ifndef _DMC_TRS_H_
#define _DMC_TRS_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#define DMC_DECLARE(type) __declspec(dllexport) type __stdcall
#else
#define DMC_DECLARE(type) type
#endif
/////////////////////////////////////////////////////////////////////////////

DMC_DECLARE(int) GetDocumentText(char* SourceDoc, char* TextFile, char* TextCharset, char** errmsg);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _DMC_TRS_H_

