// UTF8CONVERT.h : header file
//

#ifndef _CONVERT_H_
#define _CONVERT_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

void ConvertBigToGB(rtk_string_t *content);
void ConvertGBToBig(rtk_string_t *content);

int ConvertToUTF8(char* charset,rtk_string_t *content);
int ConvertToGB18030(char* charset,rtk_string_t *content);
int ConvertToGBK(char* charset,rtk_string_t *content);
int ConvertToUnicode(char* charset,rtk_string_t *content);

void DecodeToUTF8(rtk_string_t *content);
void DecodeToGB18030(rtk_string_t *content);
void DecodeToGBK(rtk_string_t *content);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _CONVERT_H_
