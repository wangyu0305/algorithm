// encoding.h : header file
//

#ifndef _ENCODING_H_
#define _ENCODING_H_

#ifdef __cplusplus
extern "C" {
#endif


int unencode_deflate(stk_http_t *http,rtk_string_t *source);
int unencode_gzip(stk_http_t *http,rtk_string_t *source);

#ifdef __cplusplus
}
#endif

#endif // _ENCODING_H_