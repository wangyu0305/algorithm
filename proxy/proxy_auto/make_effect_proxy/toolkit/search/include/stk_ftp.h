// stk_ftp.h : header file
//

#ifndef _STK_FTP_H_
#define _STK_FTP_H_

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////
// stk_ftp_t CObject

typedef struct stk_ftp_t stk_ftp_t;

struct stk_ftp_t
{
    rtk_socket_t    socket;

    char            *server;      //ftp服务器名称
    unsigned short  port;         //ftp服务器端口
    char            *username;    //用户名
    char            *password;    //密码
    char            *local;       //本地IP地址

    int             pasv;         //0 主动模式 1 被动模式
    int             type;         //传输模式，暂保留

    int             m_connected;  //是否连接成功
    long            m_resp;       //last response code
    rtk_string_t    m_respmsg;    //last response full text
    rtk_string_t    m_command;    //last command


};

void stk_ftp_init(stk_ftp_t *ftp);
void stk_ftp_term(stk_ftp_t *ftp);

int stk_ftp_connect(stk_ftp_t *ftp);
void stk_ftp_disconnect(stk_ftp_t *ftp);

int stk_ftp_put(stk_ftp_t *ftp,char *dstfile,rtk_stream_t *stream);
int stk_ftp_get(stk_ftp_t *ftp,char *srcfile,rtk_stream_t *stream);

int stk_ftp_pwd(stk_ftp_t *ftp,rtk_string_t *dirname);
int stk_ftp_exec(stk_ftp_t *ftp,char *cmd);

int stk_ftp_chdir(stk_ftp_t *ftp,char *dir);
int stk_ftp_cdup(stk_ftp_t *ftp);

int stk_ftp_mkdir(stk_ftp_t *ftp,char *dir);
int stk_ftp_rmdir(stk_ftp_t *ftp,char *dir);

void stk_ftp_get_last_error(stk_ftp_t *ftp,rtk_string_t *lastcmd,rtk_string_t *lastresp,long* lastcode);

////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_FTP_H_
