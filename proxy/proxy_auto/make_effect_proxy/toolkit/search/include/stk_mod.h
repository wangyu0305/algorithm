// stk_mod.h : header file
//

#ifndef _STK_MOD_H_
#define _STK_MOD_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////
// stk_mod_t CObject
typedef struct stk_mod_t  stk_mod_t; 

typedef struct stk_entryx_t stk_entryx_t;
struct stk_entryx_t
{
	rtk_string_t     path;
	rtk_tree_entry_t *entry;
};

void stk_entryx_init(stk_entryx_t *entryx);
void stk_entryx_term(stk_entryx_t *entryx);


struct stk_mod_t
{
    int              nsimtag;  /*相同属性的节点个数*/
    int              ntabtag;  /*子节点个数*/
    rtk_tree_entry_t *module;  /*模版父节点*/
    rtk_arrayd_t     indexd;
    rtk_lists_t      modlist;  /*模版节点列表*/
    rtk_tree_t       modtree;  /*模版树*/
};

void stk_mod_init(stk_mod_t *stkmod);
void stk_mod_term(stk_mod_t *stkmod);

int stk_mod_is_compare_node(stk_node_t *node);
int stk_mod_is_trim_node(stk_node_t *node);

void stk_mod_get_key_nodes(stk_mod_t *stkmod,char* tags,rtk_tree_entry_t *domtree,rtk_lists_t *domlistx);

int stk_mod_get_dom_list(stk_mod_t *stkmod,rtk_tree_entry_t *domtree,rtk_arrays_t *domlist);

void stk_mod_get_nodes(stk_mod_t *stkmod,char *tags,rtk_tree_entry_t *domtree,rtk_lists_t *domlistx);

stk_entryx_t* stk_mod_find_next_compare(stk_mod_t *stkmod,void **pos,rtk_lists_t *domlistx,stk_entryx_t *module);
stk_entryx_t* stk_mod_find_next_trim(stk_mod_t *stkmod,void **dom_pos,rtk_lists_t *domlistx,stk_entryx_t *module);
////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_MOD_H_
