
// cookie.h : header file
//

#ifndef _COOKIE_H_
#define _COOKIE_H_

#ifdef __cplusplus
extern "C" {
#endif

/* Process the HTTP `Set-Cookie' header*/
void cookie_handle_set_cookie (stk_session_t *session,const unsigned char *host, int port,const unsigned char *path, const unsigned char *set_cookie);
/* Generate a `Cookie' header for a request that goes to HOST:PORT and requests PATH from the server. */
void cookie_handle_get_cookie(stk_session_t *session,const unsigned char *host,int port,const unsigned char *path,int secflag,rtk_string_t *cookie_header);

#ifdef __cplusplus
}
#endif

#endif // _COOKIE_H_