// ZipFile.h : header file
//

#ifndef _ZIPFILE_H_
#define _ZIPFILE_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#define ZLIB_WINAPI
#endif

#include "zlib.h"
#include "rtk_32.h"
/////////////////////////////////////////////////////////////////////////////
// 

typedef struct stk_zip_t stk_zip_t;

struct stk_zip_t
{
    DWORD                crc32;
    DWORD                cmpsize;   
    DWORD                uncmpsize;
    rtk_string_t         filename;
    DWORD                offset; 
};

void stk_zip_from_lob(rtk_stream_t *stream,rtk_string_t *lob);
void stk_zip_to_lob(rtk_stream_t* stream,rtk_string_t *lob);

void stk_pkzip_from_lob(rtk_stream_t *stream,rtk_string_t *lob,rtk_string_t *filename);
void stk_pkzip_to_lob(rtk_stream_t *stream,rtk_string_t *lob,rtk_string_t *filename);

void stk_pkzip_from_files(rtk_stream_t *stream,rtk_string_t *filepath,rtk_list_t *filelist);
void stk_pkzip_to_files(rtk_stream_t *stream,rtk_string_t *filepath,rtk_list_t *filelist);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _ZIPFILE_H_
