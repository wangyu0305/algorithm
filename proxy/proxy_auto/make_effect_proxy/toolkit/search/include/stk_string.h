// stk_string.h : header file
//

#ifndef _STK_STRING_H_
#define _STK_STRING_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct stk_string_t stk_string_t;

struct stk_string_t
{
    const char      *buffer;
    int             length;
};

void stk_string_init(stk_string_t *string);
void stk_string_term(stk_string_t *string);

int stk_string_is_empty(stk_string_t *string);

void stk_string_shrink(stk_string_t *string);
void stk_string_trim(stk_string_t *string);

int stk_string_compare(stk_string_t *string,const char *buffer,int length);
int stk_string_ncompare(stk_string_t *string,const char *buffer,int length);

int stk_string_icompare(stk_string_t *string,const char *buffer,int length);
int stk_string_nicompare(stk_string_t *string,const char *buffer,int length);

void stk_string_copy(stk_string_t *string,stk_string_t *n_string);
void stk_string_append(rtk_string_t *string,stk_string_t *n_string);

char stk_string_get_at(stk_string_t *string,int pos);

int stk_string_find(stk_string_t *string,char *buffer,int len);

void stk_string_swap_string(rtk_string_t *string,stk_string_t *n_string);
void stk_string_swap_stream(rtk_stream_t *stream,stk_string_t *n_string);
/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_STRING_H_
