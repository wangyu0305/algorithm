
// stk_inet.h : header file
//

#ifndef _STK_INET_H_
#define _STK_INET_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  INET_OK = 0,
  INET_BAD_FUNCTION_ARGUMENT,   /* 43 */
  INET_INVALID_HEADER,
  INET_LAST /* never use! */
} code_enum;

/////////////////////////////////////////////////////////////////////////////
// options_t


typedef enum url_schemes 
{
    SCHEME_HTTP,
    SCHEME_HTTPS,
    SCHEME_FTP,
    SCHEME_INVALID
}url_schemes;

typedef enum ce_types
{
    CE_IDENTITY,
    CE_DEFLATE,
    CE_GZIP,
    CE_COMPRESS
}ce_types;

typedef enum http_code
{
    HTTP_OK = 0,
    HTTP_EMPTY_URL_NAME,
    HTTP_UNSUPPORTED_SCHEME,
    HTTP_INVALID_HOST_NAME,
    HTTP_BAD_PORT_NUMBER,
    HTTP_INVALID_USER_NAME,
    HTTP_UNTERMINATED_IPV6_ADDRESS,
    HTTP_IPV6_NOT_SUPPORTED,
    HTTP_INVALID_IPV6_ADDRESS,
    HTTP_EMPTY_CTX_OPTION,
    HTTP_EMPTY_SSL_CONTEXT,
    HTTP_SSL_INIT_FAILED,
    HTTP_OUT_OF_MEMORY,
    HTTP_BAD_CONTENT_ENCODING,
    HTTP_SOCKET_OPEN_ERROR,
    HTTP_SOCKET_CONNECT_ERROR,
    HTTP_SSL_CONNECT_ERROR,
    HTTP_REQUEST_ERROR,
    HTTP_FILESIZE_EXCEEDED,
    HTTP_HEADERSIZE_EXCEEDED,
    HTTP_CHUNK_READ_ERROR,
    HTTP_INVALID_HEADER,
    HTTP_OPERATION_TIMEDOUT,
    HTTP_HEADER_SPLIT_ERROR,
    HTTP_RESPONSE_ERROR,
    HTTP_AUTH_FAILED,
}http_code;
/////////////////////////////////////////////////////////////////////////////
// url_t

typedef struct url_t  url_t;

struct url_t
{
    rtk_string_t    url;            /* Original URL */
    url_schemes     scheme;         /* URL scheme */
    rtk_string_t    host;           /* Extracted hostname */
    int             port;           /* Port number */
    rtk_string_t    fullpath;       /* Full path */
};

/////////////////////////////////////////////////////////////////////////////
// options_t
typedef struct options_t options_t;

struct options_t
{
    int                  verbose;               /* Are we verbose? */

    int                  use_http1_1;        

    DWORD                max_allowedsize;      /* maxinum allowed file size  */

    DWORD                max_filesize;         /* Maximum file size to download */

    int                  ignore_length;        /* Do we heed content-length to check refresh?  */

    rtk_string_t         username;             /* HTTP username. */
    rtk_string_t         password;             /* HTTP password. */

    rtk_array_t          user_headers;          /* user-defined header(s)*/

    int                  keep_alive;            /* whether we use keep-alive */

    rtk_string_t         referer;               /*where the request came from*/

    rtk_string_t         proxy_server;          /* proxy name */
    DWORD                proxy_port;
    rtk_string_t         proxy_user;
    rtk_string_t         proxy_passwd;

    int                  file_type_check;      /* Do we check mimetype and extension? */

    char                 *post_data;           /* POST query string */

    int                  bsoap;
};

void options_reset(options_t *opt);

/////////////////////////////////////////////////////////////////////////////
// state_t
typedef struct state_t state_t;

struct state_t
{
    WORD                 errtype;
    DWORD                urlsize;
    DWORD                urltime;
};

/////////////////////////////////////////////////////////////////////////////
// request_t
typedef struct request_t request_t;

struct request_t
{
    rtk_string_t   method;
    rtk_string_t   arg;


    rtk_string_t   accept;
    rtk_string_t   accept_language;
    rtk_string_t   cache_control;
    rtk_string_t   connection;

    rtk_string_t   host;
    rtk_string_t   user_agent;

    rtk_arrays_t   headers;
};


/////////////////////////////////////////////////////////////////////////////
// response_t

typedef struct response_t response_t;

struct response_t
{
    rtk_string_t      data;

    //"HTTP/1.0 200 Ok\r\nDescription: some\r\n text\r\nEtag: x\r\n\r\n"
    // ^                   ^                             ^          ^
    // headers[0]          headers[1]                    headers[2] headers[3]
    char              **headers;
    int               count;

    DWORD             status;
    DWORD             urlsize;
    DWORD             urltime;
    WORD              errtype;
    WORD              errcode;

    int               contlen;    //缺省值为-1，为了区分content-length为0还是没有content-length
    int               chunked;
    int               content_encoding;
    int               keep_alive;

    rtk_string_t      date;
    rtk_string_t      last_modified;
    rtk_string_t      etag;
    rtk_string_t      filename;
    rtk_string_t      mimetype;
    rtk_string_t      charset;
    rtk_string_t      location;
    rtk_string_t      connection;
    rtk_string_t      www_authenticate;

    rtk_string_t      error_message;    //error message
};

/////////////////////////////////////////////////////////////////////////////
// stk_session_t
typedef struct stk_session_t stk_session_t ;

struct stk_session_t
{
    rtk_hash_t      cookie_jars; // store cookie_jar
    rtk_pool_t      pool;        // store cookie
    rtk_mutex_t     mutex;
};

void stk_session_init(stk_session_t *session);
void stk_session_term(stk_session_t *session);

void stk_session_destroy(stk_session_t *session);

/////////////////////////////////////////////////////////////////////////////
// stk_http_t
// stk_http_t
typedef struct stk_http_t stk_http_t;

struct stk_http_t
{
    int             active;

    rtk_socket_t    *socket;     /* The socket of the connection.  */

    options_t       opt;
    state_t         state;       /* 上一次采集的状态 */

    url_t           u;           /* 内部使用 */
    request_t       req;         /* 内部使用 */

    void            *ch;         /* 内部使用 */

    response_t      resp;

    stk_session_t   *session;

    rtk_hash_t      allow_mime_types;
    rtk_hash_t      allow_extensions;
    rtk_hash_t      dnscache;
};

void invalidate_persistent (stk_http_t *http);

void stk_http_init(stk_http_t *http);
void stk_http_term(stk_http_t *http);

int check_prefix(const unsigned char *a,const unsigned char *b);
void log_printf(options_t *opt,response_t *resp,const char *fmt, ...);

void stk_http_set_ipaddr(stk_http_t *http,char *hostname,DWORD ipaddr);
DWORD stk_http_get_ipaddr(stk_http_t *http,char *hostname);

void stk_http_set_mimetype(stk_http_t *http,char *mimetype);
void stk_http_set_extension(stk_http_t *http,char *extension);

void stk_http_set_cookie(stk_http_t *http,char *urlname,char *cookie);
void stk_http_get_cookie(stk_http_t *http,char *urlname,rtk_string_t *cookie_header);

int stk_http_set_user_header(stk_http_t *http,char *header);

options_t* stk_http_set_options(stk_http_t *http);

response_t* stk_http_get_response(stk_http_t *http);

time_t stk_http_atotm (const char *time_string);

void stk_http_option_reset(stk_http_t *http);

http_code stk_http_get(stk_http_t *http,char *urlname,rtk_string_t *source);
http_code stk_http_post(stk_http_t *http,char *urlname,char *post_data,rtk_string_t *source);


#ifdef __cplusplus
}
#endif

#endif // _STK_INET_H_