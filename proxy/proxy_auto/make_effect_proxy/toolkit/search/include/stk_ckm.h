// stk_ckm.h : header file
//

#ifndef _STK_CKM_H_
#define _STK_CKM_H_

#ifdef __cplusplus
extern "C" {
#endif

////////////////////////////////////////////////////////////////////
// stk_ckm_t CObject

#ifdef WIN32
#define _STDAPI_(type) __declspec(dllexport) type __stdcall
#else
#define _STDAPI_(type) type
#endif

#include "trs_ckm_seg.h"
#include "trs_cat.h"
#include "trs_sim.h"
#include "trs_abs.h"

typedef struct CATInfo CATInfo;
typedef struct ClsInfo ClsInfo;
typedef struct REV_SIM REV_SIM;
typedef struct ABS_REV ABS_REV;

typedef struct stk_ckm_t stk_ckm_t;

struct stk_ckm_t
{
	char*                  dirname;
    CATInfo                cat_info;
    int                    bcat;

    void                   *hrevsim;

    int                    babs;

	void                   *handle;

    int                    catcount;
    int                    threshold;
    int                    percent;
    int                    numofsub;
	int                    numofchar;
	int                    numofsen;

    rtk_string_t           errmsg;
	
};

void stk_ckm_init(stk_ckm_t *ckm);
void stk_ckm_term(stk_ckm_t *ckm);

int stk_ckm_load_cat(stk_ckm_t *ckm);
void stk_ckm_unload_cat(stk_ckm_t *ckm);

int stk_ckm_bload_cat(stk_ckm_t *ckm);
void stk_ckm_get_cat(stk_ckm_t *ckm,rtk_string_t *content,rtk_string_t *catalog);

int stk_ckm_load_sim(stk_ckm_t *ckm);
void stk_ckm_unload_sim(stk_ckm_t *ckm);

int stk_ckm_bload_sim(stk_ckm_t *ckm);

void stk_ckm_truncate_model(stk_ckm_t *ckm,rtk_string_t *lasttime);
void stk_ckm_delete_model(stk_ckm_t *ckm);

int stk_ckm_repeate_check(stk_ckm_t *ckm,rtk_string_t *id,rtk_string_t *content,rtk_string_t *simflag,long *simrank);

void stk_ckm_delete_index(stk_ckm_t *ckm,rtk_string_t *hkey);
void stk_ckm_flush_index(stk_ckm_t *ckm);

int stk_ckm_load_abs(stk_ckm_t *ckm);
void stk_ckm_unload_abs(stk_ckm_t *ckm);

int stk_ckm_bload_abs(stk_ckm_t *ckm);
void stk_ckm_get_abstract(stk_ckm_t *ckm,rtk_string_t *content,rtk_string_t *abstract,rtk_string_t *keywords);

int stk_ckm_load_extra(stk_ckm_t *ckm);
void stk_ckm_unload_extra(stk_ckm_t *ckm);

int stk_ckm_bload_extra(stk_ckm_t *ckm);
void stk_ckm_data_extra(stk_ckm_t *ckm,rtk_string_t *content,rtk_lists_t *proplist);

char* stk_ckm_get_last_error(stk_ckm_t *ckm);

int stk_ckm_load_diction();

////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _STK_CKM_H_
