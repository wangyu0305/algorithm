
// http_time.h : header file
//

#ifndef _HTTPTIME_H_
#define _HTTPTIME_H_

#ifdef __cplusplus
extern "C" {
#endif

time_t http_atotm (const char *time_string);

#ifdef __cplusplus
}
#endif

#endif // _HTTPTIME_H_