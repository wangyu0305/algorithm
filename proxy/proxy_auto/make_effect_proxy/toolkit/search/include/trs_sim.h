#ifndef _trs_api_sim_h_
#define _trs_api_sim_h_
struct REV_DOC
{
    char *id;
    double simv;
};
struct REV_SIM
{
    int revNum;
    double threshold;
    struct REV_DOC * pRevDoc;
};
struct INDUNIT
{
    char word[33];
    int df;
    int tf;
    int tfidf;
};
struct ALIGN_SEN
{
    int index;
    int score;
    int e_pos;
    int c_pos;
    int e_num;
    int c_num;
    char *e_sen;
    char *c_sen;
};
struct ALIGN_TXT
{
    int count;
    struct ALIGN_SEN *align_lst;
};
#ifdef _WIN32
typedef  void (__stdcall *pfcallback)(int hwnd,char *s);
#else
typedef  void ( *pfcallback)(int hwnd,char *s);
#endif


_STDAPI_(int) SIM_CreateModel(char *src,char *dst,int flag,char*id1,char*id2);
_STDAPI_(int) SIM_UpdateModel(char *src,void *handle,int flag,char*id1,char*id2);
_STDAPI_(int) SIM_LoadModel(char *path,void **handle);
_STDAPI_(int) SIM_LoadModelA(char *path,void **handle,int charset);
_STDAPI_(int) SIM_FreeModel(void **handle);
_STDAPI_(int) SIM_RetrieveByFile(char *content,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(int) SIM_RetrieveByText(char *content,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(void) SIM_FreeResult(struct REV_SIM *pRevSim);
_STDAPI_(int) SIM_UpdateIndex(char *id,char *content,void *handle);
_STDAPI_(int) SIM_DeleteIndex(void *handle,char *id);
_STDAPI_(int) SIM_TruncateModel(void *handle,char *timeid);
_STDAPI_(int) SIM_InsertRecord(char *content,char*id,void *handle,double threshold);
_STDAPI_(int) SIM_SimSearchByText(char *content,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(int) SIM_SetDisp(pfcallback func,int handle);
_STDAPI_(int) SIM_FlushIndex(void *handle);
_STDAPI_(int) SIM_SetAutoFlush(void *handle,int flag,int count);
_STDAPI_(int) SIM_LoadModel_MEM(char *path,void **handle);
_STDAPI_(void) SIM_FreeModel_MEM(void **handle);
_STDAPI_(int) SIM_RetrieveByText_MEM(char *content,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(int) SIM_RetrieveByFile_MEM(char *content,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(int) SIM_CreateIDX(char *src,char *dst,int flag,char*id1,char*id2);
_STDAPI_(int) SIM_BatchSearch(char*path1,char *path2);
_STDAPI_(int) SIM_BatchMerge(char *path11,char *path12,char *tdir);
_STDAPI_(int) SIM_LoadTransPairs(void **handle);
_STDAPI_(int) SIM_TextSimCE(char *c_buf,void *handle,void *thandle,struct REV_SIM *pRevSim);
_STDAPI_(int) SIM_TextSimEC(char *c_buf,void *handle,void *thandle,struct REV_SIM *pRevSim);
_STDAPI_(int) SIM_GetFeatures(struct INDUNIT*uList,char *content,int charset,int *wNum1,int simMethod,int topWords);
_STDAPI_(int) SIM_RetrieveByWord(struct INDUNIT *uList,int NumOfRev,int wNum1,struct REV_SIM *pRevSim,void *handle);
_STDAPI_(int) SIM_UpdateIndexByWord(char *id,struct INDUNIT *uList,int NumOfRev,int wNum1,void *handle);
_STDAPI_(int) SIM_AlignText(char *e_txt,char *c_txt,struct ALIGN_TXT * align_rev);
_STDAPI_(int) SIM_AlignFile(char *e_txt,char *c_txt,struct ALIGN_TXT * align_rev);
_STDAPI_(void) SIM_FreeAlignResult(struct ALIGN_TXT *pRev);
_STDAPI_(int) SIM_GetRecNum(void* handle);

#endif