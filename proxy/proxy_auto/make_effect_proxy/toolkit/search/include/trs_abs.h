#ifndef _trs_api_abs_h_
#define _trs_api_abs_h_
struct ABS_REV
{
    int percent;    /*摘要长度占全文长度的比例，如果不设置，默认设置为5*/
    char *abs;      /*摘要结果*/
    int numOfSub;   /*主题词提取个数,如果不设置，默认设置为10*/
    char *wordlist; /*主题词列表*/
    int numOfChar;  /*最大字数,如果不设置，默认设置为600*/
    int numOfSen;   /*最大句数,如果不设置，默认设置为10*/
};

_STDAPI_(int) ABS_file(char *fName,struct ABS_REV*pREV);
_STDAPI_(int) ABS_text(char *buf,struct ABS_REV*pREV);
_STDAPI_(int) ABS_biased_text(char *buf,char *subject,struct ABS_REV*pREV);
_STDAPI_(int) ABS_init();
_STDAPI_(void) ABS_free();
_STDAPI_(void) ABS_freeResult(struct ABS_REV*pREV);
_STDAPI_(int) ABS_theme(char *buf,struct ABS_REV*pREV);

#endif