#if !defined(_LEXPARSE_H)
#define _LEXPARSE_H

//#include <dblex/lexman.h>

#define TKM_WORDLENGTH              33

typedef enum
{
  TKM_ONLYPOS = 1,
  TKM_SENPOS  = 2,
  TKM_SEGPOS  = 3,
  TKM_ALLSSP  = 4
} TKE_INDEXTYPE;
typedef enum 
{
  TKM_GBCODE  = 1,
  TKM_BIGCODE = 2,
  TKM_ENGLISH = 3,
  TKM_UTF8 = 4
} TKE_LANGUAGE;
typedef  enum
{
  ENGLISH_TOKEN     = 1,    /* a search term */
  LET_PAREN_TOKEN   = 2,    /* left parenthesis */
  RGT_PAREN_TOKEN   = 3,    /* right parenthesis */
  AND_TOKEN         = 4,    /* set insertsection connective */
  OR_TOKEN          = 5,    /* set union connective */
  NOT_TOKEN         = 6,    /* set difference connective */
  END_TOKEN         = 7,    /* end of the query */
  NO_TOKEN          = 8,    /* the token is not recognized */
  CHINESE_TOKEN     = 9,    /* chinese token */
  SPACE_TOKEN       = 10,   /* space token */
  ESENTENCE_TOKEN   = 11,   /* english sentence end flag */
  CSENTENCE_TOKEN   = 12,   /* chinese sentence end flag */
  CDIGANDCH_TOKEN   = 13,   /* chinese digit and character flag */
  CSPECIAL_TOKEN    = 14,   /* special chinese character */
  LINEEND_TOKEN     = 15,   /* paragraph flag token */
  FOREIGN_TOKEN     = 16    /* for foreign words in english document, or for 4-bytes chinese */
} TKE_TokenType; 

typedef struct tagTKS_LEXTOKEN
{
  TKE_TokenType         eTokenType;     /* Token type */ 
  unsigned short                  wJumpLen;       /* the number of blank,line end and space character that to be jumped */
  char                  cRuleSign;
} TKS_LEXTOKEN;

typedef struct tagTKS_LEXENV
{
  int                  blError;
  int                  blForSearch;
  int                  blChineseAll;
  unsigned short                  wVersionSign;
  char*                 pBuffer;        /* 待切分的文本字符串 */
  unsigned int                  iBufLen;        /* 不包括字符串结束符的文本长度 */
  TKE_LANGUAGE          eLanguage;
  TKE_INDEXTYPE         eIndexType;
  int                  blStem;
  int                  blCase;
  int                  blNumber;
  unsigned short                  wMaxSeg;
  unsigned short                  wMaxSen;
  unsigned short                  wMaxPos;
  //DWORD                 wSegBegin;
  //DWORD                 wSenBegin;
  //DWORD                 wPosBegin;
  unsigned short                 wSegBegin;
  unsigned short                 wSenBegin;
  unsigned short                 wPosBegin;
  void*                 pSegmentDic;
  void*                 pCustomDic;
  void*                 pStopDic;
  void*                 pStopExt;
  void*                 pAmbiguousDic;
  void*                 pBigramDic;
  void*                 pSingleDic;
} TKS_LEXENV;

typedef struct tagTKS_LEXTERM
{
  char                  tWord[TKM_WORDLENGTH];
  unsigned short                  wSegNum;
  unsigned short                  wSenNum;
  unsigned short                  wPosNum;
  char                  cate[32];
} TKS_LEXTERM;

typedef struct tagTKS_LEXRESULT
{
  int                 wWordNum;
  TKS_LEXTERM*          rpTerm;
} TKS_LEXRESULT;

typedef struct tagTKS_LEXFIND
{
  void*                 SegmentDic;
  void*                 CommonDic;
  char*                 WildCard;
  unsigned int                  ExactHead;
  unsigned int                  FindCursor;
  unsigned int                  EndCursor;
  unsigned int                  ScopeState;
  char                  ExactWord[TKM_WORDLENGTH * 2];
} TKS_LEXFIND;

#if defined(PROTOCHECK)
int TKF_LexicalParse (TKS_LEXENV* rpSegEnv, TKS_LEXRESULT* rpSegRes);
void TKF_LexicalResultFree (TKS_LEXRESULT* rpSegRes);
const char* TKF_LexicalFindFirst (TKS_LEXFIND* FindSeed, void* SegmentDic, void* OtherDic, const char* WildCard);
const char* TKF_LexicalFindNext (TKS_LEXFIND* FindSeed);
#else
int TKF_LexicalParse ();
void TKF_LexicalResultFree ();
const char* TKF_LexicalFindFirst ();
const char* TKF_LexicalFindNext ();
#endif

#endif
