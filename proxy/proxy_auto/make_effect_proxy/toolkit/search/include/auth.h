#ifndef _STK_AUTH_H_
#define _STK_AUTH_H_

#ifdef __cplusplus
extern "C" {
#endif

int http_auth(void *http,char *dirname,char *urlname,char *username,char *password);

#ifdef __cplusplus
}
#endif

#endif // _STK_AUTH_H_