// rtk_array.h : header file
//

#ifndef _RTK_ARRAY_H_
#define _RTK_ARRAY_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_array_entry_t rtk_array_entry_t;
typedef struct rtk_array_t rtk_array_t;

struct rtk_array_entry_t
{
    char                *buffer;
};

struct rtk_array_t
{
    rtk_array_entry_t   *array;
    int                 size;
    int                 count;
};

RTK_DECLARE(void) rtk_array_init(rtk_array_t *array);
RTK_DECLARE(void) rtk_array_term(rtk_array_t *array);

RTK_DECLARE(void) rtk_array_expand(rtk_array_t *array,int size);

RTK_DECLARE(void) rtk_array_copy(rtk_array_t* array,char *buffer,int len);
RTK_DECLARE(void) rtk_array_copy_array(rtk_array_t* array,rtk_array_t* n_array);

RTK_DECLARE(void) rtk_array_append(rtk_array_t *array,char *buffer,int len);

RTK_DECLARE(void) rtk_array_append_array(rtk_array_t *array,rtk_array_t *n_array);
RTK_DECLARE(void) rtk_array_swap(rtk_array_t *array,rtk_array_t *n_array);

RTK_DECLARE(char*) rtk_array_get_at(rtk_array_t *array,int index);
RTK_DECLARE(void) rtk_array_get_at_string(rtk_array_t *array,int index,rtk_string_t *string);
RTK_DECLARE(void) rtk_array_set_at(rtk_array_t *array,int index,char *buffer,int len);

RTK_DECLARE(char*) rtk_array_get_first(rtk_array_t *array);
RTK_DECLARE(char*) rtk_array_get_last(rtk_array_t *array);

RTK_DECLARE(void) rtk_array_get_first_string(rtk_array_t *array,rtk_string_t *string);
RTK_DECLARE(void) rtk_array_get_last_string(rtk_array_t *array,rtk_string_t *string);

RTK_DECLARE(int) rtk_array_insert(rtk_array_t *array,char *buffer,int len);
RTK_DECLARE(int) rtk_array_find(rtk_array_t *array,char *buffer,int len);

RTK_DECLARE(void) rtk_array_insert_at(rtk_array_t *array,int index,char *buffer,int len);
RTK_DECLARE(void) rtk_array_remove_at(rtk_array_t *array,int index);

RTK_DECLARE(void) rtk_array_from_string(rtk_array_t *array,char *tag,rtk_string_t *string);
RTK_DECLARE(void) rtk_array_to_string(rtk_array_t *array,char *tag,rtk_string_t *string);

RTK_DECLARE(void) rtk_array_remove_all(rtk_array_t *array);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_ARRAY_H_
