// rtk_list.h : header file
//

#ifndef _RTK_LIST_H_
#define _RTK_LIST_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_list_entry_t rtk_list_entry_t;
typedef struct rtk_list_t rtk_list_t;

struct rtk_list_entry_t
{
    rtk_list_entry_t    *prev;
    rtk_list_entry_t    *next;
    char*               buffer;
};

struct rtk_list_t
{
    rtk_list_entry_t    *head;
    rtk_list_entry_t    *tail;
    int                 count;
    rtk_pool_t          *xpool;
    int                 xflag;
};

RTK_DECLARE(void) rtk_list_init(rtk_list_t *list,int xflag);
RTK_DECLARE(void) rtk_list_term(rtk_list_t *list);

RTK_DECLARE(void) rtk_list_add_head(rtk_list_t *list,char *buffer,int len);
RTK_DECLARE(void) rtk_list_add_tail(rtk_list_t *list,char *buffer,int len);

RTK_DECLARE(char*) rtk_list_add_head_new(rtk_list_t *list,int size);
RTK_DECLARE(char*) rtk_list_add_tail_new(rtk_list_t *list,int size);

RTK_DECLARE(void) rtk_list_add_head_list(rtk_list_t *list,rtk_list_t *n_list);
RTK_DECLARE(void) rtk_list_add_tail_list(rtk_list_t *list,rtk_list_t *n_list);
RTK_DECLARE(void) rtk_list_swap(rtk_list_t *list,rtk_list_t *n_list);

RTK_DECLARE(void) rtk_list_insert_before(rtk_list_t *list,void *pos,char *buffer,int len);
RTK_DECLARE(void) rtk_list_insert_after(rtk_list_t *list,void *pos,char *buffer,int len);

RTK_DECLARE(void*) rtk_list_get_head_position(rtk_list_t *list);
RTK_DECLARE(void*) rtk_list_get_tail_position(rtk_list_t *list);

RTK_DECLARE(char*) rtk_list_get_head(rtk_list_t *list);
RTK_DECLARE(char*) rtk_list_get_tail(rtk_list_t *list);

RTK_DECLARE(void) rtk_list_get_head_string(rtk_list_t *list,rtk_string_t *string);
RTK_DECLARE(void) rtk_list_get_tail_string(rtk_list_t *list,rtk_string_t *string);

RTK_DECLARE(char*) rtk_list_get_prev(rtk_list_t *list,void **pos);
RTK_DECLARE(char*) rtk_list_get_next(rtk_list_t *list,void **pos);

RTK_DECLARE(void) rtk_list_get_prev_string(rtk_list_t *list,void **pos,rtk_string_t *string);
RTK_DECLARE(void) rtk_list_get_next_string(rtk_list_t *list,void **pos,rtk_string_t *string);

RTK_DECLARE(char*) rtk_list_get_at(rtk_list_t *list,void *pos);
RTK_DECLARE(void) rtk_list_get_at_string(rtk_list_t *list,void *pos,rtk_string_t *string);
RTK_DECLARE(void*) rtk_list_find(rtk_list_t *list,char *buffer,int len);

RTK_DECLARE(void) rtk_list_remove_head(rtk_list_t *list);
RTK_DECLARE(void) rtk_list_remove_tail(rtk_list_t *list);
RTK_DECLARE(void) rtk_list_remove_at(rtk_list_t *list,void *pos);

RTK_DECLARE(void) rtk_list_from_string(rtk_list_t *list,char *tag,rtk_string_t *string);
RTK_DECLARE(void) rtk_list_to_string(rtk_list_t *list,char *tag,rtk_string_t *string);

RTK_DECLARE(void) rtk_list_remove_all(rtk_list_t *list);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_LIST_H_
