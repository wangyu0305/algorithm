// rtk_pool.h : header file
//

#ifndef _RTK_POOL_H_
#define _RTK_POOL_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_pool_entry_t rtk_pool_entry_t;
typedef struct rtk_pool_entrys_t rtk_pool_entrys_t;
typedef struct rtk_pool_list_t rtk_pool_list_t;
typedef struct rtk_pool_lists_t rtk_pool_lists_t;
typedef struct rtk_pool_t rtk_pool_t;

typedef void (*rtk_pool_term_t)(void *buffer);

struct rtk_pool_entry_t
{
    rtk_pool_entry_t    *next;
    void                *buffer;
};

struct rtk_pool_entrys_t
{
    rtk_pool_entrys_t   *prev;
    rtk_pool_entrys_t   *next;
    void                *buffer;
};

struct rtk_pool_list_t
{
    int                 index;
    rtk_pool_entry_t    *head;
    rtk_pool_entry_t    *tail;
};

struct rtk_pool_lists_t
{
    int                 index;
    rtk_pool_entrys_t   *head;
    rtk_pool_entrys_t   *tail;
};

struct rtk_pool_t
{
    int                 nolock;
    int                 memsize;
    int                 maxsize;
    int                 count;
    rtk_pool_list_t     memorylist;
    rtk_pool_lists_t    emptylist;
#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_pool_init(rtk_pool_t *pool);
RTK_DECLARE(void) rtk_pool_term(rtk_pool_t *pool);

RTK_DECLARE(void*) rtk_pool_new(rtk_pool_t *pool);
RTK_DECLARE(void) rtk_pool_free(rtk_pool_t *pool,void *buffer);

RTK_DECLARE(void) rtk_pool_empty(rtk_pool_t *pool);
RTK_DECLARE(void) rtk_pool_remove_all(rtk_pool_t *pool);
RTK_DECLARE(void) rtk_pool_free_all(rtk_pool_t *pool,rtk_pool_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_POOL_H_
