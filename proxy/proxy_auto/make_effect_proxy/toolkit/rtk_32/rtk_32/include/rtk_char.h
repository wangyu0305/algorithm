// rtk_char.h : header file
//

#ifndef _RTK_CHAR_H_
#define _RTK_CHAR_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

RTK_DECLARE(void) rtk_char_init(char **string);
RTK_DECLARE(void) rtk_char_term(char **string);

RTK_DECLARE(void) rtk_char_expand(char **string,int size);
RTK_DECLARE(void) rtk_char_trim(char *string);
RTK_DECLARE(void) rtk_char_shrink(char *string,int length);

RTK_DECLARE(int) rtk_char_is_empty(const char *string);
RTK_DECLARE(int) rtk_char_length(const char *string);

RTK_DECLARE(void) rtk_char_make_lower(char *string);
RTK_DECLARE(void) rtk_char_make_upper(char *string);

RTK_DECLARE(void) rtk_char_copy(char **string,const char *buffer,int len);
RTK_DECLARE(void) rtk_char_copy_char(char **string,char n_char);
RTK_DECLARE(void) rtk_char_copy_number(char **string,long number);

RTK_DECLARE(void) rtk_char_append(char **string,const char *buffer,int len);
RTK_DECLARE(void) rtk_char_append_char(char **string,char n_char);
RTK_DECLARE(void) rtk_char_append_number(char **string,long number);

RTK_DECLARE(long) rtk_char_get_number(char *string,long number);

RTK_DECLARE(int) rtk_char_compare(const char *string,const char *buffer,int len);
RTK_DECLARE(int) rtk_char_icompare(const char *string,const char *buffer,int len);

RTK_DECLARE(int) rtk_char_ncompare(const char *string,const char *buffer,int len);
RTK_DECLARE(int) rtk_char_nicompare(const char *string,const char *buffer,int len);

RTK_DECLARE(int) rtk_char_find_char(const char *string,char chr);
RTK_DECLARE(int) rtk_char_find_char_pos(const char *string,char chr,int pos);
RTK_DECLARE(int) rtk_char_reverse_find(const char *string,char chr);

RTK_DECLARE(int) rtk_char_find(const char *string,const char *buffer,int len);
RTK_DECLARE(int) rtk_char_find_pos(const char *string,const char *buffer,int len,int pos);

RTK_DECLARE(int) rtk_char_replace(char *string,char m_chr,char n_chr);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_CHAR_H_
