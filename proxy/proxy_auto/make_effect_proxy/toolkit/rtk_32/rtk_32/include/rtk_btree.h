// rtk_btree.h : header file
//

#ifndef _RTK_BTREE_H_
#define _RTK_BTREE_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_btree_entry_t rtk_btree_entry_t;
typedef struct rtk_btree_node_t rtk_btree_node_t;
typedef struct rtk_btree_t rtk_btree_t;

typedef void (*rtk_btree_init_t)(void *buffer);
typedef void (*rtk_btree_term_t)(void *buffer);

struct rtk_btree_entry_t
{
    char                *name;
    short               level;
    rtk_btree_entry_t   *parent;
    rtk_arrays_t        children;
};

struct rtk_btree_node_t
{
    char                *name;
    void                *buffer;
};

struct rtk_btree_t
{
    rtk_btree_entry_t   root;
    int                 count;
    rtk_pool_t          *xpool;
    int                 xflag;
    rtk_pool_t          *npool;
    int                 nflag;
    rtk_pool_t          *dpool;
    int                 dflag;
};

RTK_DECLARE(void) rtk_btree_init(rtk_btree_t* btree,int xflag,int nflag,int dflag);
RTK_DECLARE(void) rtk_btree_term(rtk_btree_t* btree);

RTK_DECLARE(void*) rtk_btree_set(rtk_btree_t* btree,char *name,rtk_btree_init_t init);
RTK_DECLARE(void*) rtk_btree_get(rtk_btree_t* btree,char *name);

RTK_DECLARE(int) rtk_btree_insert(rtk_btree_t* btree,char *name,void *buffer);
RTK_DECLARE(void*) rtk_btree_update(rtk_btree_t* btree,char *name,void *buffer);

RTK_DECLARE(int) rtk_btree_find(rtk_btree_t* btree,char *name);
RTK_DECLARE(void*) rtk_btree_remove(rtk_btree_t* btree,char *name);

RTK_DECLARE(void) rtk_btree_remove_all(rtk_btree_t* btree);
RTK_DECLARE(void) rtk_btree_free_all(rtk_btree_t* btree,rtk_btree_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_BTREE_H_
