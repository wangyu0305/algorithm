// rtk_stream.h : header file
//

#ifndef _RTK_STREAM_H_
#define _RTK_STREAM_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_stream_t rtk_stream_t;

struct rtk_stream_t
{
    BYTE             *buffer;
    int              length;
    int              size;
};

RTK_DECLARE(void) rtk_stream_init(rtk_stream_t *stream);
RTK_DECLARE(void) rtk_stream_term(rtk_stream_t *stream);

RTK_DECLARE(int) rtk_stream_is_empty(rtk_stream_t* stream);
RTK_DECLARE(void) rtk_stream_zero(rtk_stream_t *stream,int pos,int count);
RTK_DECLARE(void) rtk_stream_expand(rtk_stream_t *stream,int size);
RTK_DECLARE(void) rtk_stream_shrink(rtk_stream_t *stream,int length);

RTK_DECLARE(void) rtk_stream_copy(rtk_stream_t *stream,BYTE *buffer,int count);
RTK_DECLARE(void) rtk_stream_append(rtk_stream_t *stream,BYTE *buffer,int count);

RTK_DECLARE(void) rtk_stream_swap(rtk_stream_t *stream,rtk_stream_t *n_stream);
RTK_DECLARE(void) rtk_stream_swap_string(rtk_stream_t *stream,rtk_string_t *string);

RTK_DECLARE(BYTE) rtk_stream_get_at(rtk_stream_t *stream,int pos);
RTK_DECLARE(void) rtk_stream_set_at(rtk_stream_t *stream,int pos,BYTE byte);

RTK_DECLARE(BYTE) rtk_stream_get_byte(rtk_stream_t *stream,int* pos);
RTK_DECLARE(void) rtk_stream_set_byte(rtk_stream_t *stream,int* pos,BYTE number);

RTK_DECLARE(WORD) rtk_stream_get_word(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_word(rtk_stream_t *stream,int *pos,WORD number);

RTK_DECLARE(DWORD) rtk_stream_get_tword(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_tword(rtk_stream_t *stream,int *pos,DWORD number);

RTK_DECLARE(DWORD) rtk_stream_get_dword(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_dword(rtk_stream_t *stream,int *pos,DWORD number);

RTK_DECLARE(LWORD) rtk_stream_get_xword(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_xword(rtk_stream_t *stream,int *pos,LWORD number);

RTK_DECLARE(LWORD) rtk_stream_get_lword(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_lword(rtk_stream_t *stream,int *pos,LWORD number);

RTK_DECLARE(void) rtk_stream_get_string(rtk_stream_t *stream,int *pos,rtk_string_t *string);
RTK_DECLARE(void) rtk_stream_set_string(rtk_stream_t *stream,int *pos,rtk_string_t *string);

RTK_DECLARE(void) rtk_stream_get_stream(rtk_stream_t *stream,int *pos,rtk_stream_t *nstream);
RTK_DECLARE(void) rtk_stream_set_stream(rtk_stream_t *stream,int *pos,rtk_stream_t *nstream);

RTK_DECLARE(void) rtk_stream_get_buffer(rtk_stream_t *stream,int *pos,char *buffer,int len);
RTK_DECLARE(void) rtk_stream_set_buffer(rtk_stream_t *stream,int *pos,char *buffer,int len);

RTK_DECLARE(int) rtk_stream_get_length(DWORD number);
RTK_DECLARE(DWORD) rtk_stream_get_number(rtk_stream_t *stream,int *pos);
RTK_DECLARE(void) rtk_stream_set_number(rtk_stream_t *stream,int *pos,DWORD number);

RTK_DECLARE(DWORD) rtk_stream_crc32(rtk_stream_t *stream);
RTK_DECLARE(BYTE) rtk_stream_check(rtk_stream_t *stream);

RTK_DECLARE(int) rtk_stream_find(rtk_stream_t *stream,BYTE bt);
RTK_DECLARE(int) rtk_stream_find_pos(rtk_stream_t *stream,BYTE bt,int pos);

RTK_DECLARE(void) rtk_stream_left_string(rtk_stream_t *stream,int count,rtk_string_t *string);
RTK_DECLARE(void) rtk_stream_mid_string(rtk_stream_t *stream,int pos,int count,rtk_string_t *string);
RTK_DECLARE(void) rtk_stream_mids_string(rtk_stream_t *stream,int pos,rtk_string_t *string);
RTK_DECLARE(void) rtk_stream_right_string(rtk_stream_t *stream,int count,rtk_string_t *string);

RTK_DECLARE(void) rtk_stream_left_stream(rtk_stream_t *stream,int count,rtk_stream_t *n_stream);
RTK_DECLARE(void) rtk_stream_mid_stream(rtk_stream_t *stream,int pos,int count,rtk_stream_t *n_stream);
RTK_DECLARE(void) rtk_stream_mids_stream(rtk_stream_t *stream,int pos,rtk_stream_t *n_stream);
RTK_DECLARE(void) rtk_stream_right_stream(rtk_stream_t *stream,int count,rtk_stream_t *n_stream);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_STREAM_H_
