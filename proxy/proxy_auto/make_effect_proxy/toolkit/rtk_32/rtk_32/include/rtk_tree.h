// rtk_tree.h : header file
//

#ifndef _RTK_TREE_H_
#define _RTK_TREE_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_tree_entry_t rtk_tree_entry_t;
typedef struct rtk_tree_t rtk_tree_t;

typedef void (*rtk_tree_init_t)(void *buffer);
typedef void (*rtk_tree_term_t)(void *buffer);
typedef int (*rtk_tree_compare_t)(void *buffer,void *name);

struct rtk_tree_entry_t
{
    rtk_tree_entry_t    *parent;
    rtk_tree_entry_t    *child;
    rtk_tree_entry_t    *prev;
    rtk_tree_entry_t    *next;
    void                *buffer;
};

struct rtk_tree_t
{
    rtk_tree_entry_t    root;
    int                 count;
    rtk_pool_t          *xpool;
    int                 xflag;
    rtk_pool_t          *dpool;
    int                 dflag;
};

RTK_DECLARE(void) rtk_tree_init(rtk_tree_t *tree,int xflag,int dflag);
RTK_DECLARE(void) rtk_tree_term(rtk_tree_t *tree);

RTK_DECLARE(rtk_tree_entry_t*) rtk_tree_insert(rtk_tree_t* tree,char *tag,rtk_string_t *name,rtk_tree_init_t init,rtk_tree_compare_t compare);
RTK_DECLARE(rtk_tree_entry_t*) rtk_tree_find(rtk_tree_t* tree,char *tag,rtk_string_t *name,rtk_tree_compare_t compare);

RTK_DECLARE(void) rtk_tree_remove_all(rtk_tree_t *tree);
RTK_DECLARE(void) rtk_tree_free_all(rtk_tree_t *tree,rtk_tree_term_t term);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_TREE_H_
