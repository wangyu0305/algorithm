// rtk_timer.h : header file
//

#ifndef _RTK_TIMER_H_
#define _RTK_TIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

#define MAX_TIMER 8

typedef struct rtk_timer_t rtk_timer_t;
typedef struct rtk_timer_entry_t rtk_timer_entry_t;

typedef void (*rtk_timer_process_t)(int timeid,void *param);

struct rtk_timer_entry_t
{
    DWORD               interval;
    DWORD               nextval;
    void                *param;
};

struct rtk_timer_t
{
    int                 closed;
    DWORD               interval;
    rtk_thread_t        thread;
    rtk_timer_entry_t   timers[MAX_TIMER];
    rtk_timer_process_t process;
#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_timer_sleep(DWORD micsec);

RTK_DECLARE(void) rtk_timer_init(rtk_timer_t *timer);
RTK_DECLARE(void) rtk_timer_term(rtk_timer_t *timer);

RTK_DECLARE(void) rtk_timer_set(rtk_timer_t *timer,int timeid,DWORD interval,void *param);
RTK_DECLARE(void) rtk_timer_kill(rtk_timer_t *timer,int timeid);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_TIMER_H_
