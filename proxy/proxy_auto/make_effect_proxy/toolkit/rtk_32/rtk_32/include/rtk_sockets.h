// rtk_sockets.h : header file
//

#ifndef _RTK_SOCKETS_H_
#define _RTK_SOCKETS_H_

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////

typedef struct rtk_sockets_t rtk_sockets_t;

struct rtk_sockets_t
{
    int                 count;
    rtk_socket_t        *array;

#ifdef RTK_MULTI_THREAD
    rtk_mutex_t         mutex;
#endif
};

RTK_DECLARE(void) rtk_sockets_init(rtk_sockets_t *sockets);
RTK_DECLARE(void) rtk_sockets_term(rtk_sockets_t *sockets);

RTK_DECLARE(void) rtk_sockets_expand(rtk_sockets_t *sockets,int count);

RTK_DECLARE(rtk_socket_t*) rtk_sockets_new(rtk_sockets_t *sockets);
RTK_DECLARE(int) rtk_sockets_is_use(rtk_sockets_t *sockets,rtk_socket_t *socket);
RTK_DECLARE(void) rtk_sockets_free(rtk_sockets_t *sockets,rtk_socket_t *socket);

/////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif

#endif // _RTK_SOCKETS_H_
