/************************************************************************/
/*         AVL balance tree                                           */
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define type int

typedef struct tree_node tree_node;
typedef struct tree_root tree_root;

struct tree_node 
{
	type key;
	int heigth;
	tree_node* left,*right;
};

struct tree_root 
{
	tree_node* root;
};

int tree_max(int a,int b)
{
	return a > b ? a : b;
}

void tree_init(tree_root* root)
{
	root->root = NULL;
}

tree_node* tree_create_node(type key)
{
	tree_node* node;
	node = (tree_node*)malloc(sizeof(tree_node));
	node->key = key;
	node->left = NULL;
	node->right = NULL;
	node->heigth = 0;

	return node;
}

inline int node_height(tree_node* node)
{
	if (node->left == NULL && node->right == NULL)
	{
		return 0;
	}
	else if (node->left == NULL)
	{
		return node->right->heigth + 1;
	}
	else if (node->right == NULL)
	{
		return node->left->heigth + 1;
	}
	else
	{
		return tree_max(node->left->heigth,node->right->heigth) + 1;
	}
}

tree_node* tree_right_rotate(tree_node* node)    //rotate right
{
	tree_node* newnode;
	newnode = node->left;
	node->left = newnode->right;
    newnode->right = node;

	node->heigth = node_height(node);

	if (node->left != NULL)
	{
		node->left->heigth = node_height(node->left);
	}

	newnode->heigth = tree_max(newnode->left->heigth,newnode->right->heigth);

	return newnode;
}

tree_node* tree_left_rotate(tree_node* node)    //rotate left
{
	tree_node* newnode;
	newnode = node->right;
	node->right = newnode->left;
	newnode->left = node;

	node->heigth = node_height(node);

	if (node->right != NULL)
	{
		node->right->heigth = node_height(node->right);
	}
	
	newnode->heigth = tree_max(newnode->left->heigth,newnode->right->heigth);

	return newnode;
}

tree_node* tree_left_right_rotate(tree_node* node)   //left then right
{
	node->left = tree_left_rotate(node->left);
    return tree_right_rotate(node);
}

tree_node* tree_right_left_rotate(tree_node* node)   //right then left
{
	node->right = tree_right_rotate(node->right);
	return tree_left_rotate(node);
}

tree_node* tree_insert_node(tree_node* node,type key)
{
	if (node == NULL)
	{
		node = tree_create_node(key);
		return node;
	}
	if (key < node->key)
	{
		node->left = tree_insert_node(node->left,key);
		if ((node->right != NULL && node->left->heigth - node->right->heigth == 2) ||
			(node->right == NULL && node->heigth == 2))
		{
			if (key < node->left->key)
			{
				node = tree_right_rotate(node);
			}
			else
			{
				node = tree_left_right_rotate(node);
			}
		}
	}
	else
	{
		node->right = tree_insert_node(node->right,key);
		if ((node->left != NULL && node->right->heigth - node->left->heigth == 2) ||
			(node->left == NULL && node->heigth == 2))
		{
			if (key > node->right->key)
			{
				node = tree_left_rotate(node);
			}
			else
			{
				node = tree_right_left_rotate(node);
			}
		}
	}
	if (node->left == NULL)
	{
		node->heigth = node->right->heigth + 1;
	}
	else if (node->right == NULL)
	{
		node->heigth = node->left->heigth + 1;
	}
	else
	{
        node->heigth = tree_max(node->left->heigth,node->right->heigth) + 1;
	}

	return node;
}

bool tree_find(tree_node* node,type key)
{
	if (node == NULL)
	{
		return false;
	}
	if (node->key == key)
	{
		return true;
	}
	else if (key < node->key)
	{
        return tree_find(node->left,key);
	}
	else
	{
		return tree_find(node->right,key);
	}
}

int tree_count(tree_node* node)
{
	int t_left = 0,t_right = 0;
	if (node->left != NULL)
	{
		t_left = tree_count(node->left);
        t_left += 1;
	}
	if (node->right != NULL)
	{
		t_right = tree_count(node->right);
		t_right += 1;
	}
	
	return tree_max(t_left,t_right);
}

int tree_count_deep(tree_node* root)
{
	int t_left = 0,t_right = 0;
	if (root == NULL)
	{
		return 0;
	}
    if (root->left != NULL)
    {
		t_left = tree_count(root->left);
    }
	if (root->right != NULL)
	{
		t_right = tree_count(root->right);
	}
	if (t_right > t_left)
	{
		return t_right - t_left;
	}
	
	return t_left - t_right;
}

tree_node* tree_rotate(tree_node* node)
{
	int deep;
	deep = tree_count_deep(node);
	if (deep == 2)
	{
		tree_right_rotate(node);
	}
	if (deep == -2)
	{
		tree_left_rotate(node);
	}

	return node;
}

void tree_delete_brance(tree_node** node)
{
	if (*node == NULL)
	{
		return ;
	}
	if ((*node)->left != NULL)
	{
		tree_delete_brance(&(*node)->left);
	}
	if ((*node)->right != NULL)
	{
		tree_delete_brance(&(*node)->right);
	}
	if ((*node)->left == NULL && (*node)->right == NULL)
	{
		free((*node));
		(*node) = NULL;
	}
    
}

void tree_destory(tree_root* root)
{
	if (root == NULL || root->root == NULL)
	{
		return ;
	}
	if (root->root->left != NULL)
	{
		tree_delete_brance(&root->root->left);
	}
	if (root->root->right != NULL)
	{
		tree_delete_brance(&root->root->right);
	}
	free(root->root);
	
}

tree_node* tree_delete_node(tree_node* root,type key)
{
	if (root == NULL)
	{
		return NULL;
	}
    if (root->key == key)
    {
		if (root->right == NULL)
		{
			tree_node* temp;
			temp = root;
			root = root->left;
			free(temp);
			temp = NULL;
			return root;
		}
		else
		{
			tree_node* newnode;
			newnode = root->right;
			while (newnode->left)
			{
				newnode = newnode->left;
			}
			root->key = newnode->key;
            root->right = tree_delete_node(root->right,root->key);
			root->heigth = tree_max(root->left->heigth,root->right->heigth) + 1;
		}
    }
	else if (root->key < key)
	{
		root->right = tree_delete_node(root->right,key);
		if (root->right)
		{
			tree_rotate(root->right);
		}
	}
	else
	{
		root->left = tree_delete_node(root->left,key);
		if (root->left)
		{
			tree_rotate(root->left);
		}
	}
    if (root)
    {
		tree_rotate(root);
    }

	return root;
}

int a_main(int argc, char **argv)
{
	int a[20] = {0,1,2,3,4,5,6,7,8,9,10,11,19,13,14,18,16,17,15,12};
	tree_root treeroot;
	tree_init(&treeroot);
	for (int i = 0; i < 20; i++)
	{
		treeroot.root = tree_insert_node(treeroot.root,a[i]);
	}
	int m = tree_find(treeroot.root,a[8]);
	tree_delete_node(treeroot.root,a[5]);
    tree_destory(&treeroot);

	return 0;
}