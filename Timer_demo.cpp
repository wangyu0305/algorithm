#include <iostream>
#include <Windows.h>
#include "Timer.h"

using namespace std;

void (*func)(UINT,UINT,DWORD);

void function(/*void* pam*/UINT pam,UINT_PTR par,DWORD pas)
{
	//int i = par;
	int b = *(int*)pam;
	//int as = pas;
	//cout<<i<<endl;
	cout<<b<<endl;
	cout<<"send"<<endl;
	//cout<<as<<endl;
}

int main(int argc, char **argv)     //Timer.cpp main
{
	int m = 10;
	timer ti;
	int* p = &m;
	ti.register_handle(function,p);
	ti.set_interval(1000 * 5);
	ti.start();
	Sleep(1000);
	for (;m > 0;m--)
	{
		cout<<"print"<<endl;
		Sleep(1000);
	}
	ti.cancel();
	return 0;
}

//int main(int argc, char **argv)
//{
//	func  = function;
//	TIMERPROC fun = (TIMERPROC)func;
//	UINT timerid = SetTimer(NULL,1,10000,fun);
//    MSG m_msg;
//    while (GetMessage(&m_msg,NULL,0,0))
//    {
//		if (m_msg.message == WM_TIMER)
//		{
//			TranslateMessage(&m_msg);
//			DispatchMessage(&m_msg);
//		}
//    }
//	KillTimer(NULL,timerid);
//	return 0;
//}