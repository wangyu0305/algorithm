#include "threadPool.h"

void test_run(void* arg){
    int* i = (int*)arg;
    printf("%d\n",*i);
}

int main(int argc,char* argv[]){
    thread_pool_t* pool = (thread_pool_t*)malloc(sizeof(thread_pool_t));
    thread_pool_init(pool,10);
    int tmp[100];
    for(int i = 0; i < 100; i++){
    	tmp[i] = i;
        thread_pool_add_runner(pool,test_run,&tmp[i]);
    }
    sleep(1);
    thread_pool_destroy(&pool);
    printf("main -> %p\n",pool);
    return 0;
}
