#include "threadPool.h"
#include <pthread.h>


void* run(void *arg){
    thread_pool_t* pool = (thread_pool_t*)arg;
    printf("start thread 0x%x\n",pthread_self());
    while(1){
        pthread_mutex_lock(&(pool->mutex));
        /*no idle thread must wait */
        while(pool->runner_head == NULL && !pool->shutdown){
            pthread_cond_wait(&(pool->cond),&(pool->mutex));
        }
        if(pool->shutdown){
            pthread_mutex_unlock(&(pool->mutex));       //if close thread_pool release mutex
            pthread_exit(NULL);
        }
        thread_runner_t *runner = pool->runner_head;
        pool->runner_head = runner->next;
        pthread_mutex_unlock(&(pool->mutex));
        (runner->callback)(runner->arg);
        free(runner);
        runner = NULL;
    }
    pthread_exit(NULL);
}

void thread_pool_init(thread_pool_t* pool,int max_thread_size){
    pthread_mutex_init(&(pool->mutex),NULL);
    pthread_cond_init(&(pool->cond),NULL);
    pool->runner_head = NULL;
    pool->runner_tail = NULL;
    pool->max_thread_size = max_thread_size;
    pool->shutdown = 0;
    pool->threads = (pthread_t*)malloc(max_thread_size * sizeof(pthread_t));
    for(int i = 0; i < max_thread_size; i++){
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
        pthread_create(&(pool->threads[i]),&attr,run,(void*)pool);
    }
}

void thread_pool_destroy(thread_pool_t **pool_pointer){
    thread_pool_t *pool = *pool_pointer;
    if(!pool->shutdown){
        pool->shutdown = 1;
        pthread_cond_broadcast(&(pool->cond));           //weak up all wait threads, pool will be destory
        sleep(0.5);
        free(pool->threads);
        thread_runner_t* head = NULL;
        while(pool->runner_head != NULL){
            head = pool->runner_head;
            pool->runner_head = head->next;
            free(head);
        }
        pthread_mutex_destroy(&(pool->mutex));
        pthread_cond_destroy(&(pool->cond));
        free(pool);
    }
}

void thread_pool_add_runner(thread_pool_t* pool,void (*callback)(void* arg),void* arg){
    thread_runner_t* newrunner = (thread_runner_t*)malloc(sizeof(thread_runner_t));
    newrunner->callback = callback;
    newrunner->arg = arg;
    newrunner->next = NULL;
    pthread_mutex_lock(&(pool->mutex));
    if(pool->runner_head != NULL){
        pool->runner_tail->next = newrunner;
        pool->runner_tail = newrunner;
    }else{
        pool->runner_head = newrunner;
        pool->runner_tail = newrunner;
    }
    pthread_mutex_unlock(&(pool->mutex));
    pthread_cond_signal(&(pool->cond));              //weak up a waiting thread
}
