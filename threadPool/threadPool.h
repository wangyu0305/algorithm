#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

/*
 * struct runner 
 */

typedef struct runner{
    void (*callback)(void *arg);        //func pointer for callback
    void *arg;                          //arg for func
    struct runner *next;
}thread_runner_t;

typedef struct thread_pool{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    thread_runner_t* runner_head;        //head thread_runner of thread_pool
    thread_runner_t* runner_tail;        //tail thread_runner of thread_pool
    int shutdown;                        //close the thread_pool
    pthread_t* threads;                  //all threads of pool
    int max_thread_size;                 //max thread to use of thread_pool
}thread_pool_t;

#ifdef __cplusplus__
extern "C" {
#endif
    void* run(void *arg);
    void thread_pool_init(thread_pool_t* pool,int max_thread_size);
    void thread_pool_add_runner(thread_pool_t* pool,void (*callback)(void* arg),void *arg);
    void thread_pool_destroy(thread_pool_t **pool);
#ifdef __cplusplus__
}
#endif

#endif          //end define THREADPOOL_H_
