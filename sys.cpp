/************************************************************************/
/*       内存池      定时器                                             */
/************************************************************************/

/*          内存池           */
#include <string.h>
#include <stdlib.h>
#define INITSIZE 10
#define INCRESIZE 5
#define TYPE char

typedef struct mem_pool mem_pool_t;
typedef struct schar schar_t;

struct schar
{
	mem_pool_t* mpool;
};

struct mem_pool 
{
	TYPE* str;
	int length;
    mem_pool_t* npool;
};

void mem_pool_init(mem_pool_t** mpool)
{
	(*mpool) = (mem_pool_t*)malloc(sizeof(mem_pool_t));
	(*mpool)->str = (TYPE*)malloc(sizeof(TYPE)*INITSIZE);
	memset((*mpool)->str,0,sizeof(TYPE)*INITSIZE);
	(*mpool)->npool = NULL;
	(*mpool)->length = 0;
}

void mem_pool_increase(mem_pool_t** mpool)
{
	(*mpool) = (mem_pool_t*)malloc(sizeof(mem_pool_t));
	(*mpool)->str = (TYPE*)malloc(sizeof(TYPE)*INCRESIZE);
	memset((*mpool)->str,0,sizeof(TYPE)*INCRESIZE);
	(*mpool)->npool = NULL;
	(*mpool)->length = 0;
}

void mem_incre_reset(mem_pool_t** mpool)
{
	memset((*mpool)->str,0,sizeof(TYPE)*INCRESIZE);
	(*mpool)->length = 0;
	if ((*mpool)->npool != NULL)
	{
		mem_incre_reset(&(*mpool)->npool);
	}
}

void mem_pool_reset(mem_pool_t** mpool)
{
	memset((*mpool)->str,0,sizeof(TYPE)*INITSIZE);
	(*mpool)->length = 0;
	if ((*mpool)->npool != NULL)
	{
		mem_incre_reset(&(*mpool)->npool);
	}
}

void mem_pool_term(mem_pool_t** mpool)
{
	if ((*mpool)->npool == NULL)
	{
		free((*mpool)->str);
		(*mpool)->str = NULL;
	}
	else
	{
		mem_pool_term(&(*mpool)->npool);
	}
	free((*mpool));
	(*mpool) = NULL;
}

schar_t* schar_init(schar_t* sc)
{
	sc = (schar_t*)malloc(sizeof(schar_t));
	sc->mpool = NULL;

	return sc;
}

void schar_incre_set(mem_pool_t** mpool,char* str)
{
	if ((*mpool) == NULL)
	{
		mem_pool_increase(mpool);
	}
	(*mpool)->length = 0;
	while (*str)
	{
		(*mpool)->str[(*mpool)->length] = *str;
		(*mpool)->length++;
		str++;
		if ((*mpool)->length == INCRESIZE)
		{
			schar_incre_set(&(*mpool)->npool,str);
			return ;
		}
	}
}

void schar_set_copy(schar_t* sc,char* str)
{
	if (sc->mpool == NULL)
	{
		mem_pool_init(&sc->mpool);
	}
	mem_pool_reset(&sc->mpool);
	while (*str)
	{
		sc->mpool->str[sc->mpool->length] = *str;
		sc->mpool->length++;
		str++;
        if (sc->mpool->length == INITSIZE)
        {
			schar_incre_set(&sc->mpool->npool,str);
			return ;
        }
	}
}

char* schar_get_str(schar_t* sch)
{
	char* str,*increstr;
	int length,i = 0;
	mem_pool_t* temp;
	temp = sch->mpool;
	length = sch->mpool->length;
	str = (char*)malloc(sizeof(char)*(length));
	while (1)
	{
		increstr = temp->str;
		while (i < length)
		{
			str[i] = *increstr;
			increstr++;
			i++;
		}
		if (temp->npool != NULL && temp->npool->length != 0)
		{
			temp = temp->npool;
			length += (temp->length);
			str = (char*)realloc(str,length);
		}
		else
		{
			return str;
		}
	}

	return str;
}

void schar_term(schar_t* sc,char* str)
{
	if (str != NULL)
	{
		free(str);
	}
	if (sc == NULL)
	{
		return;
	}
	mem_pool_term(&sc->mpool);
	free(sc);
}

//int main(int argc, char **argv)
//{
//	schar_t* sc = NULL;
//	char* str = "stampyinfomationconstruct";
//	char* stam = "infomationconstruct";
//	char* sam = NULL;
//	sc = schar_init(sc);
//	schar_set_copy(sc,str);
//	schar_set_copy(sc,stam);
//	sam = schar_get_str(sc);
//  schar_term(sc,sam);
//
//	return 0;
//}

typedef struct mstring mstring_t;

struct mstring
{
	char* buffer;
	int length;
	int size;
};

void mstring_init(mstring_t* mstr)
{
	mstr->buffer = NULL;
	mstr->length = 0;
	mstr->size = 0;
}

void mstring_term(mstring_t* mstr)
{
	if (mstr->buffer != NULL)
	{
		free(mstr->buffer);
	}
	mstr->length = 0;
	mstr->size = 0;
}

void mstring_reset(mstring_t* mstr)
{
	if (mstr->buffer != NULL)
	{
		memset(mstr->buffer,0,sizeof(char)*mstr->size);
	}
	mstr->length = 0;
}

void mstring_copy(mstring_t* mstr,char* str,int slength)
{
	if (mstr->buffer == NULL)
	{
		mstr->buffer = (char*)malloc(sizeof(char)*(slength+1));
		mstr->size = slength+1;
	}
	else if (mstr->size < slength)
	{
		mstr->buffer = (char*)realloc(mstr->buffer,slength+1);
		mstr->size = slength+1;
	}
	
	mstring_reset(mstr);
	while (mstr->length < slength)
	{
		mstr->buffer[mstr->length] = *str;
		mstr->length++;
		str++;
	}
	mstr->buffer[mstr->length] = '\0';
}

void mstring_cat(mstring_t* mstr,char* str,int slength)
{
	if (mstr->buffer == NULL)
	{
		mstr->buffer = (char*)malloc(sizeof(char)*(slength+1));
		mstring_reset(mstr);
		mstr->size = slength+1;
	}
	else if ((mstr->size - mstr->length) < slength)
	{
		mstr->buffer = (char*)realloc(mstr->buffer,sizeof(char)*(slength+mstr->length+1));
		mstr->size = slength + mstr->length + 1;
	}
	while (slength--)
	{
		mstr->buffer[mstr->length] = *str;
		mstr->length++;
		str++;
	}
	mstr->buffer[mstr->length] = '\0';
}

void mstring_substring(mstring_t* mstr,int startlen,int endlen,mstring_t* sstr)
{
	if (startlen >= endlen && startlen >= 0)
	{
		if (startlen > endlen)
		{
			return ;
		}
		if (sstr->size < 1)
		{
			sstr->buffer = (char*)malloc(sizeof(char)*2);
			sstr->size = 2;
		}
		sstr->buffer[0] = mstr->buffer[startlen];
		sstr->buffer[1] = '\0';
		return ;
	}
	if (endlen > mstr->length)
	{
		endlen = mstr->length;
	}
	startlen--;
	if (sstr->buffer == NULL)
	{
		sstr->buffer = (char*)malloc(sizeof(char)*(endlen - startlen + 1));
		mstring_reset(sstr);
		sstr->size = (endlen - startlen) + 1;
	}
	else if (sstr->size < (endlen - startlen + 1))
	{
		sstr->buffer = (char*)realloc(sstr->buffer,sizeof(char)*(endlen - startlen + 1));
		mstring_reset(sstr);
		sstr->size = (endlen - startlen) + 1;
	}
	else
	{
		mstring_reset(sstr);
	}
	while (startlen < endlen)
	{
		sstr->buffer[sstr->length] = mstr->buffer[startlen];
		sstr->length++;
		startlen++;
	}
	sstr->buffer[sstr->length] = '\0';
}

/*         定时器   执行函数  执行进程 启动新进程  执行过程中发送消息或置标志位   */
#include <stdio.h>
#include <windows.h>
#include <time.h>

int flag;

int execut_func(int& i)
{
	mstring_t mstr,sstr;
	mstring_init(&mstr);
	mstring_init(&sstr);
	char* sa = "stampyinct";
	char* sb = "fomatio";
	char* sc = "nconstruct";
    mstring_copy(&mstr,sa,strlen(sa));
	mstring_copy(&mstr,sb,strlen(sb));
	mstring_cat(&mstr,sc,strlen(sc));
	mstring_substring(&mstr,5,10,&sstr);
	printf("%s\n",mstr.buffer);
	printf("%s\n",sstr.buffer);
	mstring_term(&mstr);
	mstring_term(&sstr);
    i++;
	if (i > 5)
	{
		flag = 0;
	}
	return 0;
}

void timer(long mic)
{
	int i = 0;
	while (flag)
	{
		execut_func(i);
		Sleep(mic);
	}
}

BYTE lower_bit(short number,int sbit,int ebit)
{
	BYTE lobit;
	short lonum;
	lonum = number & 0x0000000f;
	number &= 0x000000f0;
	lobit = lonum & 0x00000003;
	lobit = lobit + (number & 0x00000030);
	return lobit;
	lobit = lobit >> 4;
	lobit = 2 << lobit;
}

void bigrand(int m,int n)
{
	srand(clock());
	for (int i = 0;i < n;i++)
	{
		int sam = rand()%(n-i);
		if ( sam < m)
		{
			printf("%d\t",sam);
			m--;
		}
	}
}

//int main(int argc, char **argv)
//{
//	bigrand(10,100);
//	flag = 1;
//	lower_bit(255,0,0);
//    timer(10000);
//    return 0;
//}