#include <stdio.h>
#include <string.h>

#define MAX_SIZE 100

typedef struct
{
    void* value[MAX_SIZE];
    int i;
}stack;

void stack_init(stack* stk)
{
    memset(stk,0,sizeof(*stk));
}

void stack_push(stack* stk,void* value)
{
    if(stk->i == MAX_SIZE)
        return ;
    stk->value[stk->i] = value;
    stk->i++;
}

void* stack_pop(stack* stk)
{
    if(stk->i == 0)
        return NULL;
    stk->i--;
    void* tmp = stk->value[stk->i];
    return tmp;
}

int main(int argc,char** argv)
{
    stack stk;
    stack_init(&stk);
    char a[10] = "asdfghjkl";
    for(int i=0;i<10;i++)
    {
        stack_push(&stk,&a[i]);
        printf("%d  %c",stk.i,*(char*)(stk.value[stk.i-1]));
    }
    while(stk.i > 0)
    {
        stack_pop(&stk);
        printf("%d",stk.i);
    }
}
