/*循环队列*/                           //有错 0位不能插入
#include<stdio.h>
#include<stdlib.h>

#define MaxSize 10
typedef int ElemType;
typedef struct
{
	ElemType data[MaxSize];
	int front;
	int rear;
}CircSeqQueue;

//顺序循环队列的初始化
void QueueInitial(CircSeqQueue *pQ)
{
	//创建一个又指针pQ所指向的空顺序循环队列
	pQ->front=pQ->rear=0;
}

//顺序循环队列判空
int IsEmpty(CircSeqQueue *pQ)
{//顺序循环队列为空时返回1，否则返回0
	return pQ->front==pQ->rear;
}

//顺序循环队列判满
int IsFull(CircSeqQueue *pQ)
{//循环队列为满时返回1，否则返回0
	return (pQ->rear+1)%MaxSize==pQ->front;
}

//元素进队
void EnQueue(CircSeqQueue *pQ, ElemType e)
{//若队列不满，则元素e进队
	if(IsFull(pQ))//队列已满，退出
	{
		printf("队列溢出！\n");
		exit(1);
	}
	pQ->rear=(pQ->rear+1)%MaxSize;//队尾指针后移
	pQ->data[pQ->rear]=e;
}

//元素出队
ElemType DeQueue(CircSeqQueue *pQ)
{//若循环队列不为空，则删除队头元素，并返回它的值
	if(IsEmpty(pQ))//队列为空，退出
	{
		printf("空队列!\n");
		exit(1);
	}
	pQ->front=(pQ->front+1)%MaxSize;//队头指针后移
	return pQ->data[pQ->front];
}

//取队头元素
ElemType GetFront(CircSeqQueue *pQ)
{//若队列不为空，则返回队头元素的值
	if(IsEmpty(pQ))
	{
		printf("空队列!\n");
		exit(1);
	}
	return pQ->data[(pQ->front+1)%MaxSize];
}

//循环队列置空
void MakeEmpty(CircSeqQueue *pQ)
{//将由指针pQ所指向的队列变为孔队
	pQ->front=pQ->rear=0;
}
