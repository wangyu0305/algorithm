/*我们是用每一个元素表示一个32位的二进制字符串,这样这个元素可以保留相邻32个号码是否存在的信息,数组范围就下降到10000000/32了.
例如对于号码 89256,由于89256 mod 32=2789…8,这样我们应该置a[2789]中32位字符串的第8位(从低位数起)为1. 
*/

#define INT_BITS sizeof(int)   // INT_BITS 32
#define SHIFT 5 // 2^5=32
#define MASK 0x1f // 31
#define MAX 1024*1024*1024 //max number
int bitmap[MAX / INT_BITS];

void set(int i)              /*   * 设置第i位   * i >> SHIFT 相当于 i / (2 ^ SHIFT),   * i&MASK相当于mod操作 m mod n 运算   */
{
	bitmap[i >> SHIFT] |= 1 << (i & MASK);    //bitmap[i >> SHIFT] |=  (i & MASK) << 1;
}

int test(int i)              //获取第i位 
{
	return bitmap[i >> SHIFT] & (1 << (i & MASK));   //bitmap[i >> SHIFT] &  (i & MASK) << 1;
}

int clear(int i) 
{
	return bitmap[i >> SHIFT] & ~(1 << (i & MASK));   //bitmap[i >> SHIFT] & ~((i & MASK) << 1);
}  //清除第i位  

int my_main(int argc, char **argv)
{
	int i = 1 << 4;
	for (i = 0;i < 5;i++)
	{
		int t = 1 << (10 & MASK) | bitmap[10 >> SHIFT];
		set(i);
	}
	for (i = 0;i < 5;i++)
	{
		int rec = test(i);
	}
	return i;
}
