#include <winsock2.h>
#pragma comment(lib,"WS2_32")
#include <WS2tcpip.h>
#include <iostream>
#include <stdlib.h>

using namespace std;

#define RECV_BUFFER_SIZE 8192
const int port = 80;

class CInitSock
{
public:
	CInitSock(BYTE minorVer = 2,BYTE majorVer = 2)
	{
		WSADATA wsaData;
		WORD sockVersion = MAKEWORD(minorVer,majorVer);
		if(WSAStartup(sockVersion,&wsaData) != 0){
			exit(0);
		}
	}
	~CInitSock()
	{
		WSACleanup();
	}
};

CInitSock theSock;

int main(int argc, char **argv)
{
	SOCKADDR_IN addr_info;
	SOCKET connect_socket;
	FILE* pfile;
	char resev_buffer[RECV_BUFFER_SIZE];
	int resev_byte = 0,send_byte = 0;
	size_t pos = 0;
	int errorcode,buflen,optlen = sizeof(buflen);

    const char * inet_ntop_v4 (const void *src, char *dst, size_t size);
	//static const char * inet_ntop_v6 (const u_char *src, char *dst, size_t size);
	const char * inet_ntop(int af, const void *src, char *dst, size_t size);

	connect_socket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if (connect_socket == INVALID_SOCKET)
	{
		cout<<"INVALID_SOCKET";
		exit(1);
	}

	errorcode = getsockopt(connect_socket,SOL_SOCKET,SO_RCVBUF,(char*)&buflen,&optlen);
	buflen *= 10;
	errorcode = setsockopt(connect_socket,SOL_SOCKET,SO_RCVBUF,(char*)&buflen,optlen);
	errorcode = getsockopt(connect_socket,SOL_SOCKET,SO_RCVBUF,(char*)&buflen,&optlen);
	errorcode = getsockopt(connect_socket,SOL_SOCKET,SO_SNDBUF,(char*)&buflen,&optlen);
	buflen *= 10;
	errorcode = setsockopt(connect_socket,SOL_SOCKET,SO_SNDBUF,(char*)&buflen,optlen);
	errorcode = getsockopt(connect_socket,SOL_SOCKET,SO_SNDBUF,(char*)&buflen,&optlen);

	char* get_command = "GET /session HTTP/1.1\r\n\r\n";
	char *ptr = "t.163.com";
	char addr[32];
	struct hostent *hptr;
	if ((hptr = gethostbyname(ptr)) == NULL)
	{
		printf("gethostbyname errno host name :%s  \n",ptr);
	}

	printf("office name : %s\t",hptr->h_name);
	printf("address :%s",inet_ntop(hptr->h_addrtype,hptr->h_addr_list[0],addr,sizeof(addr)));

	addr_info.sin_family = AF_INET;
	addr_info.sin_port = htons(port);
	addr_info.sin_addr.S_un.S_addr = inet_addr(addr);

	memset(resev_buffer,0,RECV_BUFFER_SIZE);

	if (connect(connect_socket,(SOCKADDR*)&addr_info,sizeof(addr_info)) == -1)
	{
		cout<<"connect failed!";
		exit(1);
	}

    if((send_byte = send(connect_socket,get_command,strlen(get_command) + 1,0)) == -1)
	{
		cout<<"send failed!";
		closesocket(connect_socket);
	}

	pfile = fopen("head.txt","w+b");
	while ((resev_byte = recv(connect_socket,resev_buffer,RECV_BUFFER_SIZE,0)) != -1)
	{
        pos = fwrite(resev_buffer,resev_byte,1,pfile);
		if (pos == 0)
		{
			fclose(pfile);
			break;
		}
	}
	closesocket(connect_socket);

	return 0;
}


  
  
const char * 
inet_ntop_v4 (const void *src, char *dst, size_t size) 
{ 
    const char digits[] = "0123456789"; 
    int i; 
    struct in_addr *addr = (struct in_addr *)src; 
    u_long a = ntohl(addr->s_addr); 
    const char *orig_dst = dst; 
  
    if (size < INET_ADDRSTRLEN) {  
    return NULL; 
    } 
    for (i = 0; i < 4; ++i) { 
    int n = (a >> (24 - i * 8)) & 0xFF; 
    int non_zerop = 0; 
  
    if (non_zerop || n / 100 > 0) { 
        *dst++ = digits[n / 100]; 
        n %= 100; 
        non_zerop = 1; 
    } 
    if (non_zerop || n / 10 > 0) { 
        *dst++ = digits[n / 10]; 
        n %= 10; 
        non_zerop = 1; 
    } 
    *dst++ = digits[n]; 
    if (i != 3) 
        *dst++ = '.'; 
    } 
    *dst++ = '\0'; 
    return orig_dst; 
} 
  
const char * 
inet_ntop(int af, const void *src, char *dst, size_t size) 
{ 
	switch (af) { 
	case AF_INET : 
		return inet_ntop_v4 (src, dst, size); 
#ifdef INET6 
	case AF_INET6: 
		return inet_ntop_v6 ((const u_char*)src, dst, size); 
#endif 
	default : 
		return NULL; 
	} 
} 
  
  
/* 
 * Convert IPv6 binary address into presentation (printable) format. 
 */
//static const char * 
//inet_ntop_v6 (const u_char *src, char *dst, size_t size) 
//{ 
//  /* 
//   * Note that int32_t and int16_t need only be "at least" large enough 
//   * to contain a value of the specified size.  On some systems, like 
//   * Crays, there is no such thing as an integer variable with 16 bits. 
//   * Keep this in mind if you think this function should have been coded 
//   * to use pointer overlays.  All the world's not a VAX. 
//   */
//  char  tmp [INET6_ADDRSTRLEN+1]; 
//  char *tp; 
//  struct { 
//    long base; 
//    long len; 
//  } best, cur; 
//  u_long words [IN6ADDRSZ / INT16SZ]; 
//  int    i; 
//  
//  /* Preprocess: 
//   *  Copy the input (bytewise) array into a wordwise array. 
//   *  Find the longest run of 0x00's in src[] for :: shorthanding. 
//   */
//  memset (words, 0, sizeof(words)); 
//  for (i = 0; i < IN6ADDRSZ; i++) 
//      words[i/2] |= (src[i] << ((1 - (i % 2)) << 3)); 
//  
//  best.base = -1; 
//  cur.base  = -1; 
//  for (i = 0; i < (IN6ADDRSZ / INT16SZ); i++) 
//  { 
//    if (words[i] == 0) 
//    { 
//      if (cur.base == -1) 
//           cur.base = i, cur.len = 1; 
//      else cur.len++; 
//    } 
//    else if (cur.base != -1) 
//    { 
//      if (best.base == -1 || cur.len > best.len) 
//         best = cur; 
//      cur.base = -1; 
//    } 
//  } 
//  if ((cur.base != -1) && (best.base == -1 || cur.len > best.len)) 
//     best = cur; 
//  if (best.base != -1 && best.len < 2) 
//     best.base = -1; 
//  
//  /* Format the result. 
//   */
//  tp = tmp; 
//  for (i = 0; i < (IN6ADDRSZ / INT16SZ); i++) 
//  { 
//    /* Are we inside the best run of 0x00's? 
//     */
//    if (best.base != -1 && i >= best.base && i < (best.base + best.len)) 
//    { 
//      if (i == best.base) 
//         *tp++ = ':'; 
//      continue; 
//    } 
//  
//    /* Are we following an initial run of 0x00s or any real hex? 
//     */
//    if (i != 0) 
//       *tp++ = ':'; 
//  
//    /* Is this address an encapsulated IPv4? 
//     */
//    if (i == 6 && best.base == 0 && 
//        (best.len == 6 || (best.len == 5 && words[5] == 0xffff))) 
//    { 
//      if (!inet_ntop_v4(src+12, tp, sizeof(tmp) - (tp - tmp))) 
//      { 
//        errno = ENOSPC; 
//        return (NULL); 
//      } 
//      tp += strlen(tp); 
//      break; 
//    } 
//    tp += sprintf (tp, "%lX", words[i]); 
//  } 
//  
//  /* Was it a trailing run of 0x00's? 
//   */
//  if (best.base != -1 && (best.base + best.len) == (IN6ADDRSZ / INT16SZ)) 
//     *tp++ = ':'; 
//  *tp++ = '\0'; 
//  
//  /* Check for overflow, copy, and we're done. 
//   */
//  if ((size_t)(tp - tmp) > size) 
//  { 
//    errno = ENOSPC; 
//    return (NULL); 
//  } 
//  return strcpy (dst, tmp); 
//  return (NULL); 
//} 
