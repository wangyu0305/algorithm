/************************************************************************/
/*                        简单 map-reduce 
               计算从控制台输入的单词的个数                             */
/************************************************************************/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#define BUF_SIZE 2048


int my_map(char* buffer,char (*mapbuffer)[100])       //输入参数为字符串指针 map后的结果通过 mapbuffer传出 返回字符串中单词个数
{
	char* p;
	int num = 0;
    if (p = strtok(buffer," "))
    {
		strcpy(mapbuffer[num],p);
		num++;
    }
	else
		return num;
	while (p = strtok(NULL," "))
	{
		strcpy(mapbuffer[num],p);
		num++;
	}
	return num;
}

int my_reduce(char (*mapbuffer)[100],char (*reducebuffer)[100],int* count,int num)
{
	int i,j;
	int flag[BUF_SIZE] = {0};
    char tmp[100];
	int countnum = 0;

	for (i = 0; i < num; i++)
	{
		if (flag[i] == 0)
		{
			strcpy(tmp,mapbuffer[i]);
			flag[i] = 1;
			strcpy(reducebuffer[countnum],mapbuffer[i]);
			count[countnum] = 1;

			for (j = 0; j < num ;j++)
			{
				if (memcmp(tmp,mapbuffer[j],strlen(tmp)) == 0 && (strlen(tmp) == strlen(mapbuffer[j])) && (flag[j] == 0))
				{
					count[countnum]++;
					flag[j] = 1;
				}
			}
			countnum++;
		}
	}
	return countnum;
}

int main(int argc, char **argv)
{
	char buffer[BUF_SIZE];
	char mapbuffer[BUF_SIZE][100];
	char reducebuffer[BUF_SIZE][100];
	int count[BUF_SIZE] = {0};
	int num,i,countnum;
	fgets(buffer,BUF_SIZE - 1,stdin);
	buffer[strlen(buffer) - 1] = '\0';
	num = my_map(buffer,mapbuffer);
	printf("this is map result:\n");
	for (i = 0; i < num ;i++)
	{
		printf("<%s\t1>\n",mapbuffer[i]);
	}
    countnum = my_reduce(mapbuffer,reducebuffer,count,num);
	printf("this is reduce result:\n");
	for (i = 0;i < countnum;i++)
	{
		printf("<%s\t%d>\n",reducebuffer[i],count[i]);
	}
	return 0;
}