/*
                 An implementation of top-down splaying
                     D. Sleator <sleator@cs.cmu.edu>
                              March 1992
  */
 #include <stdlib.h>
 #include <stdio.h>
  int size;  /* number of nodes in the tree */
            /* Not actually needed for any of the operations */
 typedef struct tree_node Tree;
  struct tree_node
 {
     Tree * left, * right;
     int item;
 };
 
 Tree * splaytree(int i, Tree * t)
 {
  /* Simple top down splay, not requiring i to be in the tree t.  */
  /* What it does is described above.                             */
     Tree N, *l, *r, *y;
     if (t == NULL)
         return t;
     N.left = N.right = NULL;
     l = r = &N;
     for (;;)
     {
         if (i < t->item)
         {
             if (t->left == NULL)
             {
                 break;
             }
             if (i < t->left->item)
             {
                 y = t->left;                           /* rotate right */
                 t->left = y->right;
                 y->right = t;
                 t = y;
                 if (t->left == NULL)
                 {
                     break;
                 }
             }
             r->left = t;                               /* link right */
             r = t;
             t = t->left;
         }     
         else if (i > t->item)
         {    
             if (t->right == NULL)
             {
                 break;
             }
             if (i > t->right->item)
             {
                 y = t->right;                          /* rotate left */
                 t->right = y->left;
                 y->left = t;
                 t = y;
                 if (t->right == NULL)
                 {
                     break;
                 }
             }
             l->right = t;                              /* link left */
             l = t;
             t = t->right;
         }     
         else    
         {
             break;
         }
     }
     l->right = t->left;                                /* assemble */
     r->left = t->right;
     t->left = N.right;
     t->right = N.left;
     return t;
 }
  /* Here is how sedgewick would have written this.                    */
 /* It does the same thing.                                           */
 Tree * sedgewickized_splay (int i, Tree * t)
 {
     Tree N, *l, *r, *y;
     if (t == NULL)
     {
         return t;
     }
     N.left = N.right = NULL;
     l = r = &N;
     for (;;)
     {
         if (i < t->item)
         {
             if (t->left != NULL && i < t->left->item)
             {
                 y = t->left;
                 t->left = y->right;
                 y->right = t;
                 t = y;
             }
             if (t->left == NULL)
             {
                 break;
             }
             r->left = t;
             r = t;
             t = t->left;
         }
         else if (i > t->item)
         {
             if (t->right != NULL && i > t->right->item)
             {
                 y = t->right;
                 t->right = y->left;
                 y->left = t;
                 t = y;
             }
             if (t->right == NULL)
             {
                 break;
             }
             l->right = t;
             l = t;
             t = t->right;
         }
         else
         {
             break;
         }
     }
     l->right=t->left;
     r->left=t->right;
     t->left=N.right;
     t->right=N.left;
     return t;
 }
 
 Tree * insertnode(int i, Tree * t)
 {
 /* Insert i into the tree t, unless it's already there.    */
 /* Return a pointer to the resulting tree.                 */
     Tree * newtree;
     
     newtree = (Tree *) malloc (sizeof (Tree));
     if (newtree == NULL)
     {
         printf("Ran out of space\n");
         exit(1);
     }
     newtree->item = i;
     if (t == NULL)
     {
         newtree->left = newtree->right = NULL;
         size = 1;
         return newtree;
     }
     t = splaytree(i,t);
     if (i < t->item)
     {
         newtree->left = t->left;
         newtree->right = t;
         t->left = NULL;
         size ++;
         return newtree;
     }
     else if (i > t->item)
     {
         newtree->right = t->right;
         newtree->left = t;
         t->right = NULL;
         size++;
         return newtree;
     }
     else
     {
         /* We get here if it's already in the tree */
         /* Don't add it again                      */
         free(newtree);
         return t;
     }
 }
 
 Tree * deletenode(int i, Tree * t)
 {
 /* Deletes i from the tree if it's there.               */
 /* Return a pointer to the resulting tree.              */
     Tree * x;
     if (t==NULL)
     {
         return NULL;
     }
     t = splaytree(i,t);
     if (i == t->item)
     {               /* found it */
         if (t->left == NULL)
         {
             x = t->right;
         }
         else
         {
             x = splaytree(i, t->left);
             x->right = t->right;
         }
         size--;
         free(t);
         return x;
     }
     return t;                         /* It wasn't there */
 }

 bool search(int i,Tree* t)
 {
	 if (t == NULL)
	 {
		 return false;
	 }
	 if (i == t->item)
	 {
		 return true;
	 }
	 else if (i < t->item)
	 {
		 if (search(i,t->left))
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }
	 else if (i > t->item)
	 {
		 if (search(i,t->right))
		 {
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }
 }
 
 int m2_main(int argv, char *argc[])
 {
 /* A sample use of these functions.  Start with the empty tree,         */
 /* insert some stuff into it, and then delete it                        */
     Tree * root;
     int i;
     root = NULL;              /* the empty tree */
     size = 0;
     for (i = 0; i < 1024; i++)
     {
         root = insertnode((541*i) & (1023), root);
     }
     printf("size = %d\n", size);

	 if (search((1*541)&1023,root))
	 {
		 printf("found it!\n");
	 }
	 else
	 {
		 printf("not found!\n");
	 }
	 
     for (i = 0; i < 1024; i++)
     {
         root = deletenode((541*i) & (1023), root);
     }
     printf("size = %d\n", size);
	 return i;
 }