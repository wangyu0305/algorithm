

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#define  N  10

int array[N];

int lowbit(int bit)
{
	return (bit & (bit ^ (bit - 1)));
}

int array_sum(int end)
{
	int sum = 0;
    while (end > 0)
    {
		sum += array[end];
		end -= lowbit(end);
    }
	return sum;
}

void array_plus(int pos,int num)
{
	while (pos <= N)
	{
		array[pos] += num;
		pos += lowbit(pos);
	}
}

void init_array()
{
	int i;
	for (i = 0;i < N;i++)
	{
		array[i] = rand() % 1000;
	}
}

int s_array_sum(int end)
{
	int i,modn,sumi,n = N;
	while (n > 0)
	{
		modn = n % 2;
		n = n / 2;
		sumi++;
	}
	int* sumarray;
	sumarray = (int*)malloc(sizeof(int)*sumi);
	for (i = 0;i < sumi;i++)
	{
		sumarray[sumi] = array_sum(1 << sumi);
	}

	int sum = 0;
	while (end > 0)
	{
		sum += sumarray[end];
		s_array_sum(end);
		end -= lowbit(end);
	}
	return 0;
}

int m3_main(int argc, char **argv)
{
	init_array();
	array_sum(9);
	array_sum(0);
	array_plus(3,10);
	array_plus(4,20);
    return 0;
}