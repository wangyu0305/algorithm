/************************************************************************/
/*             线段树      树状数组        字典树     KMP               */
/************************************************************************/

#include <stdlib.h>


/*           线段树        */
typedef struct segmentree segmentree_t;

struct segmentree 
{
	int s;
	int e;
	int conver;
	segmentree_t* left,*right;
};

segmentree_t* create_segtree(int s,int e)
{
	segmentree_t* segtree;
	segtree = (segmentree_t*)malloc(sizeof(segmentree_t));
	segtree->s = s;
	segtree->e = e;
	segtree->conver = 0;
	segtree->left = segtree->right = NULL;

	return segtree;
}

segmentree_t* create_tree(int s,int e)
{
	int middle;
	segmentree_t* segtree;
	if (s >= e)
	{
		return NULL;
	}
 
	segtree = create_segtree(s,e);
	if (s + 1 == e)
	{
		return segtree;
	}
	middle = (s + e) / 2;
	segtree->left = create_tree(s,middle);
	segtree->right = create_tree(middle,e);

	return segtree;
}

int convert_tree(segmentree_t* segtree,int s,int e)
{
	if (segtree == NULL || s >= e)
	{
		return 0;
	}
	if (s < segtree->s)
	{
		s = segtree->s;
	}
	if (e > segtree->e)
	{
		e = segtree->e;
	}
	if (segtree->s == s && segtree->e == e)
	{
		segtree->conver += 1;
	}
	if (segtree->s <= s && (segtree->s + segtree->e) / 2 >= e)
	{
		convert_tree(segtree->left,s,e);
	}
	if ((segtree->s + segtree->e) / 2 <= s && segtree->e >= e)
	{
		convert_tree(segtree->right,s,e);
	}
	if((segtree->s + segtree->e) / 2 > s && (segtree->s + segtree->e) / 2 < e)
	{
		convert_tree(segtree->left,s,(segtree->s + segtree->e) / 2);
		convert_tree(segtree->right,(segtree->s + segtree->e) / 2,e);
	}

	return 1;
}

//int main(int argc, char **argv)
//{
//	segmentree_t* segtree;
//	segtree = create_tree(1,20);
//	convert_tree(segtree,2,11);
//    convert_tree(segtree,4,7);
//}


/*             树状数组          */

#include <memory.h>

#define ARRAYSIZE 20

int s[ARRAYSIZE / 2];          //可根据需求 需要多少数组来存储以计算之和
int c[ARRAYSIZE];

void set_array_sum()
{
	int i,j;
	s[0] = c[0] + c[1];
	for (i = 1,j = 1;i < ARRAYSIZE / 2;i++)
	{
		s[i] = s[i-1] + c[j + 1] + c[j + 2];
		j += 2;
	}
}

//int main(int argc, char **argv)
//{
//	memset(c,-1,sizeof(int)*ARRAYSIZE);
//	set_array_sum();
//}


/*             字典树           */


#define LETTERSIZE 26

typedef struct tire_node tire_node_t;
typedef struct tire_tree tire_tree_t;

struct tire_node 
{
	char a;
    tire_tree_t* next;
};

struct tire_tree 
{
	tire_node_t* cnode[LETTERSIZE];
};

void tire_init(tire_tree_t* tiretree)
{
	memset(tiretree,0,sizeof(tire_tree_t));
}

tire_node_t* tire_create(char a)
{
	tire_node_t* tire;
	tire = (tire_node_t*)malloc(sizeof(tire_node_t));
	tire->a = a;
	tire->next = NULL;

	return tire;
}

tire_tree_t* tire_tree_create()
{
	tire_tree_t* tiretree;
	tiretree = (tire_tree_t*)malloc(sizeof(tire_tree_t));
	tire_init(tiretree);

	return tiretree;
}

tire_tree_t* tire_insert(tire_tree_t** tiretree,char* str)
{
	char c;
	int i;
	if (*tiretree == NULL)
	{
		*tiretree = tire_tree_create();
	}
    c = *str;
    i = c - 'a';
	if (i < 0)
	{
		i = c - 'A';
	}

	if (i < 26)
	{
		if ((*tiretree)->cnode[i] == NULL)
		{
			(*tiretree)->cnode[i] = tire_create(c);
			if (*(++str))
			{
				tire_insert(&(*tiretree)->cnode[i]->next,str);
			}
			else
			{
				return (*tiretree);
			}
		}
		else
		{
			if (*(++str))
			{
				tire_insert(&(*tiretree)->cnode[i]->next,str);
			}
			else
			{
				return (*tiretree);
			}
		}
	}

	return (*tiretree);
}

tire_tree_t* tire_destory(tire_tree_t* tiretree)
{
	int i;
	if (tiretree == NULL)
	{
		return NULL;
	}
    for (i = 0;i < 26;i++)
    {
		if (tiretree->cnode[i] != NULL)
		{
			if (tiretree->cnode[i]->next != NULL)
			{
				tire_destory(tiretree->cnode[i]->next);
			}
			else
			{
				free(tiretree->cnode[i]);
				tiretree->cnode[i] = NULL;
			}
		}
    }

	free(tiretree);
	return NULL;
}

//int main(int argc, char **argv)
//{
//	tire_tree_t* tiretree;
//	tiretree = tire_tree_create();
//	tiretree = tire_insert(&tiretree,"ascamicp");
//	tiretree = tire_insert(&tiretree,"bstamp");
//	tiretree = tire_insert(&tiretree,"ascstamp");
//	tire_destory(tiretree);
//
//	return 0;
//}


/*        KMP        */

#include <string.h>

void make_next_array(char* p,int next[])
{
	int i,j,k;
	next[0] = -1;
	next[1] = 0;
	k = 2;
	i = 0;
	j = 1;
	while (k < strlen(p))
	{
		if (p[k] == p[j])
		{
			i++;
			j++;
		}
		else
		{
			i = 0;
			j = 1;
		}
		next[k] = i;
		k++;
	}
}

int kmp_match(char* src,char* mth)
{
	int* next,p,i,j;
	p = strlen(mth);
	if (p > strlen(src))
	{
		return 0;
	}
	next = (int*)malloc(sizeof(int)*p);

	make_next_array(mth,next);
    i = 0;
	j = 0;

	while (i < strlen(src))
	{
		if (src[i] == mth[j])
		{
			i++;
			j++;
		}
		else
		{
			j = next[j];
			if (j == -1)
			{
				i++;
				j = 0;
			}
		}
		if (j == p)
		{
			return i - j;
		}
	}

	free(next);
	next = NULL;

	return -1;
}

//int main(int argc, char **argv)
//{
//	char* a = "ggggchjiiiskddddmjiiiskj";
//	char* b = "chjiii";
//	char* c = "iiiskj";
//	char* d = "iiskdm";
//	kmp_match(a,b);
//	kmp_match(a,c);
//	kmp_match(a,d);
//
//	return 0;
//}
