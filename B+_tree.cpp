#pragma once

#ifndef BINARY_PLUS_TREE
#define BINARY_PLUS_TREE


template <typename TK, typename TV,size_t THD> class CBinaryPlusTree;

class CBinaryPlusTreeException
{
public:
	enum Type
	{
		NoEnoughMemory=0
	};
    
	CBinaryPlusTreeException(Type type)
	{
		m_type = type;
	}

private:
	Type m_type;
};


template <typename TK, typename TV,size_t THD=10>
class CBinaryPlusTreeNode
{
public:
    typedef CBinaryPlusTreeNode BPTN;
	typedef CBinaryPlusTreeNode* LPBPTN;
	typedef TV* PVAL;
	static const size_t Degree = 2* THD +1 ; //deepth
    static const size_t Rank = THD; //
	friend CBinaryPlusTree<TK,TV,THD>;
public:
	size_t Count;
	LPBPTN Parent;
	TK Keys[Degree];
	union 
	{
		LPBPTN Nodes[Degree+1]; 
        struct 
		{
           PVAL Vals[Degree];
		   LPBPTN NextLeafNode;
		};
	};
	bool IsLeaf;
    
public:

	CBinaryPlusTreeNode(bool newLeaf=false);
    ~CBinaryPlusTreeNode();
	
private: 
	bool IsRoot();
	bool Search(TK key,size_t &index);
	LPBPTN Splite(TK &key);
	size_t Add(TK &key);
	size_t Add(TK &key,TV &val);
	bool RemoveAt(size_t index);
#ifdef _DEBUG
public:
	void Print();
#endif
};


template<typename TK,typename TV, size_t THD =10>
class CBinaryPlusTree
{
public:
	typedef  CBinaryPlusTreeNode<TK,TV,THD> BPTN;
    typedef  CBinaryPlusTreeNode<TK,TV,THD>* LPBPTN;
	static const size_t Degree = 2* THD +1 ;
    static const size_t Rank = THD;

	struct FindNodeParam
	{
		LPBPTN pNode;
		size_t index;
		bool flag;

	};
	
	
public:
	LPBPTN Root;
	LPBPTN LeafHead;
	size_t KeyCount;
	size_t Level;
	size_t NodeCount;
    
public:
    CBinaryPlusTree():KeyCount(0),Level(0),NodeCount(0),Root(NULL),LeafHead(NULL)
	{	
		
	}
    ~CBinaryPlusTree()
	{
		Clear();
	}
    
	bool Add(TK key,TV val);
	bool Remove(TK key);
    
	void Clear();
	bool ContainKey(TK key);
	bool GetVal(TK key,TV & val);
	bool SetVal(TK key,TV & val);
private:
	void InitTree();
	bool AdjustAfterAdd(LPBPTN pNode);
	bool AdjustAfterRemove(LPBPTN pNode);
    void RemoveNode(LPBPTN pNode);
	void Search(LPBPTN pNode,TK &key,FindNodeParam & fnp);
	void SearchBranch(LPBPTN pNode,TK &key,FindNodeParam & fnp);

#ifdef _DEBUG
public:
	void Print();
	void PrintLeaf();
	void PrintNode(LPBPTN pNode);
#endif
};




#include "BinaryPlusTree.inc"

#endif


//CBinaryPlusTreeNode

template<typename TK,typename TV,size_t THD>
CBinaryPlusTreeNode<TK,TV,THD>::CBinaryPlusTreeNode(bool newLeaf):Count(0),Parent(NULL),IsLeaf(newLeaf)
{
	
	for(size_t i=0;i<=Degree;i++)
	{
	    Keys[i]= TK();
		Nodes[i]=NULL;
	}
	
}
 
template<typename TK,typename TV,size_t THD> 
CBinaryPlusTreeNode<TK,TV,THD>::~CBinaryPlusTreeNode()
{
	if(IsLeaf)
	{
        for(size_t i=0;i<Count;i++)
		{
		    if(Vals[i])	delete Vals[i];
		}
	}
}

template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::IsRoot()
{
	if(Parent)return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::Search(TK key,size_t &index)
{
	bool ret =false;

	if(Count==0)
	{
		index=0;
		return false;
	}
    

    if(Keys[0] > key)
    {
       index =0;
       return false;
    }
    if(Keys[Count-1] < key)
	{
		index = Count; 
		return false;
	}
	
	
	if(Count > 20)
	{
	    size_t m,s,e;
    
		s=0;e=Count-1;
	    while(s<e)
		{
			m=(s+e)/2;
			
			if(key == Keys[m])
			{
			    index = m;
				return true;    
			}
			else if(key < Keys[m])   
			{   
				e=m;
			}   
			else // key > Keys[m]
			{   
				s=m;
			}

			if(s==e-1)
			{
			    if(key == Keys[s])
				{
					index = s;
					return true;  
				}
				
				if(key == Keys[e])
				{
					index = e;
					return true;  
				}
				
				if(key < Keys[s])
				{   
					index=s;
					return false;
				}
				
				if(key < Keys[e])
				{   
					index=e;
					return false;
				}
				
				if(key > Keys[e])
				{   
					index=e+1;
					return false;
				}
				
			}
		};
		return false;
	}
	else
	{
		
		for(size_t i=0;i<Count;i++)
		{
			if(key<Keys[i])
			{
				index = i;
				ret = false;
				break;
			}
			else if(key == Keys[i])
			{
				index = i;
				ret = true;
				break;
			}
			
		}
		return ret;
	}
}



//
template<typename TK,typename TV,size_t THD>
CBinaryPlusTreeNode<TK,TV,THD>* CBinaryPlusTreeNode<TK,TV,THD>::Splite(TK &key)
{
    LPBPTN newNode = new BPTN(this->IsLeaf);
	if(newNode == NULL)
	{
		throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
		return NULL;
	}
	
	key = Keys[Rank];
	
	if(IsLeaf)
	{
		for(size_t i = Rank+1; i< Degree; i++)
		{
			newNode->Keys[i-Rank-1] = Keys[i];
			Keys[i]=TK();
			newNode->Vals[i-Rank-1] = Vals[i];
			Vals[i]= NULL; //
		}
		newNode->NextLeafNode = NextLeafNode; 
		NextLeafNode = newNode;
		
		newNode->Parent = Parent; //
		newNode->Count = Rank;
		Count = Rank+1;
	}
	else
	{
	    for(size_t i = Rank+1; i< Degree; i++)
		{
			newNode->Keys[i-Rank-1] = Keys[i];
		}
		for(size_t i = Rank+1; i<= Degree;i++)
		{
			newNode->Nodes[i-Rank-1] = Nodes[i];
		}
		newNode->Parent = Parent; 
		newNode->Count = Rank;
	    

		LPBPTN childNode;
		for(size_t i = 0;i <= newNode->Count;i++)
		{
			childNode = newNode->Nodes[i];
			if(childNode)
			{
				childNode->Parent = newNode;
			}
		}
		
		

		for(size_t i = Rank+1; i< Degree+1;i++)
		{
			Nodes[i] = NULL; 
		}

		for(size_t i = Rank; i< Degree;i++)
		{
			Keys[i] = TK();
		}

		Count = Rank;

	}	
    
	return newNode;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryPlusTreeNode<TK,TV,THD>::Add(TK &key)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key,index))
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];   
		 }
        
		 for(size_t i = Count+1; i > index;i--)
		 {
			 Nodes[i]= Nodes[i-1];
		 }

		 Keys[index]=key;
		 Nodes[index] = NULL;
		 Count++;
	 }
     return index;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryPlusTreeNode<TK,TV,THD>::Add(TK &key,TV &val)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Vals[0] = new TV();
		 if(Vals[0]==NULL)
         {
             throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
         }
         
         * Vals[0] = val;
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key,index))
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];  
			 Vals[i] = Vals[i-1]; 
		 }
         Keys[index]=key;
         Vals[index] = new TV();
         if(Vals[index]==NULL)
         {
             throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
         }
         * Vals[index] = val;
		 Count++;
	 }
     return index;
}


template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::RemoveAt(size_t index)
{
    if(index>Count-1)
    {
        return false;
    }
    
    if(IsLeaf)
    {
        if(Vals[index]) delete Vals[index];
        
        for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
			Vals[i]= Vals[i+1];
		}
		Keys[Count-1] = TK();
		Vals[Count-1]=NULL;
    }
    else
    {
	    for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
		}
		
		Keys[Count-1] = TK();
		
		for(size_t i = index;i< Count;i++)
		{
			Nodes[i]= Nodes[i+1];
		}
		Nodes[Count] = NULL;
	}
	Count--;
	return true;
}



template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::InitTree()
{
	Root = new BPTN(true);
	LeafHead = Root;
    KeyCount =0;
	NodeCount=1;
	Level=1;
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Search(LPBPTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	if(pNode->Search(key,index))
	{
	    if(pNode->IsLeaf)
	    {
			fnp.flag = true; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
		    pNode = pNode->Nodes[index];
		    while(!pNode->IsLeaf){
				pNode = pNode->Nodes[pNode->Count];
		    };
		    fnp.flag = true;
		    fnp.index = pNode->Count-1;
		    fnp.pNode = pNode;
		}
	}
	else
	{
		if(pNode->IsLeaf)
		{
			fnp.flag = false; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
			Search(pNode->Nodes[index],key,fnp);
        }
	}
}


template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::Add(TK key,TV val)
{
	if(Root==NULL)
	{
		InitTree();
	}

	FindNodeParam fnp;

	Search(Root,key,fnp); 
    if(!fnp.flag)
	{
	    fnp.pNode->Add(key,val);
        KeyCount++;

		if(fnp.pNode->Count == Degree )
		{
			return AdjustAfterAdd(fnp.pNode);
		}
		return true;
	}
	return false;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::AdjustAfterAdd(LPBPTN pNode)
{
	
	TK key;
	LPBPTN newNode = pNode->Splite(key); 
	NodeCount++;
    LPBPTN parent = pNode->Parent;

	if(parent==NULL)
	{
		LPBPTN newRoot = new BPTN();
		if(newRoot==NULL) return false;
		
		NodeCount++;
		Root = newRoot;
        Root->Add(key);
		
		Root->Nodes[0] = pNode;
		Root->Nodes[1]= newNode;
       	
		pNode->Parent = Root;
		newNode->Parent = Root;

		Level++;
		return true;
	}
	else
	{
			size_t index = parent->Add(key);
			
			parent->Nodes[index]= pNode;
            parent->Nodes[index+1]= newNode;

			if(parent->Count == Degree) 
			{
				return AdjustAfterAdd(parent);
			}
			return true;
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::RemoveNode(LPBPTN pNode)
{
	if(!pNode->IsLeaf)
	{
		for(size_t i=0;i <= pNode->Count;i++)
		{
			RemoveNode( pNode->Nodes[i] );
			pNode->Nodes[i] = NULL;
		}
	}
	delete pNode;
	NodeCount--;
	return;
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Clear()
{
	if(Root)
	{
		RemoveNode(Root);
		KeyCount=0;
		Level=0;
		Root = NULL;
		LeafHead = NULL;
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::ContainKey(TK key)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    return fnp.flag;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::GetVal(TK key,TV & val)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    if(fnp.flag)
    {
        val = *(fnp.pNode->Vals[fnp.index]);
        return true;
    }
    else return false;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::SetVal(TK key,TV & val)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    if(fnp.flag)
    {
        *(fnp.pNode->Vals[fnp.index]) = val;
        return true;
    }
    else return false;
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::SearchBranch(LPBPTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	
	if(pNode->IsLeaf) return ;
	
	if(pNode->Search(key,index))
	{
		fnp.flag = true; 
		fnp.index = index;
		fnp.pNode = pNode;
		return;
	}
	else
	{
		if(!pNode->Nodes[index]->IsLeaf) 
		{
			return SearchBranch(pNode->Nodes[index],key,fnp);
		}
		else
		{
		    fnp.index =index;
			fnp.flag = false;
			fnp.pNode= pNode;
			return ;
		}
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::Remove(TK key)
{
    FindNodeParam fnp;
    
    if(Root==NULL)
		return false;

    Search(Root,key,fnp);
	
	if(fnp.flag)
	{
		if(Root == fnp.pNode)
		{
			Root->RemoveAt(fnp.index);
			KeyCount--;
			return AdjustAfterRemove(fnp.pNode);
		}
	    
	    if( fnp.index == fnp.pNode->Count-1)
	    {
	        FindNodeParam fnpb;
	        
	        SearchBranch(Root,key,fnpb);
	        if(fnpb.flag)
	        {
				fnpb.pNode->Keys[fnpb.index] = fnp.pNode->Keys[fnp.pNode->Count-2];
			}
	    }
	    
	    fnp.pNode->RemoveAt(fnp.index);
	    KeyCount--;
	    
	    return AdjustAfterRemove(fnp.pNode);
	}
    return false;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::AdjustAfterRemove(LPBPTN pNode)
{
    if(pNode->Count >= Rank)
	{
		return true; 
	}

	if( pNode->IsRoot() ) 
	{
		if(pNode->Count==0) 
		{
			if(!pNode->IsLeaf)
			{
				Root = pNode->Nodes[0];
				pNode->Nodes[0]->Parent = NULL;
			}
			else
			{
				Root = NULL;
				LeafHead = NULL;
			}
			delete pNode;
			NodeCount--;
			Level--;
		}
		return true;
	}


	
	LPBPTN pBrother,pParent;
    size_t pos;
	
	pParent =pNode->Parent;
	pParent->Search(pNode->Keys[0],pos);
	
	if(pos == pParent->Count)
	{
		pBrother = pParent->Nodes[pos-1]; 

		if(pBrother->Count > Rank)
		{
			// check
		    if(pNode->IsLeaf)
		    {

				for(size_t i = pNode->Count;i > 0 ; i--)   
				{
					pNode->Keys[i] = pNode->Keys[i-1];
					pNode->Vals[i] = pNode->Vals[i-1];
				}
				pNode->Keys[0] = pBrother->Keys[pBrother->Count-1];
				pNode->Vals[0] = pBrother->Vals[pBrother->Count-1];
				pNode->Count++;
				
				pBrother->Keys[pBrother->Count-1] = TK();
				pBrother->Vals[pBrother->Count-1] = NULL;
				pBrother->Count--;

				pParent->Keys[pos-1] = pBrother->Keys[pBrother->Count-1];
				
				return true;
		    }
		    //check
			else  //
		    {
				//
				for(size_t i = pNode->Count;i > 0 ; i--)   
				{
					pNode->Keys[i] = pNode->Keys[i-1];
				}
				for(size_t i = pNode->Count+1;i > 0 ; i--)   
				{
					pNode->Nodes[i] = pNode->Nodes[i-1];
				}
				pNode->Keys[0] = pParent->Keys[pos-1]; 
				pParent->Keys[pos-1]= pBrother->Keys[pBrother->Count-1];
				pNode->Nodes[0] = pBrother->Nodes[pBrother->Count];
				pNode->Count++;
				pBrother->Keys[pBrother->Count-1]=TK();
				if(pBrother->Nodes[pBrother->Count])
				{
					pBrother->Nodes[pBrother->Count]->Parent = pNode;
					pBrother->Nodes[pBrother->Count] = NULL;
				}
				pBrother->Count--;
				return true;
		    }
		}
		else
		{
			//check 2
		    if(pNode->IsLeaf)
		    {
		        pParent->RemoveAt(pos-1);
                pParent->Nodes[pos-1] = pBrother;

				for(size_t i = 0;i<pNode->Count;i++)
				{
					pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
					pBrother->Vals[pBrother->Count+i] = pNode->Vals[i];
					pNode->Vals[i]=NULL;
				}
				pBrother->Count += pNode->Count; 
				pBrother->NextLeafNode = pNode->NextLeafNode;

				delete  pNode;
				NodeCount--;

				return AdjustAfterRemove(pParent); //
		    }
		    //check
			else
		    {
				pBrother->Keys[pBrother->Count]= pParent->Keys[pos-1]; //
				pBrother->Count++; 

				pParent->RemoveAt(pos-1);
				pParent->Nodes[pos-1] = pBrother; 

				//
				for(size_t i = 0;i<pNode->Count;i++)
				{
					pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
				}

				for(size_t i = 0;i<= pNode->Count;i++)
				{
					pBrother->Nodes[pBrother->Count+i] = pNode->Nodes[i];
					pNode->Nodes[i]->Parent = pBrother; 
					
				}
				
				pBrother->Count = 2* Rank; //
	            
				delete pNode;
				NodeCount--;

				return AdjustAfterRemove(pParent); //
		    
			}
		}

	} 
	
	else //
	{
		pBrother = pParent->Nodes[pos+1]; 

		if(pBrother->Count > Rank)
		{
			//check
			if(pNode->IsLeaf) //
            {
				pParent->Keys[pos] = pBrother->Keys[0]; //
				
				pNode->Keys[pNode->Count] = pBrother->Keys[0];    
				pNode->Vals[pNode->Count] = pBrother->Vals[0];    
				pBrother->Vals[0]=NULL;
				pNode->Count++;
				pBrother->RemoveAt(0);//
				return true;
            }
            //check
			else
            {
				pNode->Keys[pNode->Count] = pParent->Keys[pos];
                pNode->Nodes[pNode->Count+1] = pBrother->Nodes[0]; //
				pNode->Count++;

                pParent->Keys[pos]= pBrother->Keys[0];  //
				
				pBrother->Nodes[0]->Parent = pNode;
				
				pBrother->RemoveAt(0);//
				return true;
			}
		}
		else  //
		{
			//check
            if(pNode->IsLeaf)
			{
				//
                for(size_t i=0 ; i <Rank;i++)
				{
					pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
					pNode->Vals[pNode->Count+i] = pBrother->Vals[i];
					pBrother->Vals[i]=NULL; //
				}
                pNode->Count += Rank;
                
				delete pBrother;
				NodeCount--;

				pParent->RemoveAt(pos);
				pParent->Nodes[pos] = pNode;
			
				return AdjustAfterRemove(pParent);
			}
			//check
			else
			{
				pNode->Keys[pNode->Count]= pParent->Keys[pos]; //
				
				pParent->RemoveAt(pos);
				pParent->Nodes[pos]= pNode;  //
				pNode->Count++; //Count+1

				for(size_t i = 0;i<Rank;i++)
				{
					pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
				}

				for(size_t i = 0;i<=Rank;i++)
				{
					pNode->Nodes[pNode->Count+i] = pBrother->Nodes[i];
					pBrother->Nodes[i]->Parent = pNode;
					
				}
	            
				pNode->Count += Rank;

				delete pBrother;
				NodeCount--;
				
				return AdjustAfterRemove(pParent);
			}

			
		}
	}

}


#ifdef _DEBUG

// 打印调试函数
template<typename TK,typename TV,size_t THD>
void CBinaryPlusTreeNode<TK,TV,THD>::Print()
{
    printf("----------------------\n");
	printf("Address: 0x%X Count: %d, Parent: 0x%X  IsLeaf:%d\n",(void*)this,Count,(void*)Parent,IsLeaf);
	printf("Keys: { ");
	for(size_t i =0; i<Degree;i++)
	{
		printf("%5d ",Keys[i]);
	}
	printf(" }\n");
	
    if(IsLeaf)
    {
		printf("Vals: { ");
		for(size_t i =0; i<Degree;i++)
		{
			if(Vals[i]==NULL)
			{
			   printf("{NUL}");
			}
			else
			{
			   printf("%5d",*Vals[i]);
			}
			
		}
		printf(" }\n");
		
		printf("NextLeaf: %X\n", NextLeafNode);
    }
    else
    {
		printf("Ptrs: {");
		for(size_t i =0; i<=Degree;i++)
		{
			printf("0x%X ",(void*)Nodes[i]);
		}
		printf("}\n");
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Print()
{
    printf("*****************************************************\n");
    printf("KeyCount: %d, NodeCount: %d, Level: %d, Root: 0x%X \n",KeyCount,NodeCount,Level,(void*)Root );
	
	if(Root)
	{
		PrintNode(Root);
	}
	
	
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::PrintNode(LPBPTN pNode)
{
	
    
    pNode->Print(); 
	if(!pNode->IsLeaf) //如果不是叶子节点继续
	{
	    
		for(size_t i =0; i<= pNode->Count;i++)
		{
			PrintNode(pNode->Nodes[i]);
		}
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::PrintLeaf()
{
	LPBPTN p = LeafHead;
	while(p)
	{
	    for(size_t i=0;i<p->Count;i++)
	    {
	        printf("%5d ",p->Keys[i]);
	    }
	    printf("\n");
	    for(size_t i=0;i<p->Count;i++)
	    {
	        printf("%5d ",*(p->Vals[i]));
	    }
	    printf("\n");
	    p= p->NextLeafNode;
	}
    
}

#endif