/* -----块状链表----- 
整个过程使用指针来标记字符位置 应用于文本编辑器*/

/*数组特点是元素在内存中紧挨着存储，因而优点是定位快（O(1)），缺点是插入删除慢（O(n)）；
而链表则不同，它通过指针将不同位置的元素链接起来，因而优缺点与数组正好相反：定位慢（O(n)），插入删除快（O(1)）。
块状链表，它将数组和链表的优点结合来，各种操作的时间复杂度均为O(sqrt(n))。
*/

#include <iostream>
#include <string>
#include <stdlib.h>
#include <math.h>

using namespace std;

const int maxn = 5000;

typedef struct block 
{
	int amt;
	char vec[maxn];
	struct block *next,*fatherf;
	block(block* suf,block* ft)
	{
		amt = 0;
		next = suf;
		fatherf = ft;
	}
}block;

class text
{
public:
	text()
	{
		curtp = 0;
		tot = 1;
		curb = root = new block(NULL,NULL);
		curb->amt = 1;
	}
	int move(int size)
	{
		move_P(size);
		return 0;
	}
	int insert(int size)
	{
		insert_P(size);
		banlance_P();
		return 0;
	}
	int remove(int size)
	{
		remove_P(size);
		banlance_P();
		return 0;
	}
	int print(int size)
	{
		print_P(size);
		return 0;
	}
	int fatherv()
	{
		fatherv_P();
		return 0;
	}
	int succ()
	{
		succ_P();
		return 0;
	}

private:
	int tot;
	block *root,*curb;
	int curtp;

	inline char get_char()
	{
		char c;
		while (c = getchar(),c == '\n')
		{
		}
		return c;
	}
	inline int break_P(block* curt,int curtp,block* &father,block* &suf);
	int banlance_P();
	int move_P(int k);
	int insert_P(int size);
	int remove_P(int size);
	int print_P(int size);
	inline int fatherv_P();
	inline int succ_P();
};

int text::move_P(int k)                             /*每个链长amt  curb = curb-next移动到链表处 再移动curbp = k 数组步数*/
{
	while (k >= curb->amt) 
	{             
		k -= curb->amt;             
		curb = curb->next;       
	} 
	curtp = k;
	return 0;
}

int text::break_P(block *cb, int cp, block *&father, block *&suf)          //插入时分裂新块链表
{
    father = cb;    
    if (cp >= cb->amt - 1)
		suf = cb->next;    
	else 
	{            
		block *tmp = new block(cb->next, cb);         
		if (curtp > cp) 
		{
			curb = tmp; 
			curtp -= cp+1;
		}         
		if (cb->next != NULL) 
			cb->next->fatherf = tmp;            
		suf = tmp;            
		tmp->amt = cb->amt - cp - 1;            
		memcpy(tmp->vec, &(cb->vec[cp+1]), tmp->amt * sizeof(cb->vec[0]));            
		cb->amt = cp + 1;            
		cb->next = suf;       
	}
	return 0;
}

int text::banlance_P()                               //维护当前链表
{       
	block *curr = root, *suf, *father, *tmp;    
	int bt = (int)floor(sqrt((double)tot));          //返回小于或者等于指定表达式的最大整数
	while (curr != NULL) 
	{         
		while (curr->amt < bt/2 || curr->amt > 2*bt)  
		{
			if (curr->amt > 2*bt)                               //链表中当前块过大时 分裂
			{                      
				break_P(curr, curr->amt/2, father, suf);                      
				curr = father;                   
			} 
			else                                              //链表中当前块过小时 合并
			{                  
				if (curr->next == NULL) 
					break;                     
				memcpy(&(curr->vec[curr->amt]), &(curr->next->vec[0]),curr->next->amt * sizeof(curr->vec[0]));                     
				tmp = curr->next;                  
				if (curr->next->next != NULL) 
					curr->next->next->fatherf = curr;                  
				if (curb == tmp) 
				{                        
					curtp += curr->amt;                        
					curb = curr;                     
				}                     
				curr->amt += curr->next->amt;                     
				curr->next=curr->next->next;                  
				delete tmp;                   
			}             
		}
		curr = curr->next;       
	}
	return 0;
}

int text::insert_P(int size) 
{       
	tot += size; 
	int bt = (int)ceil(sqrt((double)tot));         //返回大于或者等于指定表达式的最大整数
	block *father, *suf;       
	break_P(curb, curtp, father, suf);             //先查看是否需要分裂
	block *node = new block(suf, father), *tmp;     
	if (suf != NULL) 
		suf->fatherf = node;
	father->next = node;    
	for (; size>0; size--) 
	{        
		if (node->amt > bt)                       //数组里的字符最多到 根号size bt大小 分成多个块 保证数组不溢出 （插入小数据量时浪费内存）
		{              
			tmp = new block(node->next, node);           
			if (node->next != NULL) 
				node->next->fatherf = tmp;              
			node->next = tmp;              
			node = tmp;           
		}           
		node->vec[node->amt++] = get_char();       
	}    
	return 1;
}

int text::remove_P(int size) 
{       
	tot-=size;       
	block *father, *suf;       
	break_P(curb, curtp, father, suf);       
	block *curr = suf, *tmp;    
	while (curr != NULL && size >= curr->amt)       //要删除的字符大小 大于一个链的总大小 直接删除该链
	{             
		size -= curr->amt;          
		if (curr->next != NULL) 
			curr->next->fatherf = curr->fatherf;          
		if (curr->fatherf != NULL) 
			curr->fatherf->next = curr->next;          
		else 
			root = curr->next; 

		tmp = curr; 
		curr = curr->next;          
		delete tmp;       
	} 
	if (curr == NULL) 
		return 1;       
	curr->amt-=size;    
	if (size > 0)         
		memmove(&(curr->vec[0]), &(curr->vec[size]), curr->amt*sizeof(curr->vec[0]));
	return 0;
}

int text::print_P(int size) 
{       
	block *curr = curb; 
	int cp = curtp;    
	for (; size > 0; size--) 
	{        
		if (cp >= curr->amt - 1) 
		{              
			curr = curr->next;              
			cp = -1;           
		}           
		printf("%c", curr->vec[++cp]);       
	} 
	printf("\n");
	return 0;
}

inline int text::fatherv_P()            //前移一个链 （指向前一个块）
{      
	curtp--;   
	if (curtp < 0) 
	{
		curb = curb->fatherf; 
	    curtp = curb->amt - 1;
	}
	return 0;
}

inline int text::succ_P()          //后移一个链 （指向后一块）
{      
	curtp ++;   
	if (curtp >= curb->amt) 
	{
		curb = curb->next; 
		curtp = 0;
	}
	return 0;
}

static int  k;
text T;             //全局对象

int y2_main() 
{       
	char c;
	while (1)
	{
		scanf("%c",&c);
		//freopen("editor.in","r",stdin);       
		//freopen("editor.out","w",stdout);           
		char str[100] = {0};                 

		//	scanf("%s", str);   

		switch (c) 
		{          
		case 'M' : 
			scanf("%d", &k); 
			T.move(k); 
			break;          
		case 'I' : 
			scanf("%d", &k); 
			T.insert(k); 
			break;          
		case 'D' : 
			scanf("%d", &k); 
			T.remove(k); 
			break;          
		case 'G' : 
			scanf("%d", &k); 
			T.print(k); 
			break;          
		case 'P' : 
			T.fatherv(); 
			break;          
		case 'N' : 
			T.succ(); 
			break;           
		}       

		//fclose(stdin);       
		//fclose(stdout);
	}
}