#include <string>

using namespace std;

void get_file_last_modify_time(char* fname, time_t* tm);

char* get_file_last_modify_time(char* filename);

void get_current_time(string* str_time);

void get_current_time(char* currtime);

void test_time(struct tm* timeinfo,string* str_time);