#include "time_func.h"
#include <Windows.h>
#include <time.h>
#include <string.h>


void get_file_last_modify_time(char* fname, time_t* tm)
{
	HANDLE hDir = CreateFile ((LPCSTR)fname, GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_DELETE,  
		NULL, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, NULL); 
	FILETIME creat_time;
	FILETIME acess_time;
	FILETIME modify_time;
	SYSTEMTIME stime;
	GetFileTime(hDir, &creat_time, &acess_time, &modify_time);
	FileTimeToSystemTime(&modify_time, &stime);
	LONGLONG llTime;
	ULARGE_INTEGER ui;
	ui.LowPart = modify_time.dwLowDateTime;
	ui.HighPart = modify_time.dwHighDateTime;
	llTime = (modify_time.dwHighDateTime << 32) + modify_time.dwLowDateTime;
	*tm = (DWORD)((LONGLONG)(ui.QuadPart - 116444736000000000) / 10000000);
	CloseHandle(hDir); 

}

void get_current_time(string* str_time)
{
	time_t rawtime;
	struct tm * timeinfo;
    char c_in[100];
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );

	sprintf(c_in,"%d-%d-%d-%d-%d-%d",timeinfo->tm_year + 1900,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
	*str_time = c_in;
}

void get_current_time(char* currtime)
{
	time_t ntime;
	struct tm* stime;
	ntime = time(NULL);
	stime = localtime(&ntime);
	char* cutime = asctime(stime);
	strcpy(currtime,cutime);
}

void test_time(struct tm* timeinfo,string* str_time)
{
	char c_in[100];
	sprintf(c_in,"%d-%d-%d-%d-%d-%d",timeinfo->tm_year + 1900,timeinfo->tm_mon+1,timeinfo->tm_mday,timeinfo->tm_hour,timeinfo->tm_min,timeinfo->tm_sec);
	*str_time = c_in;
}

void convert_url_to_filename(string* url, string* filename)
{
	filename->clear();
	char c;
	const char* urlname;
	urlname = url->c_str();
	for (int i = 0; i < url->length(); i++)
	{
		c = urlname[i];
		if ( !isdigit(c) && !islower(c) && !isupper(c) )
		{
			c = '_';
		}
		*filename += c;
	}

	*filename = *filename + ".lst";
}

char* get_file_last_modify_time(char* filename)
{
	HANDLE   hFile;   
	WIN32_FIND_DATA   wfd;   
	SYSTEMTIME   systime;   
	FILETIME   localtime;   
	char   stime[32];     //输出时间   
	memset(&wfd,   0,   sizeof(wfd));   

	if((hFile=FindFirstFile((LPCTSTR)filename,   &wfd))==INVALID_HANDLE_VALUE)   
	{   
		char   c[10];   
		DWORD   dw=GetLastError();   
		sprintf(c,   "%d",   dw);    
		return  NULL ;//失败   
	}   
	//ok,转换时间   
	FileTimeToLocalFileTime(&wfd.ftLastWriteTime,&localtime);   
	FileTimeToSystemTime(&localtime,&systime);   
	sprintf(stime,"%4d-%02d-%02d   %02d:%02d:%02d",   
		systime.wYear,systime.wMonth,systime.wDay,systime.wHour,   
		systime.wMinute,systime.wSecond);   
    return stime;
}