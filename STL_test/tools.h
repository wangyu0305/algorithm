#ifndef __STRING_TOOLS_H
#define __STRING_TOOLS_H


#define sizeof_v(var)   ((unsigned int) ((char *)(&(var) + 1) - (char *)&(var)))

#define sizeof_t(type)  ((unsigned int) ((type *)0 + 1)

void number_to_string(unsigned short number,char str[]);

unsigned short string_to_number(char *strkey);

#endif


