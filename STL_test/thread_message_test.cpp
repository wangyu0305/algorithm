#include <Windows.h>
#include <string>
#include <cstdio>
#include <process.h> 


using namespace std;

#define UM_RESULT  WM_USER + 1001
#define UM_WORK     WM_USER + 1002

DWORD WINAPI thread_func(LPVOID lParam)
{
	MSG msg;//线程的消息
	while(1)
	{
		Sleep(1000);
		//消息控制
		if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			switch(msg.message)
			{
			case WM_QUIT:
				return 0;
			case UM_WORK://启动数据处理,从线程从消息中提取其将要处理的数据
				{
					string str=(char*)msg.wParam;
					str.reserve(NULL);    //反转，只作演示
					PostMessage((HWND) lParam, UM_RESULT, (LPARAM)str.c_str(),0);//数据处理完毕，通知主线程U
				}
				break;
			default:
				break;
			}//switch
		}//if
	}//while
	return 1;
}



void thread_message_test()
{
	HANDLE thread = NULL;
	DWORD c_threadid;
	DWORD m_threadid;
	c_threadid = GetCurrentThreadId();
	thread = CreateThread(NULL,0,thread_func,(LPVOID)&c_threadid,NULL,&m_threadid);

	Sleep(1000);
	//::PostMessage((HWND)thread,UM_WORK,(LPARAM)"abcdefg",0);
	PostThreadMessage(m_threadid, UM_WORK,  (WPARAM)"abcdefg",  0);

	WaitForSingleObject(thread,INFINITE);
	//GetMessage()
    CloseHandle(thread);
}



#define MY_MSG WM_USER+100

const int MAX_INFO_SIZE = 20;

HANDLE hStartEvent; // thread start event

// thread function

unsigned __stdcall fun(void *param)

{

	printf("thread fun start \n");

	MSG msg;

	PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);

	if(!SetEvent(hStartEvent)) //set thread start event 

	{

		printf("set start event failed,errno:%d\n",::GetLastError());

		return 1;

	}



	while(true)

	{

		if(GetMessage(&msg,0,0,0)) //get msg from message queue

		{

			switch(msg.message)

			{

			case MY_MSG:
				char * pInfo = (char *)msg.wParam;

				printf("recv %s\n",pInfo);

				delete[] pInfo;

				break;

			}

		}

	};

	return 0;

}

int message_test()

{

	HANDLE hThread;

	unsigned nThreadID;

	hStartEvent = ::CreateEvent(0,FALSE,FALSE,0); //create thread start event

	if(hStartEvent == 0)

	{

		printf("create start event failed,errno:%d\n",::GetLastError());

		return 1;

	}

	//start thread

	hThread = (HANDLE)_beginthreadex( NULL, 0, &fun, NULL, 0, &nThreadID );

	if(hThread == 0)
	{

		printf("start thread failed,errno:%d\n",::GetLastError());

		CloseHandle(hStartEvent);

		return 1;

	}

	//wait thread start event to avoid PostThreadMessage return errno:1444

	::WaitForSingleObject(hStartEvent,INFINITE);

	CloseHandle(hStartEvent);

	int count = 0;

	while(true)

	{

		char* pInfo = new char[MAX_INFO_SIZE]; //create dynamic msg

		sprintf(pInfo,"msg_%d",++count);

		if(!PostThreadMessage(nThreadID,MY_MSG,(WPARAM)pInfo,0))//post thread msg

		{

			printf("post message failed,errno:%d\n",::GetLastError());

			delete[] pInfo;
		}

		::Sleep(1000);

	}

	CloseHandle(hThread);

	return 0;

} 
