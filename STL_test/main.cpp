#include <Windows.h>
#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <time.h>
#include "time_func.h"
#include "thread_message_test.h"

int m_count;
bool m_close;
void time_test();

DWORD WINAPI thread_func_1(LPVOID pram)
{
    HANDLE* mutex = (HANDLE*)pram;
	time_t c_begin;
	time_t c_end;
	c_begin = clock();
	while (m_close)
	{
		WaitForSingleObject(*mutex,INFINITE);
		m_count++;
		//Sleep(5000);
		ReleaseMutex(*mutex);
		if (m_count > 10)
		    m_close = false;
		Sleep(5000);
	}
	
	c_end = clock();
	return 0;
}

DWORD WINAPI thread_func_2(LPVOID pram)
{
	HANDLE* mutex = (HANDLE*)pram;
	time_t c_begin;
	time_t c_end;
	c_begin = clock();
	while (m_close)
	{
		WaitForSingleObject(*mutex,INFINITE);
		m_count++;
		//Sleep(5000);
		ReleaseMutex(*mutex);
		if (m_count > 10)
			m_close = false;
		Sleep(5000);
	}
	c_end = clock();
	return 0;
}

int main(int argc, char **argv)
{
    //time_test();
	//message_test();
	thread_message_test();
	m_count = 0;
	m_close = true;
	HANDLE m_mutex = NULL;
	HANDLE thread1 = NULL;
	HANDLE thread2 = NULL;
	m_mutex = CreateMutex(NULL,false,NULL);
	thread1 = CreateThread(NULL,0,thread_func_1,&m_mutex,0,NULL);
	thread2 = CreateThread(NULL,0,thread_func_2,&m_mutex,0,NULL);
	WaitForSingleObject(thread1,INFINITE);
	WaitForSingleObject(thread2,INFINITE);
    CloseHandle(thread1);
	CloseHandle(thread2);
	CloseHandle(m_mutex);
	std::vector<int> v_data;
	std::set<char*> s_str;
	s_str.insert("1234");
	s_str.insert("2345");
	s_str.insert("234");
	int n = s_str.count("23");
	v_data.push_back(50);
	v_data.push_back(50);
	char tim;
	while ((tim = getc(stdin)) != '\n')
	{
		v_data.insert(v_data.begin(),tim);
	}
	
	for (std::vector<int>::iterator itor = v_data.begin();itor != v_data.end();)
	{
		int valu = *itor;
		itor = v_data.erase(itor);
	}
	v_data.clear();
	return 0;
}

void time_test()
{
	string str_time;
	struct tm* timeinfo;
	time_t tm;
	char buftime[40];
	get_current_time(buftime);
	get_current_time(&str_time);
    str_time = get_file_last_modify_time("E:\\S_Projests\\STL_test\\STL_test\\time_func.h");
	get_file_last_modify_time("E:\\S_Projests\\STL_test\\STL_test\\time_func.h",&tm);
	timeinfo = localtime(&tm);
	test_time(timeinfo,&str_time);
}
