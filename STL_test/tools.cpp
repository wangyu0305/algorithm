#include <string.h>

void number_to_string(unsigned short number,char str[])
{
	int i=0, j=0, isNeg = 0;
	int num = 0;
	char temp[30];

	if(number < 0)
	{
		number *= -1;
		isNeg = 1;
	}

	do
	{	
		temp[i++] = (char)(number % 10 + '0');
		number /= 10;

	}
	while(number);

	if(isNeg)
	{
		temp[i++] = '-'	;	
	}

	while(i > 0)
	{
		str[j++] = temp[--i];
	}

	str[j] = '\0';
}

unsigned short string_to_number(char *strkey)
{
	unsigned short hkey = 0;
	int i,len;

	if (strkey ==0)
	{
		return 0;
	}

	len = strlen(strkey);

	for (i=0; i<len; i++)
	{
		hkey *= 10;
		hkey += strkey[i]-'0';
	}

	return hkey;
}