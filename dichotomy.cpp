

#include <stdio.h>

double half_search(double a[],int startlen,int endlen,double target)
{
	int middle;
	if (endlen <= startlen)
	{
		if (endlen == startlen)
		{
			if (a[startlen] == target)
			{
			    return a[startlen];
			}
		}
		return -1;
	}

	middle = (startlen + endlen) / 2;
	if (target == a[middle])
	{
		return a[middle];
	}
	else if (target < a[middle])
	{
		return half_search(a,startlen,middle,target);
	}
	else if (target > a[middle])
	{
		return half_search(a,middle + 1,endlen,target);
	}
}

//int main(int argc, char **argv)
//{
//	double a[20] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
//	double findget;
//	findget = half_search(a,0,19,18.5);
//    printf("%d",findget);
//	return 0;
//}