/* 伸展树 （维修数列）*/
#include <iostream>
#include <string.h>

using namespace std;

//#define max(x,y) ({  _x = (x);  _y = (y); (void) (&_x == &_y);  _x > _y ? _x : _y; })

#define maxn 500000

#define keytree (child[child[root][1]][0])

int m ,nu ,num[maxn];

int max(int a,int b)
{
	return a>b?a:b;
}

struct splay_tree 
{
	int size[maxn],child[maxn][2],root,father[maxn];
	int top1,top2,sta[maxn],que[maxn];
	int sum[maxn],maxnum[maxn],maxright[maxn],maxleft[maxn],val[maxn];
	bool rotate[maxn],same[maxn];
    
    inline void new_node(int &x,int c)         //新建一个节点
	{
		if (top2)
		{
			x = sta[top2--];
		}
		else
		{
			x = ++top1;
		}
		child[x][0] = child[x][1] = father[x] = 0;
		size[x] = 1;
		rotate[x] = same[x] = 0;
		val[x] = sum[x] = maxleft[x] = maxright[x] = maxnum[x] = c;
	}

    inline void init(int n)        //初始化 对于0节点的处理 左右各插入一个空节点
	{
		child[0][1] = child[0][0] = father[0] = size[0] = 0;
        rotate[0] = same[0] = 0;
		maxleft[0] = maxright[0] = maxnum[0] = -1001;
		sum[0] = 0;
		root = top2 = top1 = 0;

		new_node(root,-1);
		new_node(child[root][1],-1);
		father[top1] = root;
		size[root] = 2;

		int i = 1;
		for (;i <= n;i++)
		{
			cout<<"inter number:";
			cin>>num[i];
		}
		build_tree(keytree,1,n,child[root][1]);
		update_node(child[root][1]);
		update_node(root);
	}

	inline void build_tree(int &x,int left,int right,int fa)
	{
		if (left > right)
		{
			return;
		}
		int midder = (right + left) / 2;
		new_node(x,num[midder]);
		build_tree(child[x][0],left,midder - 1,x);
		build_tree(child[x][1],midder + 1,right,x);
		father[x] = fa;
		update_node(x);
	}

	inline void same_node(int x,int c)
	{
		if (x == 0)
		{
			return;
		}
		val[x] = c;
		sum[x] = size[x] * val[x];
		maxnum[x] = maxleft[x] = maxright[x] = max(sum[x],val[x]);
		same[x] = 1;
	}

	inline void reverse_node(int x)     //反转节点
	{
		if (x == 0)
		{
			return;
		}
		child[x][0] ^= child[x][1] ^= child[x][0] ^= child[x][1];
		maxleft[x] ^= maxright[x] ^= maxleft[x] ^= maxright[x];
		rotate[x] ^= 1;
	}

	inline void push_down(int x)     //将节点标记向下传递
	{
		if (x == 0)
		{
			return;
		}
		if (rotate[x])
		{
			reverse_node(child[x][0]);
			reverse_node(child[x][1]);
			rotate[x] ^= 1;
		}
		if (same[x])
		{
			same_node(child[x][0],val[x]);
			same_node(child[x][1],val[x]);
			same[x] = 0;
		}
	}

	inline void update_node(int x)        //维护当前节点
	{
		if (x == 0)
		{
			return;
		}
        size[x] = size[child[x][0]] + size[child[x][1]] + 1;
		sum[x] = val[x] + sum[child[x][1]] + sum[child[x][0]];
		maxleft[x] = max(maxleft[child[x][0]],sum[child[x][0]] + val[x] + max(0,child[x][1]));
		maxright[x] = max(maxright[child[x][1]],sum[child[x][1]] + val[x] + max(0,child[x][0]));
		maxnum[x] = max(max(maxleft[child[x][1]],0) + max(maxright[child[x][0]],0) + val[x],max(maxnum[child[x][0]],maxnum[child[x][1]]));
	}

	inline void rotate_node(int x,int c)        // c == 0 左旋 ， c == 1 右旋
	{
		int y = father[x];
		push_down(y);
		push_down(x);
        child[y][!c] = child[x][c];
		if (child[x][c])
		{
			father[child[x][c]] = y;
		}
		father[x] = father[y];
		if (father[y])
		{
			child[father[y]][child[father[y]][1] == y] = x;
		}
		child[x][c] = y;
		father[y] = x;
		update_node(y);
		if (y == root)
		{
			root = x;
		}
	}

	//splay会有死循环
	inline void splay(int x,int f)     //把 x节点转到 f节点的下面
	{
		push_down(x);
		while (father[x] != f)
		{
			if (father[father[x]] == f)
			{
				rotate_node(x,child[father[x]][0] == x);     //单旋转
			}
			else
			{
				int y = father[x], z = father[y];
				int c = (child[z][0] == father[y]);
				if (child[y][c] == x)      //之字形旋转
				{
					rotate_node(x,!c);
					rotate_node(x,c);
				}
				else                        //一字形旋转
				{
					rotate_node(y,c);
					rotate_node(x,c);
				}
			}
		}
		update_node(x);
		if (f == 0)
		{
			root = x;
		}
	}

	inline void selete_node(int k,int f)     //将第k个元素转到f节点的下面
	{
		int x = root;
		k += 1;
		while (1)
		{
			push_down(x);
			if (k == size[child[x][0]] + 1)
			{
				break;
			}
			if (k <= size[child[x][0]])
			{
				x = child[x][0];
			}
			else
			{
				k -= size[child[x][0]] + 1;
				x = child[x][1];
			}
		}
		splay(x,f);
	}

	inline void erase_node(int x)                //删除以x节点为祖先的子树  回收内存
	{
		int head = 1,rear = 1;
		que[head] = x;
		while (head <= rear)
		{
			sta[++top2] = que[head];
			if (child[que[head]][0])
			{
				que[++rear] = child[que[head]][0];
			}
			if (child[que[head]][1])
			{
				que[++rear] = child[que[head]][1];
			}
			head++;
		}
	}

	void traval(int x)
	{
		if (x)
		{
			traval(child[x][0]);
			cout<<x<<"  size: "<<size[x]<<"  left: "<<child[x][0]<<"  right:"<<child[x][1]<<"  father: "<<father[x]<<"  val: "<<val[x]<<"  sum: "<<sum[x]<<endl;
			traval(child[x][1]);
		}
	}

	void print_all()
	{
		cout<<" root: "<<root<<endl;
		traval(root);
	}

	void get_sum()
	{
		int a,b;
		cout<<"inter a  b ";
		cin>>a>>b;
		selete_node(a - 1,0);
		selete_node(a + b,root);
		cout<<sum[keytree]<<endl;
	}

	void max_sum()
	{
		selete_node(0,0);
		selete_node(size[root] - 1,root);
		cout<<"max_sum:"<<maxnum[keytree]<<endl;
	}

	void insert_num()
	{
		int pos,total,i;
		cout<<"please inter pos && total"<<endl;
		cin>>pos>>total;
        for (i = 1;i <= total;i++)
        {
			cout<<"inter num:";
			cin>>num[i];
        }
		selete_node(pos,0);
		selete_node(pos + 1,root);
		build_tree(keytree,1,total,child[root][1]);
		update_node(child[root][1]);
		update_node(root);
	}

	void delete_num()
	{
		int a,b;
		cout<<"please inter range a ~ b"<<endl;
		cin>>a>>b;
        selete_node(a - 1,0);
		selete_node(a + b,root);
		erase_node(keytree);
		keytree = 0;
		update_node(child[root][1]);
		update_node(root);
	}

	void make_same()
	{
		int a,b,c;
		cout<<"inter int type a  b  c";
		cin>>a>>b>>c;
		selete_node(a - 1,0);
		selete_node(a + b,root);
		same_node(keytree,c);
	}

	void reverse()
	{
		int a,b;
		cout<<"inter int type a  b";
		cin>>a>>b;
		selete_node(a - 1,0);
		selete_node(a + b,root);
		reverse_node(keytree);
	}
}o_splay;

int m1_main(int argc, char **argv)
{
	char s[20];
	cout<<"please inter  初始化数的大小 n  操作次数 m "<<endl;
	cin>>nu>>m;
	o_splay.init(nu);
	while (m--)
	{
		cout<<"输入要操作的函数 get_sum || max_sum || insert || delete || make_same || reverse || print_all || traval"<<endl;
		cin>>s;
		if (!strcmp(s,"get_sum"))
		{
			o_splay.get_sum();
		}
		else if(!strcmp(s,"max_sum"))
		{
            o_splay.max_sum();
		}
		else if (!strcmp(s,"insert"))
		{
			o_splay.insert_num();
		}
		else if (!strcmp(s,"delete"))
		{
			o_splay.delete_num();
		}
		else if (!strcmp(s,"make_same"))
		{
			o_splay.make_same();
		}
		else if (!strcmp(s,"reverse"))
		{
			o_splay.reverse();
		}
		else if (!strcmp(s,"print_all"))
		{
			o_splay.print_all();
		}
		else if (!strcmp(s,"traval"))
		{
			int in;
			cout<<"inter traval node :";
			cin>>in;
            o_splay.traval(in);
		}
	}
	return 0;
}