/************************************************************************/
/*                循环队列                                              */
/************************************************************************/

#include <stdio.h>
#include <memory.h>

#define INIT 20
typedef struct  
{
	char str[INIT];
	int front;
	int rare;
}Queue;

void queue_init(Queue* que)
{
	memset(que->str,0,sizeof(char)*INIT);
	que->front = 0;
	que->rare = 0;
}

void queue_insert(Queue* que,char c)
{
	if ((que->front % INIT) != (que->rare % INIT))
	{
		que->str[que->front % INIT] = c;
		que->front = que->front % INIT + 1;
	}
	else
	{
		if (que->str[que->front] == '\0')
		{
			que->str[que->front % INIT] = c;
			que->front = que->front % INIT + 1;
		}
		else
		{
			printf("队列已满\n");
		}
	}
}

void queue_delete(Queue* que)
{
	if ((que->rare % INIT) != (que->front % INIT))
	{
		que->str[que->rare] = '\0';
		que->rare = que->rare % INIT + 1;
	}
	else
	{
		if (que->str[que->rare] != '\0')
		{
			que->str[que->rare] = '\0';
			que->rare = que->rare % INIT + 1;
		}
		else
		{
			printf("队列为空\n");
		}
	}
}

int q_main(int argc, char **argv)
{
	Queue que;
	int i;
	char s[20] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','1','2','3','4','5'};
	queue_init(&que);
    for (i = 0;i < INIT;i++)
    {
		queue_insert(&que,s[i]);
    }
	queue_delete(&que);
    queue_delete(&que);
	for (i = 0;i < 4;i++)
	{
		queue_insert(&que,s[i]);
	}
	return 0;
}