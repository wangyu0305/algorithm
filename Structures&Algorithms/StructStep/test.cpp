#include <stdio.h>

#define int_sort(a,b) {a=a+b;b=a-b;a=a-b;}   //a不能等于b 

int bubble_sort(int* array,int start,int end,int sort)
{
    if (array == NULL || start >= end)
    {
        return -1;
    }
    for(int i=end;i > start;i--)
    {
        for(int j=start;j < i;j++)
        {
            if(sort > 0)
            {
                if(array[j] > array[j+1])
                {
                    int_sort(array[j],array[j+1])
                }
            }
            else
            {
                if(array[j] < array[j+1])
                {
                    int_sort(array[j],array[j+1])
                }
            }
        }
    }
}

void int2buf(int n,char* buf){
    unsigned char *p;
    p = (unsigned char*)buf;
    *p++ = (n >> 24)&0xFF;
    *p++ = (n >> 16)&0xFF;
    *p++ = (n >> 8)&0xFF;
    *p++ = n&0xFF;
}

int main(int argc, char** argv)
{
    int n = 100000000;
    char buf[5];
    int2buf(n,buf);
    printf("%s",buf);
    //int a[]={1,2,2,9,3,3,6,4};
    //bubble_sort(a,0,7,-1);
    //for(int i=0;i<8;i++)
    //{
    //    printf("%d\t",a[i]);
    //}
}
