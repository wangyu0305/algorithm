#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define SIZEOF(x) ((char*)(&x+1)-(char*)(&x))  //only value don`t type

struct List
{
    void* value;
    struct List* prev;
    struct List* next;
};

typedef struct List List;

List* list_init(void* value)
{
    List* n = (List*)malloc(sizeof(List));
    n->value = value;
    n->next = NULL;
    n->prev = NULL;
    return n;
}

void list_add(List* head,void* value)
{
    List* tmphead = head;
    while(tmphead->next)
        tmphead = tmphead->next;
    List* node = list_init(value);
    node->prev = tmphead;
    tmphead->next = node;
}

void list_del(List*  head)
{
    List* tmp = head;
    while(head)
    {
        head = head->next;
        free(tmp);
        tmp = head;
    }
}

int check_recyc(List* head)
{
    List* p = head;
    List* q = head->next;
    while(q && q->next && p!=q)
    {
        p = p->next;
        q = q->next->next;
    }
    if(q == p)
        return 0;
    return -1;
}

int main(int argc,char** argv)
{
    int a[10] = {4,3,2,6,5,7,9,1,8,10};
    List* head = list_init(&a[0]);
    for(int i=1;i<10;i++)
    {
        list_add(head,&a[i]);
    }
    printf("%d\t",check_recyc(head));
    List* phead = head;
    while(phead)
    {
        printf("%d\n",(*(int*)(phead->value)));
        phead = phead->next;
    }
    list_del(head);
}
