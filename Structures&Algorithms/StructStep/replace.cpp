/************************************************************************/
/*                  手摇算法                                         */
/************************************************************************/

#include <stdio.h>

void swap_rep(char *a,char *b)
{
	char temp;
	temp = *a;
	*a = *b;
	*b = temp;
}

void rec_replace(char a[],int index_start,int index_end,int times)
{
	int i;
	for (i = 0;i < times; i++)
	{
		swap_rep(&a[index_start],&a[index_end]);
		index_start++;
		index_end--;
	}
}

int replacement(char a[],int startlen,int endlen,int rep)
{
	if (a == NULL || rep > endlen)
	{
		return -1;
	}
	rec_replace(a,startlen,(rep + startlen) - 1,rep / 2);
	rec_replace(a,(rep + startlen),endlen,(endlen - (rep + startlen - 1)) / 2);
    rec_replace(a,startlen,endlen,(endlen - (startlen - 1)) / 2);

	return 0;
}

//int main(int argc, char **argv)
//{
//	char a[14] = {'a','b','c','d','e','f','g','h','i','j','k','1','2','3'};
//	replacement(a,1,13,13);
//	printf("%s",a);
//}
