#include <stdio.h>


typedef long type;

void swap(long* a,long* b) 
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
} 

char* strstr(const char* dest,const char* src)
{
    char* cp = (char*)dest;
    char* s1,*s2;
    if(!*src)
        return ((char*)dest);
    while(*cp)
    {
        s1 = cp;
        s2 = (char*)src;
        while(*s1 && *s2 && !(*s1 - *s2))
            s1++,s2++;
        if(!*s2)
            return cp;
        cp++;
    }
    return NULL;
}

char* strcpy(char* dest,char* src) //it`s must be dest space >= src space
{
    char* p = dest;
    if(dest == NULL || src == NULL)
        return NULL;
    while((*dest++ = *src++) != '\0');
    
    return p;
}

int strcmp(const char* d,const char* s)
{
    int cmp;
    while(*d != '\0' && *s != '\0' && *d++ == *s++)
        ;
    if(*d == '\0' && *s == '\0')
        return 0;
    else{
        cmp = *d - *s;
        if(cmp > 0)
            return 1;
        else 
            return -1;
    }
}

long strlen(const char* dest)
{
    long n = 0;
    char* desp = (char*)dest;
    while(*desp++ != '\0')
        n++;
    return n;
}

void* memcpy(void* dest,void* src,long n)
{
    char* desp = (char*)dest;
    char* srcp = (char*)src;
    while(n-- > 0)
        *desp++ = *srcp++;
    return dest;
}

void* memmove(void* dest,void* src,long n)
{
    void* ret = dest;
    char* d = (char*)dest;
    char* s = (char*)src;
    if(s > d || (s+n) <= d)
        while(n--)
            *d++ = *s++;
    else
    {
        d += n - 1;
        s += n - 1;
        while(n--)
            *d-- = *s--;
    }
    return ret;
}

void* memset(void* dest,int t,long n)
{
    if(dest == NULL)
        return NULL;
    char* ret = (char*)dest;
    while(n--)
        *ret++ = t;
    return dest;
}

void quicksort(type* a,type m,type n)
{
    long i = m,j = n,temp;
    temp = a[m];
    if(i>=j) return;
    i++;
    while(i<j)
    {
        while(i<j && a[j]>temp) 
            j--;
        while(i<j && a[i]<temp)
            i++;
        swap(&a[i],&a[j]);
    }
    if(a[i] < temp)
        swap(&a[i],&a[m]);
    for(int b=m;b<=n;b++){
        printf("%ld\t",a[b]);
    }
    printf("\n");
    quicksort(a,m,i-1);
    quicksort(a,i,n); 
}

type dichotomy(type a[],type start, type end,type key)
{
    if(start == end)
        return a[start];
    while(start < end)
    {
        int middle = (start + end)/2;
        if(a[middle] == a[key]) 
            return a[middle];
        if(middle == start)
            if(a[end] == a[key])
                return a[end];
            else 
                return -1;
        if(a[middle] > a[key])
            end = middle;
        else
            start = middle;
        printf("middle=%d\tkey=%ld\n",middle,key);
    }
    return -1;
}

unsigned int elfhash(char* str)
{
    unsigned int hash = 0;
    unsigned int x = 0;
    while(*str)
    {
        hash = (hash << 4)+(*str++);
        if((x = hash & 0xF0000000) != 0)
        {
            hash ^= (x >> 24);
            hash &= ~x;
        }
    }
    return (hash & 0x7FFFFFFF);
}

int main(int argc,char** argv)
{
    char hashstr[] = "abciiikkss";
    char smp[] = "abcd";
    type a[10] = {4,1,6,11,2,8,9,15,25,10};
    printf("hash = %d\n", elfhash(hashstr));
    printf("strcmp %d\n", strcmp(hashstr, smp));
    quicksort(a,0,9);
    for(int i =0;i<10;i++)
    {
        printf("%ld\t",a[i]);
    }
    type y = dichotomy(a,0,9,13);
    printf("find key = %ld",y);
}
