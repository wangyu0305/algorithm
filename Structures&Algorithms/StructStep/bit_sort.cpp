/*  200M memory 
 *  sort 2G file 
 *  800,000,000 * int[] 
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX 1024*1024*200
#define mask 32

int a[MAX];

void bit_set(int N){
    a[N >> 5] |= (1 << (N % mask));
}

bool bit_check(int N){
    return a[N >> 5] & (1 << (N % mask));
}

void bit_get(){
    int x, y, n;
    for(int i = 0;i < MAX; i++){
        x = a[i >> 5];
        if(x != 0){
            n = 0;
            while(x){
                y = x & 0x00000001;
                if(y != 0){
                    printf("%d \t", i*mask + y*(1 << n));
                }
                n++;
                x = x >> 1;
            }
        }
    }
}

int main(int argc, char* argv[]){
    int m[50];
    for(int i = 0; i < 50; i++){
        m[i] = random();
        bit_set(m[i]);
        printf("m[%d] = %d exist %d \n",i,m[i],bit_check(m[i]));
    }
    bit_get();
}
