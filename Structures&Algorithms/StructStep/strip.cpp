#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* str_strip(char* str)
{
    while(*str == ' ')
        str++;
    for(int i = strlen(str)-1;i >= 0;i--)
    {
        if(str[i] != ' ')
            break;
        else
            str[i] = '\0';
    }
    return str;
}

int main(int argc,char** argv)
{
    if(argc < 2)
    {
        printf("usage: strip \" str \"\n");
        exit(0);
    }
    char* input_str = argv[1];
    printf("input:%s\t",input_str);
    char* output_str = str_strip(input_str);
    printf("output:%s\n",output_str);
}
