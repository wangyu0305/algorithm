#include <stdio.h>

void my_permutation(char* pstr,char* pbegin)
{
    if(!pstr || !pbegin)
        return ;
    if(*pbegin == '\0')
        printf("%s\n",pstr);
    else{
        for(char* pch=pbegin; *pch != '\0'; ++pch)
        {
            char temp = *pch;
            *pch = *pbegin;
            *pbegin = temp;
            my_permutation(pstr, pbegin+1);
            temp = *pch;
            *pch = *pbegin;
            *pbegin = temp;
        }
    }
}

void permutation(char* pstr)
{
    my_permutation(pstr,pstr);
}

int main(int argc, char** argv)
{
    char str[] = "abcde";
    permutation(str);
}
