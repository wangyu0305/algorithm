/* quicksort by stack*/
#include <stdio.h>
#include <stdlib.h>

#define MAX 1000

void quicksort(int a[],int n){
    int i,j,L,R,tmp,top = -1;
    struct stack{
        int L,H;
    }st[MAX];

    top++;
    st[top].L = 0;
    st[top].H = n - 1;
    while(top > -1){
        i = st[top].L;
        j = st[top].H;
        top--;
        L = i;
        R = j;
        if(L < R){
            tmp = a[L];
            while(L != R){
                while(L < R && a[R] > tmp)
                    R--;
                if(L < R){
                    a[L] = a[R];
                    L++;
                }
                while(L < R && a[L] < tmp)
                    L++;
                if(L < R){
                    a[R] = a[L];
                    R--;
                }
            }
            a[L] = tmp;
            top++;
            st[top].L = i;
            st[top].H = L - 1;

            top++;
            st[top].L = L + 1;
            st[top].H = j;
        }
    }
}

int main(int argc, char* argv[]){
    int m[50];
    for(int i = 0;i < 50;i++){
        m[i] = random()%1000;
        printf("before %d\n",m[i]);
    }
    quicksort(m,50);
    for(int i = 0;i < 50;i++){
        printf("after %d\n",m[i]);
    }
}
