/************************************************************************/
/*              B树  实现插入  删除节点                                 */
/************************************************************************/

#include <stdlib.h>
#include <memory.h>

#define V 2
#define TYPE int

typedef struct btree btree_t;

struct btree 
{
	TYPE key[2*V];
    btree_t* point[2*V + 1];
	btree_t* fa;
	int index;
};

btree_t* btree_node_delete(btree_t* bt,TYPE key);

void btree_init(btree_t* bt)
{
	memset(bt,0,sizeof(btree_t));
}

btree_t* btree_create()
{
	btree_t* bt;
	bt = (btree_t*)malloc(sizeof(btree_t));
	btree_init(bt);
	return bt;
}

btree_t* btree_merge_shrink(btree_t* bt,int tin)
{
	btree_t* lftbt,*rigbt;
	int ltin,rtin,i;
    lftbt = bt->point[tin-1];
	rigbt = bt->point[tin];
	ltin = lftbt->index;
	rtin = rigbt->index;
	for (i = 0;i < rtin;i++)
	{
		lftbt->key[ltin] = rigbt->key[i];
		lftbt->point[ltin] = rigbt->point[i];
		rigbt->key[i] = 0;
		rigbt->point[i] = NULL;
		rigbt->index--;
		lftbt->point[ltin]->fa = lftbt;
		ltin++;
	}
	if (rigbt->point[i] != NULL)
	{
		lftbt->point[ltin] = rigbt->point[i];
		rigbt->point[i] = NULL;
		lftbt->point[ltin]->fa = lftbt;
	}
	free(rigbt);
	rigbt = NULL;

	return lftbt;
}

btree_t* btree_merge(btree_t* bt,int tin)
{
	btree_t* tmpbt,*fa,*pi;
	int i = tin;
	if (bt->point[tin-1]->point[bt->point[tin-1]->index] != NULL && bt->point[tin]->point[0] != NULL)
	{
		pi = bt->point[tin-1];
		while (pi->index != 0 && pi->point[pi->index])
		{
			pi = pi->point[pi->index];
		}
		bt->key[tin-1] = pi->key[pi->index-1];
		btree_node_delete(bt->point[tin-1],pi->key[pi->index-1]);
		return bt;
	}
	if (bt->index <= 1)
	{
		tmpbt = bt;
		fa = bt->fa;
		bt = btree_merge_shrink(bt,tin);
		if (fa != NULL)
		{
			bt->fa = fa;
		}
		free(tmpbt);
		tmpbt = NULL;
		return bt;
	}
	tmpbt = btree_merge_shrink(bt,tin);
	bt->point[i-1] = tmpbt;
	tmpbt->fa = bt->point[i-1];
    for (;i < 2*V; i++)
    {
		bt->key[i-1] = bt->key[i];
		bt->point[i] = bt->point[i+1];
		bt->key[i] = 0;
		bt->point[i+1] = NULL;
    }
    bt->point[i] = bt->point[i+1];
	bt->index--;
	return bt;
}

btree_t* btree_node_delete(btree_t* bt,TYPE key)
{
	int tin,pin;
	btree_t* tmpbt;
	tin = bt->index;
	while (tin)
	{
		if (key == bt->key[tin-1])
		{
			if (bt->point[tin] != NULL)
			{
				if ((bt->point[tin-1]->index + bt->point[tin]->index) <= 2*V)
				{
					bt = btree_merge(bt,tin);
				}
				else
				{
					tmpbt = bt->point[tin];
					while (tmpbt->point[0] && tmpbt->index != 0)                     
					{
						tmpbt = tmpbt->point[0];
					}
					bt->key[tin-1] = tmpbt->key[0];
					btree_node_delete(bt->point[tin],tmpbt->key[0]);
				}
			}
			else
			{
				if (bt->point[tin-1] != NULL && bt->point[tin-1]->index != 0)
				{
					tmpbt = bt->point[tin-1];
					while (tmpbt->index != 0 && tmpbt->point[tmpbt->index])
					{
						tmpbt = tmpbt->point[tmpbt->index];
					}
					bt->key[tin-1] = tmpbt->key[tmpbt->index-1];
					btree_node_delete(bt->point[tin-1],tmpbt->key[tmpbt->index-1]);
				}
				else
				{
					while (tin < 2*V)
					{
						bt->key[tin-1] = bt->key[tin];
						bt->point[tin] = bt->point[tin+1];
						bt->key[tin] = 0;
						bt->point[tin+1] = NULL;
						tin++;
					}
					bt->index--;
				}
			}
			return bt;
		}
		else if (key > bt->key[tin-1])
		{
			if (bt->point[tin] != NULL)
			{
				btree_node_delete(bt->point[tin],key);
			}
			return bt;
		}
		else
		{
			tin--;
		}
	}
	if (key < bt->key[tin])
	{
		pin = tin;
	}
	else
	{
		pin = tin+1;
	}
	if (bt->point[pin] != NULL && key <= bt->point[pin]->key[bt->point[pin]->index-1])
	{
		btree_node_delete(bt->point[pin],key);
	}
	return bt;
}

bool btree_search(btree_t* bt,TYPE key)
{
	int tin,pin;
	tin = bt->index;
	while (tin)
	{
		if (key == bt->key[tin-1])
		{
			return true;
		}
		else if (key > bt->key[tin-1])
		{
			if (bt->point[tin] != NULL)
			{
				return btree_search(bt->point[tin],key);
			}
			return false;
		}
		else
		{
			tin--;
		}
	}
	if (key < bt->key[tin])
	{
		pin = tin;
	}
	else
	{
		pin = tin+1;
	}
	if (bt->point[pin] != NULL && key <= bt->point[pin]->key[bt->point[pin]->index-1])
	{
		return btree_search(bt->point[pin],key);
	}
    return false;
}

bool btree_update(btree_t* bt,TYPE bef,TYPE aft)
{
	int tin,pin;
	tin = bt->index;
	while (tin)
	{
		if (bef == bt->key[tin-1])
		{
            bt->key[tin-1] = aft;
			return true;
		}
		else if (bef > bt->key[tin-1])
		{
			if (bt->point[tin] == NULL)
			{
				return false;
			}
			else
			{
				return btree_update(bt->point[tin],bef,aft);
			}
		}
		else
		{
			tin--;
		}
	}
	if (bef < bt->key[tin])
	{
		pin = tin;
	}
	else
	{
		pin = tin+1;
	}
	if (bt->point[pin] != NULL && bef <= bt->point[pin]->key[bt->point[pin]->index-1])
	{
		return btree_update(bt->point[pin],bef,aft);
	}
	return false;
}

void btree_insert_key(btree_t* bt,TYPE key)
{
	int tin;
    tin = bt->index;
	while (tin)
	{
		if (key < bt->key[tin-1])
		{
			bt->key[tin] = bt->key[tin-1];	
		}
		else
		{
			break;
		}
		tin--;
	}
	bt->key[tin] = key;
	bt->index++;
}

btree_t* btree_move(btree_t* bt)
{
	btree_t* tmpbt;
	int i = 0,k;
	if (((2*V)/2 + 1) == 2*V)                      //刚好一半为最后节点时
	{
		if (bt->point[(2*V)/2 + 1] == NULL)
		{
			tmpbt = btree_create();
		}
		else
		{
			tmpbt = bt->point[(2*V)/2 + 1];
			bt->point[((2*V)/2 + 1)] = NULL;
		}
	}
	else
	{
		tmpbt = btree_create();
		for (k = (2*V)/2 + 1;k < 2*V;k++,i++)           //复制一半以后节点到新节点
		{
			tmpbt->key[i] = bt->key[k];
			tmpbt->point[i] = bt->point[k];
			bt->index--;
			tmpbt->index++;
			bt->key[k] = 0;
			bt->point[k] = NULL;
		}
		if (bt->point[k] != NULL)
		{
			tmpbt->point[i] = bt->point[k];
			bt->point[k] = NULL;
		}
	}
	return tmpbt;
}

void btree_move_insert(btree_t* bt,TYPE key)
{
	int tin;
	tin = bt->index;
	if (bt->index == 2*V)
	{
		return ;
	}
	while (tin)
	{
		if (key < bt->key[tin-1])
		{
			bt->key[tin] = bt->key[tin-1];
			bt->point[tin+1] = bt->point[tin];
			bt->key[tin-1] = 0;
			bt->point[tin] = NULL;
			tin--;
		}
		else
		{
			break;
		}
	}
	bt->key[tin] = key;
	bt->index++;
}

btree_t* btree_split(btree_t* bt,TYPE key)
{
	btree_t* tmpbt,*rtbt;
	int tin,indn;
	TYPE tem;
	
	if (bt->fa == NULL)
	{
		bt->fa = btree_create();
		bt->fa->point[0] = bt;
	}
	tmpbt = btree_move(bt);
    tem = bt->key[2*V/2];
	bt->key[2*V/2] = 0;
	bt->index--;

	indn = bt->fa->index;
	if (indn >= 2*V)
	{
		rtbt = btree_split(bt->fa,tem);
		tin = rtbt->index;
		while (tin)
		{
			if (tem < rtbt->key[tin-1])
			{
				tin--;
			}
			else
			{
				break;
			}
		}
		rtbt->point[tin] = tmpbt;
		tin--;
	}
	else
	{
		rtbt = bt->fa;
		tin = rtbt->index;
		while (tin)
		{
			if (tem < rtbt->key[tin-1])
			{
				rtbt->point[tin+1] = rtbt->point[tin];
				rtbt->key[tin] = rtbt->key[tin-1];
				tin--;
			}
			else
			{
				break;
			}
		}
		rtbt->key[tin] = tem;
		rtbt->point[tin+1] = tmpbt;
		rtbt->index++;
	}
	tmpbt->fa = rtbt;
	
	if (key < tem)
	{
		btree_move_insert(rtbt->point[tin],key);
        return rtbt->point[tin];
	}
	btree_move_insert(rtbt->point[tin+1],key);
	return rtbt->point[tin+1];
}

btree_t* btree_insert_node(btree_t* bt,TYPE key)
{
	int tin;
	int pin;
	if (bt == NULL)
	{
		bt = btree_create();
		bt->key[bt->index] = key;
		bt->index++;
		return bt;
	}
	tin = bt->index - 1;
	while (tin)
	{
		if (key < bt->key[tin])
		{
			tin--;
		}
		else
		{
			break;
		}
	}
	if (key < bt->key[tin])
	{
		pin = tin;
	}
	else
	{
		pin = tin+1;
	}
	if (bt->point[pin] != NULL)
	{
		btree_insert_node(bt->point[pin],key);
	}
	else
	{
		if (bt->index < 2*V)
		{
			btree_insert_key(bt,key);
		}
		else
		{
            bt = btree_split(bt,key);
			return bt->fa;
		}
	}
	return bt;
}

btree_t* btree_insert(btree_t* bt,TYPE key)
{
	bt = btree_insert_node(bt,key);
	while(bt->fa != NULL)
	{
		bt = bt->fa;
	}
	return bt;
}

void btree_destory(btree_t* bt)
{
	if (bt == NULL)
	{
		return ;
	}
	for (int i = 0;i <= 2*V; i++)
	{
		if (bt->point[i] != NULL)
		{
			btree_destory(bt->point[i]);
			bt->point[i] = NULL;
		}
	}
	free(bt);
}

int main(int argc, char **argv)
{
	btree_t* bt = NULL;
	int ba[20] = {5,-1,1,0,3,4,9,5,2,6,8,18,16,13,10,17,15,11,14,7};
	for (int i = 0;i < 20;i++)
	{
		bt = btree_insert(bt,ba[i]);
	}
	int t = btree_search(bt,ba[2]);
	t = btree_search(bt,20);
	//t = btree_update(bt,ba[5],10);
	bt = btree_node_delete(bt,ba[7]);
	bt = btree_node_delete(bt,ba[10]);
	btree_destory(bt);

	return 0;
}