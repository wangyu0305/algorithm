#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <memory.h>
#include <string.h>

static int hashsize[] = {
	17,             /* 0 */
	37,             /* 1 */
	79,             /* 2 */
	163,            /* 3 */
	331,            /* 4 */
	673,            /* 5 */
	1361,           /* 6 */
	2729,           /* 7 */
	5471,           /* 8 */
	10949,          /* 9 */
	21911,          /* 10 */
	43853,          /* 11 */
	87719,          /* 12 */
	175447,         /* 13 */
	350899,         /* 14 */
	701819,         /* 15 */
	1403641,        /* 16 */
	2807303,        /* 17 */
	5614657,        /* 18 */
	11229331,       /* 19 */
	22458671,       /* 20 */
	44917381,       /* 21 */
	89834777,       /* 22 */
	179669557,      /* 23 */
	359339171,      /* 24 */
	718678369,      /* 25 */
	1437356741,     /* 26 */
	2147483647      /* 27 (largest signed int prime) */
};  

typedef struct tablenode 
{
	int data;
	char* str;
	void* buffer; 
	tablenode* next;
}tablenode;

typedef struct hashtable 
{
	tablenode* hashnode[17];
}hashtable;

hashtable* create_hashtable(unsigned int nodecount);

tablenode* search_table(hashtable* table,unsigned int nodecount,int data,char* str);

bool insert_table(hashtable* table,unsigned int nodecount,int data,void* buffer,char* str);

bool clearall_table(hashtable* table,unsigned int nodecount);

bool delete_tablenode(hashtable* table,unsigned int nodecount,int data,char* str);

bool print_node(hashtable* table,unsigned int nodecount,int data,char* str);

void print_table(hashtable* table,unsigned int nodecount);

