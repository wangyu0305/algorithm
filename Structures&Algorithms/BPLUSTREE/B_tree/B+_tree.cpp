#pragma once

#ifndef BINARY_PLUS_TREE
#define BINARY_PLUS_TREE

//前置类定义
template <typename TK, typename TV,size_t THD> class CBinaryPlusTree;

class CBinaryPlusTreeException
{
public:
	enum Type
	{
		NoEnoughMemory=0
	};
    
	CBinaryPlusTreeException(Type type)
	{
		m_type = type;
	}

private:
	Type m_type;
};


template <typename TK, typename TV,size_t THD=10>
class CBinaryPlusTreeNode
{
public:
    typedef CBinaryPlusTreeNode BPTN;
	typedef CBinaryPlusTreeNode* LPBPTN;
	typedef TV* PVAL;
	static const size_t Degree = 2* THD +1 ; //节点的度
    static const size_t Rank = THD; //节点的最小应满足的度
	friend CBinaryPlusTree<TK,TV,THD>;
public:
	size_t Count;
	LPBPTN Parent; //指向父节点
	TK Keys[Degree]; 
	union 
	{
		LPBPTN Nodes[Degree+1]; 
        struct 
		{
           PVAL Vals[Degree];
		   LPBPTN NextLeafNode; //指向下一个叶子节点
		};
	};
	bool IsLeaf; //是否叶子标识
    
public:
	//创建新节点，参数为false表示创建枝干节点，否则为叶子节点
	CBinaryPlusTreeNode(bool newLeaf=false);
    ~CBinaryPlusTreeNode();
	
private: 
	bool IsRoot();
	bool Search(TK key,size_t &index);//查找一个key，返回对应的索引值
	LPBPTN Splite(TK &key); //分裂节点
	size_t Add(TK &key); //在枝干插入一个key,返回对应的索引值
	size_t Add(TK &key,TV &val); //在叶子节点插入一个key,返回对应的索引值
	bool RemoveAt(size_t index);
#ifdef _DEBUG
public:
	void Print();
#endif
};


template<typename TK,typename TV, size_t THD =10>
class CBinaryPlusTree
{
public:  //类型声明和常量
	typedef  CBinaryPlusTreeNode<TK,TV,THD> BPTN;
    typedef  CBinaryPlusTreeNode<TK,TV,THD>* LPBPTN;
	static const size_t Degree = 2* THD +1 ; //节点的度
    static const size_t Rank = THD; //节点的最小应满足的度

	struct FindNodeParam
	{
		LPBPTN pNode; //找到节点的指针
		size_t index; // 节点关键字序号
		bool flag; // 是否找到

	};
	
	
public:
	LPBPTN Root;
	LPBPTN LeafHead; //叶子指针起点
	size_t KeyCount; //key数量
	size_t Level; //层数
	size_t NodeCount; //节点数
    
public:
    CBinaryPlusTree():KeyCount(0),Level(0),NodeCount(0),Root(NULL),LeafHead(NULL)
	{	
		
	}
    ~CBinaryPlusTree()
	{
		Clear();
	}
    
	bool Add(TK key,TV val);
	bool Remove(TK key);
    
	void Clear();
	bool ContainKey(TK key); //是否包含某个key
	bool GetVal(TK key,TV & val); // 查找指定key的值
	bool SetVal(TK key,TV & val); // 设定key对应的值 
private:
	void InitTree();//初始化树
	bool AdjustAfterAdd(LPBPTN pNode);
	bool AdjustAfterRemove(LPBPTN pNode);
    void RemoveNode(LPBPTN pNode);
	void Search(LPBPTN pNode,TK &key,FindNodeParam & fnp);//查找一直到叶子节点
	void SearchBranch(LPBPTN pNode,TK &key,FindNodeParam & fnp); //查找枝干

#ifdef _DEBUG
public:
	void Print();
	void PrintLeaf(); //按序打印叶子节点
	void PrintNode(LPBPTN pNode);
#endif
};




#include "BinaryPlusTree.inc" //包含具体的函数定义文件

#endif


//CBinaryPlusTreeNode 成员函数

template<typename TK,typename TV,size_t THD>
CBinaryPlusTreeNode<TK,TV,THD>::CBinaryPlusTreeNode(bool newLeaf):Count(0),Parent(NULL),IsLeaf(newLeaf)
{
	
	for(size_t i=0;i<=Degree;i++) //所有子节点指针清零
	{
	    Keys[i]= TK();
		Nodes[i]=NULL;
	}
	
}
 
template<typename TK,typename TV,size_t THD> 
CBinaryPlusTreeNode<TK,TV,THD>::~CBinaryPlusTreeNode()
{
	if(IsLeaf)
	{
        for(size_t i=0;i<Count;i++)
		{
		    if(Vals[i])	delete Vals[i];
		}
	}
}

template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::IsRoot()
{
	if(Parent)return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::Search(TK key,size_t &index)
{
	bool ret =false;

	if(Count==0) //如果是空集返回0
	{
		index=0;
		return false;
	}
    
    // 边界值处理 大部分的节点查找只需通过边界判定
    if(Keys[0] > key)
    {
       index =0;
       return false;
    }
    if(Keys[Count-1] < key)
	{
		index = Count; 
		return false;
	}
	
	
	if(Count > 20) //折半查找 2* log(n,2) < 顺序查找 (1+n)/2 ,在20次以上折半查找更好
	{
	    size_t m,s,e;
    
		s=0;e=Count-1;
	    while(s<e)
		{
			m=(s+e)/2;
			
			if(key == Keys[m])
			{
			    index = m;
				return true;    
			}
			else if(key < Keys[m])   
			{   
				e=m;
			}   
			else // key > Keys[m]
			{   
				s=m;
			}

			if(s==e-1)  //当s 和 e 即将相会时
			{
			    if(key == Keys[s])
				{
					index = s;
					return true;  
				}
				
				if(key == Keys[e])
				{
					index = e;
					return true;  
				}
				
				if(key < Keys[s])
				{   
					index=s;
					return false;
				}
				
				if(key < Keys[e])
				{   
					index=e;
					return false;
				}
				
				if(key > Keys[e])
				{   
					index=e+1;
					return false;
				}
				
			}
		};
		return false;
	}
	else //顺序查找
	{
		
		for(size_t i=0;i<Count;i++)
		{
			if(key<Keys[i])
			{
				index = i;
				ret = false;
				break;
			}
			else if(key == Keys[i])
			{
				index = i;
				ret = true;
				break;
			}
			
		}
		return ret;
	}
}



// 分裂成两个，
template<typename TK,typename TV,size_t THD>
CBinaryPlusTreeNode<TK,TV,THD>* CBinaryPlusTreeNode<TK,TV,THD>::Splite(TK &key)
{
    LPBPTN newNode = new BPTN(this->IsLeaf);
	if(newNode == NULL)
	{
		throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
		return NULL;
	}
	
	key = Keys[Rank]; //设定返回值
	
	if(IsLeaf) // 如果是叶子节点，其key值复制一份上移 分成Rank+1 和 Rank
	{
		for(size_t i = Rank+1; i< Degree; i++) //拷贝键值
		{
			newNode->Keys[i-Rank-1] = Keys[i];
			Keys[i]=TK(); //后段Key置零
			newNode->Vals[i-Rank-1] = Vals[i];
			Vals[i]= NULL; //后段值指针置零
		}
		newNode->NextLeafNode = NextLeafNode; 
		NextLeafNode = newNode;
		
		newNode->Parent = Parent; //共用一个父
		newNode->Count = Rank;
		Count = Rank+1;
	}
	else //枝干节点 每个节点变成 Rank，上移一个节点
	{
	    for(size_t i = Rank+1; i< Degree; i++) //拷贝键值
		{
			newNode->Keys[i-Rank-1] = Keys[i];
		}
		for(size_t i = Rank+1; i<= Degree;i++) //拷贝子节点指针
		{
			newNode->Nodes[i-Rank-1] = Nodes[i];
		}
		newNode->Parent = Parent; 
		newNode->Count = Rank;
	    
		//修改新节点所有子节点父指向新节点，而非原有节点
		LPBPTN childNode;
		for(size_t i = 0;i <= newNode->Count;i++)
		{
			childNode = newNode->Nodes[i];
			if(childNode)
			{
				childNode->Parent = newNode;
			}
		}
		
		
		//将后段节点key值清空
		for(size_t i = Rank+1; i< Degree+1;i++) //后段子节点指针置零
		{
			Nodes[i] = NULL; 
		}

		for(size_t i = Rank; i< Degree;i++) //后段Key置零
		{
			Keys[i] = TK();
		}

		Count = Rank;

	}	
    
	return newNode;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryPlusTreeNode<TK,TV,THD>::Add(TK &key)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key,index)) //相同的key不处理
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];   
		 }
        
		 for(size_t i = Count+1; i > index;i--)
		 {
			 Nodes[i]= Nodes[i-1];
		 }

		 Keys[index]=key;
		 Nodes[index] = NULL; //枝干则需进行调整
		 Count++;
	 }
     return index;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryPlusTreeNode<TK,TV,THD>::Add(TK &key,TV &val)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Vals[0] = new TV();
		 if(Vals[0]==NULL)
         {
             throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
         }
         
         * Vals[0] = val;//引入新值
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key,index)) //相同的key不处理
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];  
			 Vals[i] = Vals[i-1]; 
		 }
         Keys[index]=key;
         Vals[index] = new TV();
         if(Vals[index]==NULL)
         {
             throw CBinaryPlusTreeException(CBinaryPlusTreeException::Type::NoEnoughMemory);
         }
         * Vals[index] = val;//引入新值
		 Count++;
	 }
     return index;
}


template<typename TK,typename TV,size_t THD>
bool CBinaryPlusTreeNode<TK,TV,THD>::RemoveAt(size_t index)
{
    if(index>Count-1)
    {
        return false;
    }
    
    if(IsLeaf)
    {
        if(Vals[index]) delete Vals[index];//释放内存
        
        for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
			Vals[i]= Vals[i+1];
		}
		Keys[Count-1] = TK();
		Vals[Count-1]=NULL;
    }
    else
    {
	    for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
		}
		
		Keys[Count-1] = TK();
		
		for(size_t i = index;i< Count;i++)
		{
			Nodes[i]= Nodes[i+1];
		}
		Nodes[Count] = NULL;
	}
	Count--;
	return true;
}


//CBinaryPlusTree 成员函数

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::InitTree()
{
	Root = new BPTN(true);
	LeafHead = Root;
    KeyCount =0;
	NodeCount=1;
	Level=1;
}

//B+树的查找必需找到叶子节点才会找到对应的
template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Search(LPBPTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	if(pNode->Search(key,index)) //找到了关键字
	{
	    if(pNode->IsLeaf)
	    {
			fnp.flag = true; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
		    pNode = pNode->Nodes[index]; //根据分裂规则，上置的key属于左子节点
		    while(!pNode->IsLeaf){ //如果不是叶子继续往下查找
				pNode = pNode->Nodes[pNode->Count]; //一定在子节点最后一个key的树里  
		    };
		    fnp.flag = true;
		    fnp.index = pNode->Count-1; //一定是找到的叶子节点最后一个
		    fnp.pNode = pNode;
		}
	}
	else
	{
		if(pNode->IsLeaf) //如果是叶子节点，则不继续往下查找
		{
			fnp.flag = false; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
			Search(pNode->Nodes[index],key,fnp);
        }
	}
}


template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::Add(TK key,TV val)
{
	if(Root==NULL) //如果没有初始化，重新初始化
	{
		InitTree();
	}

	FindNodeParam fnp;

	Search(Root,key,fnp); 
    if(!fnp.flag) //相同key,不管它
	{
	    fnp.pNode->Add(key,val);//在找到的叶子节点插入key
        KeyCount++;

		if(fnp.pNode->Count == Degree ) //插入后值等于Degree，必需进行调整
		{
			return AdjustAfterAdd(fnp.pNode);
		}
		return true;
	}
	return false; //如果键值相同
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::AdjustAfterAdd(LPBPTN pNode)
{
	
	TK key;
	LPBPTN newNode = pNode->Splite(key); 
	NodeCount++;
    LPBPTN parent = pNode->Parent;

	if(parent==NULL) //父节点是空，表示当前节点是根节点
	{
		LPBPTN newRoot = new BPTN(); //新建一个子点 
		if(newRoot==NULL) return false;//如果内存不够分配失败
		
		NodeCount++;
		Root = newRoot; //LeafHead不需改动
        Root->Add(key);
		
		Root->Nodes[0] = pNode;
		Root->Nodes[1]= newNode;
       	
		pNode->Parent = Root;
		newNode->Parent = Root;

		Level++; //层次提升1
		return true;
	}
	else
	{
			size_t index = parent->Add(key); //取得父节点插入提升值以后的索引
			
			parent->Nodes[index]= pNode;
            parent->Nodes[index+1]= newNode;

			if(parent->Count == Degree) 
			{
				return AdjustAfterAdd(parent);
			}
			return true;
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::RemoveNode(LPBPTN pNode)
{
	if(!pNode->IsLeaf) //如果节点不是叶子节点
	{
		for(size_t i=0;i <= pNode->Count;i++)
		{
			RemoveNode( pNode->Nodes[i] );
			pNode->Nodes[i] = NULL;
		}
	}
	delete pNode;
	NodeCount--;
	return;
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Clear()
{
	if(Root)
	{
		RemoveNode(Root);
		KeyCount=0;
		Level=0;
		Root = NULL;
		LeafHead = NULL;
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::ContainKey(TK key)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    return fnp.flag;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::GetVal(TK key,TV & val)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    if(fnp.flag)
    {
        val = *(fnp.pNode->Vals[fnp.index]);
        return true;
    }
    else return false;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::SetVal(TK key,TV & val)
{
    FindNodeParam fnp;
    Search(Root,key,fnp);
    if(fnp.flag)
    {
        *(fnp.pNode->Vals[fnp.index]) = val;
        return true;
    }
    else return false;
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::SearchBranch(LPBPTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	
	if(pNode->IsLeaf) return ; //叶子节点直接返回错误
	
	if(pNode->Search(key,index)) //找到了关键字
	{
		fnp.flag = true; 
		fnp.index = index;
		fnp.pNode = pNode;
		return;
	}
	else //没找到往下查找
	{
		if(!pNode->Nodes[index]->IsLeaf) 
		{
			return SearchBranch(pNode->Nodes[index],key,fnp);
		}
		else //已查到叶子节点的上一层
		{
		    fnp.index =index;
			fnp.flag = false;
			fnp.pNode= pNode;
			return ;//未找到
		}
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::Remove(TK key)
{
    FindNodeParam fnp;
    
    if(Root==NULL)
		return false;

    Search(Root,key,fnp);
	
	if(fnp.flag) //查到key
	{
	    //只有最后一个叶子节点了
		if(Root == fnp.pNode)
		{
			Root->RemoveAt(fnp.index);
			KeyCount--;
			return AdjustAfterRemove(fnp.pNode);
		}
	    
	    if( fnp.index == fnp.pNode->Count-1) //这个key在叶子是最后一个，那么它肯定在枝干中
	    {
	        FindNodeParam fnpb; //枝干查找参数
	        
	        SearchBranch(Root,key,fnpb); //在枝干中查找
	        if(fnpb.flag)
	        {
				fnpb.pNode->Keys[fnpb.index] = fnp.pNode->Keys[fnp.pNode->Count-2];// 取倒数第二个key
			}
	    }
	    
	    fnp.pNode->RemoveAt(fnp.index); //肯定是叶子节点，进行移除,
	    KeyCount--;
	    
	    return AdjustAfterRemove(fnp.pNode);
	}
    return false; //没找到报失败
}

//在删除节点后对节点进行调整
template<typename TK,typename TV, size_t THD>
bool CBinaryPlusTree<TK,TV,THD>::AdjustAfterRemove(LPBPTN pNode)
{
    if(pNode->Count >= Rank)  //如果被删除key的节点其关键字个数不小于rank不需处理
	{
		return true; 
	}
	
	//如果调整的是根节点 
	if( pNode->IsRoot() ) 
	{
		if(pNode->Count==0) 
		{
			if(!pNode->IsLeaf)//是枝干节点
			{
				Root = pNode->Nodes[0]; //重新调整根节点
				pNode->Nodes[0]->Parent = NULL; //此时这个节点应为根节点
			}
			else
			{
				Root = NULL; //最后一个节点也没了
				LeafHead = NULL;
			}
			delete pNode;
			NodeCount--;
			Level--;
		}
		return true;
	}


	
	LPBPTN pBrother,pParent;
    size_t pos;
	
	pParent =pNode->Parent;
	pParent->Search(pNode->Keys[0],pos); //在父节点中指向自身的索引
	
	if(pos == pParent->Count)  //左兄弟
	{
		pBrother = pParent->Nodes[pos-1]; 

		if(pBrother->Count > Rank)  //大于Rank
		{
			// check
		    if(pNode->IsLeaf) //叶子 
		    {
		        //全体右移
				for(size_t i = pNode->Count;i > 0 ; i--)   
				{
					pNode->Keys[i] = pNode->Keys[i-1];
					pNode->Vals[i] = pNode->Vals[i-1];
				}
				pNode->Keys[0] = pBrother->Keys[pBrother->Count-1];
				pNode->Vals[0] = pBrother->Vals[pBrother->Count-1];
				pNode->Count++;
				
				pBrother->Keys[pBrother->Count-1] = TK(); //删除最后一个
				pBrother->Vals[pBrother->Count-1] = NULL;
				pBrother->Count--;

				pParent->Keys[pos-1] = pBrother->Keys[pBrother->Count-1];
				
				return true;
		    }
		    //check
			else  //非叶子
		    {
				//全体右移
				for(size_t i = pNode->Count;i > 0 ; i--)   
				{
					pNode->Keys[i] = pNode->Keys[i-1];
				}
				for(size_t i = pNode->Count+1;i > 0 ; i--)   
				{
					pNode->Nodes[i] = pNode->Nodes[i-1];
				}

				//父key下移
				pNode->Keys[0] = pParent->Keys[pos-1]; 
				pParent->Keys[pos-1]= pBrother->Keys[pBrother->Count-1];  //兄弟最后一个key上移
				//调整本节点
				pNode->Nodes[0] = pBrother->Nodes[pBrother->Count]; //指向兄弟节点最后一个子指针
				pNode->Count++;
				//调整兄弟节点
				//key清零
				pBrother->Keys[pBrother->Count-1]=TK(); 
				//调整子节点
				if(pBrother->Nodes[pBrother->Count])
				{
					pBrother->Nodes[pBrother->Count]->Parent = pNode; //调整其父指针
					pBrother->Nodes[pBrother->Count] = NULL; //置零
				}
				pBrother->Count--; //兄弟节点减一
				return true; //父节点数未动，故不再调整
		    }
		}
		else  //如果兄弟节点key数等于Rank
		{
			//check 2
		    if(pNode->IsLeaf)
		    {
		        pParent->RemoveAt(pos-1); //父节点移除最后一个
                pParent->Nodes[pos-1] = pBrother;//修正父的子节点指针

                //拷贝节点到兄弟
				for(size_t i = 0;i<pNode->Count;i++)
				{
					pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
					pBrother->Vals[pBrother->Count+i] = pNode->Vals[i];
					pNode->Vals[i]=NULL;
				}
				pBrother->Count += pNode->Count; 
				pBrother->NextLeafNode = pNode->NextLeafNode;

				delete  pNode;
				NodeCount--;

				return AdjustAfterRemove(pParent); //向上调整父
		    }
		    //check
			else
		    {
				pBrother->Keys[pBrother->Count]= pParent->Keys[pos-1]; //父key下移
				pBrother->Count++; 

				pParent->RemoveAt(pos-1);
				pParent->Nodes[pos-1] = pBrother; 

				//拷贝节点
				for(size_t i = 0;i<pNode->Count;i++)
				{
					pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
				}

				for(size_t i = 0;i<= pNode->Count;i++)
				{
					pBrother->Nodes[pBrother->Count+i] = pNode->Nodes[i];
					pNode->Nodes[i]->Parent = pBrother; 
					
				}
				
				pBrother->Count = 2* Rank; //一定是这个值
	            
				delete pNode;
				NodeCount--;

				return AdjustAfterRemove(pParent); //继续向上调整
		    
			}
		}

	} 
	
	else //右兄弟
	{
		pBrother = pParent->Nodes[pos+1]; 

		if(pBrother->Count > Rank)
		{
			//check
			if(pNode->IsLeaf) //当前节点是叶子
            {
				pParent->Keys[pos] = pBrother->Keys[0]; //将兄弟的第一个key移到枝干
				
				pNode->Keys[pNode->Count] = pBrother->Keys[0];    
				pNode->Vals[pNode->Count] = pBrother->Vals[0];    
				pBrother->Vals[0]=NULL;
				pNode->Count++;
				pBrother->RemoveAt(0);//移除第一个
				return true;
            }
            //check
			else
            {
				pNode->Keys[pNode->Count] = pParent->Keys[pos];
                pNode->Nodes[pNode->Count+1] = pBrother->Nodes[0]; //兄弟第一个指针
				pNode->Count++;

                pParent->Keys[pos]= pBrother->Keys[0];  //兄弟第一个key上移
				
				pBrother->Nodes[0]->Parent = pNode;
				
				pBrother->RemoveAt(0);//移除第一个
				return true;
			}
		}
		else  //如果兄弟节点key数等于Rank
		{
			//check
            if(pNode->IsLeaf)
			{
				//拷贝兄弟节点
                for(size_t i=0 ; i <Rank;i++)
				{
					pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
					pNode->Vals[pNode->Count+i] = pBrother->Vals[i];
					pBrother->Vals[i]=NULL; //置零
				}
                pNode->Count += Rank;
                
				delete pBrother;
				NodeCount--;

				pParent->RemoveAt(pos);
				pParent->Nodes[pos] = pNode;
			
				return AdjustAfterRemove(pParent);
			}
			//check
			else
			{
				pNode->Keys[pNode->Count]= pParent->Keys[pos]; //父key下移
				
				pParent->RemoveAt(pos);
				pParent->Nodes[pos]= pNode;  //修正父节点指针
				pNode->Count++; //Count+1

				for(size_t i = 0;i<Rank;i++)
				{
					pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
				}

				for(size_t i = 0;i<=Rank;i++)
				{
					pNode->Nodes[pNode->Count+i] = pBrother->Nodes[i];
					pBrother->Nodes[i]->Parent = pNode;
					
				}
	            
				pNode->Count += Rank;

				delete pBrother;
				NodeCount--;
				
				return AdjustAfterRemove(pParent); //继续向上调整
			}

			
		}
	}

}


#ifdef _DEBUG

// 打印调试函数
template<typename TK,typename TV,size_t THD>
void CBinaryPlusTreeNode<TK,TV,THD>::Print()
{
    printf("----------------------\n");
	printf("Address: 0x%X Count: %d, Parent: 0x%X  IsLeaf:%d\n",(void*)this,Count,(void*)Parent,IsLeaf);
	printf("Keys: { ");
	for(size_t i =0; i<Degree;i++)
	{
		printf("%5d ",Keys[i]);
	}
	printf(" }\n");
	
    if(IsLeaf)
    {
		printf("Vals: { ");
		for(size_t i =0; i<Degree;i++)
		{
			if(Vals[i]==NULL)
			{
			   printf("{NUL}");
			}
			else
			{
			   printf("%5d",*Vals[i]);
			}
			
		}
		printf(" }\n");
		
		printf("NextLeaf: %X\n", NextLeafNode);
    }
    else
    {
		printf("Ptrs: {");
		for(size_t i =0; i<=Degree;i++)
		{
			printf("0x%X ",(void*)Nodes[i]);
		}
		printf("}\n");
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::Print()
{
    printf("*****************************************************\n");
    printf("KeyCount: %d, NodeCount: %d, Level: %d, Root: 0x%X \n",KeyCount,NodeCount,Level,(void*)Root );
	
	if(Root)
	{
		PrintNode(Root);
	}
	
	
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::PrintNode(LPBPTN pNode)
{
	
    
    pNode->Print(); 
	if(!pNode->IsLeaf) //如果不是叶子节点继续
	{
	    
		for(size_t i =0; i<= pNode->Count;i++)
		{
			PrintNode(pNode->Nodes[i]);
		}
	}
}

template<typename TK,typename TV, size_t THD>
void CBinaryPlusTree<TK,TV,THD>::PrintLeaf()
{
	LPBPTN p = LeafHead;
	while(p)
	{
	    for(size_t i=0;i<p->Count;i++)
	    {
	        printf("%5d ",p->Keys[i]);
	    }
	    printf("\n");
	    for(size_t i=0;i<p->Count;i++)
	    {
	        printf("%5d ",*(p->Vals[i]));
	    }
	    printf("\n");
	    p= p->NextLeafNode;
	}
    
}

#endif