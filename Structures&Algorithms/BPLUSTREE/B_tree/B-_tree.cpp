/*
B-树的特点，键与值均分布枝干中
*/

#pragma once
#ifndef BINARY_MINUS_TREE
#define BINARY_MINUS_TREE

class CBinaryMinusTreeException
{
public:
	enum Type
	{
		NoEnoughMemory=0,
	};
    
	CBinaryMinusTreeException(Type type)
	{
		m_type = type;
	}

private:
	Type m_type;
};

template<typename TK,typename TV,size_t THD>class CBinaryMinusTreeNode; //前置声明

template<typename TK,typename TV,size_t THD>class CBinaryMinusTree; //前置声明

template<typename TK,typename TV,size_t THD=10>
class BinaryMinusTreeKey
{
public:
    friend CBinaryMinusTreeNode<TK,TV,THD>;
	friend CBinaryMinusTree<TK,TV,THD>;
private:
	TK Key; //键值
	TV* pVal; //数据指针
	
public:	
	BinaryMinusTreeKey():Key(TK()),pVal(NULL)
	{}
   	
	~BinaryMinusTreeKey()
	{
	   if(pVal)
	   {
		   delete pVal;
		   pVal=NULL;
	   }
	}
    
	TV GetValue()
	{
		if(pVal)return *pVal;
		else return TV();
	}

};


template<typename TK,typename TV, size_t THD =10>
class CBinaryMinusTreeNode
{
public:
    typedef CBinaryMinusTreeNode BMTN;
	typedef CBinaryMinusTreeNode* LPBMTN;
	typedef BinaryMinusTreeKey<TK,TV,THD> BMKEY;
	static const size_t Degree = 2* THD +1 ; //节点的度
    static const size_t Rank = THD; //节点的最小应满足的度
	friend CBinaryMinusTree<TK,TV,THD>;
public:
	size_t Count;
	LPBMTN Parent; //指向父节点
	BMKEY  Keys[Degree]; 
    LPBMTN Nodes[Degree+1]; 
    
public:
	CBinaryMinusTreeNode():Count(0),Parent(NULL)
	{
		for(size_t i=0;i<=Degree;i++) //所有子节点指针清零
		{
			Nodes[i]=NULL;
		}
	}

private: 
	bool IsRoot();
	bool IsLeaf();
	bool Search(TK key,size_t &index);//查找一个key，返回对应的索引值
	LPBMTN Splite(BMKEY &key); //分裂节点
	size_t Add(BMKEY &key); //插入一个key,返回对应的索引值
	bool RemoveAt(size_t index);
	void Print();
};


template<typename TK,typename TV, size_t THD =10>
class CBinaryMinusTree
{
public:  //类型声明和常量
	typedef  CBinaryMinusTreeNode<TK,TV,THD> BMTN;
    typedef  CBinaryMinusTreeNode<TK,TV,THD>* LPBMTN;
	typedef  BinaryMinusTreeKey<TK,TV,THD> BMKEY;
	
	struct FindNodeParam
	{
		LPBMTN pNode; //找到节点的指针
		size_t index; // 节点关键字序号
		bool flag; // 是否找到

	};
	
	static const size_t Degree = 2* THD +1 ; //节点的度
    static const size_t Rank = THD; //节点的最小应满足的度
public:
	LPBMTN Root;
	size_t KeyCount; //key数量
	size_t Level; //层数
	size_t NodeCount; //节点数

public:
    CBinaryMinusTree():Root(NULL),KeyCount(0),Level(0),NodeCount(0)
	{	
	}
    ~CBinaryMinusTree()
	{
		Clear();
	}
    
	bool Add(TK key,TV val);
	bool Remove(TK key);
    void Print();
	void Clear();
private:
	void InitTree();//初始化树
	bool AdjustAfterAdd(LPBMTN pNode);
	bool AdjustAfterRemove(LPBMTN pNode);
    void RemoveNode(LPBMTN pNode);
	void Search(LPBMTN pNode,TK &key,FindNodeParam & fnp);
	void PrintNode(LPBMTN pNode);
};

#include "BinaryMinusTree.inc"

#endif


template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::Search(TK key,size_t &index)
{
	bool ret =false;

	if(Count==0) //如果是空集肯定是叶子节点，故而返回0是没有问题的
	{
		index=0;
		return false;
	}
    
    // 边界值处理 大部分的节点查找只需通过边界判定
    if(Keys[0].Key > key)
    {
       index =0;
       return false;
    }
    if(Keys[Count-1].Key < key)
	{
		index = Count; 
		return false;
	}
	
	
	if(Count > 20) //折半查找 2* log(n,2) < 顺序查找 (1+n)/2 ,在20次以上折半查找更好
	{
	    size_t m,s,e;
    
		s=0;e=Count-1;
	    while(s<e)
		{
			m=(s+e)/2;
			
			if(key == Keys[m].Key)
			{
			    index = m;
				return true;    
			}
			else if(key < Keys[m].Key)   
			{   
				e=m;
			}   
			else // key > Keys[m].Key
			{   
				s=m;
			}

			if(s==e-1)  //当s 和 e 即将相会时
			{
			    if(key == Keys[s].Key)
				{
					index = s;
					return true;  
				}
				
				if(key == Keys[e].Key)
				{
					index = e;
					return true;  
				}
				
				if(key < Keys[s].Key)
				{   
					index=s;
					return false;
				}
				
				if(key < Keys[e].Key)
				{   
					index=e;
					return false;
				}
				
				if(key > Keys[e].Key)
				{   
					index=e+1;
					return false;
				}
				
			}
		};
		return false;
	}
	else //顺序查找
	{
		
		for(size_t i=0;i<Count;i++)
		{
			if(key<Keys[i].Key)
			{
				index = i;
				ret = false;
				break;
			}
			else if(key == Keys[i].Key)
			{
				index = i;
				ret = true;
				break;
			}
			
		}
		return ret;
	}
}

// 分裂成两个，并上移key
template<typename TK,typename TV,size_t THD>
CBinaryMinusTreeNode<TK,TV,THD>* CBinaryMinusTreeNode<TK,TV,THD>::Splite(BMKEY &key)
{
    LPBMTN newNode = new BMTN();
	if(newNode == NULL)
	{
		throw CBinaryMinusTreeException(CBinaryMinusTreeException::Type::NoEnoughMemory);
		return NULL;
	}
		
    for(size_t i = Rank+1; i< Degree; i++) //拷贝键值
	{
		newNode->Keys[i-Rank-1] = Keys[i];
	}
	for(size_t i = Rank+1; i<= Degree;i++) //拷贝子节点指针
	{
		newNode->Nodes[i-Rank-1] = Nodes[i];
	}
	newNode->Parent = Parent; 
    newNode->Count = Rank;
    
    //修改新节点所有子节点父指向新节点，而非原有节点
    for(size_t i = 0;i <= newNode->Count;i++)
    {
        LPBMTN childNode= newNode->Nodes[i];
        if(childNode)
        {
			childNode->Parent = newNode;
        }
    }


	key = Keys[Rank];
	//key.pVal=NULL; //必需置零，否则有可能析构值

	//将后段节点key值清空
    for(size_t i = Rank+1; i< Degree+1;i++) //后段子节点指针置零
	{
		Nodes[i] = (LPBMTN)NULL; 
	}

	for(size_t i = Rank; i< Degree;i++) //后段Key置零
	{
		Keys[i].Key = TK();
		Keys[i].pVal = (TV*)NULL;
	}

	Count = Rank;

	return newNode;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::IsRoot()
{
	if(Parent)return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::IsLeaf()
{
	if(Nodes[0])return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryMinusTreeNode<TK,TV,THD>::Add(BMKEY &key)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key.Key,index)) //相同的key不处理
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];   
		 }
        
		 for(size_t i = Count+1; i > index;i--)
		 {
			 Nodes[i]= Nodes[i-1];
		 }

		 Keys[index]=key;
		 Nodes[index] = NULL; //如果是叶子节点应为零，而枝干则需进行调整
		 Count++;
	 }
     return index;
}

//只移除节点，不管内存释放
template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::RemoveAt(size_t index)
{
    if(index>Count-1)
    {
        return false;
    }
    else
	{
	    for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
		}
		
		Keys[Count-1].Key = TK();
		Keys[Count-1].pVal = NULL;

		for(size_t i = index;i< Count;i++)
		{
			Nodes[i]= Nodes[i+1];
		}
		Nodes[Count] = NULL;
		Count--;
		return true;
	}
}




//初始化树
template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::InitTree()
{
    Root = new BMTN();
	KeyCount =0;
	NodeCount=1;
	Level=1;
}

//查找某个键值，并返回对应的信息
// 其算法为 找到key匹配的节点，并返回相关索引，如未找到
//          则继续向子节点查找，如果仍未找到，且子节点为叶子节点，则返回最相近的索引和该叶子节点
template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Search(LPBMTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	if(pNode->Search(key,index)) //找到了关键字
	{
		fnp.flag = true; 
		fnp.index = index;
		fnp.pNode = pNode;
	}
	else
	{
		LPBMTN childNode = pNode->Nodes[index];
        if(childNode==NULL) //如果是叶子节点，则不继续往下查找
		{
			fnp.flag = false; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
			Search(childNode,key,fnp);
        }
	}
}



// 如果有此key或内存不够则返回false
template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::Add(TK key,TV val)
{
	if(Root==NULL) //如果没有初始化，重新初始化
	{
		InitTree();
	}

	FindNodeParam fnp;

	Search(Root,key,fnp);
    if(!fnp.flag) //相同key,不管它
	{
		BMKEY newKey;
		newKey.Key = key;
		newKey.pVal = new TV(); // 分配值大小的内存
        *(newKey.pVal) = val;
		
	    fnp.pNode->Add(newKey);	//在找到的叶子节点插入key
        newKey.pVal =NULL; //防止值被析构

		KeyCount++;

		if(fnp.pNode->Count == Degree ) //插入后值等于Degree，必需进行调整
		{
			return AdjustAfterAdd(fnp.pNode);
		}
		return true;
	}
	return false; //如果键值相同
}

//在插入后调整节点，其原理是
//将当前节点分裂，并将中间节点提升到父节点
template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::AdjustAfterAdd(LPBMTN pNode)
{
	
	BMKEY key;
	LPBMTN newNode = pNode->Splite(key); 
	NodeCount++;
    LPBMTN parent = pNode->Parent;

	if(parent==NULL) //父节点是空，表示当前节点是根节点
	{
		LPBMTN newRoot = new BMTN(); //新建一个子点 
		if(newRoot==NULL) return false;//如果内存不够分配失败
		
		NodeCount++;
		Root = newRoot;
        Root->Add(key);
		
		key.pVal = NULL; //防止析构时将值地址delete !!!!!!

		Root->Nodes[0] = pNode;
		Root->Nodes[1]= newNode;
       	
		pNode->Parent = Root;
		newNode->Parent = Root;

		Level++; //层次提升1
		return true;
	}
	else
	{
			size_t index = parent->Add(key); //取得父节点插入提升值以后的索引
			
			key.pVal = NULL;  //防止析构时将值地址delete !!!!!!
			
			parent->Nodes[index]= pNode;
            parent->Nodes[index+1]= newNode;

			if(parent->Count == Degree) 
			{
				return AdjustAfterAdd(parent);
			}
			return true;

	}
}


template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::RemoveNode(LPBMTN pNode)
{
	if(!pNode->IsLeaf()) //如果节点不是叶子节点
	{
		for(size_t i=0;i <= pNode->Count;i++)
		{
			RemoveNode( pNode->Nodes[i] );
			pNode->Nodes[i] = NULL;
		}
	}
	delete pNode;
	NodeCount--;
	return;
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Clear()
{
	if(Root)
	{
		RemoveNode(Root);
		KeyCount=0;
		Level=0;
		Root = NULL;
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::Remove(TK key)
{
    if(Root==NULL)
		return false;

    FindNodeParam fnp;
	
	Search(Root,key,fnp);
	
	if(fnp.flag)
	{
		LPBMTN pRemoveNode; //指向应调整的节点
		LPBMTN pNode = fnp.pNode;
       
		pNode->Keys[fnp.index].Key=TK();
		delete pNode->Keys[fnp.index].pVal; //释放值指针的内存块 !!!
		pNode->Keys[fnp.index].pVal=NULL;

		// 删除节点
		if(pNode->IsLeaf())  //是叶子节点
		{
			if(pNode->RemoveAt(fnp.index))
			{
				KeyCount--;
			}
			
			pRemoveNode=pNode;
		}
		else  //是枝干节点
		{
			//找到比要删除key稍大的key,返回其节点指针，它肯定是一个叶子节点
			pRemoveNode = pNode->Nodes[fnp.index+1]; 
			if(!pRemoveNode->IsLeaf())
			{
				do
				{
                    pRemoveNode = pRemoveNode->Nodes[0];     
				}while(!pRemoveNode->IsLeaf());
			}
			
			pNode->Keys[fnp.index] = pRemoveNode->Keys[0]; //用找到的key来替代原有应删除的key
            pRemoveNode->RemoveAt(0); //移除第一个元素
			KeyCount--;
		}
		return AdjustAfterRemove(pRemoveNode);
    }
    return false; //没找到报失败
}

//在删除节点后对节点进行调整
template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::AdjustAfterRemove(LPBMTN pNode)
{
	//如果调整的是根节点
	if( pNode->IsRoot() ) 
	{
		if(pNode->Count==0) 
		{
			Root = pNode->Nodes[0]; //重新调整根节点
			if(pNode->Nodes[0]) pNode->Nodes[0]->Parent = NULL; //此时这个节点应为根节点

			delete pNode;
			NodeCount--;
			Level--;
		}
		return true;
	}

	if(pNode->Count >= Rank)  //如果被删除key的节点其关键字个数不小于rank不需处理
	{
		return true; 
	}


	LPBMTN pBrother,pParent;
    size_t pos;
	
	pParent =pNode->Parent;
	pParent->Search(pNode->Keys[0].Key,pos); //在父节点中指向自身的索引
	
	if(pos == pParent->Count)  //左兄弟
	{
		pBrother = pParent->Nodes[pos-1]; 

		if(pBrother->Count > Rank)  //左兄弟可以上移最后一个key到父节点，父节点的key下移到本节点
		{
			//全体右移
			for(size_t i = pNode->Count;i > 0 ; i--)   
			{
                pNode->Keys[i] = pNode->Keys[i-1];
			}
			for(size_t i = pNode->Count+1;i > 0 ; i--)   
			{
                pNode->Nodes[i] = pNode->Nodes[i-1];
			}

			//父key下移
            pNode->Keys[0] = pParent->Keys[pos-1]; 
            pParent->Keys[pos-1]= pBrother->Keys[pBrother->Count-1];  //兄弟最后一个key上移
			//调整本节点
			pNode->Nodes[0] = pBrother->Nodes[pBrother->Count]; //指向兄弟节点最后一个子指针
			pNode->Count++;
            //调整兄弟节点
			//key清零
			pBrother->Keys[pBrother->Count-1].Key=TK(); 
			pBrother->Keys[pBrother->Count-1].pVal=NULL; 
			//调整子节点
			if(pBrother->Nodes[pBrother->Count])
			{
				pBrother->Nodes[pBrother->Count]->Parent = pNode; //调整其父指针
				pBrother->Nodes[pBrother->Count] = NULL; //置零
			}
			pBrother->Count--; //兄弟节点减一
			return true; //父节点数未动，故不再调整
		}
		else  //如果兄弟节点key数等于Rank，则将父节点的key下移，与两兄弟合并
		{
			pBrother->Keys[pBrother->Count]= pParent->Keys[pos-1]; //父key下移
			pBrother->Count++; 

			pParent->RemoveAt(pos-1);
			pParent->Nodes[pos-1] = pBrother; 

			//拷贝节点
			for(size_t i = 0;i<pNode->Count;i++)
			{
				pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
				pNode->Keys[i].pVal=NULL; //置零防止重复析构
            }

			LPBMTN pChild;
			for(size_t i = 0;i<= pNode->Count;i++)
			{
				pChild = pNode->Nodes[i];
				if(pChild)
				{
					pBrother->Nodes[pBrother->Count+i] = pChild;
					pChild->Parent = pBrother; 
				}
			}
			
			pBrother->Count = 2* Rank; //一定是这个值
            
			delete pNode;
			NodeCount--;

			return AdjustAfterRemove(pParent); //继续向上调整
		}

	} 
	else //右兄弟
	{
		pBrother = pParent->Nodes[pos+1]; 

		if(pBrother->Count > Rank)
		{
			//父key下移，本节点最后一个指针指向兄弟节点的第一个子节点
            pNode->Keys[pNode->Count] = pParent->Keys[pos];
			pNode->Nodes[pNode->Count+1] = pBrother->Nodes[0];
			if(pBrother->Nodes[0]) 
				pBrother->Nodes[0]->Parent = pNode;
			pNode->Count++;
			
			pParent->Keys[pos]= pBrother->Keys[0];  //兄弟第一个key上移
            
			pBrother->RemoveAt(0);//移除第一个
			return true;
		}
		else  //如果兄弟节点key数等于Rank，则将父节点的key下移，与两兄弟合并
		{
			pNode->Keys[pNode->Count]= pParent->Keys[pos]; //父key下移
			pParent->RemoveAt(pos);
			pParent->Nodes[pos]= pNode;  //修正父节点指针
            pNode->Count++;

			for(size_t i = 0;i<Rank;i++)
			{
				pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
				pBrother->Keys[i].pVal=NULL;
            }

			LPBMTN pChild; 
			for(size_t i = 0;i<=Rank;i++)
			{
				pChild = pBrother->Nodes[i];
				if(pChild)
				{
					pNode->Nodes[pNode->Count+i] = pChild;
					pChild->Parent = pNode;
				}
			}
            
            pNode->Count = 2* Rank;

			delete pBrother;
			NodeCount--;
			
			return AdjustAfterRemove(pParent); //继续向上调整
		}
	}

}

// 用于Debug的函数 需要根据模板引入的不同TV类型修改
template<typename TK,typename TV,size_t THD>
void CBinaryMinusTreeNode<TK,TV,THD>::Print()
{
    printf("----------------------\n");
	printf("Address: 0x%X Count: %d, Parent: 0x%X \n",(void*)this,Count,(void*)Parent);
	printf("Keys: {");
	for(size_t i =0; i<Degree;i++)
	{
		printf("%5d ",Keys[i].Key);
	}
	printf("}\n");
    
    printf("Vals: {");
	for(size_t i =0; i<Degree;i++)
	{
		//printf("%d ",Keys[i].GetValue());  //此行可以修改
		Keys[i].GetValue().Print();
	}
	printf("}\n");
   
	printf("Ptrs: {");
	for(size_t i =0; i<=Degree;i++)
	{
		printf("0x%X ",(void*)Nodes[i]);
	}
	printf("}\n");
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Print()
{
    printf("*****************************************************\n");
    printf("KeyCount: %d, NodeCount: %d, Level: %d, Root: 0x%X \n",KeyCount,NodeCount,Level,(void*)Root );
	
	if(Root)
	{
		PrintNode(Root);
	}
	
	
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::PrintNode(LPBMTN pNode)
{
	if(pNode == NULL) return;

    pNode->Print(); //打印当前节点
	
	for(size_t i =0; i<= pNode->Count;i++)
	{
		PrintNode(pNode->Nodes[i]);
	}
}

