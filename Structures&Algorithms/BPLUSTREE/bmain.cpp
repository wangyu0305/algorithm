#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "bplustree.h"

int as[20] = {1,20,18,17,11,3,4,2,8,6,7,5,16,15,19,12,13,14,9,10};
void build_tree(BPLUSTREE* tree,int count)
{
	int i;
	tree->clear_tree();
	srand((unsigned)time(NULL));
	/*for (i = 0;i < count;i++)
	{
		int X = rand()%9999 + 1;
		(void)tree->insert_key(X);
	}*/
	for (i = 0;i < 20;i++)
	{
		(void)tree->insert_key(as[i]);
	}
	printf("successed!\n");
}

void search_data(BPLUSTREE* tree,int data)
{
	char path[255] = {0};
	(void)tree->search_tree(data,path);
	printf("\n%s",path);
}

void insert_data(BPLUSTREE* tree,int data)
{
	bool success = tree->insert_key(data);
	if (success == true)
	{
		printf("successed!\n");
	}
	else
	{
		printf("failed!\n");
	}
}

void delete_data(BPLUSTREE* tree,int data)
{
	bool success = tree->delete_key(data);
	if (success == true)
	{
		printf("successed!\n");
	} 
	else
	{
		printf("failed!\n");
	}
}

BPLUSTREE* rotate_tree(BPLUSTREE* tree)
{
	BPLUSTREE* newtree = tree->rotate_tree();
	delete tree;
	printf("successed!\n");
	return newtree;
}

void print_tree(BPLUSTREE* tree)
{
	tree->print_tree();
}

void check_tree(BPLUSTREE* tree)
{
	bool success = tree->check_tree();
	if (success == true)
	{
		printf("successed!\n");
	}
	else
	{
		printf("failed!\n");
	}
}

int main(int argc, char **argv)
{
	int x = 1;
	int y = 0;
	BPLUSTREE* tree = new BPLUSTREE;
	while (x != 0)
	{
		printf("\n\n");
		printf("    *******************************************************************\n");
		printf("    *           欢迎进入B+树演示程序，请选择相应功能。                *\n");
		printf("    *           1。随机建立一棵B+树；                                 *\n");
		printf("    *           2。在B+树中查找一个数；                               *\n");
		printf("    *           3。在B+树中插入一个数；                               *\n");
		printf("    *           4。在B+树中删除一个数；                               *\n");
		printf("    *           5。对B+树旋转，进行重新平衡；                         *\n");
		printf("    *           6。显示整个B+树；                                     *\n");
		printf("    *           7。检查整个B+树；                                     *\n");
		printf("    *           0。退出程序；                                         *\n");
		printf("    *******************************************************************\n");
		printf("\n    您的选择是：");

		scanf("%d",&x);
		switch (x)
		{
		case 1:
			printf("请输入数据个数 （10~150）：\n");
			scanf("%d",&y);
			build_tree(tree,y);
			break;
		case 2:
			printf("请输入要查找的数值：\n");
			scanf("%d",&y);
			search_data(tree,y);
			break;
		case 3:
			printf("请输入要插入的数值：\n");
			scanf("%d",&y);
			insert_data(tree,y);
			break;
		case 4:
			printf("请输入要删除的数值：\n");
			scanf("%d",&y);
			delete_data(tree,y);
			break;
		case 5:
			printf("旋转树！\n");
			tree = rotate_tree(tree);
			break;
		case 6:
			printf("显示树！\n");
			print_tree(tree);
			break;
		case 7:
			printf("检查树！\n");
			check_tree(tree);
			break;
		case 0:
			delete tree;
			return 0;
			break;
		default:
			break;
		}
	}
	delete tree;
	return 0;
}