/*         //线性时间选择算法
    //数组分成m段 每段排序取中间值 取得的中间值再排序后取中间值k 比k大方左边 比k小方右边 再对0~k递归分段 最后返回前一段的第k值
   // 最差时间复杂度O（n）                                       */

#include <stdio.h>
#include <time.h>

void my_bubble(int a[],int first,int end);

int my_partation(int a[],int first,int end,int iter);

int select_k(int a[],int first,int end,int k);


//int main(int argc, char **argv)
//{
//	int a[20] = {5,2,4,6,8,1,14,52,36,47,53,33,42,22,76,18,72,47,45,61};
//	int la = clock();
//	int m = select_k(a,0,19,4);
//	int ne = clock();
//	printf("%d",ne -la);
//	return m;
//}