#include <stdio.h>
#include <string.h>

typedef unsigned char uchar;          //后缀数组构造算法





void CreateSuffixArray(uchar* szText,int L, int** _S, int** _R, int** _T1, int** _T2)
{
	  int i, h, h2, *T, *S1, *S2, *R, *B;

	  S1 = *_S;       // h阶后缀数组
	  S2 = *_T1;      // 2h阶后缀数组
	  R = *_R;        // h阶Rank数组
	  B = *_T2;       // 某个桶空余空间尾部的索引，兼任2h阶Rank数组

	  // 花O(n)的时间对h = 1进行计数排序
	  for(i = 0; i < 256; i++)
		  B[i] = 0;
	  for(i = 0; i < L; i++)
		  B[szText[i]]++;
	  for(i = 1; i < 256; i++)
		  B[i] += B[i - 1];
	  for(i = 0; i < L; i++)
		  S1[--B[szText[i]]] = i;

	  // 计算Rank(1)，因为仅仅是1阶的Rank，所有有并列的
	  for(R[S1[0]] = 0, i = 1; i < L; i++)
	  {
		  if(szText[S1[i]] == szText[S1[i - 1]])
			  R[S1[i]] = R[S1[i - 1]];
		  else
			  R[S1[i]] = R[S1[i - 1]] + 1;
	  }

	  // log(n)趟O(n)的倍增排序
	  // SA(h) => Rank(h) => SA(2h) => Rank(2h) => ...

	  for(h = 1; h < L && R[S1[L - 1]] < L - 1; h <<= 1)
	  {
		  // 计算Rank(h)相同的后缀形成的h桶尾部的索引
		  // 即有多少个后缀的h前缀相同，它们被放在一个桶中
		  for(i = 0; i < L; i++)
			  B[R[S1[i]]] = i;

		  // 求SA(2h)
		  // 在同一个h桶中，所有的后缀的h前缀肯定相同，
		  // 那么比较他们的2h前缀，只要比较其2h前缀后半的
		  // 长度为h的串即可，而这个串恰恰是后面某个后缀的
		  // h前缀，所以我们逆向遍历有序的SA(h)，
		  // 将S1[i] - h号前缀放到它所在桶的最后一个空位置，
		  // 同时，桶尾前进一个位置，这样即形成了2h桶排序
		  for(i = L - 1; i >= 0; i--)
			  if(h <= S1[i])
				  S2[B[R[S1[i] - h]]--] = S1[i] - h;

		  // 对于长度不超过h的最后几个后缀，由于在h阶段
		  // 它们每个实际上都已经独立分桶了(长度为h的也是)
		  // 而且他们的桶中有且仅有一个元素，
		  // 所以只要直接复制他们h阶段的SA值就可以了
		  // 同时，由于采用滚动数组，所以S2中“残留”了
		  // h/2个有效的数据，所以最终我们只需复制h/2个数据
		  for(i = L - h, h2 = L - (h >> 1); i < h2; i++)
			  S2[B[R[i]]] = i;

		  T = S1; S1 = S2; S2 = T;

		  // 计算Rank(2h)
		  // 2h阶段是否要分桶只需看相邻两个2h前缀前后两半
		  // h前缀是否全部h阶相等
		  for(B[S1[0]] = 0, i = 1; i < L; i++)
		  {
			  // 这里不用考虑S1[i] + h会越界
			  // 如果i达到了S1[i] + h越界的数值，
			  // 那么前面一个条件显然不会满足了
			  // 因为此时i前缀肯定已经独立分桶了
			  if(R[S1[i]] != R[S1[i - 1]] ||
				  R[S1[i] + h] != R[S1[i - 1] + h])
			  {
				  B[S1[i]] = B[S1[i - 1]] + 1;
			  }
			  else
				  B[S1[i]] = B[S1[i - 1]];
		  }

		  T = B; B = R; R = T;
	  }

	  if(*_S != S1)
		  *_S = S1, *_T1 = S2;
	  if(*_R != R)
		  *_R = R, *_T2 = B;
}

void CalculateHeight(uchar* szText,int L, int* S, int* R, int* H, int* T)
{
	int i, j, k;

	for(k = 0, i = 0; i < L; i++)
	{
		if(R[i] == 0)
			H[i] = 0;
		else
		{
			for(j = S[R[i] - 1]; szText[i + k] == szText[j + k]; k++);

			H[R[i]] = k;

			if(k > 0)
				k--;
		}
	}
}

char C[200002];
int  D[4][200001];

int y_main()
{
	int i, l1, l2, b;
	int *S, *R, *H, *T;

	gets(C);
	l1 = (int)strlen(C);
	C[l1] = '$';
	gets((char*)C + l1 + 1);
	l2 = l1 + 1 + (int)strlen(C + l1 + 1);

	S = D[0]; R = D[1];
	H = D[2]; T = D[3];

	CreateSuffixArray((uchar*)C, l2, &S, &R, &H, &T);
	CalculateHeight((uchar*)C, l2, S, R, H, T);

	// 求两个串的最长公共子串，只要让两个串s1、s2
	// 连接在一起形成一个新串，求出新串的SA、Rank和Height
	// 很显然，最长公共子串肯定出现在后缀数组某相邻两项之中
	// 根据Height的定义，扫描一遍Height数组，找相邻两个分别开始于
	// s1和s2串某个位置的后缀，求出所有满足这个条件的最大Height即可

	for(b = 0, i = 1; i < l2; i++)
	{
		if(S[i] < l1 && S[i - 1] > l1 ||
			S[i] > l1 && S[i - 1] < l1)
		{
			if(H[i] > b)
				b = H[i];
		}
	}

	printf("%d/n", b);

	return 0;
}




//后缀数组模板   
#define maxn 5000
#define nMax 5000

int wa[maxn],wb[maxn],wv[maxn],ws[maxn];//这些都是需要用到的中间变量   
int cmp(int *r,int a,int b,int l)  
{  
	return r[a]==r[b]&&r[a+l]==r[b+l];  
}  
void da(int *r,int *sa,int n,int m)  
//这里的n应该是字符串长度 + 1，最后一位为追加的0，我的感觉这里主要是为了方便下面height的求解，如果sa[0]出现在字符中间，则需要进行一些判断，从而增加了代码复杂度   
//r为所求数组，sa为后缀数组   
{  
	int i,j,p,*x=wa,*y=wb,*t;  
	//x首先存储原数组，然后变为rank数组   
	//y对应排序好的第二关键字所在位置   
	for(i=0;i<m;i++) ws[i]=0;//基数排序统计对应元素个数   
	for(i=0;i<n;i++) ws[x[i]=r[i]]++;  
	for(i=1;i<m;i++) ws[i]+=ws[i-1];  
	for(i=n-1;i>=0;i--) sa[--ws[x[i]]]=i;  
	for(j=1,p=1;p<n;j*=2,m=p)//m = p,表示排名的最大值；p == n是终止条件；需要对p进行初始化，最小值需要为1,   
	{  
		//这两步对y进行处理，记录第二关键字所在位置，从小到大   
		for(p=0,i=n-j;i<n;i++) y[p++]=i;  
		for(i=0;i<n;i++) if(sa[i]>=j) y[p++]=sa[i]-j;//因为这里对应的是sa[]，而sa[]中元素为有序已经排序好的，所以直接赋值即可   

		for(i=0;i<n;i++) wv[i]=x[y[i]];//wv[]第二关键字所在位置的排名，“统计元素种类，为了下面ws[]的使用”   
		for(i=0;i<m;i++) ws[i]=0;  
		for(i=0;i<n;i++) ws[wv[i]]++;  
		for(i=1;i<m;i++) ws[i]+=ws[i-1];  
		for(i=n-1;i>=0;i--) sa[--ws[wv[i]]]=y[i];//这里需要使用y[i]来赋值，y[]对应从小到大排序好第二关键字所在位置   

		for(t=x,x=y,y=t,p=1,x[sa[0]]=0,i=1;i<n;i++)  
			//上面的交换算法作用是将x数组所有元素赋值给y，如果直接y = x，则会使x,y指向同一地址   
			x[sa[i]]=cmp(y,sa[i-1],sa[i],j)?p-1:p++;//将sa[]转变为rank，使用x[]来存储。p记录后缀字符串不同的个数，如果等于n则可以结束   
	}  
	return;  
}  


//求height[]，后缀数组的使用   

int rank[nMax], height[nMax];  
int calHeight(int *r, int *sa, int n)//这里的n为实际字符串长度   
{  
	int i, j, k = 0;  
	for(i = 1; i < n; ++ i) rank[sa[i]] = i;    //将sa[]转变为rank[]。这里从1开始即可，因为rank[sa[0]]始终为0   
	for(i = 0; i < n; height[rank[i ++]] = k)//到不了n，也就是说取不到追加的0，这样就可以避免rank[]等于0的情况，从而方便下面rank[i] - 1的运算   
		//h[i] = height[rank[i]]，h[i] >= h[i - 1] - 1。h[i]表示后缀数组suffix(i)与前面相邻后缀数组的最长公共字符串   
		for(k ? k -- : 0, j = sa[rank[i] - 1]; r[i + k] == r[j + k]; k ++);   
	return 1;
}  
