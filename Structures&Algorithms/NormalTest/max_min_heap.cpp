#include "max_min_heap.h"
//最大最小堆 
//

bool level(int i)
{
	double k = log((double)i) / log((double)2);
	if ((int)k % 2 == 0)
	{
		return true;         //最小堆
	}
	return false;           //最大堆
}

void max_varify(element heap[],int n,element item)
{
	int i = n;
	int parent = n / 4;
	while (parent)
	{
		if (heap[parent].key < item.key)
		{
			heap[i] = heap[parent];
			i = parent;
			parent = parent / 4;
		}
		else
		{
			break;
		}
	}
	heap[i] = item;
}

void min_varify(element heap[],int n,element item)
{
	int i = n;
	int parent = n / 4;
	while (parent)
	{
		if (heap[parent].key > item.key)
		{
			heap[i] = heap[parent];
			i = parent;
			parent = parent / 4;
		}
		else
		{
			break;
		}
	}
	heap[i] = item;
}

bool insert_node(element heap[],int* n,element item)
{
	(*n)++;
	if (*n > max_size)
	{
		printf("heap is full\n");
		exit(1);
	}
	if (*n == 1)
	{
		heap[*n] = item;
		return true;
	}
	int parent = *n / 2; //   / 2;
	switch (level(parent))
	{
	case true:
		if (heap[parent].key > item.key)
		{
			heap[*n] = heap[parent];
			min_varify(heap,parent,item);
		} 
		else
		{
			max_varify(heap,parent,item);
		}
		break;
	case false:
		if (heap[parent].key < item.key)
		{
			heap[*n] = heap[parent];
			max_varify(heap,parent,item);
		}
		else
		{
			min_varify(heap,parent,item);
		}
		break;
	}
	return false;
}

class A
{
	double stamp;
	int temp;
	char sa[0];
};

class B
{
	char sc[0];

};

//int main(int argc, char **argv)
//{
//	int m,i;
//	int n = 0;
//	double* type = 0;
//	*type++;
//	i = (char*)type - (char*)0;
//	m = sizeof(int);
//	m = sizeof(A);
//	i = sizeof(B);
//    for (i = 1;i <= 1000;i++)
//    {
//		m = rand() % 1000;
//		element item;
//		item.key = i;
//		insert_node(heap,&n,item);
//    }
//    return 0;
//}