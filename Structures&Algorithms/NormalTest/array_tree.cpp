/* 树状数组 */

/*传统数组(共n个元素)的元素修改和连续元素求和的复杂度分别为O(1)和O(n)。
树状数组通过将线性结构转换成伪树状结构（线性结构只能逐个扫描元素，而树状结构可以实现跳跃式扫描），
使得修改和求和复杂度均为O(lgn)，大大提高了整体效率。
*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

#define  N  10

int array[N];

int lowbit(int bit)
{
	return (bit & (bit ^ (bit - 1)));
}

int array_sum(int end)
{
	int sum = 0;
    while (end > 0)
    {
		sum += array[end];
		end -= lowbit(end);
    }
	return sum;
}

//增加某个元素的大小
void array_plus(int pos,int num)
{
	while (pos <= N)
	{
		array[pos] += num;
		pos += lowbit(pos);
	}
}

void init_array()
{
	int i;
	for (i = 0;i < N;i++)
	{
		array[i] = rand() % 1000;
	}
}

int s_array_sum(int end)
{
	int i,modn,sumi,n = N;
	while (n > 0)
	{
		modn = n % 2;
		n = n / 2;
		sumi++;
	}
	int* sumarray;
	sumarray = (int*)malloc(sizeof(int)*sumi);
	for (i = 0;i < sumi;i++)
	{
		sumarray[sumi] = array_sum(1 << sumi);
	}

	int sum = 0;
	while (end > 0)
	{
		sum += sumarray[end];
		s_array_sum(end);
		end -= lowbit(end);
	}
	return 0;
}

int m3_main(int argc, char **argv)
{
	init_array();
	array_sum(9);
	array_sum(0);
	array_plus(3,10);
	array_plus(4,20);
    return 0;
}