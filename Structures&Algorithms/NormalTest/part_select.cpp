/*         //线性时间选择算法
    //数组分成m段 每段排序取中间值 取得的中间值再排序后取中间值k 比k大方左边 比k小方右边 再对0~k递归分段 最后返回前一段的第k值
   // 最差时间复杂度O（n）                                       */


void my_bubble(int a[],int first,int end)         //冒泡排序 排出所需区间的数组顺序
{
	int i,j;
	for (i = first;i < end; i++)
	{
		for (j = end;j > i;j--)
		{
			if (a[j] < a[j - 1])
			{
				int temp = a[j];
				a[j] = a[j - 1];
				a[j - 1] = temp;
			}
		}
	}
}

int my_partation(int a[],int first,int end,int iter)      //分治法 比 iter 小的放左边 比iter大的放右边
{
	while (1)
	{
		while (a[first] < iter && end > first)
		{
			first += 1;
		}
		while (a[end] > iter && end > first)
		{
			end -= 1;
		}
		if (end <= first)
		{
			break;
		}
		int temp = a[first];
		a[first] = a[end];
		a[end] = temp;
	}
	return a[end];
}

int select_k(int a[],int first,int end,int k)
{
	int kit;
	if (k % 2 == 0)
	{
		kit = k + 1;
	}
	else
	{
		kit = k + 2;
	}
	if ((end - first) <= kit)
	{
		my_bubble(a,first,end);
		return a[first + k - 1];
	}
	int i;
	for (i = 0;i < (end - first) / kit;i++)
	{
		my_bubble(a,(i * kit),(i * kit + kit - 1));
	}
	for (i = 0;i < (end - first) / kit;i++)
	{
		int temp = a[i];
		a[i] = a[i * kit + kit / 2];                  //每次取出中间值 放在前边
		a[i * kit + kit / 2] = temp;
	}
	my_bubble(a,0,((end - first) / kit));
	int tp = my_partation(a,first,end,a[((0 + end - first) / kit) / 2]);      //在中间值里选出中间值 比该值小放左边 比该值大放右边 在对左边的递归 
	return select_k(a,first,tp,k);
}

int p_main(int argc, char **argv)
{
	int parray[11] = {2,4,6,3,55,33,0,22,66,32,25};
	int t = select_k(parray,0,10,9);
	return 0;
}
