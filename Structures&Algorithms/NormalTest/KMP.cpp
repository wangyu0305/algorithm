/* KMP算法 */
/*在匹配过程称，若发生不匹配的情况，如果next[j]>=0，则目标串的指针i不变，将模式串的指针j移动到next[j]的位置继续进行匹配；若next[j]=-1，则将i右移1位，并将j置0，继续进行比较。*/
#include <string.h>

void get_next(char* p,int* next)
{
	size_t j;
	int k;
	j = 0;
	k = -1;
	next[0] = -1;
    while (j < strlen(p) - 1)
    {
		if (k == -1 || p[k] == p[j])
		{
			j++;
			k++;
			next[j] = k;
		}
		else
		{
			k = next[k];
		}
    }
}

int KMP_match(char* s,char* p)
{
	size_t i;
	int j;
	int next[50];
	memset(next,-1,sizeof(next));
	i = 0;
	j = 0;
	get_next(p,next);
	while (i < strlen(s))
	{
		if (j == -1 || s[i] == p[j])
		{
			j++;
			i++;
		}
		else
		{
			j = next[j];
		}
		if (j == strlen(p))
		{
			return (int)(i - j);
		}
	}
	return -1;
}

//int main(int argc, char **argv)
//{
//	char* s = "bdecfbdecfbdecfdeacfhebdecfdebdecfbdecfhbacebdecfbdecfhbacebdecfh";
//	char* p = "bdecfbdecfhbacebdecfh";
//	int k = KMP_match(s,p);
//	return k;
//}