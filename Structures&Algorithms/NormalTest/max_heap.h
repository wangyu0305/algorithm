#include <stdlib.h>
#include <iostream>

using namespace std;

template <typename T>
class Heap
{
public:
	Heap(int size = 10)
	{
		current_size = 0;
        max_size = size;
		heap_array = new T[max_size];
	};
	~Heap()
	{
		if (heap_array)
		{
			delete[] heap_array;
		}
	};
    int get_current_size()
	{
		return current_size;
	};
	int get_left_child(int pos)
	{
		return (2 * pos + 1);
	};
	int get_right_child(int pos)
	{
		return (2 * pos + 2);
	};
	int get_parent(int pos)
	{
		return (pos - 1) / 2;
	};
	void shift_down(int pos)
	{
		int largest = get_left_child(pos);
		while (largest < current_size)
		{
			if (largest < current_size - 1 && heap_array[largest] < heap_array[largest + 1])
			{
				largest++;
			}
			if (heap_array[pos] < heap_array[largest])
			{
				T temp = heap_array[pos];
				heap_array[pos] = heap_array[largest];
				heap_array[largest] = temp;
                pos = largest;
				largest = get_left_child(pos);
			}
			else
			{
				return;
			}
		}

	};
	void shift_up(int pos)
	{
		int parent = get_parent(pos);
		T temp = heap_array[pos];
		while (pos && temp > heap_array[parent])
		{
			heap_array[pos] = heap_array[parent];
			pos = parent;
			parent = get_parent(pos);
		}
		heap_array[pos] = temp;
	};
	T& max_heap()
	{
		if (current_size == 0)
		{
			cout<<"heap is empty!\n";
			exit(1);
		}
		T temp = heap_array[0];
		heap_array[0] = heap_array[--current_size];
		heap_array[current_size] = heap_array[0];
		if (current_size > 1)
		{
			shift_down(0);
		}
		return heap_array[current_size];    /*return heap_array[0];*/
	};
	bool insert_heap(const T& new_heap)
	{
		if (current_size == max_size)
		{
			cout<<"heap is full";
			return false;
		}
		heap_array[current_size] = new_heap;
		shift_up(current_size);
		current_size++;
		return true;
	};
	bool remove_heap(int pos,T& node)
	{
		if (pos < 0 || pos > current_size)
		{
			cout<<"position is error!\n";
			return false;
		}
		node = heap_array[pos];
		heap_array[pos] = heap_array[--current_size];
		if (heap_array[pos] > heap_array[get_parent(pos)])
		{
			shift_up(pos);
		}
		else
		{
			shift_down(pos);
		}
		return true;
	};
	int find_heap(const T& key)
	{
		int pos = 0;
		if (current_size < 1 || key > heap_array[pos])
		{
			cout<<"not found largger than this key\n";
			return -1;
		}
		while (pos < current_size)
		{
			if (key == heap_array[pos])
			{
				return pos;
			}
			pos++;
		}
        return -1;
	};
	void print_heap()
	{
		int i;
		for (i = 0;i < current_size;i++)
		{
        
			cout<<heap_array[i]<<"\t";

		}
		cout<<endl;
	};

private:
	int current_size;
	int max_size;
	T* heap_array;
};

