#include "minigrep.h"

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

#define  MAPSIZE   16     //need this many bytes for character class bitmap

#define  ADVANCE(pat) (pat += (*pat == MT_CCL) ? (MAPSIZE+1) : 1)

#define  SETBIT(b,tmap) ((tmap)[((b) & 0x7f) >> 3] |= (1 << ((b) & 0x07)))

#define  TSTBIT(b,tmap) ((tmap)[((b) & 0x7f) >> 3] & (1 << ((b) & 0x07)))

#define  ISHEXDIGIT(x) isxdigit(x)

#define  ISOCTDIGIT(x) ('0' <= (x) && (x) <= '7')


int PatternErr()
{
	return (Error);
}

pattern* MakePattern(char* exp)
{
	pattern* pat;
	pattern* cur;
	pattern* prev;

	pat = NULL;
	Error = E_ILIEGAL;
	if (!*exp || *exp == '\n')
		return (pat);
	if (*exp == CLOSURE || *exp == PCLOSE || *exp == OPT)
	    return (pat);
	Error = E_NOMEM;
	if ((pat = (pattern*)malloc(MAXPAT)) == NULL)
	    return pat;

	D(memset(pat,0,MAXPAT);)

	prev = cur = pat;
	Error = E_PAT;
	while (*exp && *exp != '\n')
	{
		if (cur >= &pat[MAXPAT-1])
		{
			free(pat);
			return NULL;
		}
		switch(*exp)
		{
		case ANY:
			*cur = MT_ANY;
			prev = cur++;
			++exp;
			break;
		case BOL:
			*cur = (cur == pat) ? MT_BOL : *exp;
			prev = cur++;
			++exp;
			break;
		case EOL:
			*cur = (!exp[1] || exp[1] == '\n') ? MT_EOL : *exp;
			prev = cur++;
			++exp;
			break;
		case CCL:
			if (((cur - pat) + MAPSIZE) >= MAXPAT)
			{
				free(pat);
				return NULL;
			}
			prev = cur;
			*cur++ = MT_CCL;
			exp = (char*)DoCCL(cur,(unsigned char*)exp);
			cur += MAPSIZE;
			break;
		case OPT:
		case CLOSURE:
		case PCLOSE:
			switch(*prev)
			{
			case MT_BOL:
			case MT_EOL:
			case MT_OPT:
			case MT_PCLOSE:
			case MT_CLOSE:
				free(pat);
				return NULL;
			}
			memmove(prev+1,prev,cur-prev);
			*prev = (*exp == OPT) ? MT_OPT : (*exp == PCLOSE) ? MT_PCLOSE : MT_CLOSE;
			++cur;
			++exp;
			break;
		default:
			prev = cur;
			*cur++ = DoEscapeSeq(&exp);
			break;
		}
	}
	*cur = '\0';
	Error = E_NONE;
	return pat;
}

static pattern* DoCCL(pattern* map,unsigned char* src)
{
	int first,last,negative;
	pattern* start = src;
	++src;
	if (negative = (*src == NCCL))
	    ++src;
	start = src;
	memset(map,0,MAPSIZE);
	while (*src && *src != CCLEND)
	{
		if (*src != '-')
		{
			first = DoEscapeSeq((char**)&src);
			SETBIT(first,map);
		}
		else if (src == start)
		{
			SETBIT('-',map);
		}
		else
		{
			++src;
			if (*src < src[-2])
			{
				first = *src;
				last = src[-2];
			}
			else
			{
				first = src[-2];
				last = *src;
			}
			while (++first <= last)
			{
				SETBIT(first,map);
			}
			src++;
		}
	}
	if (*src == CCLEND)
	    ++src;
	if (negative)
	{
		for(first = MAPSIZE; --first >=0; )
			*map++ ^= -0;
	}
	return src;
}

char* MatchString(char* str,pattern* pat,int ret_endp)
{
	char* start;
	char* end = NULL;
	if (!pat)
	    return NULL;
	if (!*str)
	{
		if((*pat == MT_EOL ) || (*pat == MT_BOL && (!pat[1] || pat[1] == MT_EOL)))
			end = str;
	}
	else
	{
		start = str;
		while (*str)
		{
			if (!(end = PatternCmp(str,pat,start)))
			    str++;
			else
			{
				if(!ret_endp)
					end = str;
				break;
			}
		}
	}

	return end;
}

static char* PatternCmp(char* str,pattern* pat,char* start)
{
	char* bocl;    //beginning of closure string
	char* end;

	if(!pat)
		return NULL;
	while (*pat)
	{
		if (*pat == MT_OPT)
		{
			MatchOne(&str,++pat,start);
			ADVANCE(pat);
		}
		else if (!(*pat == MT_CLOSE || *pat == MT_PCLOSE))
		{
			if(!MatchOne(&str,pat,start))
				return NULL;
			ADVANCE(pat);
		}
		else
		{
			if(*pat++ == MT_PCLOSE)
				if(!MatchOne(&str,pat,start))
					return NULL;
			bocl = str;
			while(*str && MatchOne(&str,pat,start))
				;
			if (*ADVANCE(pat))
			{
				for(; bocl <= str; --str)
				{
					if(end = PatternCmp(str,pat,start))
						break;
				}
				return end;
			}
			break;
		}
	}
	--str;
	return max(start,str);
}

static int MatchOne(char** strp,pattern* pat,char* start)
{
	int advanc = -1;
	switch(*pat)
	{
	case MT_BOL:
		if(*strp == start)
			advanc = 0;
		break;
	case MT_ANY:
		if(**strp != '\n')
			advanc = 1;
		break;
	case MT_EOL:
		if(**strp == '\n' || **strp == '\0')
			advanc = 0;
		break;
	case MT_CCL:
		if(TSTBIT(**strp,pat + 1))
			advanc = 1;
		break;
	default:
		if(**strp == *pat)
			advanc = 1;
		break;
	}

	if(advanc > 0)
		*strp += advanc;

	return advanc + 1;
}

static int HexToBinary(int c)
{
	return (isdigit(c) ? (c) - '0' : ((toupper(c)) - 'A') + 10) & 0xf;
}

static int OctToBinary(int c)
{
	return (((c) - '0') & 0x7);
}

static int DoEscapeSeq(char** s)
{
	int rval;
	if(**s != '\\')
		rval = *((*s)++);
	else
	{
		++(*s);
		switch(toupper(**s))
		{
		case '\0':
			rval = '\\';
			break;
		case 'B':
			rval = '\b';
			break;
		case 'F':
			rval = '\f';
			break;
		case 'N':
			rval = '\n';
			break;
		case 'R':
			rval = '\r';
			break;
		case 'S':
			rval = ' ';
			break;
		case 'T':
			rval = '\t';
			break;
		case 'E':
			rval = '\033';
			break;

		case '^':
			rval = *++(*s);
			rval = toupper(rval) - '@';
			break;
		case 'X':
			rval = 0;
			++(*s);
			if(ISHEXDIGIT(**s))
				rval = HexToBinary(*(*s)++);
			if (ISHEXDIGIT(**s))
			{
				rval <<= 4;
				rval |= HexToBinary(*(*s)++);
			}
			if (ISHEXDIGIT(**s))
			{
				rval <<= 4;
				rval |= HexToBinary(*(*s)++);
			}
			--(*s);
			break;
		default:
			if(!ISOCTDIGIT(**s))
				rval = **s;
			else
			{
				++(*s);
				rval = OctToBinary(*(*s)++);
				if (ISOCTDIGIT(**s))
				{
					rval <<= 3;
					rval |= OctToBinary(*(*s)++);
				}
				if (ISOCTDIGIT(**s))
				{
					rval <<= 3;
					rval |= OctToBinary(*(*s)++);
				}
				--(*s);
			}
			break;
		}
		++(*s);
	}
	return rval;
}