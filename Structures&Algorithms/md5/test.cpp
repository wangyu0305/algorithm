#include <iostream>
#include <fstream>
#include <ostream>
#include <iomanip>

//#include "libChooseFathrGrid.h"

using namespace std;

int GetDataFromFile(const char* fileName, float* &pData);

int TransDataToMatrix(float* &pData, float** &ppDataMatrix, int nRow, int nCol);

int WriteDataToFile(const char* fileName, float* &pData);

const int yr=38;
const int np_chn=545;
const int lat1=73;
const int lon1=144;
const int nf=3;
const int np=20;

int m_main()
{
        float *cor_ci = NULL;

        int nResult = -1;
        nResult = GetDataFromFile("..\\data\\cor_ci_gho500_may.dat", cor_ci);
        if ( nResult != 0 )
        {
                cout << "Read cor_ci_gho500_may.dat failed!" << endl;
                exit(1);
        }        
        
        float **dataCorCiMay;
        dataCorCiMay = new float*[lat1*lon1];
        for ( int i = 0; i != lat1*lon1; ++i )
        {
                dataCorCiMay = new float[np_chn];
        }

        nResult = TransDataToMatrix(cor_ci, dataCorCiMay, lat1*lon1, np_chn);
        if ( nResult != 0 )
        {
                cout << "Read data file failed!" << endl;
                exit(1);
        }

        if ( NULL != cor_ci )
        {
                delete [] cor_ci;
                cor_ci = NULL;
        }
}

int GetDataFromFile(const char* fileName, float* &pData)
{
        FILE* pFile = fopen(fileName,"rb");
        if (NULL == pFile)
        {
                cout << "Open " << fileName << " failed!" << endl;
                return 1;
        }

        int nOrg = -1;
        int nEnd = -1;
        int nFileLen = -1;

        fseek(pFile, 0, SEEK_SET);
        nOrg = ftell(pFile);
        fseek(pFile, 0, SEEK_END);
        nEnd = ftell(pFile);
        fseek(pFile, 0, SEEK_SET);
        nFileLen = nEnd - nOrg;

        pData = new float[nFileLen];
        fread(pData, nFileLen, 1, pFile);
        fclose(pFile);

        return 0;
}

int TransDataToMatrix(float* &pData, float** dataMatrix, int nRow, int nCol)
{
        for ( int c = 0; c < nCol; ++c )
        {
                for ( int r = 0; r < nRow; ++r )
                {
                        dataMatrix[r][c] = *(pData++);
                }
        }

        return 0;
}

int WriteDataToFile(const char* fileName, float* &pData)
{
        //
} 
