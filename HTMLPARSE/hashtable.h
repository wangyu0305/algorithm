#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct node 
{
	int level;
	int tag;      //类型  类型<head>下的level 区别<body>下的level
	char* str;
	struct node* next;
}node;

typedef struct hashtable 
{
	node* nodelevel[20];
}hashtable;

hashtable* create_table();

node* print_node(hashtable* table,int level);

bool insert_node(hashtable* table,int level,int tag,char* str);

bool delete_table(hashtable* table);