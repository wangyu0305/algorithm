#include "parse.h"
#include "stack.h"

int parse()
{
	char ch;
	char nh;
	char tag[20];
	tag[0] = '\0';
//	memset(tag,0,20);
	char line[line_file_size];
	char attr[file_increment];
	char content[init_file_size];
	content[0] = '\0';
	//memset(content,0,10000);
	char attr_content[init_file_size];
	attr_content[0] = '\0';
	char* zw = new char;
	FILE* pf;
	int head = 0;
	int now;
	int level = 0;
	int n;
	int m;
	int l;
	int i = 0;

	stackstruct* stack;
	stack = init_stack();

	pf = fopen("1.html","rb");
	do 
	{
		now = 0;
		fseek(pf,head,SEEK_SET);
		fread(zw,sizeof(char),1,pf);
		ch = *zw;
		now += 1;

		if (ch == '<')
		{
			fseek(pf,0,SEEK_CUR);
			fread(zw,sizeof(char),1,pf);
			nh = *zw;
			now += 1;
			tag[0] = '<';

			if (((nh >= 'a') && (nh <= 'z')) || ((nh >= 'A') && (nh <= 'Z')))
			{
				n = 1;
				while (nh != ' ' && nh != '>')
				{
					tag[n] = nh;
					fseek(pf,0,SEEK_CUR);
					fread(zw,sizeof(char),1,pf);
					nh = *zw;
					n += 1;
                    now += 1;
				}
                
				if (nh == '>')
				{
					bool success;
					tag[n] = '>';
					tag[n + 1] = '\0';
					success = push_stack(stack,tag);
                    if (success == false)
                    {
						return 1;
                    }
					head += now;
					level += 1;
					continue;
				}
				else
				{
					bool success;
				    tag[n] = '\0';
					success = push_stack(stack,tag);
					if (success == false)
					{
						return 1;
					}
				}

                m = 0;
				fseek(pf,0,SEEK_CUR);
				fread(zw,sizeof(char),1,pf);
				nh = *zw;
				now += 1;

				while (nh != '>')
				{
					attr[m] = nh;
					fseek(pf,0,SEEK_CUR);
					fread(zw,sizeof(char),1,pf);
					nh = *zw;
					m += 1;
					now += 1;
				}
				
				attr[m] = '\0';
				if (attr[m - 1] == '>' && attr[m - 2] == '/')
				{
					bool success = pop_stack(stack);
					if (success == false)
					{
						return false;
					}
				}
				head += now;
				level += 1;
				if (attr_content == NULL)
				{
					strcpy_s(attr_content,init_file_size,attr);
					continue;
				}
				else
				{
                    strcat_s(attr_content,init_file_size,attr);               //需要改成属性存储结构
					continue;
				}

				//l = 0;
				//while (nh != '<')
				//{
				//	line[l] = nh;
				//	fseek(pf,0,SEEK_CUR);
    //                nh = fgetc(pf);
				//	l += 1;
				//	now += 1;
				//}
				//line[l] = '\0';
				//if (content == NULL)
				//{
				//	strcpy_s(content,init_file_size,line);
				//}
				//else
				//{
    //                strcat_s(content,init_file_size,line);            //需要改成文本内容存储结构
				//}
				
			}

			if (nh == '/')
			{
				n = 1;
				while (nh != '>')
				{
					
					fseek(pf,0,SEEK_CUR);
					fread(zw,sizeof(char),1,pf);
					nh = *zw;
					tag[n] = nh;
					n += 1;
					now += 1;
				}

			    tag[n] = '\0';
                if (NULL != find_stack(stack,tag))    //栈中查找元素
                {
					level -= 1;
                }
                                     
				head += now;
				
				continue;
				
			}
            
			if (nh == '!')
			{
				while (nh != '>')
				{
					fseek(pf,0,SEEK_CUR);
					fread(zw,sizeof(char),1,pf);
					nh = *zw;
					now += 1;
				}
				head += now;
				continue;
			}

			while (nh != '>')
			{
				fseek(pf,0,SEEK_CUR);
				fread(zw,sizeof(char),1,pf);
				nh = *zw;
				now += 1;
			}
			head += now;

			level += 1;
		}
		else
		{

			//if (ch == '\r')
			//{
			//	fread(zw,sizeof(char),1,pf);
			//	nh = *zw;
			//	if (nh == '\n')
			//	{
			//		if (feof(pf) == 0)
			//		{
			//			break;
			//		}
			//	}
			//}
			nh = ch;
			l = 0;
			while (nh != '<')
			{
				line[l] = nh;
				fseek(pf,0,SEEK_CUR);
				fread(zw,sizeof(char),1,pf);
				nh = *zw;
				l += 1;
				now += 1;
			}
			line[l] = '\0';

			if (line[0] != '\n')
			{
				strcat_s(content,init_file_size,line);             //对于<AAA>xxx</AAA>之间的文本xxx，这里将作为TextTag来处理  需要存储一下
			}
			
			if (nh == '<')
			{
				now -= 1;
			}
            head += now;
		}
		show_stack(stack);
		printf("tag is :%s\n",tag);
		
	} while (ch != EOF/*feof(pf) != 0*/);            
    
	fclose(pf);
	delete_stack(stack);
	return 0;

}







//char** get_file(FILE* pf)
//{
//	char* str_file[];
//
//	char* str_line;
//
//	str_file = (char*)malloc(init_file_size * sizeof(char));
//
//	str_line = (char*)malloc(line_file_size * sizeof(char));
//
//	char ch;
//
//	int i = 0;
//
//	int n = 0;
// 
//	int file_size = init_file_size;
//
//	int line_size = line_file_size;
// 
//	pf = fopen("1.html","r");
//
//	if (f == NULL)
//	{
//		return NULL;
//	}
//	do 
//	{
//
//		do 
//		{
//			ch = fgetc(pf);
//			if (i >= line_size - 1)
//			{
//				str_line = (char*)realloc(str_line,(line_size + file_increment) * sizeof(char));
//
//				line_size += file_increment;
//			}
//			strcat(str_line,&ch);
//
//			i += 1;
//		} while (ch != '\n');
//
//		if (i >= file_size - 1)
//		{
//			str_file = (char*)realloc(str_file,(file_size + line_file_size) * sizeof(char));
//
//			file_size += line_file_size;
//		}
//		
//        str_file[n] = str_line;
//
//		n += 1;
//
//	} while (ch != EOF);
//	
//    return str_file;
//}






