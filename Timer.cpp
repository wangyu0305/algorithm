#include "Timer.h"

thread_c::thread_c():stop_flag(0),m_thread(INVALID_HANDLE_VALUE)
{

}

thread_c::~thread_c()
{
	stop();
}

int thread_c::threadproc(LPVOID p)
{
	thread_c* thread = (thread_c*)p;
	thread->run();
	CloseHandle(thread->m_thread);
	thread->m_thread = INVALID_HANDLE_VALUE;
	return 0;
}

void thread_c::start()
{
	unsigned long* p = NULL;
	m_thread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)threadproc,(void*)this,0,p);
}


void thread_c::stop()
{
	stop_flag = true;
	if (m_thread != INVALID_HANDLE_VALUE)
	{
		if (WaitForSingleObject(m_thread,INFINITE) != WAIT_ABANDONED)
		{
			CloseHandle(m_thread);
		}
		m_thread = INVALID_HANDLE_VALUE;
	}
}

bool thread_c::is_stop()
{
	return stop_flag;
}

