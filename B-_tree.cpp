

#pragma once
#ifndef BINARY_MINUS_TREE
#define BINARY_MINUS_TREE

class CBinaryMinusTreeException
{
public:
	enum Type
	{
		NoEnoughMemory=0,
	};
    
	CBinaryMinusTreeException(Type type)
	{
		m_type = type;
	}

private:
	Type m_type;
};

template<typename TK,typename TV,size_t THD>class CBinaryMinusTreeNode;

template<typename TK,typename TV,size_t THD>class CBinaryMinusTree;

template<typename TK,typename TV,size_t THD=10>
class BinaryMinusTreeKey
{
public:
    friend CBinaryMinusTreeNode<TK,TV,THD>;
	friend CBinaryMinusTree<TK,TV,THD>;
private:
	TK Key; //
	TV* pVal; //
	
public:	
	BinaryMinusTreeKey():Key(TK()),pVal(NULL)
	{}
   	
	~BinaryMinusTreeKey()
	{
	   if(pVal)
	   {
		   delete pVal;
		   pVal=NULL;
	   }
	}
    
	TV GetValue()
	{
		if(pVal)return *pVal;
		else return TV();
	}

};


template<typename TK,typename TV, size_t THD =10>
class CBinaryMinusTreeNode
{
public:
    typedef CBinaryMinusTreeNode BMTN;
	typedef CBinaryMinusTreeNode* LPBMTN;
	typedef BinaryMinusTreeKey<TK,TV,THD> BMKEY;
	static const size_t Degree = 2* THD +1 ;
    static const size_t Rank = THD;
	friend CBinaryMinusTree<TK,TV,THD>;
public:
	size_t Count;
	LPBMTN Parent; //ָ�򸸽ڵ�
	BMKEY  Keys[Degree]; 
    LPBMTN Nodes[Degree+1]; 
    
public:
	CBinaryMinusTreeNode():Count(0),Parent(NULL)
	{
		for(size_t i=0;i<=Degree;i++)
		{
			Nodes[i]=NULL;
		}
	}

private: 
	bool IsRoot();
	bool IsLeaf();
	bool Search(TK key,size_t &index);
	LPBMTN Splite(BMKEY &key);
	size_t Add(BMKEY &key);
	bool RemoveAt(size_t index);
	void Print();
};


template<typename TK,typename TV, size_t THD =10>
class CBinaryMinusTree
{
public:
	typedef  CBinaryMinusTreeNode<TK,TV,THD> BMTN;
    typedef  CBinaryMinusTreeNode<TK,TV,THD>* LPBMTN;
	typedef  BinaryMinusTreeKey<TK,TV,THD> BMKEY;
	
	struct FindNodeParam
	{
		LPBMTN pNode;
		size_t index;
		bool flag;

	};
	
	static const size_t Degree = 2* THD +1 ;
    static const size_t Rank = THD;
public:
	LPBMTN Root;
	size_t KeyCount;
	size_t Level;
	size_t NodeCount;

public:
    CBinaryMinusTree():Root(NULL),KeyCount(0),Level(0),NodeCount(0)
	{	
	}
    ~CBinaryMinusTree()
	{
		Clear();
	}
    
	bool Add(TK key,TV val);
	bool Remove(TK key);
    void Print();
	void Clear();
private:
	void InitTree();
	bool AdjustAfterAdd(LPBMTN pNode);
	bool AdjustAfterRemove(LPBMTN pNode);
    void RemoveNode(LPBMTN pNode);
	void Search(LPBMTN pNode,TK &key,FindNodeParam & fnp);
	void PrintNode(LPBMTN pNode);
};

#include "BinaryMinusTree.inc"

#endif


template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::Search(TK key,size_t &index)
{
	bool ret =false;

	if(Count==0)
	{
		index=0;
		return false;
	}

    if(Keys[0].Key > key)
    {
       index =0;
       return false;
    }
    if(Keys[Count-1].Key < key)
	{
		index = Count; 
		return false;
	}
	
	
	if(Count > 20)
	{
	    size_t m,s,e;
    
		s=0;e=Count-1;
	    while(s<e)
		{
			m=(s+e)/2;
			
			if(key == Keys[m].Key)
			{
			    index = m;
				return true;    
			}
			else if(key < Keys[m].Key)   
			{   
				e=m;
			}   
			else // key > Keys[m].Key
			{   
				s=m;
			}

			if(s==e-1)
			{
			    if(key == Keys[s].Key)
				{
					index = s;
					return true;  
				}
				
				if(key == Keys[e].Key)
				{
					index = e;
					return true;  
				}
				
				if(key < Keys[s].Key)
				{   
					index=s;
					return false;
				}
				
				if(key < Keys[e].Key)
				{   
					index=e;
					return false;
				}
				
				if(key > Keys[e].Key)
				{   
					index=e+1;
					return false;
				}
				
			}
		};
		return false;
	}
	else
	{
		
		for(size_t i=0;i<Count;i++)
		{
			if(key<Keys[i].Key)
			{
				index = i;
				ret = false;
				break;
			}
			else if(key == Keys[i].Key)
			{
				index = i;
				ret = true;
				break;
			}
			
		}
		return ret;
	}
}

template<typename TK,typename TV,size_t THD>
CBinaryMinusTreeNode<TK,TV,THD>* CBinaryMinusTreeNode<TK,TV,THD>::Splite(BMKEY &key)
{
    LPBMTN newNode = new BMTN();
	if(newNode == NULL)
	{
		throw CBinaryMinusTreeException(CBinaryMinusTreeException::Type::NoEnoughMemory);
		return NULL;
	}
		
    for(size_t i = Rank+1; i< Degree; i++)
	{
		newNode->Keys[i-Rank-1] = Keys[i];
	}
	for(size_t i = Rank+1; i<= Degree;i++)
	{
		newNode->Nodes[i-Rank-1] = Nodes[i];
	}
	newNode->Parent = Parent; 
    newNode->Count = Rank;

    for(size_t i = 0;i <= newNode->Count;i++)
    {
        LPBMTN childNode= newNode->Nodes[i];
        if(childNode)
        {
			childNode->Parent = newNode;
        }
    }


	key = Keys[Rank];

    for(size_t i = Rank+1; i< Degree+1;i++)
	{
		Nodes[i] = (LPBMTN)NULL; 
	}

	for(size_t i = Rank; i< Degree;i++)
	{
		Keys[i].Key = TK();
		Keys[i].pVal = (TV*)NULL;
	}

	Count = Rank;

	return newNode;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::IsRoot()
{
	if(Parent)return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::IsLeaf()
{
	if(Nodes[0])return false;
	return true;
}

template<typename TK,typename TV,size_t THD>
size_t CBinaryMinusTreeNode<TK,TV,THD>::Add(BMKEY &key)
{
     size_t index=0;
	 if(Count==0) 
     {
		 Keys[0]=key;
		 Count=1;
		 return 0;
	 }
	 
	 if(!Search(key.Key,index))
	 {
         for(size_t i = Count; i > index;i--)
		 {
			 Keys[i] = Keys[i-1];   
		 }
        
		 for(size_t i = Count+1; i > index;i--)
		 {
			 Nodes[i]= Nodes[i-1];
		 }

		 Keys[index]=key;
		 Nodes[index] = NULL;
		 Count++;
	 }
     return index;
}

template<typename TK,typename TV,size_t THD>
bool CBinaryMinusTreeNode<TK,TV,THD>::RemoveAt(size_t index)
{
    if(index>Count-1)
    {
        return false;
    }
    else
	{
	    for(size_t i = index;i< Count-1;i++)
		{
			Keys[i]= Keys[i+1];
		}
		
		Keys[Count-1].Key = TK();
		Keys[Count-1].pVal = NULL;

		for(size_t i = index;i< Count;i++)
		{
			Nodes[i]= Nodes[i+1];
		}
		Nodes[Count] = NULL;
		Count--;
		return true;
	}
}




template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::InitTree()
{
    Root = new BMTN();
	KeyCount =0;
	NodeCount=1;
	Level=1;
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Search(LPBMTN pNode,TK &key,FindNodeParam & fnp)
{
	size_t index=0;
	if(pNode->Search(key,index))
	{
		fnp.flag = true; 
		fnp.index = index;
		fnp.pNode = pNode;
	}
	else
	{
		LPBMTN childNode = pNode->Nodes[index];
        if(childNode==NULL)
		{
			fnp.flag = false; 
			fnp.index = index;
			fnp.pNode = pNode;
		}
		else
		{
			Search(childNode,key,fnp);
        }
	}
}


template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::Add(TK key,TV val)
{
	if(Root==NULL) //
	{
		InitTree();
	}

	FindNodeParam fnp;

	Search(Root,key,fnp);
    if(!fnp.flag)
	{
		BMKEY newKey;
		newKey.Key = key;
		newKey.pVal = new TV();
        *(newKey.pVal) = val;
		
	    fnp.pNode->Add(newKey);
        newKey.pVal =NULL;

		KeyCount++;

		if(fnp.pNode->Count == Degree )
		{
			return AdjustAfterAdd(fnp.pNode);
		}
		return true;
	}
	return false;
}

template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::AdjustAfterAdd(LPBMTN pNode)
{
	
	BMKEY key;
	LPBMTN newNode = pNode->Splite(key); 
	NodeCount++;
    LPBMTN parent = pNode->Parent;

	if(parent==NULL)
	{
		LPBMTN newRoot = new BMTN(); //
		if(newRoot==NULL) return false;//
		
		NodeCount++;
		Root = newRoot;
        Root->Add(key);
		
		key.pVal = NULL; //

		Root->Nodes[0] = pNode;
		Root->Nodes[1]= newNode;
       	
		pNode->Parent = Root;
		newNode->Parent = Root;

		Level++; //
		return true;
	}
	else
	{
			size_t index = parent->Add(key);
			
			key.pVal = NULL;
			
			parent->Nodes[index]= pNode;
            parent->Nodes[index+1]= newNode;

			if(parent->Count == Degree) 
			{
				return AdjustAfterAdd(parent);
			}
			return true;

	}
}


template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::RemoveNode(LPBMTN pNode)
{
	if(!pNode->IsLeaf())
	{
		for(size_t i=0;i <= pNode->Count;i++)
		{
			RemoveNode( pNode->Nodes[i] );
			pNode->Nodes[i] = NULL;
		}
	}
	delete pNode;
	NodeCount--;
	return;
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Clear()
{
	if(Root)
	{
		RemoveNode(Root);
		KeyCount=0;
		Level=0;
		Root = NULL;
	}
}

template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::Remove(TK key)
{
    if(Root==NULL)
		return false;

    FindNodeParam fnp;
	
	Search(Root,key,fnp);
	
	if(fnp.flag)
	{
		LPBMTN pRemoveNode;
		LPBMTN pNode = fnp.pNode;
       
		pNode->Keys[fnp.index].Key=TK();
		delete pNode->Keys[fnp.index].pVal;
		pNode->Keys[fnp.index].pVal=NULL;

		//
		if(pNode->IsLeaf())
		{
			if(pNode->RemoveAt(fnp.index))
			{
				KeyCount--;
			}
			
			pRemoveNode=pNode;
		}
		else  //
		{
			//
			pRemoveNode = pNode->Nodes[fnp.index+1]; 
			if(!pRemoveNode->IsLeaf())
			{
				do
				{
                    pRemoveNode = pRemoveNode->Nodes[0];     
				}while(!pRemoveNode->IsLeaf());
			}
			
			pNode->Keys[fnp.index] = pRemoveNode->Keys[0]; //
            pRemoveNode->RemoveAt(0); //
			KeyCount--;
		}
		return AdjustAfterRemove(pRemoveNode);
    }
    return false; //
}

//
template<typename TK,typename TV, size_t THD>
bool CBinaryMinusTree<TK,TV,THD>::AdjustAfterRemove(LPBMTN pNode)
{
	//
	if( pNode->IsRoot() ) 
	{
		if(pNode->Count==0) 
		{
			Root = pNode->Nodes[0]; //
			if(pNode->Nodes[0]) pNode->Nodes[0]->Parent = NULL; //

			delete pNode;
			NodeCount--;
			Level--;
		}
		return true;
	}

	if(pNode->Count >= Rank)  //
	{
		return true; 
	}


	LPBMTN pBrother,pParent;
    size_t pos;
	
	pParent =pNode->Parent;
	pParent->Search(pNode->Keys[0].Key,pos);
	
	if(pos == pParent->Count)  //
	{
		pBrother = pParent->Nodes[pos-1]; 

		if(pBrother->Count > Rank)  //
		{
			//
			for(size_t i = pNode->Count;i > 0 ; i--)   
			{
                pNode->Keys[i] = pNode->Keys[i-1];
			}
			for(size_t i = pNode->Count+1;i > 0 ; i--)   
			{
                pNode->Nodes[i] = pNode->Nodes[i-1];
			}

            pNode->Keys[0] = pParent->Keys[pos-1]; 
            pParent->Keys[pos-1]= pBrother->Keys[pBrother->Count-1];

			pNode->Nodes[0] = pBrother->Nodes[pBrother->Count];
			pNode->Count++;


			pBrother->Keys[pBrother->Count-1].Key=TK(); 
			pBrother->Keys[pBrother->Count-1].pVal=NULL; 

			if(pBrother->Nodes[pBrother->Count])
			{
				pBrother->Nodes[pBrother->Count]->Parent = pNode;
				pBrother->Nodes[pBrother->Count] = NULL;
			}
			pBrother->Count--;
			return true;
		}
		else
		{
			pBrother->Keys[pBrother->Count]= pParent->Keys[pos-1];
			pBrother->Count++; 

			pParent->RemoveAt(pos-1);
			pParent->Nodes[pos-1] = pBrother; 


			for(size_t i = 0;i<pNode->Count;i++)
			{
				pBrother->Keys[pBrother->Count+i] = pNode->Keys[i];
				pNode->Keys[i].pVal=NULL;
            }

			LPBMTN pChild;
			for(size_t i = 0;i<= pNode->Count;i++)
			{
				pChild = pNode->Nodes[i];
				if(pChild)
				{
					pBrother->Nodes[pBrother->Count+i] = pChild;
					pChild->Parent = pBrother; 
				}
			}
			
			pBrother->Count = 2* Rank;
            
			delete pNode;
			NodeCount--;

			return AdjustAfterRemove(pParent);
		}

	} 
	else
	{
		pBrother = pParent->Nodes[pos+1]; 

		if(pBrother->Count > Rank)
		{
            pNode->Keys[pNode->Count] = pParent->Keys[pos];
			pNode->Nodes[pNode->Count+1] = pBrother->Nodes[0];
			if(pBrother->Nodes[0]) 
				pBrother->Nodes[0]->Parent = pNode;
			pNode->Count++;
			
			pParent->Keys[pos]= pBrother->Keys[0];
            
			pBrother->RemoveAt(0);
			return true;
		}
		else
		{
			pNode->Keys[pNode->Count]= pParent->Keys[pos];
			pParent->RemoveAt(pos);
			pParent->Nodes[pos]= pNode;
            pNode->Count++;

			for(size_t i = 0;i<Rank;i++)
			{
				pNode->Keys[pNode->Count+i] = pBrother->Keys[i];
				pBrother->Keys[i].pVal=NULL;
            }

			LPBMTN pChild; 
			for(size_t i = 0;i<=Rank;i++)
			{
				pChild = pBrother->Nodes[i];
				if(pChild)
				{
					pNode->Nodes[pNode->Count+i] = pChild;
					pChild->Parent = pNode;
				}
			}
            
            pNode->Count = 2* Rank;

			delete pBrother;
			NodeCount--;
			
			return AdjustAfterRemove(pParent);
		}
	}

}


template<typename TK,typename TV,size_t THD>
void CBinaryMinusTreeNode<TK,TV,THD>::Print()
{
    printf("----------------------\n");
	printf("Address: 0x%X Count: %d, Parent: 0x%X \n",(void*)this,Count,(void*)Parent);
	printf("Keys: {");
	for(size_t i =0; i<Degree;i++)
	{
		printf("%5d ",Keys[i].Key);
	}
	printf("}\n");
    
    printf("Vals: {");
	for(size_t i =0; i<Degree;i++)
	{

		Keys[i].GetValue().Print();
	}
	printf("}\n");
   
	printf("Ptrs: {");
	for(size_t i =0; i<=Degree;i++)
	{
		printf("0x%X ",(void*)Nodes[i]);
	}
	printf("}\n");
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::Print()
{
    printf("*****************************************************\n");
    printf("KeyCount: %d, NodeCount: %d, Level: %d, Root: 0x%X \n",KeyCount,NodeCount,Level,(void*)Root );
	
	if(Root)
	{
		PrintNode(Root);
	}
	
	
}

template<typename TK,typename TV, size_t THD>
void CBinaryMinusTree<TK,TV,THD>::PrintNode(LPBMTN pNode)
{
	if(pNode == NULL) return;

    pNode->Print();
	
	for(size_t i =0; i<= pNode->Count;i++)
	{
		PrintNode(pNode->Nodes[i]);
	}
}

