#include<iostream>
using namespace std;

typedef struct BSTNode
{
	char data;
	int bf;
	BSTNode *lchild,*rchild;
}BSTNode,*BSTree;


void R_Rotate(BSTree &p)
{
	BSTree lc;
	lc=p->lchild;
	p->lchild=lc->rchild;
	lc->rchild=p;
	p=lc;
}


void L_Rotate(BSTree &p)
{
	BSTree rc;
	rc=p->rchild;
	p->rchild=rc->lchild;
	rc->lchild=p;
	p=rc;
}




#define LH 1
#define EH 0
#define RH -1


void LeftBalance(BSTree &T)
{
	BSTree lc,rd;
	lc=T->lchild;
	switch(lc->bf)
	{
	case LH:T->bf=lc->bf=EH;R_Rotate(T);break;
	case RH:rd=lc->rchild;
		switch(rd->bf)
		{
		case LH:T->bf=RH;lc->bf=EH;break;
		case EH:T->bf=lc->bf=EH;break;
		case RH:T->bf=EH; lc->bf=EH;break;
		}
		rd->bf=EH;
		L_Rotate(T->lchild);
		R_Rotate(T);
	}
}


void RightBalance(BSTree &T)
{
	BSTree lc,rd;
	lc=T->rchild;
	switch(lc->bf)
	{
	case RH:T->bf=lc->bf=EH;L_Rotate(T);break;
	case LH:rd=lc->lchild;
		switch(rd->bf)
		{
		case LH:T->bf=EH;lc->bf=RH;break;
		case EH:T->bf=lc->bf=EH;break;
		case RH:T->bf=LH; lc->bf=EH;break;
		}
		rd->bf=EH;
		R_Rotate(T->rchild);
		L_Rotate(T);
	}
}


int InsertAVL(BSTree &T,char e,bool &taller)
{
	if(!T){
		T=(BSTree)malloc(sizeof(BSTNode));
		T->lchild=T->rchild=NULL;
		T->bf=EH;
		T->data=e;
		taller=true;
	}
	else
	{
		if(e==T->data)
		{
			taller=false; return 0;
		}


		else if(e<T->data)
		{
			if(!InsertAVL(T->lchild,e,taller)) return 0;
			if(taller)
				switch(T->bf)
			{
				case LH:LeftBalance(T);taller=false;break;
				case EH: T->bf=LH;taller=true;break;
				case RH: T->bf=EH;taller=false;break;
			}
		}


		else
		{
			if(!InsertAVL(T->rchild,e,taller)) return 0;
			if(taller)
				switch(T->bf)
			{
				case LH:T->bf=EH; taller=false;break;
				case EH:T->bf=RH;taller=true;break;
				case RH:RightBalance(T);taller=true;break;
			}
		}

	}
	return 1;
}


int Print(BSTree T)
{
	if(!T) return 1;
	Print(T->lchild);
	cout<<T->data<<",";
	Print(T->rchild);
	return 0;
}

int PrintTree(BSTree &T,int layer)
{
	if(!T) return 1;

	for(int i=0;i<layer;i++)
		cout<<" ";
	cout<<T->data;
	layer++;
	for(int i=layer;i<20;i++)
		cout<<"#";
	cout<<endl;
	PrintTree(T->lchild,layer);
	PrintTree(T->rchild,layer);
	return 0;
}


int SearchAVL(BSTree &T,char e)
{
	if(!T) {cout<<e<<"���ڸ�ƽ���������!"<<endl<<endl;return 0;}
	if(T){
		if(T->data==e) {cout<<e<<"��ƽ���������."<<endl<<endl;return 0;}
		else if(T->data>e) SearchAVL(T->lchild,e);
		else SearchAVL(T->rchild,e);
	}

}


int Delete(BSTree &T,char e,bool &shorter)
{
	BSTree p,q;
	e=0;
	p=T;
	if(!T->rchild)
	{
		T=T->lchild;
		free(p);
		shorter=true;
	}
	else if(!T->lchild)
	{
		T=T->rchild;
		free(p);
		shorter=true;
	}
	else
	{
		q=T->lchild;
		// Push(S,p);
		while(q->rchild!=NULL)
		{
			//Push(S,q->rchild);
			q=q->rchild;
		}
		e=q->data;
	}
	return e;
}


void Delete_Leftbalance(BSTree &T,bool &shorter)
{
	BSTree rc=T->rchild,ld;

	switch(rc->bf)
	{
	case LH:
		ld=rc->lchild;
		rc->lchild=ld->rchild;
		ld->rchild=rc;
		T->rchild=rc->lchild;
		rc->lchild=T;
		switch(ld->bf)
		{
		case LH:T->bf=EH;
			rc->bf=RH;
			break;
		case EH:T->bf=rc->bf=EH;
			break;
		case RH:T->bf=LH;
			rc->bf=EH;
			break;
		}
		ld->bf=EH;
		T=rc;
		shorter=true;break;
	case EH:
		T->rchild=rc->lchild;
		rc->lchild=T;
		rc->bf=LH;
		T->bf=RH;
		T=rc;
		shorter=EH;break;
	case RH:
		T->rchild=rc->lchild;
		rc->lchild=T;
		rc->bf=T->bf=EH;
		T=rc;
		shorter=true;break;
	}
}



void Delete_Rightbalance(BSTree &T,bool &shorter)
{
	BSTree p1,p2;
	p1=T->lchild;

	switch(p1->bf)
	{
	case LH:T->lchild=p1->rchild;
		p1->rchild=T;
		p1->bf=T->bf=EH;
		T=p1;
		shorter=true;
		break;
	case EH:T->lchild=p1->rchild;
		p1->rchild=T;
		p1->bf=RH;
		T->bf=LH;
		T=p1;
		shorter=false;
		break;
	case RH:p2=p1->rchild;
		p1->rchild=p2->lchild;
		p2->lchild=p1;
		T->lchild=p2->rchild;
		p2->rchild=T;
		switch(p2->bf)
		{
		case LH:T->bf=RH;p1->bf=EH;break;
		case EH:T->bf=EH;p1->bf=EH;break;
		case RH:T->bf=EH;p1->bf=LH;break;
		}
		p2->bf=EH;
		T=p2;
		shorter=true;break;
	}
}


BSTree DeleteAVL(BSTree &T,char e,bool &shorter)
{
	int judge;
	BSTree q;

	if(!T) return NULL;
	else
	{
		if(e==T->data)
		{
			judge=Delete(T,e,shorter);

			if(judge!=0)
			{
				q=T;
				DeleteAVL(T,judge,shorter);
				q->data=judge;
			}
		}
		else {
			if(e<T->data)
			{
				DeleteAVL(T->lchild,e,shorter);

				if(shorter)
				{
					switch(T->bf)
					{
					case LH:T->bf=EH;shorter=true;break;
					case EH:T->bf=RH;shorter=false;break;
					case RH:Delete_Leftbalance(T,shorter);shorter=true;break;
					}
				}
			}

			else
			{
				DeleteAVL(T->rchild,e,shorter);

				if(shorter)
					switch(T->bf)
				{
					case LH:Delete_Rightbalance(T,shorter);shorter=true;break;
					case EH:T->bf=LH;shorter=false;break;
					case RH:T->bf=EH;shorter=true;break;
				}
			}
		}
	}
	return T;
}



int b_main()
{
	BSTree T=NULL;
	char e,judge='y',choice,out;
	bool taller=false;
	//SqStack S;
	//InitStack(S);

	do{
		judge='y';
		cout<<endl<<endl;

		cout<<" ***************************************"<<endl;
		cout<<" * *"<<endl;
		cout<<" * *"<<endl;
		cout<<" *  *"<<endl;
		cout<<" * *"<<endl;
		cout<<" *"<<endl;
		cout<<" ***************************************"<<endl;

		do{
			cout<<"";
			cin>>choice;
		}while(choice!='s'&&choice!='i'&&choice!='d'&&choice!='q');

		switch(choice)
		{
		case 'i':e='@';
			cout<<""<<endl;
			cout<<endl<<"�";
			while(e!='#')
			{
				cin>>e;
				if(e!='#')
					InsertAVL(T,e,taller);

				//
			}
			break;

		case 's':while(judge=='y')
				 {

					 if(T)
					 {
						 cout<<endl<<":";
						 cin>>e;

						 SearchAVL(T,e);
						 cout<<"(y/n)?";
						 cin>>judge;
					 }
					 else {cout<<"!"<<endl;break;}
				 }
				 break;

		case 'd':while(judge=='y')
				 {
					 if(!T) {cout<<""<<endl<<endl;break;}

					 cout<<endl<<"";
					 cin>>e;
					 DeleteAVL(T,e,taller);
					 cout<<endl<<endl<<""<<endl;
					 PrintTree(T,1);

					 cout<<endl<<"(y/n)?";
					 cin>>judge;
				 }
				 break;

		case 'q':out='y';cout<<endl<<""<<endl<<""<<endl;;break;
		}

	}while(out!='y');

	return 0;
}

