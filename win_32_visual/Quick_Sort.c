/*#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int x[100];

void swap(int ai,int bi)
{
	int temp;
	if(x[ai] <= x[bi])
		return ;
	else {
		temp = x[ai];
		x[ai] = x[bi];
		x[bi] = temp;
	}
}

int randint(int s,int e)
{
	printf("s:%d\n",s);
	printf("e:%d\n",e);
	assert(s <= e);
	if((e - s) == 1){
		swap(s,e);
		return (-1);
	}
	return rand()%(e - s);
}

void quicksort(int l,int u)
{
	int i,m,temp,j;
	if(l >= u) return ;
	printf("l x[%d] %d\n",l,x[l]);
	printf("u x[%d] %d\n",u,x[u]);
	temp = randint(l,u);
	if(temp == -1)
		return ;
    swap(l,temp);
	m = temp;
	printf("m:%d\n",m);
	for(i = l+1;i < m; i++){
		if(x[i] > x[m])
			swap(i,m);
	}
	for(j = m + 1;j <= u;j++){
		if(x[j] < x[m])
	        swap(m,j);
	}
	quicksort(l,m-1);
	printf("\n x[0]\n %d",x[0]);
	quicksort(m+1,u);
	printf("\n x[0]\n %d",x[0]);
}

int main(int argc,char *argv[])
{
	int i = 0,m,j,n;
	int *pi;
	char a;
	printf("input number of data : exp 2 5 6 8 enter\n");
    while(scanf("%d",&x[i++]),(a = getchar()) != '\n')
      ;
	pi = x;
	//x[i+1] = '\0';
	for(j = 0;j < i; j++){
	printf("%d,",pi[j]);
	}
	printf("\nx :%d",sizeof(x));
	printf("\npi :%d",sizeof(pi));
    m = sizeof(pi)/sizeof(*pi);
	n = sizeof(x)/sizeof(*x);
	printf("\nm %d",m);
	printf("\nn %d\n",n);
	m = i - 1;
	quicksort(0,m);
	for(j = 0;j < i;j++)
	{
	   printf("x[%d] %d\t,",j,x[j]);
	}
	getchar();
	return 0;
}
*/


/*#define N 10
#include<stdio.h>
 
int sort(int a[],int low,int high){
 int middle;
 middle=a[low];
 while(low!=high)
 {
  while(low<high&&middle>a[low]){
   low++;
   
  a[high]=a[low];
  }
  while(low<high&&middle<a[high])
   high--;
  a[low]=a[high];
 }
 a[low]=middle;
 return low;
}
void ks(int a[],int low,int high){
 int key;
 
if(low>high)return;
key=sort(a,low,high);
ks(a,low,key-1);
ks(a,key+1,high);
}

int main(int argc,char *argv[])
{
 
 
 int a[10]={23,24,235,341,324,134,63,43,2335,2},i,n=10;
  ks(a,0,n-1);
   for(i=0;i<n;i++)
  {
   printf("%d,",a[i]);
  
  }
   getchar();
   
 return 0;
}*/

#include <stdio.h>

int x[100];

int sort(int l,int h)
{
	int key;
	key = x[l];
	while(l != h)
	{
		while(l < h && key < x[l])
		{
			l++;
			x[h] = x[l];
		}
		while(l < h && key > x[h])
			h--;
		x[l] =x[h];
	}
	x[l] = key;
	return l;
}

void quick_sort(int l,int h)
{
	int key;
	if(l > h)return ;
	key = sort(l,h);
	quick_sort(l,key - 1);
	quick_sort(key + 1,h);
}

int main(int argc,char* argv[])
{
	int i,j;
	char a;
	i = 0;
	while(scanf("%d",&x[i++]),(a = getchar()) != '\n')
		;
	quick_sort(0,i - 1);
	for(j = 0;j < i -1;j++)
	{
		printf("%d,",x[j]);
	}
	getchar();
	return 0;
}