#include <iostream>
#include <time.h>

using namespace std;

//const int width = 10;
//const int height = 10;
//
//const int north = 0;
//const int east = 1;
//const int south = 2;
//const int west = 3;
//
//char grid[width * height];
//
//void reset_grid()
//{
//	int i;
//    for (i = 0;i < width * height;++i)
//    {
//		grid[i] = '#';
//    }
//}
//
//int coordinates_index(int x,int y)               //x ， y 坐标位置
//{
//	return y * width + x;
//}
//
//bool is_bounds(int x,int y)     //返回 true 当x y 都在墙内时
//{
//	if (x < 0 || x > width)
//	{
//		return false;
//	}
//	if (y < 0 || y > height)
//	{
//		return false;
//	}
//	return true;
//}
//
//void make_map(int x,int y)
//{
//	grid[coordinates_index(x,y)] = '$';
//	int dirs[4],i;
//	dirs[0] = north;
//	dirs[1] = east;
//	dirs[2] = south;
//	dirs[3] = west;
//	for (i = 0;i < 4;++i)
//	{
//		int r = rand() % 3;
//        int temp = dirs[r];
//		dirs[r] = dirs[i];
//		dirs[i] = temp;
//	}
//	for (i = 0;i < 4;++i)
//	{
//		int dx = 0,dy = 0;
//		switch (dirs[i])
//		{
//		case north:
//			dy = -1;
//			break;
//		case south:
//			dy = 1;
//			break;
//		case east:
//			dx = 1;
//			break;
//		case west:
//			dx = -1;
//			break;
//		}
//
//		int y2 = y + (dy << 1);
//		int x2 = x + (dx << 1);
//		if (is_bounds(x2,y2))
//		{
//			if (grid[coordinates_index(x2,y2)] == '#')
//			{
//				grid[coordinates_index(x2 - dx,y2 - dy)] = '$';
//				make_map(x2,y2);
//			}
//		}
//	}
//}
//
//void print_map()
//{
//	int i,j;
//	for (i = 0;i < height; ++i)
//	{
//		for (j = 0 ;j < width; ++j)
//		{
//			cout<<grid[coordinates_index(i,j)];
//		}
//		cout<<endl;
//	}
//}

//int main(int argc, char **argv)
//{
//	srand((unsigned int)time(0));
//	reset_grid();
//	make_map(1,1);
//	print_map();
//	return 0;
//}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <memory.h>
/* define the size of maze */

#define MAX_COL 6
#define MAX_ROW 6
#define TRUE 1
#define FALSE 0
#define IS_USABLE(a, b) (a >= 0 && a < MAX_ROW) && (b >= 0 && b < MAX_COL) && maze[a][b] && (!my_maze[a][b])

static int maze[MAX_ROW][MAX_COL];
static int target_maze[MAX_ROW][MAX_COL];
static void init_maze();
static int move_to(int i, int j, int (*maze)[MAX_COL], int count);
static void print_maze(int (*maze)[MAX_COL]);

static void init_maze()
{ 
	int i, j; 
	srand((unsigned) time(NULL)); 
	for (i = 0; i < MAX_ROW; i++)  
		for(j = 0; j < MAX_COL; j++) 
		{   
			maze[i][j] = (int) (rand() % 2);  
		} 
		maze[1][0] = 1; 
		/* start point */ 
		maze[MAX_ROW - 1][MAX_COL - 2] = 1; 
		/* end point */
}

static int move_to(int _i,int _j, int (*in_maze)[MAX_COL], int count) 
{ 
	int my_maze[MAX_ROW][MAX_COL], i, j; 
	if (!in_maze) {  
		for (i = 0; i < MAX_ROW; i++)   
			for(j = 0; j < MAX_COL; j++) {    
				my_maze[i][j] = 0;   
			}  
	} else {  
		for (i = 0; i < MAX_ROW; i++)   
			for(j = 0; j < MAX_COL; j++) 
			{    my_maze[i][j] = in_maze[i][j];  
		}  
	} 
	my_maze[_i][_j] = count; 
	/* reach the end point */ 
	if (_i == MAX_ROW - 1 && _j == MAX_COL - 2) 
	{  
		for (i = 0; i < MAX_ROW; i++)   
			for(j = 0; j < MAX_COL; j++) 
			{    
				target_maze[i][j] = my_maze[i][j];   
			}  
			return TRUE; 
	} 
	if (IS_USABLE(_i - 1, _j)) 
	{  
		if (move_to(_i - 1, _j, my_maze, count + 1))   
			return TRUE; 
	} 
	if (IS_USABLE(_i + 1, _j)) 
	{  
		if (move_to(_i + 1, _j, my_maze, count + 1))   return TRUE; 
	}  
	if (IS_USABLE(_i, _j - 1)) 
	{  
		if (move_to(_i, _j - 1, my_maze, count + 1))   return TRUE; 
	}  if (IS_USABLE(_i, _j + 1)) 
	{ 
		if (move_to(_i, _j + 1, my_maze, count + 1))   return TRUE; 
	} 
	return FALSE;
}
static void print_maze(int (*maze)[MAX_COL]) 
{ 
	int i, j; 
	for (i = 0; i < MAX_ROW; i++) 
	{  
		for(j = 0; j < MAX_COL; j++) 
		{   
			if (maze[i][j] != 0)    
				printf("%d ", maze[i][j]);   
			else     
				printf("  "); 
		}  
		printf("\n"); 
	}
}

int main()
{ 
	while(1) 
	{  
		init_maze();  printf("Out put the maze :\n");  
		print_maze(maze);  
		if (move_to(1, 0, NULL, 1)) 
		{  
			printf("Out put the path :\n");   
			print_maze(target_maze);  
			break; 
		} 
		else 
		{   
			printf("No way!\n");  
		} 
	}
	return 0;
}