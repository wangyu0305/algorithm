#pragma once
#include <list>

#define LOGIN 1
#define LOGOUT 2
#define P2PTRANS 3
#define GETALLUSER 4

#define SERVER_PORT 8789

struct stLoginMessage
{
	char userName[10];
	char password[10];
};

struct stLogoutMessage
{
	char userName[10];
};

struct stP2PTranslate
{
	char userName[10];
};

struct stMessage
{
	int iMessageType;
	union _message
	{
		stLoginMessage loginmember;
		stLogoutMessage logoutmember;
		stP2PTranslate translatemessage;
	}message;
};

struct stUserListNode
{
	char userName[10];
	unsigned int ip;
	unsigned short port;
};

struct stServerToClient
{
	int iMessageType;
	union _message
	{
		stUserListNode user;
	}message;
};

#define P2PMESSAGE 100
#define P2PMESSAGEACK 101
#define P2PSOMEONEWANTTOCALLYOU 102

#define P2PTRASH 103

struct stP2PMessage
{
	int iMessageType;
	int iStringLen;
	unsigned short Port;
};

using namespace std;
typedef list<stUserListNode *> UserList;