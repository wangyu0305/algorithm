#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <string.h>

int sqlite3_exec_callback(void *data, int nColumn,char **colValues, char **colNames);

void test_sqlite()
{
	sqlite3 * conn = NULL;
	char * err_msg = NULL;
	char sql[500] = "";

	// 打开数据库, 创建连接
	if(sqlite3_open("proxy", &conn) != SQLITE_OK)
	{
		printf("无法打开！");
	}

	sprintf(sql, "CREATE TABLE `proxy_pool` ( \
		`p_startid` int(100) NOT NULL, \
		`p_ip` varchar(200) NOT NULL DEFAULT '', \
		`p_port` varchar(100) NOT NULL DEFAULT '', \
		`p_response` varchar(1024) DEFAULT NULL, \
		`p_right` int(38) DEFAULT NULL, \
		`p_wrong` int(38) DEFAULT NULL, \
		PRIMARY KEY (`p_ip`,`p_port`) \
		) ;"); 
	if (sqlite3_exec(conn, sql, NULL, NULL, &err_msg) != SQLITE_OK) 
	{ 
		printf("操作失败，错误代码: %s", err_msg); 
		exit(-1); 
	} 

	//添加10条记录
	for (int i = 0; i < 10; i++)
	{
		// 执行SQL
		sprintf(sql, "INSERT INTO proxy_pool (id, name, age) VALUES (%d, '%s', %d)", i, "testPeople", i);
		if (sqlite3_exec(conn, sql, NULL, NULL, &err_msg) != SQLITE_OK) 
		{     
			printf("操作失败，错误代码: %s", err_msg);     
			exit(-1); 
		} 
	}

    //用函数指针取数据
	sprintf(sql, "SELECT * FROM test_for_cpp");
	sqlite3_exec(conn, sql, &sqlite3_exec_callback, 0, &err_msg);

	// 关闭连接。
	if (sqlite3_close(conn) != SQLITE_OK)
	{
		printf("无法关闭，错误代码: %s\n", sqlite3_errmsg(conn));
		exit(-1);
	}

	printf("操作成功！\n");

}

int sqlite3_exec_callback(void *data, int nColumn, char **colValues, char **colNames)
{
	for (int i = 0; i < nColumn; i++)
	{
		printf("%s\t", colValues[i]);
	}
	printf("\n");

	return 0;
}


int main(int argc,char **argv)
{
	//test_sqlite();
	int rc,i,ncols;
	sqlite3 *db;
	sqlite3_stmt *stmt;
	char *sql;
	const char *tail;
	//打开数据

	rc=sqlite3_open("proxy",&db);
	if (rc)
	{
		fprintf(stderr,"Can't open database: %s\n",sqlite3_errmsg(db));
		sqlite3_close(db);
		exit(1);
	}

	sql="select * from t";
	//预处理

	rc=sqlite3_prepare(db,sql,(int)strlen(sql),&stmt,&tail);
	if (rc!=SQLITE_OK)
	{
		fprintf(stderr,"SQLerror:%sn",sqlite3_errmsg(db));
	}

	rc=sqlite3_step(stmt);
	ncols=sqlite3_column_count(stmt);
	while (rc==SQLITE_ROW)
	{

		for (i=0; i<ncols; i++)
		{
			if(i==0)
			{
				fprintf(stderr,"'%s'",sqlite3_column_text(stmt,i));
			}
			else
			{
				fprintf(stderr,",'%s'",sqlite3_column_text(stmt,i));
			}
		}
		fprintf(stderr,"\n");
		rc=sqlite3_step(stmt);
	}
	//释放statement

	sqlite3_finalize(stmt);
	//关闭数据库

	sqlite3_close(db);
	return 0;
}



