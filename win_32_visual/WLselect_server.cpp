#ifdef WIN32

#include <windows.h>
#include <io.h>
#include <winsock.h>
#pragma comment(lib,"WS2_32")
#else

#include <sys/select.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>
#include <unistd.h>

#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>


int port = 8789;

int main(void)
{
	int i,maxi,listenfd,connfd,sockfd,maxfd;
       // int FD_S = 1023;
	int nready,client[FD_SETSIZE];
	unsigned int n;
	fd_set rset;
	char line[65535];
	int clilen;//socket_t clilen;

#ifdef WIN32
WSADATA wsa_data;
int wsa_i;
wsa_i = WSAStartup(MAKEWORD(2,2),&wsa_data);
#endif

    struct sockaddr_in cliaddr,servaddr;
	listenfd=socket(AF_INET,SOCK_STREAM,0);
	memset(&servaddr,0,sizeof(servaddr));
	servaddr.sin_family=AF_INET;
	servaddr.sin_port=htons(port);
	servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
    bind(listenfd,(struct sockaddr *)&servaddr,sizeof(servaddr));
	listen(listenfd,20);
	maxfd=listenfd;
	maxi=-1;
	for(i=0;i<FD_SETSIZE;i++)
		client[i]=-1;
	FD_ZERO(&rset);
      // FD_SET(listenfd,&rset);
	while(1)
	{
               FD_SET(listenfd,&rset);
		if((nready=select(maxfd+1,&rset,NULL,NULL,NULL))<0){
                   if(EINTR == errno)
                        continue;
                    else
                      fprintf(stderr,"begin select failure\n");
                  }
		if(FD_ISSET(listenfd,&rset)){
		clilen=sizeof(cliaddr);
		connfd=accept(listenfd,(struct sockaddr *)&cliaddr,&clilen);
               printf("accept success\n");
		for(i=0;i<FD_SETSIZE;i++){
			if(client[i]<0)
		     {
			client[i]=connfd;
			break;
		     }
                   }
		  if(i==FD_SETSIZE)
		perror("too many clients\n");
	    printf("starting add fd_set");
                  FD_SET(connfd,&rset);
               printf("add connfd success\n");
		if(connfd>maxfd)
			maxfd=connfd;
		if(i>maxi)
			maxi=i;
	    }
	 for(i=0;i<=maxi;i++)
	{
		 if((sockfd=client[i])<0)
			 continue;
		 if(FD_ISSET(sockfd,&rset))
		{
			 n=recv(sockfd,line,65535,0);
			printf("recv from client %s\n",line);
                           send(sockfd,line,n,0);
                        printf("send from server %s\n",line);	
                       if(n == 0){
                               close(sockfd);
				 FD_CLR(sockfd,&rset);
		             client[i]=-1;
                            }
                  }
          }
    //   if(--nready<=0)
   //    break;
	}
	#ifdef WIN32
		WSACleanup();
	#endif
     return 0;
}
