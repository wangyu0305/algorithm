#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

struct message
{ 
  long msg_type;
  char msg_text[512];
};

int main()
{
  int qid,rid;
  key_t key;
  int len;
  struct message msg,msg1;
  if((key = ftok(".",1)) == -1)
  {
    printf("ftok error\n");
    exit(1);
  }
  if((qid = msgget(key,IPC_CREAT|0666)) == -1)
  {
    printf("msgget error\n");
    exit(1);
  }
  if((rid = msgget(key,IPC_CREAT|0666)) == -1)
  {
    printf("msgget rid error\n");
    exit(1);
  }
  printf("open queue:%d\n",qid);
  puts("Please enter the message to queue:");
  fgets(msg.msg_text,512,stdin);
  msg.msg_type = getpid();
  len = strlen(msg.msg_text);
  if((msgsnd(qid,&msg,len,0)) < 0)
  {
    printf("error msgsnd\n");
    exit(1);
  }
  if(msgrcv(rid,&msg1,512,0,0) < 0)
  {
    printf("error msgrcv");
    exit(1);
  }
  printf("message is :%s\n",msg1.msg_text);
  if((msgctl(qid,IPC_RMID,NULL)) < 0)
  {
    printf("msgctl error");
    exit(1);
  }
  msgctl(rid,IPC_RMID,NULL);
  exit(0);
}
