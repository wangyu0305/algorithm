//共享内存的生产者 即存入内容到共享内存
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>

typedef struct seg
{
	char name[4];
	int age;
}segmet;

int main(int argc,char* argv[])
{
	int mem_key;
	long mem_id;
	segmet* mem_addr;
	int i,isAttached;
	char temp;
	mem_key = ftok(".",1);  //产生一个唯一的共享ID
	if((mem_id = shmget(mem_key,2048,IPC_CREAT)) < 0){    //获取一个共享内存
		printf("shmget failed\n");
	}
	else{
		printf("shmget success\n");
        system("ipcs -m");
	}
	temp = 'a';
	for(i = 0;i < 10;i++)
	{
		isAttached = 0;
		if((mem_addr = (segmet*)shmat(mem_id,0,0)) == (segmet*)(-1))  //映射共享内存
			printf("%s,Failed to attach share memory,times[%02d],",argv[0],i);
		else{
			temp += 1;
			memcpy((*(mem_addr + i)).name,&temp,1);
			(*(mem_addr + i)).age = 20 + i;
			isAttached = 1;
			printf("%s,Success attached share memory,times[%02d]",argv[0],i);
		}
        if(isAttached){
			shmdt((segmet*)mem_addr);
			printf("%s Success detached,times[%02d]",argv[0],i);
		}
		if(i == 5){
			shmctl(mem_id,IPC_RMID,NULL);
			printf("%s Remove executed times[%02d]",argv[0],i);
		}
	}
	return 0;
}
