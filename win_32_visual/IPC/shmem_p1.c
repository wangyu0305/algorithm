//共享内存消费者 读取shmem_p2存入的数据
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#define BUFSZ 2048

typedef struct seg
{
	char name[4];
	int age;
}segmet;

int main(int argc,char* argv[])
{
	long mem_id;
	segmet* mem_addr;
	int shm_id,i;
	int isAttached = 0;
	char* shmadd;

	shm_id = ftok(".",1);
	
	if((mem_id = shmget(shm_id,BUFSZ,IPC_CREAT)) < 0){ 
		perror("shmget");
		exit(1);
	}
	else{
		printf("create shared-memory: %d\n",shm_id);
		system("ipcs -m");
	}
	if((mem_addr = (segmet*)shmat(mem_id,0,0)) == (void*)(-1)){
		printf("%s Failed to attach shared memory,errno:%d\n",argv[0],errno);
	}
	else{
		isAttached = 1;
		printf("%s Successfully attached share memory",argv[0]);
		for(i = 0;i < 10;i++)
		{
			printf("name :%s\n",(*(mem_addr + i)).name);
			printf("age %d\n",(*(mem_addr + i)).age);
		}
		system("ipcs -m");
	}
	sleep(10);

	if(isAttached){
         shmdt((segmet*)mem_addr);
		 printf("%s Success detached share memory",argv[0]);
	}
	if((mem_addr = (void*)shmat(mem_id,0,0)) == (void*)(-1))
		printf("Failed to attach the removed shared memory,errno");
	return 0;
}


