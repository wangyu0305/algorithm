#include <WinSock2.h>
#include <Windows.h>
#include <WS2tcpip.h>
#include <stdio.h>
#pragma comment(lib,"wsock32")
#define DEFAULT_PORT "8789"
#define MAX_REQUEST 1024
#define BUF_SIZE 4096
DWORD WINAPI CommunicationThread(LPVOID lpParameter)
{
	DWORD dwTid=GetCurrentThreadId();
	SOCKET socket=(SOCKET)lpParameter;
	LPSTR szRequest=(LPSTR)HeapAlloc(GetProcessHeap(),0,MAX_REQUEST);
	int iResult;
	int bytesSent;
	iResult=recv(socket,szRequest,MAX_REQUEST,0);
	if(iResult==0)
	{
		printf("Connection closing...\n");
		HeapFree(GetProcessHeap(),0,szRequest);
		closesocket(socket);
		return 1;
	}
	else if(iResult==SOCKET_ERROR)
	{
		printf("recv failed : %d\n",WSAGetLastError());
		HeapFree(GetProcessHeap(),0,szRequest);
		closesocket(socket);
		return 1;
	}
	else if(iResult>0)
	{
		printf("\tCommunicationThread(%d)\tBytes received:%d\n",dwTid,iResult);
		printf("\tCommunicationThread(%d)\trequest sreing is (%s)\n",dwTid,szRequest);
		if(lstrcmpi((LPCWSTR)szRequest,(LPCWSTR)"download file")==0)
		{
			HANDLE hFile;
			LPVOID lpReadBuf;
			DWORD dwBytesRead;
			DWORD dwFileSize;
			DWORD dwSendFile=0;
			hFile=CreateFile((LPCWSTR)"download.txt",GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
			if(hFile==INVALID_HANDLE_VALUE)
			{
				printf("\tCommunicationThread\tCould not open file (error %d)\n",GetLastError());
				send(socket,"error",6,0);
				closesocket(socket);
				return 1;
			}
			lpReadBuf=HeapAlloc(GetProcessHeap(),0,BUF_SIZE);
			dwFileSize=GetFileSize(hFile,NULL);
			while(1)
			{
				if(!ReadFile(hFile,lpReadBuf,BUF_SIZE,&dwBytesRead,NULL))
				{
					printf("\tCommunicationThread\tCould not read from file (error %d)\n",GetLastError());
					closesocket(socket);
					CloseHandle(hFile);
						return 1;
				}
				bytesSent=send(socket,(char*)lpReadBuf,dwBytesRead,0);
				if(bytesSent==SOCKET_ERROR)
				{
					printf("\tCommunicationThread\tsend error %d\n",WSAGetLastError());
					closesocket(socket);
					CloseHandle(hFile);
					return 1;
				}
				printf("\tCommunicationThread(%d)\tsend %d bytes\n",dwTid,bytesSent);
				dwSendFile+=dwBytesRead;
				if(dwSendFile==dwFileSize)
				{
					printf("\tCommunicationThread\tFile download ok\n");
					break;
				}
			}
			HeapFree(GetProcessHeap(),0,lpReadBuf);
			CloseHandle(hFile);
			closesocket(socket);
		}
		else if(lstrcmpi((LPCWSTR)szRequest,(LPCWSTR)"get information")==0)
		{
			bytesSent=send(socket,"this is information",lstrlen((LPCWSTR)"this is information")+1,0);
			if(bytesSent==SOCKET_ERROR)
			{
				printf("\tCommunicationThread\tsend error %d\n",WSAGetLastError());
				closesocket(socket);
				return 1;
			}
			printf("\tCommunicationThread(%d)\tsend %d bytes\n",dwTid,bytesSent);
		}
		else
		{
			printf("unreferenced request\n");
		}
	}
	HeapFree(GetProcessHeap(),0,szRequest);
	closesocket(socket);
	return 0;
}
int __cdecl main(void)
{
	const char * inet_ntop(int af, const void *src, char *dst, size_t size);
	static const char * inet_ntop_v4 (const void *src, char *dst, size_t size);
	//static const char * inet_ntop_v6 (const u_char *src, char *dst, size_t size);
	WSADATA wsaData;
	SOCKET ListenSocket=INVALID_SOCKET;
	SOCKET ClientSocket=INVALID_SOCKET;
	struct addrinfo *result=NULL,hints;

	char *ptr = "www.google.com";
	char addr[32];
	struct hostent *hptr;

	int iResult;
	iResult=WSAStartup(MAKEWORD(2,2),&wsaData);
	if(iResult!=0)
	{
		printf("WSAStartup failure %d\n",iResult);
		return 1;
	}
    
	if ((hptr = gethostbyname(ptr)) == NULL)
	{
		printf("gethostbyname errno host name :%s  \n",ptr);
	}
    
	printf("office name : %s",hptr->h_name);
	printf("address :%s",inet_ntop(hptr->h_addrtype,hptr->h_addr_list[0],addr,sizeof(addr)));
	ZeroMemory(&hints,sizeof(hints));
	hints.ai_family=AF_INET;
	hints.ai_socktype=SOCK_STREAM;
	hints.ai_protocol=IPPROTO_TCP;
	hints.ai_flags=AI_PASSIVE;
	iResult=getaddrinfo(NULL,DEFAULT_PORT,&hints,&result);
	if(iResult!=0)
	{
		printf("getadrinfo failed:%d\n",iResult);
		WSACleanup();
		return 1;
	}
	ListenSocket=socket(result->ai_family,result->ai_socktype,result->ai_protocol);
	if(ListenSocket==INVALID_SOCKET)
	{
		printf("socket failed: %d\n",WSAGetLastError());
		freeaddrinfo(result);
		return 1;
	}
	iResult=bind(ListenSocket,result->ai_addr,(int)result->ai_addrlen);
	if(iResult==SOCKET_ERROR)
	{
		printf("bind failed: %d\n",WSAGetLastError());
		freeaddrinfo(result);
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	printf("bind\n");
	freeaddrinfo(result);
	iResult=listen(ListenSocket,SOMAXCONN);
	printf("start listen......\n");
	if(iResult==SOCKET_ERROR)
	{
		printf("listen failed: %d\n",WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	while(1)
	{
		printf("ready to accept\n");
		ClientSocket=accept(ListenSocket,NULL,NULL);
		printf("accept a connetion\n");
		if(ClientSocket==INVALID_SOCKET)
		{
			printf("accept failed: %d\n",WSAGetLastError());
			closesocket(ListenSocket);
			break;
		}
		if(!CreateThread(NULL,0,CommunicationThread,(LPVOID)ClientSocket,0,NULL))
		{
			printf("Create Thread error (%d)",GetLastError());
			break;
		}
	}
	WSACleanup();
	return 0;
}


const char * 
inet_ntop(int af, const void *src, char *dst, size_t size) 
{ 
    switch (af) { 
    case AF_INET : 
    return inet_ntop_v4 (src, dst, size); 
#ifdef INET6 
    case AF_INET6: 
         return inet_ntop_v6 ((const u_char*)src, dst, size); 
#endif 
    default : 
    return NULL; 
    } 
} 
  
  
static const char * 
inet_ntop_v4 (const void *src, char *dst, size_t size) 
{ 
    const char digits[] = "0123456789"; 
    int i; 
    struct in_addr *addr = (struct in_addr *)src; 
    u_long a = ntohl(addr->s_addr); 
    const char *orig_dst = dst; 
  
    if (size < INET_ADDRSTRLEN) {  
    return NULL; 
    } 
    for (i = 0; i < 4; ++i) { 
    int n = (a >> (24 - i * 8)) & 0xFF; 
    int non_zerop = 0; 
  
    if (non_zerop || n / 100 > 0) { 
        *dst++ = digits[n / 100]; 
        n %= 100; 
        non_zerop = 1; 
    } 
    if (non_zerop || n / 10 > 0) { 
        *dst++ = digits[n / 10]; 
        n %= 10; 
        non_zerop = 1; 
    } 
    *dst++ = digits[n]; 
    if (i != 3) 
        *dst++ = '.'; 
    } 
    *dst++ = '\0'; 
    return orig_dst; 
} 
  
  
  
/* 
 * Convert IPv6 binary address into presentation (printable) format. 
 */
//static const char * 
//inet_ntop_v6 (const u_char *src, char *dst, size_t size) 
//{ 
//  /* 
//   * Note that int32_t and int16_t need only be "at least" large enough 
//   * to contain a value of the specified size.  On some systems, like 
//   * Crays, there is no such thing as an integer variable with 16 bits. 
//   * Keep this in mind if you think this function should have been coded 
//   * to use pointer overlays.  All the world's not a VAX. 
//   */
//  char  tmp [INET6_ADDRSTRLEN+1]; 
//  char *tp; 
//  struct { 
//    long base; 
//    long len; 
//  } best, cur; 
//  u_long words [IN6ADDRSZ / INT16SZ]; 
//  int    i; 
//  
//  /* Preprocess: 
//   *  Copy the input (bytewise) array into a wordwise array. 
//   *  Find the longest run of 0x00's in src[] for :: shorthanding. 
//   */
//  memset (words, 0, sizeof(words)); 
//  for (i = 0; i < IN6ADDRSZ; i++) 
//      words[i/2] |= (src[i] << ((1 - (i % 2)) << 3)); 
//  
//  best.base = -1; 
//  cur.base  = -1; 
//  for (i = 0; i < (IN6ADDRSZ / INT16SZ); i++) 
//  { 
//    if (words[i] == 0) 
//    { 
//      if (cur.base == -1) 
//           cur.base = i, cur.len = 1; 
//      else cur.len++; 
//    } 
//    else if (cur.base != -1) 
//    { 
//      if (best.base == -1 || cur.len > best.len) 
//         best = cur; 
//      cur.base = -1; 
//    } 
//  } 
//  if ((cur.base != -1) && (best.base == -1 || cur.len > best.len)) 
//     best = cur; 
//  if (best.base != -1 && best.len < 2) 
//     best.base = -1; 
//  
//  /* Format the result. 
//   */
//  tp = tmp; 
//  for (i = 0; i < (IN6ADDRSZ / INT16SZ); i++) 
//  { 
//    /* Are we inside the best run of 0x00's? 
//     */
//    if (best.base != -1 && i >= best.base && i < (best.base + best.len)) 
//    { 
//      if (i == best.base) 
//         *tp++ = ':'; 
//      continue; 
//    } 
//  
//    /* Are we following an initial run of 0x00s or any real hex? 
//     */
//    if (i != 0) 
//       *tp++ = ':'; 
//  
//    /* Is this address an encapsulated IPv4? 
//     */
//    if (i == 6 && best.base == 0 && 
//        (best.len == 6 || (best.len == 5 && words[5] == 0xffff))) 
//    { 
//      if (!inet_ntop_v4(src+12, tp, sizeof(tmp) - (tp - tmp))) 
//      { 
//        errno = ENOSPC; 
//        return (NULL); 
//      } 
//      tp += strlen(tp); 
//      break; 
//    } 
//    tp += sprintf (tp, "%lX", words[i]); 
//  } 
//  
//  /* Was it a trailing run of 0x00's? 
//   */
//  if (best.base != -1 && (best.base + best.len) == (IN6ADDRSZ / INT16SZ)) 
//     *tp++ = ':'; 
//  *tp++ = '\0'; 
//  
//  /* Check for overflow, copy, and we're done. 
//   */
//  if ((size_t)(tp - tmp) > size) 
//  { 
//    errno = ENOSPC; 
//    return (NULL); 
//  } 
//  return strcpy (dst, tmp); 
//  return (NULL); 
//} 
