#include <stdio.h>
#include <WinSock2.h>
#pragma comment(lib,"wsock32")
#define RECV_BUFFER_SIZE 8192
int port = 8789;
void main(int argc,char argv[])
{
	SOCKADDR_IN clientService;
	SOCKET ConnectSocket;
	WSADATA wsaData;
	LPVOID recvbuf;
	int bytesSent;
	int bytesRecv=0;
	char sendbuf[32]="get information";
	int iResult=WSAStartup(MAKEWORD(2,2),&wsaData);
	if(iResult!=NO_ERROR)
		printf("Error at WSAStartup()\n");
	ConnectSocket=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	if(ConnectSocket==INVALID_SOCKET)
	{
		printf("Error at socket():%ld\n",WSAGetLastError());
		WSACleanup();
		return ;
	}
	clientService.sin_family=AF_INET;
	clientService.sin_addr.S_un.S_addr=inet_addr("127.0.0.1");
	clientService.sin_port=htons(port);
	if(connect(ConnectSocket,(SOCKADDR*)&clientService,sizeof(clientService))==SOCKET_ERROR)
	{
		printf("Failed to connect(%d)\n",WSAGetLastError());
		WSACleanup();
		return ;
	}
	if(argc==2&&(!strcmp(&argv[1],"-d")))
	{
		strncpy(sendbuf,"download file",32);
	}
	bytesSent=send(ConnectSocket,sendbuf,strlen(sendbuf)+1,0);
	if(bytesSent==SOCKET_ERROR)
	{
		printf("send error(%d)\n",WSAGetLastError());
		closesocket(ConnectSocket);
		return ;
	}
	printf("Bytes Sent:%ld\n",bytesSent);
	recvbuf=HeapAlloc(GetProcessHeap(),0,RECV_BUFFER_SIZE);
	while(bytesRecv!=SOCKET_ERROR)
	{
		bytesRecv=recv(ConnectSocket,(char*)recvbuf,RECV_BUFFER_SIZE,0);
		if(bytesRecv==0)
		{
			printf("Connection Closed.\n");
			break;
		}
		HeapFree(GetProcessHeap(),0,recvbuf);
		WSACleanup();
		return ;
	}
}
