#ifndef _FTPD_H
#define _FTPD_H

#define FTPD_DEBUG(fmt,...) ftpd_debug(__FINE__,__LINE__,fmt,__VA_ARGS__)
#define FTPD_ARR_LEN(arr) (sizeof(arr)/sizeof(arr[0]))

#define FTPD_VER "1.0"
#define FTPD_DEF_SRV_PORT  21
#define FTPD_LISTEN_QU_LEN 8
#define FTPD_LINE_END "/r/n"

#define FTPD_OK 0
#define FTPD_ERR (-1)
#define PATH_MAX 128
#define FTPD_CHECK_LOGIN() 
    do
    {
		if(!ftpd_cur_user){
			ftpd_send_resp(ctrlfd,530,"first please");
			return FTPD_ERR;
		}
    }
    while (0)

struct ftpd_cmd_struct
{
	char *cmd_name;
	int (*cmd_handler)(int ctrlfd,char * cmd_line)
};
	
struct ftpd_user_struct
{
		char user[128];
		char pass[128];
};

	#endif

