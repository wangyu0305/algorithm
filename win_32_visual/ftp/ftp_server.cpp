#ifndef _FTPD_H
#define _FTPD_H

#define FTPD_DEBUG(fmt,...) ftpd_debug(__FINE__,__LINE__,fmt,__VA_ARGS__)
#define FTPD_ARR_LEN(arr) (sizeof(arr)/sizeof(arr[0]))

#define FTPD_VER "1.0"
#define FTPD_DEF_SRV_PORT  21
#define FTPD_LISTEN_QU_LEN 8
#define FTPD_LINE_END "/r/n"

#define FTPD_OK 0
#define FTPD_ERR (-1)
#define PATH_MAX 128
#define FTPD_CHECK_LOGIN() 
    do
    {
		if(!ftpd_cur_user){
			ftpd_send_resp(ctrlfd,530,"first please");
			return FTPD_ERR;
		}
    }
    while (0)

struct ftpd_cmd_struct
{
	char *cmd_name;
	int (*cmd_handler)(int ctrlfd,char * cmd_line)
};
	
struct ftpd_user_struct
{
		char user[128];
		char pass[128];
};

	#endif


#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <sys/socket.h>
#include <netinet.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdarg.h>
#include <signal.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <pwd.h>
#include <grp.h>
#include <dirent.h>
#include "ftp_server.h"

static int ftpd_do_user(int ,char*);
static int ftpd_do_pass(int ,char*);
static int ftpd_do_pwd(int ,char*);
static int ftpd_do_cwd(int ,char*);
static int ftpd_do_syst(int ,char*);
static int ftpd_do_list(int ,char*);
static int ftpd_do_size(int ,char*);
static int ftpd_do_dele(int ,char*);
static int ftpd_do_type(int ,char*);
static int ftpd_do_port(int ,char*);
static int ftpd_do_pasv(int ,char*);
static int ftpd_do_retr(int ,char*);
static int ftpd_do_stor(int ,char*);
static int ftpd_do_quit(int ,char*);

int ftpd_debug_on;
int ftpd_srv_port = FTPD_DEF_SRV_PORT;
int ftpd_cur_child_num;
int ftpd_quit_flag;
struct ftpd_user_struct* ftpd_cur_user;
int ftpd_pasv_fd = -1;
int ftpd_pasv_connfd = -1;
int ftpd_port_connfd = -1;
char ftpd_home_dir[PATH_MAX];

struct ftpd_cmd_struct ftpd_cmds[] = {
	{"USER",ftpd_do_user},
	{"PASS",ftpd_do_pass},
	{"PWD",ftpd_do_pwd},
	{"XPWD",ftpd_do_pwd},
	{"CWD",ftpd_do_cwd},
	{"LIST",ftpd_do_list},
	{"NLST",ftpd_do_list},
	{"SYST",ftpd_do_syst},
	{"TYPE",ftpd_do_type},
	{"SIZE",ftpd_do_size},
	{"DELE",ftpd_do_dele},
	{"RMD",ftpd_do_dele},
	{"PORT",ftpd_do_port},
	{"PASV",ftpd_do_pasv},
	{"RETR",ftpd_do_retr},
	{"STOR",ftpd_do_stor},
	{"QUIT",ftpd_do_quit},
	{NULL,NULL}
};

struct ftpd_user_struct ftpd_usrt[] = {
	{"anonymous"," "},
	{"ftp"," "}
};

char ftpd_srv_resps[][256] = {
	    "150   Begin transfer"      FTPD_LINE_END,
	    "200   OK"                  FTPD_LINE_END,
		"213  %d"                   FTPD_LINE_END,
		"215 UNIX Type: L8"         FTPD_LINE_END,
		"220 FTPD"                  FTPD_LINE_END,
		"221 Goodbye"               FTPD_LINE_END,
		"226 Transfer complete"     FTPD_LINE_END,
		"227 Entering Passive Mode(%d,%d,%d,%d,%d,%d)" FTPD_LINE_END,
		"230 User %s logged in"     FTPD_LINE_END,
		"250 CWD command successful" FTPD_LINE_END,
		"257 /"%s/" is current directory" FTPD_LINE_END,
		"331 Password required for %s"  FTPD_LINE_END,
		"500 Unsupport command %s"     FTPD_LINE_END,
		"530 Login %s"               FTPD_LINE_END,
		"550 Error"                 FTPD_LINE_END,
};
	
static void ftpd_debug(const char* file,int line,const char* fmt,...)
{
	va_list ap;
	if(!ftpd_debug_on)
		return ;
	fprintf(stderr,"(%s:%d:%d)",file,line,getpid());
	va_start(ap,fmt);
	vfprintf(stderr,fmt,ap);
	va_end(ap);
}

static void ftpd_usage(void)
{
	printf("ftpd"FTPD_VER"usage:/n");
	printf("ftpd -p port -d/n");
}

static void ftpd_parse_args(int argc,char* argv[])
{
	int opt;
	int err = 0;
	while((opt = getopt(argc,argv,"p:dh")) != -1){
		switch(opt){
			case 'p':
				ftpf_srv_port = atoi(optarg);
			err = (ftpd_srv_port < 0 || ftpd_srv_port > 65535);
			break;

			case 'd':
				ftpd_debug_on = 1;
			break;

			default:
				err = 1;
			break;
		}
		if(err){
            ftpd_usage();
			exit(-1);
		}
	}
	FTPD_DEBUG("srv port:%d/n",ftpd_srv_port);
}

static void ftpd_sigchild(int signo)
{
	int status;
    if(signo != SIGCHLD)
		return ;
	while(waitpid(-1,&status,WNOHANG) > 0)
		ftpd_cur_child_num--;
	FTPD_DEBUG("caught a SIGCHLD,ftpd_cur_child_num = %d/n",ftpd_cur_child_num);
}

static int ftpd_init(void)
{
	signal(SIGPIPE,SIG_IGN);
	signal(SIGCHLD,ftpd_sigchild);
	getcwd(ftpd_home_dir,sizeof(ftpd_home_dir));
	return FTPD_OK;
}

static int ftpd_create_srv(void)
{
	int fd;
	int on =1;
	int err;
	struct sockaddr_in srvaddr;
	if((fd = socket(AF_INET,SOCK_STREAM,0)) < 0){
		FTPD_DEBUG("socket() failed: %s/n",strerror(errno));
		return fd;
	}
	err = setsockopt(fd,SOL_SOCKET,SO_REUSEADDR,&on,sizeof(on));
	if(err < 0){
		FTPD_DEBUG("setsockopt()failed:%s/n",strerror(errno));
		close(fd);
		return FTPD_ERR;
	}
	
	memset(&srvaddr,0,sizeof(srvaddr));
	srvaddr.sin_family = AF_INET;
	srvaddr.sin_port = htons(ftpd_srv_port);
	srvaddr.sin_addr.s_addr = htonl(INADDR_ANY);

    err = bind(fd,(struct sockaddr*)&srvaddr,sizeof(srvaddr));
	if(err < 0){
		FTPD_DEBUG("bind() failed: %s/n", strerror(errno));
        close(fd);
        return FTPD_ERR;
        }

    err = listen(fd,FTPD_LISTEN_QU_LEN);
	if(err < 0){
		FTPD_DEBUG("listen() failed: %s/n", strerror(errno));
        close(fd);
        return FTPD_ERR;
        }

    if(ftpd_debug_on){
		int len = sizeof(srvaddr);
		getsockname(fd,(struct sockaddr*)&srvaddr,&len);
		FTPD_DEBUG("create srv listen socket OK:%s:%d/n",inet_ntoa(srvaddr.sin_addr),ntohs(srvaddr.sin_port));
	}
	return fd;

static char* ftpd_srv_resp_num2msg(int num)
{
	int i;
	char buf[8];
	snprintf(buf,sizeof(buf),"%d",num);
	if(strlen(buf) != 3)
		return NULL;
	for(i = 0;i < FTPD_ARR_LEN(ftpd_srv_resps);i++)
		if(strncmp(buf,ftpd_srv_resps[i],strlen(buf)) == 0)
		   return ftpd_srv_resps[i];
	return NULL;
}

static int ftpd_send_msg(int fd,char* msg,int len)
{
	int n,off = 0,left = len;
	while(1){
         n = write(fd,msg + off,left);
		 if(n < 0){
			 if(error == EINTR)
				 continue;
			 return n;
		 }
		 if(n < left){
			 left -= n;
			 off += n;
			 continue ;
		 }
		 return len;
	}
}

static int ftpd_recv_msg(int fd,char buf[],int len)
{
	int n;
	while(1){
		n = read(fd,buf,len)
			if(n < 0){
			if(error == EINTR)
				continue ;
			return n;
		}
		return n;
	}
}

static int ftpd_send_resp(int fd,int num,...)
	{
	char* cp = ftpd_srv_resp_num2msg(num);
	va_list ap;
	char buf[BUFSIZ];
	if(!cp){
		FTPD_DEBUG("ftpd_srv_resp_num2msg(%d) failed/n",num);
		return FTPD_ERR;
	}

	va_start(ap,num);
	vsnprintf(buf,sizeof(buf),cp,ap);
	va_end(ap);
    
	FTPD_DEBUG("send resp:%s/n",buf);
	if(ftpd_send_msg(fd,buf,strlen(buf)) != strlen(buf)){
		FTPD_DEBUG("ftpd_send_msg()failed:%s/n",strerror(errno));
		return FTPD_ERR;
	}
	return FTPD_OK;
	}

	static void ftpd_trim_lineend(char* cp)
	{
		if(cp && strlen(cp) > 0){
			char* cp2 = &cp[strlen(cp) - 1];
			while(*cp2 == '/r' || *cp2 == '/n')
				if(--cp2 < cp)
				break;
			cp2[1] = '/0';
		}
	}

	static int ftpd_get_connfd(void)
	{
		int fd;
		if(ftpd_pasv_fd >= 0){
			fd = accept(ftpd_pasv_fd,NULL,NULL);
			if(fd >= 0){
				close(ftpd_pasv_fd);
				ftpd_pasv_fd = -1;
				ftpd_pasv_connfd = fd;
				return fd;
			}
			else 
				FTPD_DEBUG("accept()failed:%s/n",strerror(errno));
		}
		else if(ftpd_port_connfd >= 0)
			return ftpd_port_connfd;
		return (-1);
	}

static int ftpd_close_all_fd(void)
	{
	if(ftpd_pasv_fd >= 0){
		close(ftpd_pasv_fd);
		ftpd_pasv_connfd = -1;
	}
	if(ftpd_pasv_connfd >= 0){
		close(ftpd_pasv_connfd);
		ftpd_pasv_connfd = -1;
	}
	if(ftpd_port_connfd >= 0){
		close(ftpd_port_connfd);
		ftpd_port_connfd = -1;
	}
	return FTPD_OK;
	}

	static int ftpd_do_user(int ctrlfd,char* cmdline)
	{
		char* cp = strchr(cmdline,' ');
		if(cp){
			int i;
			for(i = 0;i < FTPD_ARR_LEN(ftpd_user);i++)
				if(strcmp(cp+1,ftpd_users[i],user) == 0){
				FTPD_DEBUG("user(%s) is found/n",cp+1);
				ftpd_cur_user = &ftpd_users[i];
				break;
			}
			if(!ftpd_cur_user)
				FTPD_DEBUG("user(%s)not found/n",cp+1);
                 /*
                 * If user name is bad, we still don't close the connection 
                 * and send back the 331 response to ask for password.
                 */
           return ftpd_send_resp(ctrlfd,331,cp+1);
		}
		return ftpd_send_resp(ctrlfd,550);
	}

	static int ftpd_do_pass(int ctrlfd,char* cmdline)
	{
		char* space = strchr(cmdline,' ');
		if(ftpd_cur_user && space){
			if(strlen(ftpd_cur_user->pass) == 0 || strcmp(space+1,ftpd_cur_user->pass) == 0){
				FTPD_DEBUG("password for %s OK /n",ftpd_cur_user->user);
				return ftpd_send_resp(ctrlfd,230,ftpd_cur_user->user);
			}
			FTPD_DEBUG("password for %s ERR/n",ftpd_cur_user->user);
		}
         /*
         * User and pass don't match or 
         * cmd line does not contain a space character.
         */
		 ftpd_cur_user = NULL;
		 return ftpd_send_resp(ctrlfd,530,"incorrect");
	}

	static int ftpd_do_pwd(int ctrlfd,char* cmdline)
	{
		char curdir[PATH_MAX];
		char* cp;
        FTPD_CHECK_LOGIN();
		getcwd(curdir,sizeof(curdir));
		cp = &curdir[strlen(ftpd_home_dir)];
		return ftpd_send_resp(ctrlfd,257,(*cp == '/0' ? "/" : cp));
	}

	static int ftpd_do_cwd(int ctrlfd,char* cmdline)
	{
		char* space = strchr(cmdline.' ');
		char curdir[PATH_MAX];
		FTPD_CHECK_LOGIN();
		if(!space)
			return ftpd_send_resp(ctrlfd,550);
        getcwd(curdir,sizeof(curdir));
		if(strcmp(curdir,ftpd_home_dir) == 0 && space[1] == '.' && space[2] == '.')
			return ftpd_send_resp(ctrlfd,550);
		if(space[1] == '/'){
			if(chdir(ftpd_home_dir) == 0){
				if(space[2] == '/0' || chdir(space + 2) == 0)
					return ftpd_send_resp(ctrlfd,250);
			}
			chdir(curdir);
			return ftpd_send_resp(ctrlfd,550);
		}
		if(chdir(space + 1) == 0)
			return ftpd_send_resp(ctrlfd,250);
		chdir(curdir);
		return ftpd_send_resp(ctrlfd,550);
	}
/*
* This function acts as a implementation like 'ls -l' shell command.
*/

static int ftpd_get_list(char buf[],int len)
	{
	DIR* dir;
	struct dirent* ent;
	int off = 0;
    if((dir = opendir(".")) < 0){
		FTPD_DEBUG("opendir() failed:%s/n",strerror(errno));
		return FTPD_ERR;
	}
	buf[0] = '/0';
	while((ent = readdir(dir)) != NULL){
		char* filename = ent->d_name;
		struct stat st;
		char mode[] = "-------";
		struct passwd* pwd;
		struct group* grp;
		struct tm* ptm;
		char timebuf[BUFSIZ];
		int timelen;
		if(strcmp(filename,".") == 0 || strcmp(filename,"..") == 0)
			continue ;
        if(stat(filename,&st) < 0){
			closedir(dir);
			FTPD_DEBUG("stat() failed:%s/n",strerror(errno));
			return FTPD_ERR;
		}
		if(S_ISDIR(st.st_mode))
			mode[0] = 'd';
		if(st.st_mode & S_IRUSR)
			mode[1] = 'r';
		if(st.st_mode & S_IWUSR)
			mode[2] = 'w';
		if(st.st_mode & S_IXUSR)
			mode[3] = 'x';
        if(st.st_mode & S_IRGRP)
			mode[4] = 'r';
		if(st.st_mode & S_IWGRP)
			mode[5] = 'w';
		if(st.st_mode & S_IXGRP)
			mode[6] = 'x';
		if(st.st_mode & S_IROTH)
			mode[7] = 'r';
		if(st.st_mode & S_IWOTH)
			mode[8] = 'w';
		if(st.st_mode & S_IXOTH)
			mode[9] = 'x';
		mode[10] = '/0';
		off += snprintf(buf + off,len - off,"%s",mode);
  /* hard link number, this field is nonsense for ftp */
        off += snprintf(buf + off,len - off,"%d",1);
		if((pwd = getpwuid(st.st_uid)) == NULL){
			closedir(dir);
			return FTPD_ERR;
		}
		off += snprintf(buf + off,len - off,"%s",pwd->pw_name);
 /* group */

		if((grp = getgrgid(st.st_gid)) == NULL){
			closedir(dir);
			return FTPD_ERR;
		}
		off += snprintf(buf + off,len - off,"%s",grp->gr_name);
 /* size */
        off += snprintf(buf + off,len - off,"%d",10,st.st_size);
 /* mtime */
        ptm = localtime(&st.st_mtime);
		if(ptm && (timelen = strftime(timebuf,sizeof(timebuf),"%b %d %H:%S",ptm)) > 0){
			timebuf[timelen] = '/0';
			off += snprintf(buf + off,len - off,"%s",timebuf);
		}
		else {
			closedir(dir);
			return FTPD_ERR;
		}
		off += snprintf(buf + off,len - off,"%s/r/n",filename);
	}
	return off;
	}

	static int ftpd_do_list(int ctrlfd,char* cmdline)
	{
		char buf[BUFSIZ];
		int n;
		int fd;
		FTPD_CHECK_LOGIN();
		if((fd = ftpd_get_connfd()) < 0){
			FTPD_DEBUF("LIST cmd:no available fd %s","/n");
			goto err_label;
		}

		ftpd_send_resp(ctrlfd,150);
        n = ftpd_get_list(buf,sizeof(buf));
		if(n >= 0){
			if(ftpd_send_msg(fd,buf,n) != n){
				FTPD_DEBUG("ftpd_send_msg()failed :%s/n",strerror(errno));
				goto err_label;
			}
		}
		else{
			FTPD_DEBUG("ftpd_get_list()failed %s ","/n");
			goto err_label;
		}
		ftpd_close_all_fd();
		return ftpd_send_resp(ctrlfd,226);

		err_label:
			ftpd_close_all_fd();
		return ftpd_send_resp(ctrlfd,550);
	}

	static int ftpd_do_syst(int ctrlfd,char* cmdline)
	{
		FTPD_CHECK_LOGIN();
		return ftpd_send_resp(ctrlfd,215);
	}

	static int ftpd_do_size(int ctrlfd,char* cmdline)
	{
       char* space = strchr(cmdline,' ');
	   struct stat st;
	   FTPD_CHECK_LOGIN();
	   if( !space || lstat(space + 1,&st) < 0){
		   FTPD_DEBUG("SIZE cmd err:%s/n",cmdline);
		   return ftpd_send_resp(ctrlfd,550);
	   }
	   return ftpd_send_resp(ctrlfd,213,st.st_size);
	}

	static int ftpd_do_dele(int ctrlfd,char* cmdline)
	{
		char* space = strchr(cmdline,' ');
		struct stat st;
		FTPD_CHECK_LOGIN();
		if(!space || lstat(space + 1,&st) < 0 || remove(space + 1) < 0){
			FTPD_DEBUG("DELE cmd err:%s/n",cmdline);
			return ftpd_send_resp(ctrlfd,550);
		}
		return ftpd_send_resp(ctrlfd,200);
	}

	static int ftpd_do_type(int ctrlfd,char* cmdline)
	{
        FTPD_CHECK_LOGIN();
		return ftpd_send_resp(ctrlfd,200);
	}
/*
* Parse PORT cmd and fetch the ip and port, 
* and both in network byte order.
*/

static int ftpd_get_port_mode_ipport(char* cmdline,unsigned int * ip,unsigned short * port)
	{
	char* cp = strchr(cmdline,' ');
    int i;
	unsigned char buf[6];
	if(!cp)
		return FTPD_ERR;
    for(cp++,i = 0;i < FTPD_ARR_LEN(buf);i++)
		{
		buf[i] = atoi(cp);
		cp = strchr(cp,',');
		if(!cp && i < FTPD_ARR_LEN(buf) - 1)
			return FTPD_ERR;
		cp++;
		}
		if(ip)
			*ip = *(unsigned int *)&buf[0];
		if(port)
			*port = *(unsigned short*)&buf[4];
        return FTPD_OK;
	}
/*
* Ftp client shipped with Windows XP uses PORT
* mode as default to connect ftp server.
*/

static int ftpd_do_port(int ctrlfd,char* cmdline)
{
	unsigned int ip;
	unsigned short port;
	struct sockaddr_in sin;
	FTPD_CHECK_LOGIN();
	
	if(ftpd_get_port_mode_ipport(cmdline,&ip,&port) != FTPD_OK){
		FTPD_DEBUG("ftpd_get_port_mode_ipport() failed %s","/n");
		goto err_label;
	}
	memset(&sin,0,sizeof(sin));
	sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = ip;
	sin.sin_port = port;
	FTPD_DEBUG("PORT cmd:%s:%d/n",inet_ntoa(sin.sin_addr),ntohs(sin.sin_port));
	
	if(ftpd_port_connfd >= 0){
		close(ftpd_port_connfd);
		ftpd_port_connfd = -1;
	}
	
	ftpd_port_connfd = socket(AF_INET,SOCK_STREAM,0);
	
	if(ftpd_port_connfd < 0){
		FTPD_DEBUG("socket()failed:%s/n",strerror(errno));
		goto err_label;
	}
	
	if(connect(ftpd_port_connfd,(struct sockaddr*)&sin,sizeof(sin)) < 0){
		FTPD_DEBUG("bind()failed: %s:/n",strerror(errno));
		goto err_label;
	}
 
    FTPD_DEBUG("PORT mode connect OK %s","/n");
	return ftpd_send_resp(ctrlfd,200);

	err_label:
		if(ftpd_port_connfd >= 0){
		close(ftpd_port_connfd);
		ftpd_port_connfd = -1;
	}
	return ftpd_send_resp(ctrlfd,550);
}

static int ftpd_do_pasv(int ctrlfd,char* cmdline)
{
    struct sockaddr_in pasvaddr;
	int len;
	unsigned int ip;
	unsigned short port;

	FTPD_CHECK_LOGIN();

	if(ftpd_pasv_fd >= 0){
		close(ftpd_pasv_fd);
        ftpd_pasv_fd = -1;
	}
	ftpd_pasv_fd = socket(AF_INET,SOCK_STREAM,0);
	if(ftpd_pasv_fd < 0){
		FTPD_DEBUG("socket()failed:%s/n",strerror(errno));
		return ftpd_send_resp(crelfd,550);
	}
         /* 
         * must bind to the same interface as ctrl connectin came from.
         */
     len = sizeof(pasvaddr);
	 getsockname(ctrlfd,(struct sockaddr*)&pasvaddr,&len);
	 pasvaddr.sin_port = 0;

	 if(bind(ftpd_psv_fd,(struct sockaddr*)&pasvaddr,sizeof(pasvaddr)) < 0){
		 FTPD_DEBUG("bind() failed %s/n",strerror(errno));
		 close(ftpd_pasv_fd);
		 ftpd_pasv_fd = -1;
		 return ftpd_send_resp(ctrlfd,550);
	 }
	 
	 if(listen(ftpd_psav_fd,FTPD_LISTEN_QU_LEN) < 0){
		 FTPD_DEBUG("listen()failed:%s/n",strerror(errno));
		 close(ftpd_pasv_fd);
		 ftpd_pasv_fd = -1;
		 return ftpd_send_resp(ctrlfd,550);
	 }

	 len = sizeof(pasvaddr);
	 getsockname(ftpd_pasv_fd,(struct sockaddr*)&pasvaddr,&len);
	 ip = ntohl(pasvaddr.sin_addr.s_addr);
	 port = ntohs(pasvaddr.sin_port);
	 FTPD_DEBUG("local bind:%s:%d/n",inet_ntoa(pasvaddr.sin_addr),port);
        /* 
         * write local ip/port into response msg
         * and send to client.
         */
     return ftpd_send_resp(ctrlfd,227,(ip>>24)&0xff,(ip>>16)&0xff,(ip>>8)&0xff,ip&0xff,(port>>8)&0xff,port&0xff);
}

static int ftpd_do_retr(int ctrlfd,char* cmdline)
{
      char buf[BUFSIZ];
	  char* space = srtchr(cmdline,' ');
	  struct stat st;
	  int fd = -1,n;
	  int connfd;
	  FTPD_CHECK_LOGIN();
	  if(!space || lstat(space + 1,&st) < 0){
		  FTPD_DEBUG("RETR cmd err:%s/n",cmdline);
		  goto err_label;
	  }

      if((connfd = ftpd_get_connfd()) < 0){
		  FTPD_DEBUG("ftpd_get_connfd()failed %s","/n");
		  goto err_label;
	  }

	  ftpd_send_resp(ctrlfd,150);
/* begin to read file and write it to conn socket */
      if((fd = open(space + 1,O_RDONLY)) < 0){
		  FTPD_DEBUG("open()failed:%s/n",strerror(error));
		  goto err_label;
	  }
      while(1){
		  if((n = read(fd,buf,sizeof(buf))) < 0){
			  if(errno == EINTR)
				  continue ;
			  FTPD_DEBUG("read()failed:%s/n",strerror(errno));
			  goto err_label;
		  }
		  if(n == 0)
			  break;
		  if(ftpd_send_msg(connfd,buf,n) != n){
			  FTPD_DEBUG("ftpd_send_msg()failed:%s/n",strerror(errno));
			  goto err_label;
		  }
	  }
	  FTPD_DEBUG("RETR(%s)OK/n",space + 1);
	  if(fd >= 0)
		  close(fd);
	  ftpd_close_all_fd();
	  return ftpd_send_resp(ctrlfd,226);
      
	  err_label:
		  if(fd >= 0)
		  close(fd);
	  ftpd_clsoe_all_fd();
	  return ftpd_send_resp(ctrlfd,550);
}

static int ftpd_do_sort(int ctrlfd,char* cmdline)
{
     char buf[BUFSIZ];
	 char* space = strchr(cmdline,' ');
	 struct stat st;
     int fd = -1,n;
	 int left,off;
	 int connfd;
	 FTPD_CHECK_LOGIN();
//Should add some permission control mechanism here.
     if(!space || lstat(space + 1,&st) == 0){
		 FTPD_DEBUG("STOR cmd err:%s/n",cmdline);
		 goto err_label;
	 }

	 if((connfd = ftpd_get_connfd()) < 0){
		 FTPD_DEBUG("ftpd_get_connfd()failed%s","/n");
		 goto err_label;
	 }

     if((fd = open(space + 1,O_WRONLY | O_CREAT | O_TRUNC,0600)) < 0){
		 DEBUG("open()failed:%s/n",strerror(errno));
		 goto err_label;
	 }
/* begin to read data from socket and wirte to disk file */
     while(1){
		 if((n = ftpd_recv_msg(connfd,buf,sizeof(buf))) < 0){
			 FTPD_DEBUG("ftpd_recv_msg()failed%s:/n",strerror(errno));
			 goto err_label;
		 }
		 if(n == 0)
			 break;
		 left = n;
		 off = 0;
		 while(left > 0){
			 int nwrite;
			 if((nwrite = write(fd,buf + off,left)) < 0){
				 if(errno == EINTR)
					 continue ;
				 FTPD_DEBUG("write()failed:%s/n",strerror(errno));
				 goto err_label;
			 }
			 off += nwrite;
			 left -= nwrite;
		 }
	 }
	 FTPD_DEBUG("STOR(%s)OK/n",space + 1);
	 if(fd >= 0)
		 close(fd);
	 ftpd_close_all_fd();
	 sync();
	 return ftpd_send_resp(ctrlfd,226);

	 err_label:
		 if(fd >= 0){
		 close(fd);
		 unlink(space + 1);
	 }
	 ftpd_close_all_fd();
     return ftpd_send_resp(ctrlfd,550);
}

static int ftpd_do_quit(int ctrlfd,char* cmdline)
{
	ftpd_send_resp(ctrlfd,221);
	ftpd_quit_flag = 1;
	return FTPD_OK;
}

static int ftpd_do_request(int ctrlfd,char buf[])
{
	char* end = &buf[strlen(buf) - 1];
	char* space = strchr(buf,' ');
	int i;
	char save;
	int err;
	if(*end == '/n' || *end == '/r'){
         ftpd_trim_lineend(buf);  //this is a valid ftp request.
         if(!space)
			 space = &buf[strlen(buf)];
		 save = *space;
		 *space = '/0';
		 for(i = 0;ftpd_cmds[i].cmd_name;i++)
		{
			 if(strcmp(buf,ftpd_cmds[i].cmd_name) == 0){
				 *space = save;
				 FTPD_DEBUG("recved a valid ftp cmd:%s/n",buf);
                 return ftpd_cmds[i].cmd_handler(ctrlfd,buf);
			 }
		}
		*space = save;  //unrecognized cmd
		FTPD_DEBUG("recved a unsupported ftp cmd:%s/n",buf);
		*space = '/0';
		err = ftpd_send_resp(ctrlfd,500,buf);
		*space = save;
		return err;
	}
	FTPD_DEBUG("recved a invalid ftp cmd: %s/n",buf);
        /* 
         * Even if it's a invalid cmd, we should also send  
         * back a response to prevent client from blocking.
         */
     return ftpd_send_resp(ctrlfd,550);
}
 static int ftpd_ctrl_conn_handler(int connfd)
{
      char buf[BUFSIZ];
	  int buflen;
	  int err = FTPD_OK;
         /* 
         * Control connection has set up,
         * we can send out the first ftp msg.
         */
      if(ftpd_send_resp(connfd,220) != FTPD_OK){
		  close(connfd);
		  FTPD_DEBUG("Close the ctrl connection OK%s","/n");
		  return FTPD_ERR;
	  }
         /*
         * Begin to interact with client and one should implement
         * a state machine here for full compatibility. But we only
         * show a demonstration ftp server and i do my best to 
         * simplify it. Base on this skeleton, you can write a
         * full-funtional ftp server if you like. ;-)
         */
        while(1){
			buflen = ftpd_recv_msg(connfd,buf,sizeof(buf));
			if(buflen < 0){
				FTPD_DEBUG("ftpd_recv_msg()failed:%s/n",strerror(errno));
				err = FTPD_ERR;
				break;
			}
			if(buflen == 0)
				break ;/* this means client launch a active close */
			buf[buflen] = '/0';
            ftpd_do_request(connfd,buf);
                /* 
                 * The negative return value from cuftpd_do_request 
                 * should not cause the breaking of ctrl connection.
                 * Only when the client send QUIT cmd, we exit and
                 * launch a active close.
                 */
             if(ftpd_quit_flag)
				 break;
		}
		close(connfd);
		FTPD_DEBUG("close the ctrl connection OK %s","/n");
		return err;
}

static int ftpd_do_loop(int listenfd)
{
	int connfd;
	int pid;
	while(1){
		FTPD_DEBUG("Server ready ,wait client's connection...%s","/n");
		connfd = accept(listen,NULL,NULL);
		if(connfd < 0){
			FTPD_DEBUG("accept() failed:%s/n",strerror(errno));
            continue ;
		}
		if(ftpd_debug_on){
			struct sockaddr_in cliaddr;
			int len = sizeof(cliaddr);
			getpeername(connfd,(struct sockaddr*)&cliaddr,&len);
			FTPD_DEBUG("accept a conn from %s:%d/n",inet_ntoa(cliaddr.sin_addr),ntohs(cliaddr.sin_port));
		}
		if((pid = fork()) < 0){
			FTPD_DEBUG("fork() failed:%s/n",strerror(errno));
            close(connfd);
			continue ;
		}
		if(pid > 0){
			close(connfd);//parent
			ftpd_cur_chlid_num++;
			continue ;
		}
         close(listenfd);//child
		 signal(SIGCHLD,SIG_IGN);
		 if(ftpd_ctrl_conn_handler(connfd) != FTPD_OK)
			 exit(-1);
		 exit(0);
	}
	return FTPD_OK;
}

 int main(int argc,char* argv[])
	{
	  int listenfd;
	  ftpd_parse_args(argc,argv);
	  ftpd_init();
	  listenfd = ftpd_create_srv();
	  if(listenfd >= 0)
		  ftpd_do_loop(listenfd);
	  exit(0);
	}