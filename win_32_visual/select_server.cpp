#include <winsock2.h>
#pragma comment(lib,"WS2_32")
#include <stdio.h>
#include <cstring>

class CInitSock
{
public:
	CInitSock(BYTE minorVer = 2,BYTE majorVer = 2)
	{
		WSADATA wsaData;
		WORD sockVersion = MAKEWORD(minorVer,majorVer);
		if(WSAStartup(sockVersion,&wsaData) != 0){
			exit(0);
		}
	}
	~CInitSock()
	{
		WSACleanup();
	}
};

CInitSock theSock;

int main()
{
	USHORT nPort = 8789;
	SOCKET sListen,sRead;
	fd_set fdSocket;
	int nRet,i,Addrlen,nRead;
	char sText[256];
	sListen = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	sockaddr_in sin,addrRemote;
	memset(&sin,0,sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_port = htons(nPort);
	sin.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	if(bind(sListen,(sockaddr * )&sin,sizeof(sin)) == SOCKET_ERROR){
		printf("Failed bind\n");
		return -1;
	}
	listen(sListen,20);
	FD_ZERO(&fdSocket);
	FD_SET(sListen,&fdSocket);
	while(TRUE)
	{
		fd_set fdRead = fdSocket;
		nRet = select(0,&fdRead,NULL,NULL,NULL);
		if(nRet > 0){
			for(i = 0;i <(int)fdSocket.fd_count;i++)
			{
				if(FD_ISSET(fdSocket.fd_array[i],&fdRead)){
					if(fdSocket.fd_array[i] == sListen){
						if(fdSocket.fd_count < FD_SETSIZE){
						     Addrlen = sizeof(addrRemote);
							 sRead = accept(sListen,(SOCKADDR *)&addrRemote,&Addrlen);
							 FD_SET(sRead,&fdSocket);
							 printf("接受到连接(%s)\n",inet_ntoa(addrRemote.sin_addr));
						}
						else {
							printf("too much connections!\n");
							continue;
						}
					}
					else{
						nRead = recv(fdSocket.fd_array[i],sText,strlen(sText),0);
						if(nRead > 0){
							sText[nRead] = '\0';
							printf("接收到数据:%s\n",sText);
						}
						else {
							closesocket(fdSocket.fd_array[i]);
							FD_CLR(fdSocket.fd_array[i],&fdSocket);
						}
						send(fdSocket.fd_array[i],sText,strlen(sText),0);
					}
				}
			}
		}else {
			printf("Failed select()\n");
			break;
		}
	}
	return 0;
}


		







