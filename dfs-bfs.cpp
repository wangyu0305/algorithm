#include <stdio.h>
#include <malloc.h>
#define Maxsize 100

intdef struct //构造邻接矩阵
{
	int vertex[Maxsize]; //存放图中的顶点的信息
	int arc[Maxsize][Maxsize];//存放图中的边的信息
	int vertexNum,arcNum;      //图的顶点数和边数
}MGraph;

int visited[Maxsize];           //全局数组，初始为0表示所有顶点尚未访问 


void DFSTraverse(MGraph *G,int v)    //深度优先遍历算法（邻接表、递归）
{                                    //v是初始顶点编号 
	G=(MGraph *)malloc(sizeof(MGraph));                                
	int j=0;
	G->vertex[v]=v;                
	printf("%d",G->vertex[v]);visited[v]=1;//设置已访问标志 
	for(j=0;j<G->vertexNum;j++)
		if(G->arc[v][j]==1 && visited[j]==0) DFSTraverse(G,j); 
} 


void DFSTraverse1(MGraph *G,int v)    //深度优先遍历算法（邻接表、非递归）
{                                    //标志数组visited[n]已经初始化为0 
	G=(MGraph *)malloc(sizeof(MGraph));                                    
	int S[Maxsize]; 
	int i=0,j=0;                     
	int top=-1;
	G->vertex[v]=v;                        //初始化顺序栈S，假设不会发生溢出
	printf("%d",G->vertex[v]);visited[v]=1; S[++top]=v;
	while(top!=-1)
	{
		v=S[top];                    //注意不出栈 
		for(j=0;j<G->vertexNum;j++)
			if(G->arc[v][j]==1 && visited[j]==0)
			{
				printf("%d",(*G).vertex[j]); visited[j]=1; S[++top]=j;
				break;
			}
			if(j==G->vertexNum) top--;//顶点j没有未曾访问的邻接点
	}
}

//void main()
//{
//	int i,j,v;
//	MGraph *g;
//	int Graph[5][5]={
//		{0,1,0,1,1},
//		{1,0,1,1,0},
//		{0,1,0,1,1},
//		{1,1,1,0,1},
//		{1,0,1,1,0},};
//		g->vertexNum=5;g->arcNum=7;
//		for (i=0;i<g->vertexNum;i++)    
//			for (j=0;j<g->vertexNum;j++)
//				g->arc[i][j]=Graph[i][j];
//		printf("从顶点0开始的DFS(递归算法):\n");
//		DFSTraverse(g,0);printf("\n");
//		visited[v]=0;
//		printf("从顶点0开始的DFS(非递归算法):\n");
//		DFSTraverse1(g,0);     
//} 

#include <stdio.h>
unsigned hamdist(unsigned x, unsigned y)
{
	unsigned dist = 0, val = x ^ y;

	// Count the number of set bits
	while(val)
	{
		++dist; 
		val &= val - 1;
	}

	return dist;
}

int main()
{
	unsigned x=11211;
	unsigned y=11201;
	unsigned z=hamdist(x,y);
	printf("%d",z);
}


//**************************计算海明距离*************************************//


//#include <stdio.h>
//
//unsigned haiming(char x,char y)
//{
//	unsigned dist = 0;
//	char val = x ^ y;
//	while (val)
//	{
//		++dist;
//		val &= val - 1;
//	}
//
//	return dist;
//}
//
//int main(int argc,char *argv[])
//{
//  
//	char* x ;
//	x =  "uuuua";
//	char* y ;
//	y =  "uuuub";
//	char a;
//	char b;
//	unsigned u;
//	unsigned c = 0;
//	while (*x || *y)
//	{
//		a = *x;
//		b = *y;
//		u = haiming(a,b);
//        if (u == 0)
//        {
//			++x;
//			++y;
//        }
//		else
//		{
//			c += u;
//			++x;
//			++y;
//		}
//	}
//	
//	printf("%d",c);
//}


//*********************************快排*****************************//
#include "quicksort.h"

int q_main(int argc,char *argv[])
{
	int i = 0,m,j,n;
	int *pi;
	char a;
	int x[100];

	printf("input number of data : exp 2 5 6 8 enter\n");
	while(scanf("%d",&x[i++]),(a = getchar()) != '\n')
		;
	pi = x;
	//x[i+1] = '\0';
	for(j = 0;j < i; j++){
		printf("%d,",pi[j]);
	}

	printf("\nx :%d",sizeof(x));

	printf("\npi :%d",sizeof(pi));

	m = sizeof(pi)/sizeof(*pi);

	n = sizeof(x)/sizeof(*x);

	printf("\nm %d",m);
	printf("\nn %d\n",n);
    
	show(x, m, 0, i - 1);

	m = i - 1;

	quicksort(x, i, 0, m);

	for(j = 0;j < i;j++)
	{
		printf("x[%d] %d\t,",j,x[j]);
	}

	getchar();

	return 0;
}

