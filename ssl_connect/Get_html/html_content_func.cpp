/************************************************************************/
/* 对获取的html内容做处理       解析htm                                 */
/************************************************************************/

#include <cstring>
#include <string>
#include "html_content_func.h"

using namespace std;

__int64 parse_number(const unsigned char *num)         /*字符转数字 atoll atof*/
{
	double n=0,sign=1,scale=0;
	int subscale=0,signsubscale=1;

	// Could use sscanf for this?
	if (*num=='-') 
	{
		sign=-1;
		num++;    // Has sign?
	}

	if (*num=='0') 
	{
		num++;            // is zero
	}

	if (*num>='1' && *num<='9')    
	{
		do    
		{
			n=(n*10.0)+(*num++ -'0');
		}
		while (*num>='0' && *num<='9');    // Number?
	}

	if (*num=='.') 
	{
		num++;        
		do    
		{
			n=(n*10.0)+(*num++ -'0');
			scale--;
		}
		while (*num>='0' && *num<='9');
	}    // Fractional part?

	return (__int64)n;
}


int con_get_next_field(int pos,string *context,string *tag,int *begin,int *end)      //获取 <  > 标签的位置 并得到< >之间的tag
{
	char* p_char;
	int index;
	int b_tag;
	int b_find;

	*tag = "";               //tag 清零

	*begin = -1;
	*end = -1;
	b_tag = false;
	b_find = false;

	if(pos < 0 || pos >= context->length())
	{
		return false;
	}

	p_char = (char*)context->c_str();
	p_char += pos;
	index = pos;
	while(*p_char && index < context->length())
	{
		switch(*p_char)
		{
		case '<':
			*begin = index;
			b_tag = false;
			break;
		case '>':
			*end = index;
			if(*begin >= 0 && *end >= 0 && *end > *begin)
			{
				*tag = context->substr(*begin+1,*end-*begin-1);
				b_tag = true;
			}
			break;
		case '=':
			if(b_tag)
			{
				*end = index;
				b_find = true;
			}
			break;
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			break;
		default:
			if(b_tag)
			{
				b_tag = false;
				*begin = -1;
				*end = -1;
			}
			break;
		}
		if(b_find)
		{
			return true;
		}
		p_char ++;
		index ++;
	}

	return false;
}

void con_cfg_get_name(soc_cfg_t *cfg,string *name,string *val)
{
	if(name->compare("send_timeout") == 0)
	{
		cfg->send_timeout = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if (name->compare("recv_timeout") == 0)
	{
		cfg->recv_timeout = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if (name->compare("connect_retries") == 0)
	{
		cfg->connect_retries = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if (name->compare("connect_timeout") == 0)
	{
		cfg->connect_timeout = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if (name->compare("max_connect") == 0)
	{
		cfg->max_connect = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if (name->compare("max_pthread") == 0)
	{
		cfg->max_pthread = (int)parse_number((const unsigned char*)val->c_str());
	}
	else if(name->compare("port") == 0)
	{
		cfg->port = (int)parse_number((const unsigned char*)val->c_str());
	}
}

void con_cfg_get_param(soc_cfg_t *cfg,string *context)
{
	int pos,begin,end;
	string* tag = NULL;
	string* name = NULL;
	string val;

	pos = 0;
	while(con_get_next_field(pos,context,tag,&begin,&end))
	{
		val = context->substr(pos,begin-pos);

		if(name != NULL && name->length() > 0)
		{
			con_cfg_get_name(cfg,name,&val);
		}
		name = tag;
		pos = end + 1;
	}

	begin = context->find("\r\n",pos);
	if(begin != (int)context->npos)
	{
		val = context->substr(pos,begin-pos);
	}
	else
	{
		val = context->substr(pos,context->length());
	}

	if(name != NULL && name->length() > 0)
	{
		con_cfg_get_name(cfg,name,&val);
	}

}


int stk_cfg_get_next_val(int *pos,string *content,string *val)
{
	char* p_char;
	int index;
	int begin;
	int end;
	int b_quat1;
	int b_quat2;
	int b_char;

	begin = -1;
	end = -1;
	b_quat1 = false;
	b_quat2 = false;
	b_char = false;

	*val = "";

	if(*pos < 0 || *pos >= content->length())
	{
		return false;
	}

	p_char = (char*)content->c_str();
	p_char += *pos;
	index = *pos;
	while(*p_char && index < content->length())
	{
		switch(*p_char)
		{
		case ';':
			if(b_char || (b_quat1 && b_quat2) || (!b_char && !b_quat1))
			{
				if(!b_quat2)
				{
					end = index;
				}
				if(end >= 0 && begin >= 0 && end > begin)
				{
					*val = content->substr(begin,end-begin);
				}

				index ++;
				*pos = index;

				return true;
			}
			break;
		case '\"':
			if(b_quat1)
			{
				b_quat2 = true;
				end = index;
			}
			if(!b_char)
			{
				b_quat1 = true;
			}
			break;
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			break;
		default:
			if(!b_quat1)
			{
				b_char = true;
			}
			b_quat2 = false;
			if(begin == -1)
			{
				begin = index;
			}
			break;
		}
		p_char ++;
		index ++;
	}

	if(end == -1)
	{
		end = index;
	}
	if(end >= 0 && begin >= 0 && end > begin)
	{
		*val = content->substr(begin,end-begin);
	}

	*pos = content->length();

	return true;
}

int stk_cfg_get_next_tag(int npos,string* context,string* strtag,int* rtag,int* nbegin,int* nend)
{
	char* pchar;
	int ncount;
	int index;
	int nchar;
	int nbar;
	int bfind;
	int bquat;
	int squat;
	int dquat;

	*rtag = false;
	*strtag = "";
	ncount = context->length();
	nchar = -1;
	nbar = -1;
	*nbegin = -1;
	*nend = -1;
	bfind = false;
	bquat = false;
	squat = false;
	dquat = false;

	if(npos < 0 || npos >= ncount)
	{
		return false;
	}

	pchar = (char*)context->c_str();
	pchar += npos;
	index = npos;
	while(*pchar && index < ncount)
	{
		switch(*pchar)
		{
		case '<':
			if(!bquat)
			{
				*nbegin = index;
				*rtag = false;
				nchar = -1;
				nbar = -1;
			}
			else if(*nbegin == -1)
			{
				*nbegin = index;
				*rtag = false;
				nchar = -1;
				nbar = -1;
				bquat = false;
				squat = false;
				dquat = false;
			}
			break;
		case '>':
			*nend = index;
			if(*nbegin >= 0 && *nend >= 0 && *nend > *nbegin)
			{
				if(nchar >= 0)
				{
					if(nbar >= 0)
					{
						*strtag = context->substr(nchar,nbar-nchar);
					}
					else
					{
						*strtag = context->substr(nchar,(*nend)-nchar);
					}
				}
				bfind = true;
			}
			break;
		case '\'':
			if(!dquat)
			{
				squat = !squat;
				bquat = squat;
			}
			break;
		case '\"':
			if(!squat)
			{
				dquat = !dquat;
				bquat = dquat;
			}
			break;
		case ' ':
		case '\t':
		case '\r':
		case '\n':
			if(*nbegin >= 0 && nchar >= 0 && nbar == -1)
			{
				nbar = index;
			}
			break;
		case '/':
			if(!bquat)
			{
				if(*nbegin >= 0)
				{
					if(nchar == -1)
					{
						*rtag = true;
					}
					else if(nbar == -1)
					{
						nbar = index;
					}
				}
			}
			break;
		default:
			if(!bquat)
			{
				if(*nbegin >= 0 && nchar == -1)
				{
					nchar = index;
				}
			}
			break;
		}
		if(bfind)
		{
			return true;
		}
		pchar ++;
		index ++;
	}

	return false;
}

int stk_cfg_get_next_key(int* npos,string* content,string* strkey,string* strvalue)
{
	char* pchar;
	int ncount;
	int index;
	int nequal;
	int bquat;
	int squat;
	int dquat;

	*strkey = "";
	*strvalue = "";

	ncount = content->length();
	nequal = -1;
	bquat = false;
	squat = false;
	dquat = false;

	if(*npos < 0 || *npos >= ncount)
	{
		return false;
	}

	pchar = (char*)content->c_str();
	pchar += *npos;
	index = *npos;
	while(*pchar && index < ncount)
	{
		switch(*pchar)
		{
		case '=':
			if(!bquat && nequal < 0)
			{
				nequal = index;
			}
			break;
		case ' ':
		case ';':
		case '\t':
		case '\r':
		case '\n':
			if(!bquat && nequal >= 0)
			{
				*strkey = content->substr(*npos,nequal-(*npos));
				*strvalue = content->substr(nequal+1,index-nequal-1);

				index ++;
				*npos = index;

				return true;
			}
			break;
		case '\'':
			if(!dquat)
			{
				squat = !squat;
				bquat = squat;
			}
			break;
		case '\"':
			dquat = !dquat;
			bquat = dquat;
			squat = false;
			break;
		default:
			break;
		}

		pchar ++;
		index ++;
	}

	if(nequal >= 0)
	{
		*strkey = content->substr(*npos,nequal-(*npos));
		*strvalue = content->substr(nequal+1,content->length());
	}

	*npos = ncount;

	return true;
}