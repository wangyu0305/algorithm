

#include <Windows.h>
#include <stdio.h>
#include <string>
#include <errno.h>

#pragma comment(lib,"wsock32.lib")

using std::string;

void soc_start()
{
	WSADATA wsaData;
	int iResult=WSAStartup(MAKEWORD(2,2),&wsaData);
	if(iResult!=NO_ERROR)
		printf("Error at WSAStartup()\n");
}

void soc_finish()
{
	WSACleanup();
}

/**       TCP     **/

SOCKET soc_init()                          //tcp_connect_init
{
	SOCKET sock;
	sock = socket(AF_INET,SOCK_STREAM,0);
	if (sock == -1)
	{
		fprintf(stderr, "Socket Error:%s\a\n", strerror(errno));
		exit(1);
	}

	return sock;
}

void soc_term(SOCKET sock)
{
	closesocket(sock);
}

SOCKET soc_listen(SOCKET sock,int port,int max_listen)             //tcp  bind  listen
{
	sockaddr_in s_addrin;

	memset(&s_addrin,0,sizeof(s_addrin));
	s_addrin.sin_family = AF_INET;
	s_addrin.sin_port = htons(port);
	s_addrin.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	if (bind(sock,(sockaddr*)&s_addrin,sizeof(s_addrin)) == SOCKET_ERROR)
	{
		printf("bind failed!\n");
		return -1;
	}

	listen(sock,max_listen);

	return sock;
}

SOCKET soc_accept(SOCKET sock,int port,int max_listen)
{
	SOCKET sock_accept;
	sockaddr_in s_addrRemote;
	int remoteAddrlen;

	remoteAddrlen = sizeof(s_addrRemote);

	sock_accept = accept(sock,(sockaddr*)&s_addrRemote,&remoteAddrlen);
	printf("接受到连接(%s)\n",inet_ntoa(s_addrRemote.sin_addr));

	return sock_accept;
}

SOCKET soc_connect(SOCKET sock,int port,char* remoteIp)
{
	sockaddr_in s_addrRemote;

	memset(&s_addrRemote,0,sizeof(s_addrRemote));
	s_addrRemote.sin_family = AF_INET;
	s_addrRemote.sin_port = htons(port);
	s_addrRemote.sin_addr.S_un.S_addr = inet_addr(remoteIp);

	if (connect(sock,(sockaddr*)&s_addrRemote,sizeof(s_addrRemote)) == SOCKET_ERROR)
	{
		printf("Failed to connect(%d)\n",WSAGetLastError());
		return -1;
	}

	return sock;
}

string soc_recv(SOCKET sock)
{
	char recvstr[1024] = {0};
	string recvstring;
	int recvlen = 0;

	while (1)
	{
		recvlen = recv(sock,recvstr,1,0);
		if (recvlen <= 0)
		{
			break;
		}
		recvstring = recvstring + recvstr;
	}
	
	return recvstring;
}

void soc_send(SOCKET sock,string sendstring)
{
	char* sendstr;
	int stringlen = sendstring.size();
	int sendlen = 0;
	sendstr = (char*)sendstring.c_str();

	do 
	{
		sendlen = send(sock,sendstr,stringlen,0);
		stringlen -= sendlen;
		sendstr += sendlen;
	} while (stringlen > 0);
}


/**   UDP  **/

SOCKET soc_init_udp()
{
	SOCKET sock;
	sock = socket(AF_INET,SOCK_DGRAM,0);
	if (sock == -1)
	{
		fprintf(stderr, "Socket Error:%s\a\n", strerror(errno));
		exit(1);
	}

	return sock;
}

void soc_term_udp(SOCKET sock)
{
	closesocket(sock);
}

SOCKET soc_bind_udp(SOCKET sock,int port)           //udp server
{
	sockaddr_in s_addrin;

	memset(&s_addrin,0,sizeof(s_addrin));
	s_addrin.sin_family = AF_INET;
	s_addrin.sin_port = htons(port);
	s_addrin.sin_addr.S_un.S_addr = htonl(INADDR_ANY);

	if (bind(sock,(sockaddr*)&s_addrin,sizeof(s_addrin)) == SOCKET_ERROR)
	{
		printf("bind failed!\n");
		return -1;
	}

	return sock;
}

string soc_recvfrom_udp(SOCKET sock,string recvstring,string remoteIp)
{
	sockaddr_in s_addrremote;
	int remoteAddrlen;
	int recvlen = 0;
	remoteAddrlen = sizeof(sockaddr);
	char recvstr[1024] = {0};

	while (1)
	{
		recvlen = recvfrom(sock,recvstr,1,0,(sockaddr*)&s_addrremote,&remoteAddrlen);
		if (recvlen <= 0)
		{
			break;
		}
		recvstring = recvstring + recvstr;
	}

	remoteIp = remoteIp + inet_ntoa(s_addrremote.sin_addr);
	
    return recvstring;
}

void soc_sendto_udp(SOCKET sock,string sendstring,string remoteIp,int port)
{
	char* sendstr;
	char* remIp;
	sendstr  = (char*)sendstring.c_str();
	remIp  = (char*)remoteIp.c_str();

	SOCKADDR_IN addrin;
	addrin.sin_addr.S_un.S_addr = inet_addr(remIp);
	addrin.sin_family = AF_INET;
	addrin.sin_port=htons(port);

	sendto(sock,sendstr,strlen(sendstr)+1,0,(sockaddr*)&addrin,sizeof(SOCKADDR));

}