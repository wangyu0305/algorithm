/************************************************************************/
/*  对于URL信息的处理 得出主机名　IP　端口                             */
/*  获取得到的html内容存放的地址等                                     */
/************************************************************************/

#ifndef __URL_PARSE_H
#define __URL_PARSE_H

#define _CRT_SECURE_NO_DEPRECATE 
#ifdef __cplusplus
extern "C" {
#endif

	void url_get_host(char *url, char *web, char *filePath, int *port);
	char* save_filename(char* filepath);
	char* url_get_ip(char* web);

#ifdef __cplusplus
}
#endif

#endif          // end __URL_PARSE_H