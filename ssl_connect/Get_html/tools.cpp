#include <Windows.h>

char *con_strstr(const char *s1, const char *s2)            //�ַ����Ƚ�
{ 
	int n; 
	if (*s2) 
	{ 
		while (*s1) 
		{ 
			for (n=0; *(s1 + n) == *(s2 + n); n++) 
			{ 
				if (!*(s2 + n + 1)) 
					return (char *)s1; 
			} 
			s1++; 
		} 
		return NULL; 
	} 
	else 
		return (char *)s1; 
}

unsigned int iptrans_char_to_int(char* ipaddr)
{
	unsigned int ipinfo = 0,temp;
	int point = 0;
	byte ipA,ipB,ipC,ipD,num = 0;
	if(ipaddr == NULL)
		return 0;
	if (strcmp(ipaddr,"localhost") == 0)
	{
		ipA = 127;
		ipB = 0;
		ipC = 0;
		ipD = 1;
	}
	else
	{
		while (*ipaddr != '\0')
		{
			switch (*ipaddr)
			{
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				num = num*10 + (*ipaddr - '0');
				break;
			case '.':
				switch (point)
				{
				case 0:
					ipA = num;
					num = 0;
					break;
				case 1:
					ipB = num;
					num = 0;
					break;
				case 2:
					ipC = num;
					num = 0;
					break;
				}
				point++;
				break;
			}
			ipaddr++;
		}
		ipD	= num;
	}
	temp = ipA & 0xff;
	ipinfo |= (temp << 24);
	temp = ipB & 0xff;
	ipinfo |= (temp << 16);
	temp = ipC & 0xff;
	ipinfo |= (temp << 8);
	temp = ipD & 0xff;
	ipinfo |= (temp);

	return ipinfo;
}

void byte_to_char(byte ipinfo,char* ipchar,int* i)
{
	int m = 0;
	char tmpchar[24];
	do 
	{
		tmpchar[m] = (ipinfo % 10) + '0';
		ipinfo /= 10;
		m++;
	} while (ipinfo);
	do 
	{
		m--;
		ipchar[*i] = tmpchar[m];
		(*i)++;
	} while (m > 0);
}

void iptrans_int_to_char(unsigned int ipinfo,char* ip_addr_char)
{
	//static char ip_addr_char[17];
	int i = 0;
	unsigned int temp;
	byte ipA,ipB,ipC,ipD;
	temp = ipinfo;
	ipA = (byte)(temp >> 24);
	temp = ipinfo;
	ipB = (byte)((temp & 0x00ff0000) >> 16);
	temp = ipinfo;
	ipC = (byte)((temp & 0x0000ff00) >> 8);
	temp = ipinfo;
	ipD = (byte)(temp & 0x000000ff);

	byte_to_char(ipA,ip_addr_char,&i);
	ip_addr_char[i] = '.';
	i++;
	byte_to_char(ipB,ip_addr_char,&i);
	ip_addr_char[i] = '.';
	i++;
	byte_to_char(ipC,ip_addr_char,&i);
	ip_addr_char[i] = '.';
	i++;
	byte_to_char(ipD,ip_addr_char,&i);
	ip_addr_char[i] = '\0';
}