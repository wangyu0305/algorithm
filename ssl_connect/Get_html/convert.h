/************************************************************************/
/*            编码转换     Windows下unicode gb2312 utf8                 */
/*                                                                      */
/*        GB2312是中国规定的汉字编码,也可以说是简体中文的字符集编码;    */
/*        GBK 是 GB2312的扩展 ,除了兼容GB2312外,它还能显示繁体中文,     */
/*        还有日文的假名                                                */
/*                                                                      */
/************************************************************************/

#ifndef __CONVERT_H
#define __CONVERT_H

#include <string>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	static void UTF_8ToUnicode(wchar_t* pOut,char *pText);  // 把UTF-8转换成Unicode
	static void UnicodeToUTF_8(char* pOut,wchar_t* pText);  //Unicode 转换成UTF-8
	static void UnicodeToGB2312(char* pOut,wchar_t uData);  // 把Unicode 转换成 GB2312 
	static void Gb2312ToUnicode(wchar_t* pOut,char *gbBuffer);// GB2312 转换成　Unicode
	static void GB2312ToUTF_8(string& pOut,char *pText, int pLen);//GB2312 转为 UTF-8
	static void UTF_8ToGB2312(string &pOut, char *pText, int pLen);//UTF-8 转为 GB2312

	void UnicodeToGBK(char* pOut,wchar_t pText);
	void GBKToUnicode(wchar_t* pOut,char* gbk);
	void UnicodeToGB18030(char* pOut,wchar_t pText);
	void GB18030ToUnicode(wchar_t* pOut,char* gb18030);
	long UTF8ToGBK(const void * lpUTF8Str, string & str);
	long GBKToUTF8(const void* lpGBKstr,string& str);
	long GBKToGB18030(const void* lpGBKstr,string& str18030);
	long GB18030ToGBK(const void* lpGB18030str,string& gbk);

#ifdef __cplusplus
}
#endif

#endif          //__CONVERT_H
