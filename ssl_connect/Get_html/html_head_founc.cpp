

#include <stdio.h>
#include <string>

using namespace std;

string message_get_request(char* urlpath,char* addrip,int port,char* cookie)
{
	string getHead;
    char request[1024];

	sprintf(request, "GET /%s HTTP/1.1\r\nAccept: */*\r\nAccept-Language: zh-cn\r\n\
User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)\r\n\
Host: %s:%d\r\nConnection: Close\r\n", urlpath, addrip,
            port);

	getHead = getHead + request;
	getHead = getHead + "Cookie: " + cookie + "\r\n\r\n";
	
	return getHead;
}

string message_post_request(char* urlpath,char* addrip,int port,char* cookie,char* postData)
{
	string getHead;
    char request[1024];

	sprintf(request, "POST /%s HTTP/1.1\r\nAccept: */*\r\nAccept-Language: zh-cn\r\n\
User-Agent: Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)\r\n\
Host: %s:%d\r\nConnection: Close\r\n", urlpath, addrip,
            port);

	getHead = getHead + request;
	getHead = getHead + "Cookie: " + cookie + "\r\n\r\n";
	getHead = getHead + "\r\n";
	
	return getHead;
}

string get_response_head(string* content)
{
	string head;
	char* contentstr = (char*)content->c_str();
	int i = 0;

	while (*contentstr != NULL)
	{
		if (i < 4)
		{
			if (*contentstr == '\r' || *contentstr == '\n')
			{
				i++;
			}
			else
			{
				i = 0;
			}
			head = head + *contentstr;
			contentstr++;
		}
		else
		{
			*content = contentstr;
			break;
		}
	}

	return head;
}

string response_head_find(string head,char* findstr)
{
	string respstr;
    char* str;
	int findstrlen = strlen(findstr);
	int i = 0;

	string::size_type position;
	position = head.find(findstr);
    if (position != head.npos)
    {
		respstr = head.substr(position,head.length());
    }

	str = (char*)respstr.c_str();
	respstr.swap(string());
	if (str == NULL)
	{
		return NULL;
	}
	str = str + findstrlen;
	while (*str != NULL)
	{
		if (i < 2)
		{
			if (*str == '\r' || *str == '\n')
			{
				i++;
			}
			else
			{
				i = 0;
			}
			respstr = respstr + *str;
			str++;
		}
		else
		{
			break;
		}
	}

	return respstr;
}