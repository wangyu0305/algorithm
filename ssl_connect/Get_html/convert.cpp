/************************************************************************/
/*            编码转换     Windows下unicode gb2312 utf8                 */
/*                                                                      */
/*        GB2312是中国规定的汉字编码,也可以说是简体中文的字符集编码;    */
/*        GBK 是 GB2312的扩展 ,除了兼容GB2312外,它还能显示繁体中文,     */
/*        还有日文的假名                                                */
/*                                                                      */
/************************************************************************/

#include <string>
#include <iostream>
#include <Windows.h>
#include <locale>

using namespace std;

static void UTF_8ToUnicode(wchar_t* pOut,char *pText);  // 把UTF-8转换成Unicode
static void UnicodeToUTF_8(char* pOut,wchar_t* pText);  //Unicode 转换成UTF-8
static void UnicodeToGB2312(char* pOut,wchar_t uData);  // 把Unicode 转换成 GB2312 
static void Gb2312ToUnicode(wchar_t* pOut,char *gbBuffer);// GB2312 转换成　Unicode
static void GB2312ToUTF_8(string& pOut,char *pText, int pLen);//GB2312 转为 UTF-8
static void UTF_8ToGB2312(string &pOut, char *pText, int pLen);//UTF-8 转为 GB2312


void UTF_8ToUnicode(wchar_t* pOut,char *pText)
{
	char* uchar = (char *)pOut;

	uchar[1] = ((pText[0] & 0x0F) << 4) + ((pText[1] >> 2) & 0x0F);
	uchar[0] = ((pText[1] & 0x03) << 6) + (pText[2] & 0x3F);

	return;
}

void UnicodeToUTF_8(char* pOut,wchar_t* pText)       // 注意 WCHAR高低字的顺序,低字节在前，高字节在后
{
	char* pchar = (char *)pText;

	pOut[0] = (0xE0 | ((pchar[1] & 0xF0) >> 4));
	pOut[1] = (0x80 | ((pchar[1] & 0x0F) << 2)) + ((pchar[0] & 0xC0) >> 6);
	pOut[2] = (0x80 | (pchar[0] & 0x3F));

	return;
}

void UnicodeToGB2312(char* pOut,wchar_t uData)
{
	WideCharToMultiByte(CP_ACP,NULL,&uData,1,pOut,sizeof(wchar_t),NULL,NULL);
	
	return;
}

void Gb2312ToUnicode(wchar_t* pOut,char *gbBuffer)
{
	::MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED,gbBuffer,2,pOut,1);
	
	return ;
}

void UnicodeToGBK(char* pOut,wchar_t pText)
{
	int nRetlen = 0;
	nRetlen = ::WideCharToMultiByte(936,0,&pText,-1,NULL,NULL,NULL,NULL);      //使用 936 代表 GBK
	nRetlen = ::WideCharToMultiByte(936,0,&pText,-1,pOut,nRetlen,NULL,NULL);
}

void GBKToUnicode(wchar_t* pOut,char* gbk)
{
	int nRetlen = 0;
	nRetlen = ::MultiByteToWideChar(936,0,gbk,-1,NULL,NULL);
	nRetlen = ::MultiByteToWideChar(936,0,gbk,-1,pOut,nRetlen);
}

void UnicodeToGB18030(char* pOut,wchar_t pText)
{
	int nRetlen = 0;
	nRetlen = ::WideCharToMultiByte(54936,0,&pText,-1,NULL,NULL,NULL,NULL);          //使用 54936 代表 18030
	nRetlen = ::WideCharToMultiByte(54936,0,&pText,-1,pOut,nRetlen,NULL,NULL);
}

void GB18030ToUnicode(wchar_t* pOut,char* gb18030)
{
	int nRetlen = 0;
	nRetlen = ::MultiByteToWideChar(54936,0,gb18030,-1,NULL,NULL);
	nRetlen = ::MultiByteToWideChar(54936,0,gb18030,-1,pOut,nRetlen);
}

void GB2312ToUTF_8(string& pOut,char *pText, int pLen)
{
	char buf[4];
	int nLength = pLen* 3;
	char* rst = new char[nLength];

	memset(buf,0,4);
	memset(rst,0,nLength);

	int i = 0;
	int j = 0;      
	while(i < pLen)
	{
		//如果是英文直接复制就可以
		if( *(pText + i) >= 0)
		{
			rst[j++] = pText[i++];
		}
		else
		{
			wchar_t pbuffer;
			Gb2312ToUnicode(&pbuffer,pText+i);

			UnicodeToUTF_8(buf,&pbuffer);

			unsigned short int tmp = 0;
			tmp = rst[j] = buf[0];
			tmp = rst[j+1] = buf[1];
			tmp = rst[j+2] = buf[2];    

			j += 3;
			i += 2;
		}
	}
	rst[j] = '\0';

	//返回结果
	pOut = rst;             
	delete []rst;   

	return;
}

void UTF_8ToGB2312(string &pOut, char *pText, int pLen)
{
	char * newBuf = new char[pLen];
	char Ctemp[4];
	memset(Ctemp,0,4);

	int i =0;
	int j = 0;

	while(i < pLen)
	{
		if(pText[i] > 0)
		{
			newBuf[j++] = pText[i++];                       
		}
		else                 
		{
			WCHAR Wtemp;
			UTF_8ToUnicode(&Wtemp,pText + i);

			UnicodeToGB2312(Ctemp,Wtemp);

			newBuf[j] = Ctemp[0];
			newBuf[j + 1] = Ctemp[1];

			i += 3;    
			j += 2;   
		}
	}
	newBuf[j] = '\0';
	pOut = newBuf;
	delete []newBuf;

	return; 
}

long UTF8ToGBK(const void * lpUTF8Str, string & str) 
{ 
	if(lpUTF8Str == NULL)    return -1; 

	int nRetLen = 0; 

	//获取转换到Unicode编码后所需要的字符空间长度 
	nRetLen = ::MultiByteToWideChar(CP_UTF8, 0, 
		(char *)lpUTF8Str, -1, NULL, NULL);         

	WCHAR *lpUnicodeStr = new WCHAR[nRetLen + 1];        
	//为Unicode字符串空间 

	//转换到Unicode编码 
	nRetLen = ::MultiByteToWideChar(CP_UTF8, 0, 
		(char *)lpUTF8Str, -1, lpUnicodeStr, nRetLen); 

	if(!nRetLen) 
	{ 
		delete []lpUnicodeStr; return -1; 
	} 

	//获取转换到GBK编码后所需要的字符空间长度 
	nRetLen = ::WideCharToMultiByte(CP_ACP, 0, lpUnicodeStr,
		-1, NULL, NULL, NULL, NULL);         

	CHAR *lpGBKStr = new CHAR[nRetLen + 1]; 

	nRetLen = ::WideCharToMultiByte(CP_ACP, 0, lpUnicodeStr, 
		-1, (char *)lpGBKStr, nRetLen, NULL, NULL);       
	//转换到GBK编码 

	if(!nRetLen) 
	{ 
		delete []lpUnicodeStr; 
		delete []lpGBKStr; 
		return -2; 
	} 

	str = lpGBKStr; 

	delete []lpUnicodeStr; 
	delete []lpGBKStr; 
	
	return 0; 
}

long GBKToUTF8(const void* lpGBKstr,string& str)
{
	if (lpGBKstr == NULL)
	{
		return -1;
	}
	int nRetlen = 0;

	nRetlen = ::MultiByteToWideChar(CP_ACP,0,(char*)lpGBKstr,-1,NULL,NULL);
	WCHAR* lpUnicodestr = new WCHAR[nRetlen + 1];

	nRetlen = ::MultiByteToWideChar(CP_ACP,0,(char*)lpGBKstr,-1,lpUnicodestr,nRetlen);
	if (!nRetlen)
	{
		delete []lpUnicodestr;
		return -1;
	}

	nRetlen = ::WideCharToMultiByte(CP_UTF8,0,lpUnicodestr,-1,NULL,NULL,NULL,NULL);
	char* lpUTF8str = new char[nRetlen + 1];

	nRetlen = ::WideCharToMultiByte(CP_UTF8,0,lpUnicodestr,-1,(char*)lpUTF8str,nRetlen,NULL,NULL);
	if (!nRetlen)
	{
		delete []lpUnicodestr;
		delete []lpUTF8str;
		return -2;
	}

	str = lpUTF8str;

	delete []lpUnicodestr;
	delete []lpUTF8str;
	
	return 0;
}

long GBKToGB18030(const void* lpGBKstr,string& str18030)
{
	if (lpGBKstr == NULL)
	{
		return -1;
	}
	int nRetlen = 0;

	nRetlen = ::MultiByteToWideChar(936,0,(char*)lpGBKstr,-1,NULL,NULL);
	WCHAR* lpUnicode = new WCHAR[nRetlen + 1];

	nRetlen = ::MultiByteToWideChar(936,0,(char*)lpGBKstr,-1,lpUnicode,nRetlen);
	if (!nRetlen)
	{
		delete []lpUnicode;
		return -1;
	}

	nRetlen = ::WideCharToMultiByte(54936,0,lpUnicode,-1,NULL,NULL,NULL,NULL);
	char* lpgb18030 = new char[nRetlen + 1];

	nRetlen = ::WideCharToMultiByte(54936,0,lpUnicode,-1,(char*)lpgb18030,nRetlen,NULL,NULL);
	if (!nRetlen)
	{
		delete []lpUnicode;
		delete []lpgb18030;
		return -2;
	}

	str18030 = lpgb18030;

	delete []lpUnicode;
	delete []lpgb18030;
	
	return 0;
}

long GB18030ToGBK(const void* lpGB18030str,string& gbk)
{
	if (lpGB18030str == NULL)
	{
		return -1;
	}
	int nRetlen = 0;

	nRetlen = ::MultiByteToWideChar(54936,0,(char*)lpGB18030str,-1,NULL,NULL);
	WCHAR* lpUnicode = new WCHAR[nRetlen + 1];

	nRetlen = ::MultiByteToWideChar(54936,0,(char*)lpGB18030str,-1,lpUnicode,nRetlen);
	if (!nRetlen)
	{
		delete []lpUnicode;
		return -1;
	}

	nRetlen = ::WideCharToMultiByte(936,0,lpUnicode,-1,NULL,NULL,NULL,NULL);
	char* lpgbk = new char[nRetlen + 1];

	nRetlen = ::WideCharToMultiByte(936,0,lpUnicode,-1,(char*)lpgbk,nRetlen,NULL,NULL);
	if (!nRetlen)
	{
		delete []lpUnicode;
		delete []lpgbk;
		return -2;
	}

	gbk = lpgbk;

	delete []lpUnicode;
	delete []lpgbk;

	return 0;
}