/************************************************************************/
/*     调用openssl库函数 获取加密连接                                   */
/************************************************************************/

#include <string>
#include <openssl/crypto.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

#pragma comment(lib, "libeay32.lib")
#pragma comment(lib, "ssleay32.lib") 

using namespace std;

void html_ssl_ctx_init(SSL_CTX *ctx)
{
	SSL_library_init();
	//OpenSSL_add_ssl_algorithms();
	SSL_load_error_strings();
	ctx = SSL_CTX_new(SSLv23_client_method());
	if (ctx == NULL) {
		ERR_print_errors_fp(stderr);
		exit(1);
	}
}

void html_ssl_ctx_term(SSL_CTX* ctx)
{
	SSL_CTX_free(ctx);
	ERR_free_strings();
}

void html_ssl_ctx_set_parameters(SSL_CTX* ctx,char* cacert,char* certpath,char* keypath)
{
	SSL_CTX_set_verify (ctx, SSL_VERIFY_PEER, NULL);
	SSL_CTX_load_verify_locations (ctx, cacert, NULL);
	if (0 == SSL_CTX_use_certificate_file (ctx, certpath, SSL_FILETYPE_PEM))
	{
		ERR_print_errors_fp (stderr);
		exit (1);
	}
	if (0 == SSL_CTX_use_PrivateKey_file (ctx, keypath, SSL_FILETYPE_PEM))
	{
		ERR_print_errors_fp (stderr);
		exit (1);
	}
	if (!SSL_CTX_check_private_key (ctx))
	{
		printf ("Private key does not match the certificate public key/n");
		exit (1);
	}
}

SSL* html_ssl_init(SSL_CTX *ctx,SOCKET sock)
{
	int ret;
	SSL *ssl;

	ssl = SSL_new(ctx);
	if (ssl == NULL) {
		ERR_print_errors_fp(stderr);
		exit(1);
	}

	/* 把socket和SSL关联 */
	ret = SSL_set_fd(ssl, sock);
	if (ret == 0) {
		ERR_print_errors_fp(stderr);
		exit(1);
	}

	return ssl;
}

void html_ssl_term(SSL* ssl)
{
	SSL_shutdown(ssl);
	SSL_free(ssl);
}

//构建随机数生成机制,WIN32平台必需
void html_ssl_rand_creat()     
{
	RAND_poll();
	while (RAND_status() == 0) {
		unsigned short rand_ret = rand() % 65536;
		RAND_seed(&rand_ret, sizeof(rand_ret));
	}
}

void html_ssl_connect(SSL* ssl)
{
	int ret;
	ret = SSL_connect(ssl);
	if (ret != 1) {
		ERR_print_errors_fp(stderr);
		exit(1);
	}
}

void html_ssl_accept(SSL* ssl)
{
	int k = SSL_accept (ssl);
	if (0 == k)
	{
		printf ("%d/n", k);
		printf ("SSL connect fail!/n");
		exit (1);
	}

	X509 *client_cert;
	client_cert = SSL_get_peer_certificate (ssl);
	printf ("find a customer to try to connect/n");
	if (client_cert != NULL)
	{
		printf ("Client certificate:/n");
		char *str =
			X509_NAME_oneline (X509_get_subject_name (client_cert), 0, 0);
		if (NULL == str)
		{
			printf ("auth error!/n");
			exit (1);
		}
		printf ("subject: %s/n", str);
		str = X509_NAME_oneline (X509_get_issuer_name (client_cert), 0, 0);
		if (NULL == str)
		{
			printf ("certificate name is null/n");
			exit (1);
		}
		printf ("issuer: %s/n", str);
		printf ("connect successfully/n");
		X509_free (client_cert);
		OPENSSL_free (str);
	}
	else
	{
		printf ("can not find the customer's certificate/n");
		exit (1);
	}
}

string html_ssl_recv(SSL* ssl)
{
	char recvstr[1024];
	string recvstring;
	int recvlen;

	while (1)
	{
		recvlen = SSL_read(ssl,recvstr,1024);
		if (recvlen <= 0)
		{
			break;
		}
		recvstring += recvstr;
	}

	return recvstring;
}

void html_ssl_send(SSL* ssl,string sendstring)
{
	char* sendstr = (char*)sendstring.c_str();
	int stringlen = sendstring.size();
    int sendlen = 0;

	do 
	{
		sendlen = SSL_write(ssl,sendstr,stringlen);
		if (sendlen == -1)
		{
			ERR_print_errors_fp(stderr);
		}
		stringlen -= sendlen;
		sendstr += sendlen;
	} while (stringlen > 0);
}