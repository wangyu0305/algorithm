

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <WinSock2.h>
#include <WS2tcpip.h>

using namespace std;

//功能：搜索字符串右边起的第一个匹配字符

char* Rstrchr(char* findstr, char findchar)
{
	int i = strlen(findstr);

	if (!(*findstr))
	{
		return NULL;
	}
	while (findstr[i - 1])
	{
		if (strchr(findstr + (i - 1), findchar))
		{
			return (findstr + (i - 1));
		}
		else
		{
			i--;
		}
	}

	return NULL;
}

char* save_filename(char* filepath)
{
	char localpath[256];
	char* pt;

	if (filepath && *filepath)
		pt = Rstrchr(filepath, '/');
	else
		pt = 0;

	memset(localpath, 0, sizeof(localpath));
	if (pt && *pt) {
		if ((pt + 1) && *(pt + 1))
			strcpy(localpath, pt + 1);
		else
			memcpy(localpath, filepath, strlen(filepath) - 1);
	} else if (filepath && *filepath)
		strcpy(localpath, filepath);
	else
		strcpy(localpath, "index.html");

	return localpath;
}

//功能：从字符串src中分析出网站地址和端口，并得到用户要下载的文件


void url_get_host(char *url, char *web, char *filePath, int *port)
{
	char *pA;
	char *pB;
	memset(web, 0, sizeof(web));
	memset(filePath, 0, sizeof(filePath));
	*port = 0;
	bool is_ssl = false;

	if (!(*url))
	{
		return;
	}

	pA = url;
	if (!strncmp(pA, "http://", strlen("http://")))
	{
		pA = url + strlen("http://");
	}
	else if (!strncmp(pA, "https://", strlen("https://")))
	{
		pA = url + strlen("https://");
		is_ssl = true;
	}

	pB = strchr(pA, '/');
	if (pB)
	{
		memcpy(web, pA, strlen(pA) - strlen(pB));
		if (pB + 1) 
		{
			memcpy(filePath, pB + 1, strlen(pB) - 1);
			filePath[strlen(pB) - 1] = 0;
		}
	} 
	else
	{
		memcpy(web, pA, strlen(pA));
	}

	if (pB)
	{
		web[strlen(pA) - strlen(pB)] = 0;
	}
	else
	{
		web[strlen(pA)] = 0;
	}

	pA = strchr(web, ':');
	if (pA)
	{
		*port = atoi(pA + 1);
	}
	else if (is_ssl)
	{
		*port = 443;
	}
	else
	{
		*port = 80;
	}
}

static const char * 
inet_ntop_v4 (const void *src, char *dst, size_t size) 
{ 
	const char digits[] = "0123456789"; 
	int i; 
	struct in_addr *addr = (struct in_addr *)src; 
	u_long a = ntohl(addr->s_addr); 
	const char *orig_dst = dst; 

	if (size < INET_ADDRSTRLEN) {  
		return NULL; 
	} 
	for (i = 0; i < 4; ++i) { 
		int n = (a >> (24 - i * 8)) & 0xFF; 
		int non_zerop = 0; 

		if (non_zerop || n / 100 > 0) { 
			*dst++ = digits[n / 100]; 
			n %= 100; 
			non_zerop = 1; 
		} 
		if (non_zerop || n / 10 > 0) { 
			*dst++ = digits[n / 10]; 
			n %= 10; 
			non_zerop = 1; 
		} 
		*dst++ = digits[n]; 
		if (i != 3) 
			*dst++ = '.'; 
	} 
	*dst++ = '\0'; 
	return orig_dst; 
} 

const char * 
inet_ntop(int af, const void *src, char *dst, size_t size) 
{ 
	switch (af) { 
	case AF_INET : 
		return inet_ntop_v4 (src, dst, size); 
#ifdef INET6 
	case AF_INET6: 
		return inet_ntop_v6 ((const u_char*)src, dst, size); 
#endif 
	default : 
		return NULL; 
	} 
} 

string url_get_ip(char* web)
{
	struct hostent* hostinfo;
	string retstring;
	char* officeName;
	char addr[25];

	if ((hostinfo = gethostbyname(web)) == NULL)
	{
		fprintf(stderr, "Gethostname error, %s\n", strerror(errno));
		return NULL;
	}

	officeName = hostinfo->h_name;

	inet_ntop(hostinfo->h_addrtype,hostinfo->h_addr_list[0],addr,sizeof(addr));

	retstring = addr;
	return retstring;
}