/************************************************************************/
/*     调用openssl库函数 获取加密连接                                   */
/************************************************************************/

#ifndef __HTML_SSL_CONNECT_H
#define __HTML_SSL_CONNECT_H


#include <string>

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	void html_ssl_ctx_init(SSL_CTX *ctx);
	void html_ssl_ctx_term(SSL_CTX* ctx);
	void html_ssl_ctx_set_parameters(SSL_CTX* ctx,char* cacert,char* certpath,char* keypath);
	SSL* html_ssl_init(SSL_CTX *ctx,SOCKET sock);
	void html_ssl_term(SSL* ssl);

	void html_ssl_rand_creat();

	void html_ssl_connect(SSL* ssl);
	void html_ssl_accept(SSL* ssl);

	string html_ssl_recv(SSL* ssl);
    void html_ssl_send(SSL* ssl,string sendstring);

#ifdef __cplusplus
}
#endif

#endif            //end  __HTML_SSL_CONNECT_H