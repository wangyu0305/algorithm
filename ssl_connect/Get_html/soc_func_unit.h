/************************************************************************/
/* 套接口的通用函数 封装做接口函数使用                                  */
/************************************************************************/

#ifndef __SOC_FUNC_UNIT_H
#define __SOC_FUNC_UNIT_H


#include <string>
using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	void soc_start();
	void soc_finish();
	SOCKET soc_init();
	void soc_term(SOCKET sock);

	SOCKET soc_listen(SOCKET sock,int port,int max_listen);
	SOCKET soc_accept(SOCKET sock,int port,int max_listen);
    SOCKET soc_connect(SOCKET sock,int port,char* remoteIp);
    string soc_recv(SOCKET sock);
	void soc_send(SOCKET sock,string sendstring);

	SOCKET soc_init_udp();
	void soc_term_udp(SOCKET sock);

	SOCKET soc_bind_udp(SOCKET sock,int port);

	string soc_recvfrom_udp(SOCKET sock,string recvstring,string remoteIp);
	void soc_sendto_udp(SOCKET sock,string sendstring,string remoteIp,int port);

#ifdef __cplusplus
}
#endif

#endif                 //end  __SOC_FUNC_UNIT_H