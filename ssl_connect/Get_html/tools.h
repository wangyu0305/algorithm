/************************************************************************/
/*                       常用工具函数                                   */
/************************************************************************/

#ifndef __NORMAL_TOOLS_FUNC
#define __NORMAL_TOOLS_FUNC

#ifdef __cplusplus
extern "C" {
#endif

	__declspec(dllexport)  char *con_strstr(const char *s1, const char *s2);
	unsigned int iptrans_char_to_int(char* ipaddr);
	void iptrans_int_to_char(unsigned int ipinfo,char* ip_addr_char);

#ifdef __cplusplus
}
#endif
#endif