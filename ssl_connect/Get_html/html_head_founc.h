/************************************************************************/
/*   发送http报文头的处理  接收http报文头解析                           */
/************************************************************************/

#ifndef __HTML_HEAD_FOUNC_H
#define __HTML_HEAD_FOUNC_H

#define _CRT_SECURE_NO_DEPRECATE 
#include <string>
using namespace std;

#ifdef __cplusplus
extern "C" {
#endif

	string message_get_request(char* urlpath,char* addrip,int port,char* cookie);
	string message_post_request(char* urlpath,char* addrip,int port,char* cookie,char* postData);
	string get_response_head(string* content);

#ifdef __cplusplus
}
#endif

#endif          //end  __HTML_HEAD_FOUNC_H