/************************************************************************/
/* 对获取的html内容做处理       解析htm                                 */
/************************************************************************/

#ifndef __HTML_CONTENT_FUNC_H
#define __HTML_CONTENT_FUNC_H

#define _CRT_SECURE_NO_DEPRECATE 

#include <string>
using namespace std;

typedef struct connect_config soc_cfg_t;

struct connect_config 
{
	int send_timeout;              //发送超时
	int recv_timeout;              //接收超时
	int connect_timeout;           //连接超时
	int max_connect;               //作为接收端最大允许连接数
	int connect_retries;           //作为发送端连接重试次数
	int max_pthread;               //启动线程数
	int port;                      //绑定端口
};
#ifdef __cplusplus
extern "C" {
#endif
__declspec(dllexport) void con_cfg_get_param(soc_cfg_t *cfg,string *context);

#ifdef __cplusplus
}
#endif

#endif    //end  __HTML_CONTENT_FUNC_H