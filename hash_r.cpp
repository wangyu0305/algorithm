#include "../hash.h"

unsigned int elf_hash(char* str,int count)
{
	int hash = 0;
	int x = 0;
	int i;
	for (i = 0;i < count;i++)
	{
		hash = (hash << 4) + (*str);
		if ((x = hash & 0xF0000000L) != 0)
		{
			hash ^= (x >> 24);
		}
		hash &= ~x;
	}
	return hash;
}